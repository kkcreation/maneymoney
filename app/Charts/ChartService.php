<?php

namespace App\Charts;

use App\Http\Controllers\HomeController;
use App\Models\User;

class ChartService
{
    public function dashboardChart($chartName, User $user)
    {
        $data = $this->{$chartName}($user);

     //   return json_encode($data);
        return json_encode(50);
    }

    private function reservedMoney($user){
        $userIncome = $user->userIncome;
        $userExpenses = $user->userExpenses;
        $userAssets = $user->userAssets;
        $userDebts = $user->userDebts;
        $homeController = new HomeController();
        $data['reserved_money_to_expense'] = $homeController->reservedMoneyToExpense($userAssets,$userExpenses);
        return $data;
    }
}
