<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DowngradeMembership extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:downgrade-customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downgrade customer to regular member in case of expired membership';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Downgrade Membership!");
        \Log::info("Timestamp: ".Carbon::now()->format('Y-m-d H:i:s'));
        /** Get all active customers */
        $customers = User::where('role','customer')
            ->whereNotNull('membership_expired_at')
            ->where('membership_expired_at','<=',Carbon::now()->format('Y-m-d H:i:s'))
            ->get();

        foreach($customers as $customer){
            $customer->role = 'member';
            $customer->save();
            \Log::info("Downgraded Customer Name: ".$customer->first_name);
        }
        /*
           Write your database logic we bellow:
           Item::create(['name'=>'Ajay kumar']);
        */
    }
}
