<?php

namespace App\Console\Commands;

use App\Models\AMCFund;
use App\Services\SECService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncAMCFundList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sec:sync-amc-fund-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronise AMC fund list from SEC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Sync AMC fund list from SEC!");
        \Log::info("Start Timestamp: " . Carbon::now()->format('Y-m-d H:i:s'));
        // AMCFund::query()->truncate();
        /** Get all active customers */
        $SECService = new SECService();
        $allAMC = $SECService->getAllAMC();
        // $fundCollection = [];
        $count = 1;
        foreach ($allAMC as $AMC) {
            //if (in_array($AMC->unique_id, ['C0000000021', 'C0000000023', 'C0000000124', 'C0000000182','C0000000239','C0000000290','C0000000324','C0000000329','C0000000460','C0000000569','C0000000623'])) {
            // SKIP
            //} else {
            $this->info($count . ' - ' . $AMC->name_en);
            $relatedFunds = $SECService->getAMCFunds($AMC->unique_id);
            foreach ($relatedFunds as $fund) {
                $fundClasses = $SECService->getFundClasses($fund->proj_id);
                if (isset($fundClasses)) {
                    foreach ($fundClasses as $class) {
                        $fundItem = [];
                        $lastUpdateDate = [];
                        $fundItem['unique_id'] = $fund->unique_id;
                        $fundItem['proj_id'] = $class->proj_id;
                        $fundItem['proj_abbr_name'] = $class->class_abbr_name;
                        $lastUpdateDate['last_upd_date'] = $class->last_upd_date;
                        // $fundCollection[] = $fundItem;
                        $this->info($fundItem['proj_abbr_name']);
                        $newFund = AMCFund::updateOrCreate(
                            $fundItem,
                            $lastUpdateDate
                        );
                    }
                } else {
                    $fundItem = [];
                    $lastUpdateDate = [];
                    $fundItem['unique_id'] = $fund->unique_id;
                    $fundItem['proj_id'] = $fund->proj_id;
                    $fundItem['proj_abbr_name'] = $fund->proj_abbr_name;
                    $lastUpdateDate['last_upd_date'] = $fund->last_upd_date;
                    // $fundCollection[] = $fundItem;
                    $this->info($fundItem['proj_abbr_name']);
                    $newFund = AMCFund::updateOrCreate(
                        $fundItem,
                        $lastUpdateDate
                    );
                }
            }
            $count++;
            //}
        }
        //   $fund = AMCFund::upsert($fundCollection,['unique_id','proj_id','proj_abbr_name'],['last_upd_date']);
        \Log::info("End Timestamp: " . Carbon::now()->format('Y-m-d H:i:s'));
        \Log::info("Synchronisation is completed!");
    }
}
