<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use OmiseCustomer;

class UpdateOmiseCustomerId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'omise:update-customer-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Omise customer id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * ----------------------------------------------------------------------
     * THIS IS ONE TIME USE FUNCTION.
     * ----------------------------------------------------------------------
     *
     * @return void
     */
    public function handle():void
    {
        /** Customers */
        $users = User::all();

        foreach ($users as $user) {
            if ($user->omise_customer_id === NULL) {
                try {
                    $omiseCustomerId = OmiseCustomer::create(
                        [
                            'email' => $user->email,
                            'description' => 'The user ID: ' . $user->id,
                        ]
                    );

                    $user->omise_customer_id = $omiseCustomerId['id'];
                    $user->save();
                } catch (\Exception $exception) {
                    $this->info('Failed update user ID:' . $user);
                    continue;
                }
            }
        }
    }
}
