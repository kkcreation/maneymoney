<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\WealthWish;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get auth member list.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function authMemberList()
    {
        $users = User::all();
        return view('auth-member-list', ['authMemberList' => $users]);
    }

    public function memberUpdate($id)
    {
        $user = User::find($id);
        return view('member-info', ['user' => $user]);
    }

    public function updateMemberInfo(Request $request)
    {
        // return $request->all();
        $request->validate([
            'userid'                      => ['required'],
            'role'                        => ['sometimes', 'string', 'in:admin,member,customer,vip'],
            'membership_expired_at'       => ['sometimes'],
            'membership_payment_interval' => ['sometimes'],
            /*   'membership_latest_renewed_at' => ['sometimes']*/
        ]);
        try {
            $user = User::find($request->get('userid'));
            /** Update role */
            $updateRole = 0;
            if ($request->has('role') && $request->get('role') != $user->role) {
                $user->role = $request->get('role');
                $updateRole++;
            }
            $updateExpiry = 0;
            /** Update Membership Payment Interval */
            if ($request->has('membership_payment_interval') && $request->get('membership_payment_interval') != $user->membership_payment_interval) {
                $user->membership_payment_interval = $request->get('membership_payment_interval');
                $updateExpiry++;
            }
            /** Update Membership Latest Renewal */
            if ($request->has('membership_latest_renewed_at') && $request->get('membership_latest_renewed_at') != $user->membership_latest_renewed_at) {
                $user->membership_latest_renewed_at = $request->get('membership_latest_renewed_at');
                $updateExpiry++;
            }
            /** Update Membership Expiry Datetime */
            if ($updateExpiry != 0) {
                if ($user->membership_payment_interval != 'none' && $user->membership_payment_interval != 'lifetime') {
                    $extension = 0;
                    if ($user->membership_payment_interval == 'monthly') {
                        $extension = 1;
                    } else if ($user->membership_payment_interval == 'half_yearly') {
                        $extension = 6;
                    } else if ($user->membership_payment_interval == 'yearly') {
                        $extension = 12;
                    }
                    $newDateTime = Carbon::parse($user->membership_latest_renewed_at)->addMonth($extension);
                    $user->membership_expired_at = $newDateTime;
                } else {
                    $user->membership_expired_at = null;
                }
            }
            if ($updateRole != 0) {
                if (in_array($user->role, ['admin', 'member'])) {
                    $user->membership_payment_interval = 'none';
                    $user->membership_latest_renewed_at = null;
                    $user->membership_expired_at = null;
                }
            }
            $user->update();

            Session::flash('message', 'Member Info Updated successfully!');
            Session::flash('alert-class', 'alert-success');
            $response = response()->json([
                'isSuccess' => true,
                'Message'   => "Member Info Updated successfully!"
            ]); // Status code here
            return redirect('/member-info/' . $user->id);
        } catch (\Exception $exception) {
            Session::flash('message', 'Something went wrong!');
            Session::flash('alert-class', 'alert-danger');
            $response = response()->json([
                'isSuccess' => false,
                'Message'   => "Something went wrong!"
            ], 201); // Status code here
            return redirect('/member-info/' . $user->id);
        }
    }
}
