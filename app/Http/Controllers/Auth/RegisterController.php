<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessCreateOmiseCustomerId;
use App\Models\FinancialStatementVersion;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Services\WealthService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $wealthService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(WealthService $wealthService)
    {
        $this->middleware('guest');
        $this->wealthService = $wealthService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'dob'      => ['required', 'date', 'before:today'],
            'avatar'   => ['nullable', 'image', 'mimes:jpg,jpeg,png', 'max:3024'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User | \Exception
     */
    protected function create(array $data)
    {

        if (request()->has('avatar')) {
            $avatar = request()->file('avatar');
            $avatarName = time() . '.' . $avatar->getClientOriginalExtension();
            $avatarPath = public_path('/images/');
            $avatar->move($avatarPath, $avatarName);
        }
        // set timezone

        $newUser = User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => Hash::make($data['password']),
            'dob'      => date('Y-m-d', strtotime($data['dob'])),
            'avatar'   => request()->has('avatar') ? "/images/" . $avatarName : null,
        ]);

        $newUser->timezone = $this->getTimezone();
        $newUser->save();

        if (!$newUser) {
            throw new ModelNotFoundException();
        }
        $newVersion = new FinancialStatementVersion();
        $newVersion->user_id = $newUser->id;
        $newVersion->submission_date = Carbon::now()->format('Y-m') . '-01';
        $newVersion->save();
        // Generate Initial Personal Income-Expense Statement + Balance Sheet
        $this->wealthService->generateUserIncome($newUser, $newVersion);
        $this->wealthService->generateUserExpense($newUser, $newVersion);
        $this->wealthService->generateUserAsset($newUser, $newVersion);
        $this->wealthService->generateUserDebt($newUser, $newVersion);

        // Generate Omise Customer Id
        ProcessCreateOmiseCustomerId::dispatch(
            $newUser->id
        );

        return $newUser;
    }

    protected function getTimezone()
    {
        if (request()->has('timezone')) {
            return request('timezone');
        } else {
            return null;
        }
    }
}
