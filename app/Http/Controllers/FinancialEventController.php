<?php

namespace App\Http\Controllers;

use App\Models\AMCFund;
use App\Models\Asset;
use App\Models\User;
use App\Models\WealthWish;
use App\Services\PortfolioService;
use App\Services\SECService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\FinancialEvent;
use Illuminate\Support\Facades\Auth;

class FinancialEventController extends Controller
{
    /**
     * @var SECService
     */
    private $SECService;
    /**
     * @var PortfolioService
     */
    private $portfolioService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SECService $SECService, PortfolioService $portfolioService)
    {
        $this->middleware('auth');
        $this->SECService = $SECService;
        $this->portfolioService = $portfolioService;
    }
    public function index(Request $request)
    {
        return view('investment-memo');
    }

    public function events(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = FinancialEvent::whereDate('event_start', '>=', $request->start)
            ->whereDate('event_end', '<=', $request->end)
            ->where('user_id', auth()->user()->id)
            ->get(
                [
                    'id',
                    'event_title as title',
                    'event_start as start',
                    'event_end as end',
                    'event_tag as tag',
                    'event_action as action',
                    'asset_id as asset_id',
                    'wealth_wish_id as wealth_wish_id',
                    'conversion_rate',
                    'unit',
                    'price',
                    'fee',
                    'color as className'
                ]
            );
        return response()->json($data);
    }


    public function manageEvents(Request $request)
    {
        $eventTag = $request->event_tag;
        $eventAction = $request->event_action;
        $color = 'bg-secondary';
        switch ($eventAction) {
            case 'buy':
                $color = 'bg-success';
                break;
            case 'sell':
                $color = 'bg-danger';
                break;
            case 'deposit':
                $color = 'bg-primary';
                break;
            case 'withdraw':
                $color = 'bg-secondary';
                break;
            default:
                break;
        }
        $currency = 'fiat';
        $conversionRate = 0;
        $unit = 0;
        $price = $request->price;
        $fee = 0;

        if (!empty($request->asset_id)) {
            $asset = Asset::find($request->asset_id);
            $eventTag = $request->event_tag;
            if ($asset->id == 22) {
                $currency = 'stable_coin';
                $conversionRate = $request->conversion_rate;
            }
            $unit = $request->unit;
            $fee = $request->fee;
        }
        if (!empty($request->wealth_wish_id)) {
            $wealthWish = WealthWish::find($request->wealth_wish_id);
        }
        switch ($request->type) {
            case 'create':
                $calendarEvent = FinancialEvent::create([
                    'user_id'         => auth()->user()->id,
                    'event_title'     => $request->event_title,
                    'event_start'     => $request->event_start,
                    'event_end'       => $request->event_end,
                    'wealth_wish_id'  => isset($wealthWish) ? $wealthWish->id : null,
                    'asset_id'        => isset($asset) ? $asset->id : null,
                    'event_tag'       => $eventTag,
                    'event_action'    => $eventAction,
                    'currency'        => $currency,
                    'conversion_rate' => $conversionRate,
                    'unit'            => $unit,
                    'price'           => $price,
                    'fee'             => $fee,
                    'color'           => $color
                ]);

                return response()->json($calendarEvent);
                break;
            case 'edit':
                $calendarEvent = FinancialEvent::find($request->id)->update([
                    'event_title'     => $request->event_title,
                    'event_start'     => $request->event_start,
                    'event_end'       => $request->event_end,
                    'wealth_wish_id'  => isset($wealthWish) ? $wealthWish->id : null,
                    'asset_id'        => isset($asset) ? $asset->id : null,
                    'event_tag'       => $eventTag,
                    'event_action'    => $eventAction,
                    'currency'        => $currency,
                    'conversion_rate' => $conversionRate,
                    'unit'            => $unit,
                    'price'           => $price,
                    'fee'             => $fee,
                    'color'           => $color
                ]);
                return response()->json($calendarEvent);
                break;
            case 'edit-date':
                $event = FinancialEvent::find($request->id);
                $price = 0;
                $data = $this->getDailyNAV($event->event_tag, Carbon::parse($request->event_start)->format('Y-m-d'));
                if (!empty($data)) {
                    if ($event->event_action == 'buy') {
                        $price = $data->sell_price;
                    } else if ($event->event_action == 'sell') {
                        $price = $data->buy_price;
                    }
                }
                $calendarEvent = $event->update([
                    'event_start' => $request->event_start,
                    'event_end'   => $request->event_end,
                    'price'       => $price
                ]);
                return response()->json($calendarEvent);
                break;
            case 'delete':
                $calendarEvent = FinancialEvent::find($request->id)->delete();
                return response()->json($calendarEvent);
                break;
            default:
                break;
        }
    }

    public function getFundInfo(Request $request): ?\Illuminate\Http\JsonResponse
    {
        $projectAbbrName = $this->SECService->getProjectAbbrName($request->name);
        if (empty($projectAbbrName)) {
            $fundInfo = $this->SECService->getFundInfo($request->name);
            $projectID = $fundInfo[0]->proj_id;
            $sequence = 0;
        } else {
            $projectID = $projectAbbrName[0]->proj_id;
            $fundClasses = $this->SECService->getFundClasses($projectID);
            $i = 0;
            $sequence = null;
            $r = false;
            $c = 0;
            foreach ($fundClasses as $class) {
                if (preg_match("/ชนิดรับซื้อคืนอัตโนมัติ/", $class->class_name)) {
                    $r = true;
                    $c = $i;
                }
                if ($class->class_abbr_name == $request->name) {
                    $sequence = $i;
                    if ($r) {
                        if ($sequence < $c) {
                            // INPUT INDEX < R INDEX >> do nothing
                        } else if ($sequence == $c) {
                            // INPUT INDEX = R INDEX >> $sequence = null;
                            $sequence = null;
                        } else if ($sequence > $c) {
                            // INPUT INDEX > R INDEX >> $sequence--;
                            $sequence--;
                        }
                    }
                }
                $i++;
            }
        }
        $navDate = $request->nav_date;
        $fundDailyNAV = $this->SECService->getFundDailyNAV($projectID, $navDate);

        if (isset($fundDailyNAV)) {
            if (isset($sequence)) {
                return response()->json($fundDailyNAV->amc_info[$sequence]);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    /** @Todo waiting for SEC feedback about missing price  */
    public function getDailyNAV($fundName, $navDate)
    {
        /** Get fund proj_id by fund name from amc_funds table */
        $fund = AMCFund::where('proj_abbr_name', $fundName)->first();
        $projectID = $fund->proj_id;
        $fundClasses = $this->SECService->getFundClasses($projectID);
        if (isset($fundClasses)) {
            $i = 0;
            $sequence = null;
            $r = false;
            $c = 0;
            foreach ($fundClasses as $class) {
                if (preg_match("/ชนิดรับซื้อคืนอัตโนมัติ/", $class->class_name)) {
                    $r = true;
                    $c = $i;
                }
                if ($class->class_abbr_name == $fundName) {
                    $sequence = $i;
                    if ($r) {
                        if ($sequence < $c) {
                            // INPUT INDEX < R INDEX >> do nothing
                        } else if ($sequence == $c) {
                            // INPUT INDEX = R INDEX >> $sequence = null;
                            $sequence = null;
                        } else if ($sequence > $c) {
                            // INPUT INDEX > R INDEX >> $sequence--;
                            $sequence--;
                        }
                    }
                }
                $i++;
            }
        } else {
            $sequence = 0;
        }
        $fundDailyNAV = $this->SECService->getFundDailyNAV($projectID, $navDate);

        if (isset($fundDailyNAV)) {
            if (isset($sequence)) {
                return $fundDailyNAV->amc_info[$sequence];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getAllFunds(Request $request)
    {
        $data = $request->all();
        $query = $data['tag'];
        $filter_data = AMCFund::select('proj_abbr_name')
            ->where('proj_abbr_name', 'LIKE', '%' . $query . '%')
            ->get();

        $filter_data = collect($filter_data->pluck('proj_abbr_name'));
        return response()->json($filter_data->values()->all());
    }

    public function getGoalBasedInvestmentList()
    {
        $user = User::find(Auth::user()->id);
        return view('portfolio')->with(['investmentMemo' => $user->investmentMemo->groupBy('wealth_wish_id')]);
    }

    public function getPortfolioInfo()
    {
        $user = User::find(Auth::user()->id);
        $data = $this->portfolioService->getPortfolioInfo($user->investmentMemo);
        return response()->json($data);
    }
}
