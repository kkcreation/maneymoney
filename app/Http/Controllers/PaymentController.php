<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Requests\CheckOutRequest;
use App\Services\MembershipService;
use App\Services\OmiseService;
use App\Services\PaymentService;
use App\Utility\Response\Traits\StandardResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Imagick;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PaymentController extends Controller
{
    use StandardResponse, LogsActivity;

    private $omiseService;
    private $paymentService;
    private $membershipService;
    /**
     * PaymentController constructor.
     * @param OmiseService $omiseService
     * @param PaymentService $paymentService
     * @param MembershipService $membershipService
     */
    public function __construct(OmiseService $omiseService, PaymentService $paymentService, MembershipService $membershipService)
    {
        $this->middleware('auth');
        $this->omiseService    = $omiseService;
        $this->paymentService  = $paymentService;
        $this->membershipService = $membershipService;
    }

    /**
     * @param CheckOutRequest $request
     * @return JsonResponse
     */
    public function checkout(CheckOutRequest $request)
    {
        $paymentData = $request->validated();

        try {
            $transaction = $this->omiseService->makeTransaction($paymentData);

            /**
             * @READ : This process should do only staging server
             * ----------
             * Handle credit card payment method in staging
             */
            if ($transaction['status'] === 'successful') {

                /** @WARNING : Old codes implemented for no 3DS */
                $payment        = $this->paymentService->recordTransaction($transaction);
                $updatedUser = $this->membershipService->updateMembershipStatusWithUserId(Auth::user()->id, 'member');

                $this->paymentService->recordCurrentState($payment);

                // ProcessMemberShipEmail::dispatch($updatedUser);

                $url = config('app.url');
                // Return authorize uri to client for confirm payment
                return $this->jsonSuccessfulResponse('Transaction is pending. Please verify your payment.', 200, [
                    'user_id'     => $paymentData['user_id'],
                    'authorize_uri' => $transaction['authorize_uri'],
                    'return_uri'    => $url . "/payments/verify"
                ]);

            } elseif ($transaction['status'] === 'pending') {
                /* @READ : This process will process in production and some src in staging! */

                $this->paymentService->recordTransaction($transaction);

                if ($transaction['source']['type'] === 'promptpay') {
                    $imageUri = $transaction['source']['scannable_code']['image']['download_uri'];

                    if (config('app.env') === 'production') {
                        $fileName = md5(Auth::user()->id . time()) . '.png';
                        $image    = new Imagick();
                        // @TODO : Please find the best way to solve this issue because If omise change the image, we must change this logic again.
                        // Handle PromptPay logo to correct position
                        $imageTemp = str_replace('image x="0" y="88" width="100%" height="170" transform="translate(0,-85)"', 'image x="160" y="88" width="420" height="170" transform="translate(0,-85)"', file_get_contents($imageUri));
                        $imageTemp = str_replace('image x="0" y="180" width="100%" height="170"', 'image x="220" y="180" width="300" height="170"', $imageTemp);
                        // Add border to image to make it complete
                        $imageTemp = str_replace('<rect x="0" y="0" width="100%" height="100%" style="fill:rgb(255,255,255);" />', '<rect x="0" y="0" width="100%" height="100%" stroke="white" style="fill:rgb(255,255,255); " />', $imageTemp);
                        $imageTemp = str_replace('<rect x="0" y="0" width="100%" height="176" style="fill:rgb(14,61,103);" />', '<rect x="0" y="0" width="100%" height="176" stroke="rgb(14,61,103)" style="fill:rgb(14,61,103);" />', $imageTemp);
                        $image->readImageBlob($imageTemp);
                        $image->setImageFormat("png32");
                        $image->writeImage(storage_path('app/public/promptpay_qrcode/' . $fileName));

                        $imageUri = config('app.url') . '/storage/promptpay_qrcode/' . $fileName;
                    }

                    return $this->jsonSuccessfulResponse('Transaction is pending. Please verify your payment.', 200, [
                        'unique_id' => $paymentData['unique_id'],
                        'qrcode'    => $imageUri
                    ]);
                }

                $url = config('app.url');
                // Return authorize uri to client for confirm payment
                return $this->jsonSuccessfulResponse('Transaction is pending. Please verify your payment.', 200, [
                    'user_id'     => Auth::user()->id,
                    'authorize_uri' => $transaction['authorize_uri'],
                    'return_uri'    => $url . "/payments/verify",
                ]);
            } else { // Record other transaction status
                $this->paymentService->recordTransaction($transaction);
                throw new \OmiseException($transaction['failure_code']);
            }

            /** @WARNING : Old codes implemented for no 3DS */

            return $this->jsonSuccessfulResponse('Successfully created transaction.', 201, [
                'transaction' => $transaction['status'],
            ]);
        } catch (\OmiseException $e) {
            $message = !empty($e->getMessage()) ? $this->paymentService->mapTransactionErrorMessage($e->getMessage()) : 'Unsuccessfully created transaction.';
            return $this->jsonFailResponse($message, 400, [
                'transaction'              => 'failed',
                'transaction_failure_code' => $e->getMessage() ?? null
            ]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function validateBeforeCheckOut()
    {
        try {
            $membership = User::where('id', Auth::user()->id)->first();
            if ($membership) {
                if($membership->role == 'customer') {
                    return $this->jsonSuccessfulResponse('Successfully validated gift.', 200, ['data' => $membership]);
                }
            }
            throw new \Exception();
        } catch (\Exception $e) {
            return $this->jsonFailResponse('Unsuccessfully validated gift.', 400);
        }
    }

    /**
     * @return JsonResponse|RedirectResponse
     */
    public function verifyAfterCheckOut()
    {
        // Condition redirect for web client
            $url = config('app.url');
            if ($this->paymentService->verifyPayment()) {
                return redirect()->away($url . "/membership/success");
            }

            return redirect()->away($url . "/membership/confirmation?transaction=failed");

    }

    /**
     * @return JsonResponse
     */
    public function checkPaymentStatus()
    {
        // Return json response to client to handle next step
        if ($this->paymentService->checkStatus()) {
            return $this->jsonSuccessfulResponse('Successfully checked payment.', 200);
        }

        // Use jsonFailResponse to avoid error in client
        return $this->jsonFailResponse('Pending...', 200);
    }

    public function getActivitylogOptions(): LogOptions
    {
        // TODO: Implement getActivitylogOptions() method.
    }
}
