<?php

namespace App\Http\Controllers;

use App\Models\MailLog;
use App\Models\User;

class PostmarkWebhookController extends Controller
{

    /**
     * Record incoming postmark webhook.
     *
     * @throws \Exception
     */
    public function store()
    {
        // if found our webhook token
        if (!request()->hasHeader('postmark-webhook-token') || request()->header('postmark-webhook-token') != config('postmark.webhook_token')) {
            return response()->json([
                'success' => false,
                'description' => 'Not allowed.'
            ], 403);
        }

        // get inputs
        $inputs = request()->only([
            'RecordType',
            'MessageID',
            'Tag',
            'Description',
        ]);

        $mailLog = MailLog::where('message_id', $inputs['MessageID'])->first();
        if (!$mailLog) {
            return response()->json([
                'success' => false,
                'description' => 'Not found.'
            ], 404);
        }

        // Check existence of booking
        if (!empty($mailLog->user_id)) {
            if (User::where('id', $mailLog->user_id)->exists()) {
                $model = User::where('id', $mailLog->user_id)->first();
            } else {
                $model = $userEmailDeliveryLog;
            }
        } else {
            $model = $userEmailDeliveryLog;
        }
        // get RLAX system user
        if (User::where('email', 'system@rlax.me')->exists()) {
            $causer = User::where('email', 'system@rlax.me')->first();
        } else {
            $causer = null;
        }

        // prepare log statement
        if ($inputs['RecordType'] == 'Delivery') {
            $dynamicLogMessage = 'The ' . $inputs['Tag'] . ' email is delivered.';
        } else if ($inputs['RecordType'] == 'Open') {
            $dynamicLogMessage = 'The delivered ' . $inputs['Tag'] . ' email is read.';
        } else if ($inputs['RecordType'] == 'Click') {
            $dynamicLogMessage = 'The opened ' . $inputs['Tag'] . ' email is clicked.';
        } else { // Bouncing
            if (!empty($inputs['Description'])) {
                $dynamicLogMessage = $inputs['Tag'] . ' email | ' . $inputs['Description'];
            } else {
                $dynamicLogMessage = $inputs['Tag'] . ' email is bounced.';
            }
        }

        return response()->json([
            'success' => true,
            'description' => 'Successful add a new record.'
        ], 201);
    }
}
