<?php

namespace App\Http\Controllers;

use App\Models\DebtKiller;
use App\Models\User;
use App\Models\WealthWish;
use App\Requests\DebtActionPlanRequest;
use App\Services\DebtKillerService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserDebtController extends Controller
{
    /**
     * @var DebtKillerService
     */
    private $debtKillerService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DebtKillerService $debtKillerService)
    {
        $this->middleware('auth');
        $this->debtKillerService = $debtKillerService;
    }

    /**
     * Get debt killer list by user id.
     *
     * @return mixed
     */
    public function debtKillerList()
    {
        $user = User::find(Auth::user()->id);
        return view('debt-killer-list')->with(['debtKillers' => $user->debtKillers->sortBy('expected_clearance_date')]);
    }

    public function debtKillerCreate()
    {
        return view('debt-killer-create');
    }

    public function debtKillerUpdate($id)
    {
        $debtKiller = DebtKiller::find($id);
        return view('debt-killer-update', ['debtKiller' => $debtKiller]);
    }

    public function debtKillerDetail($id)
    {
        $debtKiller = DebtKiller::find($id);
        return view('debt-killer-detail', ['debtKiller' => $debtKiller]);
    }

    /**
     * Create new wealth wish.
     *
     * @return mixed
     */
    public function storeData(Request $request)
    {
        try {
            $durationInMonths = $request->get('expected_duration');
            $expectedClearanceDate = Carbon::parse($request->get('start_clearance_date'))->addMonth($durationInMonths);
            $expectedClearanceDate = Carbon::parse($expectedClearanceDate)->format('Y-m-' . $request->get('due_date'));
            $expectedPayment = $this->debtKillerService->pmtWithAnnualRate($request->get('annual_interest_rate'), $request->get('expected_duration'), $request->get('outstanding_balance'));
            $totalInterest = ($expectedPayment * $request->get('expected_duration')) - $request->get('outstanding_balance');
            $newDebt = DebtKiller::create([
                'user_id'                 => Auth::user()->id,
                'debt_id'                 => $request->has('debttype') ? $request->get('debttype') : '',
                'creditor'                => $request->get('creditor'),
                'outstanding_balance'     => $request->get('outstanding_balance'),
                'expected_duration'       => $request->get('expected_duration'),
                'annual_interest_rate'    => $request->has('annual_interest_rate') ? $request->get('annual_interest_rate') : '',
                'start_clearance_date'    => $request->get('start_clearance_date'),
                'expected_clearance_date' => $expectedClearanceDate,
                'expected_payment'        => $expectedPayment,
                'statement_date'          => $request->get('statement_date'),
                'due_date'                => $request->get('due_date'),
                'notes'                   => $request->get('notes'),
                'total_interest_amount'   => $totalInterest
            ]);

        } catch (\Exception $e) {
            //return response()->json(['status' => 'exception', 'msg' => $e->getMessage()]);
            Session::flash('message', 'Something went wrong! ' . $e->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect('/debt-killer-create');
        }
        Session::flash('message', 'Created successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/debt-killer-list');
        //return response()->json(['status' => "success", 'debt_killer_id' => $newDebt->id]);
    }

    /**
     * Update existing debt killer.
     *
     * @return mixed
     */
    public function updateData(Request $request)
    {
        try {

            $debt = DebtKiller::find($request->get('debtkillerid'));
            if (!$debt) {
                throw new ModelNotFoundException();
            }
            $durationInMonths = $request->get('expected_duration');
            $expectedClearanceDate = Carbon::parse($request->get('start_clearance_date'))->addMonth($durationInMonths);
            $expectedClearanceDate = Carbon::parse($expectedClearanceDate)->format('Y-m-' . $request->get('due_date'));
            if ($debt->outstanding_balance != $request->get('outstanding_balance') && !empty($debt->expected_payment)) {
                // Re-calculate action plan
                // annualRate, period, futureValue
                if ($debt->annual_interest_rate == 0) {
                    $debt->expected_payment = $request->get('outstanding_balance') / $durationInMonths;
                } else {
                    $payment = $this->debtKillerService->pmtWithAnnualRate($request->get('annual_interest_rate'), $durationInMonths, $request->get('outstanding_balance'));
                    $debt->expected_payment = $payment;
                }
            } else {
                if (($debt->expected_duration != $request->get('expected_duration') || $debt->annual_interest_rate != $request->get('annual_interest_rate')) && !empty($debt->expected_payment)) {
                    // Re-calculate action plan
                    // annualRate, period, futureValue
                    if ($debt->annual_interest_rate == 0) {
                        $debt->expected_payment = $request->get('outstanding_balance') / $durationInMonths;
                    } else {
                        $payment = $this->debtKillerService->pmtWithAnnualRate($request->get('annual_interest_rate'), $durationInMonths, $request->get('outstanding_balance'));
                        $debt->expected_payment = $payment;
                    }
                }
            }
            $debt->debt_id = $request->get('debttype');
            $debt->creditor = $request->get('creditor');
            $debt->notes = $request->get('notes');
            $debt->start_clearance_date = $request->get('start_clearance_date');
            $debt->expected_clearance_date = $expectedClearanceDate;
            $debt->annual_interest_rate = $request->get('annual_interest_rate');
            $debt->outstanding_balance = $request->get('outstanding_balance');
            $debt->expected_duration = $durationInMonths;
            $debt->statement_date = $request->get('statement_date');
            $debt->due_date = $request->get('due_date');
            $debt->total_interest_amount = ($debt->expected_payment * $debt->expected_duration) - $debt->outstanding_balance;
            $debt->update();

        } catch (\Exception $e) {
            Session::flash('message', 'Something went wrong! ' . $e->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect('/debt-killer-update/' . $debt->id);
        }
        Session::flash('message', 'Update successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect('/debt-killer-list');
    }

    /**
     * Update Action Plan of Debt Killer.
     *
     * @return mixed
     */
    public function updateDebtActionPlan(DebtActionPlanRequest $request)
    {
        $input = $request->validated();
        try {
            $debt = DebtKiller::find($input['debtkillerid']);
            if (!$debt) {
                throw new ModelNotFoundException();
            }

            /*$debt->annual_interest_rate = $input['annualrate'];*/
            $debt->type = $input['type'];
            // $wish->advanced_payment = $request->get('advanced_payment');
            if ($debt->expected_duration != $input['expectedduration']) {
                $debt->expected_duration = ceil($input['expectedduration']);
                $expectedClearanceDate = Carbon::parse($debt->start_clearance_date)->addMonth(ceil($debt->expected_duration));
                $expectedClearanceDate = Carbon::parse($expectedClearanceDate)->format('Y-m-' . $debt->due_date);
                $debt->expected_clearance_date = $expectedClearanceDate;
            }
            $debt->expected_payment = $input['expectedpayment'];
            $debt->total_interest_amount = $input['totalinterest'];
            $debt->update();

            Session::flash('message', 'Action Plan Saved successfully!');
            Session::flash('alert-class', 'alert-success');

            return redirect('/debt-killer-detail/' . $debt->id);

        } catch (\Exception $e) {
            Session::flash('message', 'Something went wrong! ' . $e->getMessage());
            Session::flash('alert-class', 'alert-danger');

            return redirect('/debt-killer-detail/' . $debt->id);
        }
    }

    public function deleteDebtData(Request $request)
    {
        $debtKillerID = $request->get('debtkillerid');
        // Delete wealth wish
        DebtKiller::where('id', $debtKillerID)->delete();
        return 'Delete Successfully';
    }
}
