<?php

namespace App\Http\Controllers;

use App\Models\FinancialStatementVersion;
use App\Models\User;
use App\Models\WealthWish;
use App\Services\WealthService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class WealthController extends Controller
{
    /**
     * @var WealthService
     */
    private $wealthService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(WealthService $wealthService)
    {
        $this->middleware('auth');
        $this->wealthService = $wealthService;

    }

    /**
     * Update personal statement
     */
    public function updateFinancialStatement(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $insertOrUpdateVersion = $this->wealthService->insertOrUpdateVersion($user, $request->get('submission_date'));
            //$date = Carbon::createFromFormat('Y-m-d', $request->get('submission_date'))->format('Y-m');
            $date = $request->get('submission_date');
            try {
                if ($insertOrUpdateVersion === 'update') {

                    $version = $user->financialStatementVersions->filter(function ($q) use ($date) {
                        return Str::startsWith($q['submission_date'], $date);
                    });
                    $userIncome = $user->userIncome->where('version_id', $version->first()->id);
                    foreach ($userIncome as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }

                    $userExpenses = $user->userExpenses->where('version_id', $version->first()->id);
                    foreach ($userExpenses as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }
                    $userAssets = $user->userAssets->where('version_id', $version->first()->id);
                    foreach ($userAssets as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }

                    $userDebts = $user->userDebts->where('version_id', $version->first()->id);
                    foreach ($userDebts as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }

                    Session::flash('message', 'Financial Statement Updated successfully!');
                    Session::flash('alert-class', 'alert-success');
                    $response = response()->json([
                        'isSuccess' => true,
                        'Message'   => "Financial Statement Updated successfully!"
                    ], 200); // Status code here
                } else {
                    $newVersion = new FinancialStatementVersion();
                    $newVersion->user_id = $user->id;
                    if (!empty($date)) {
                        $newVersion->submission_date = $date.'-01';
                    } else {
                        $newVersion->submission_date = Carbon::now()->format('Y-m').'-01';
                    }
                    $newVersion->save();
                    // Generate Initial Personal Income-Expense Statement + Balance Sheet
                    $this->wealthService->generateUserIncome($user, $newVersion, $request->get('submission_date'));
                    $this->wealthService->generateUserExpense($user, $newVersion, $request->get('submission_date'));
                    $this->wealthService->generateUserAsset($user, $newVersion, $request->get('submission_date'));
                    $this->wealthService->generateUserDebt($user, $newVersion, $request->get('submission_date'));

                   /* $userIncome = $user->userIncome->where('version_id', $newVersion->id);
                    foreach ($userIncome as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }

                    $userExpenses = $user->userExpenses->where('version_id', $newVersion->id);
                    foreach ($userExpenses as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }
                    $userAssets = $user->userAssets->where('version_id', $newVersion->id);
                    foreach ($userAssets as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }

                    $userDebts = $user->userDebts->where('version_id', $newVersion->id);
                    foreach ($userDebts as $item) {
                        $item->amount = $request->get($item->key);
                        $item->save();
                    }*/

                    Session::flash('message', 'Financial Statement Created successfully!');
                    Session::flash('alert-class', 'alert-success');
                    $response = response()->json([
                        'isSuccess' => true,
                        'Message'   => "Financial Statement Created successfully!"
                    ], 200); // Status code here
                }
            } catch (\Exception $exception) {
                Session::flash('message', 'Something went wrong!' . $exception);
                Session::flash('alert-class', 'alert-danger');
                $response = response()->json([
                    'isSuccess' => false,
                    'Message'   => "Something went wrong!"
                ], 201); // Status code here
            }
        } else {
            Session::flash('message', 'Something went wrong!');
            Session::flash('alert-class', 'alert-danger');
            $response = response()->json([
                'isSuccess' => false,
                'Message'   => "Something went wrong!"
            ], 201); // Status code here
        }
        return redirect('/wealth-profile/'.$date);
    }

    /**
     * Update personal balance sheet
     */
    /*  public function updateBalanceSheet(Request $request, $id)
      {
          $user = User::find($id);
          if ($user) {
              $userAssets = $user->userAssets;
              foreach($userAssets as $item){
                  $item->amount = $request->get($item->key);
                  $item->save();
              }

              $userDebts = $user->userDebts;
              foreach($userDebts as $item){
                  $item->amount = $request->get($item->key);
                  $item->save();
              }
              Session::flash('message', 'Balance Sheet Updated successfully!');
              Session::flash('alert-class', 'alert-success');
              $response = response()->json([
                  'isSuccess' => true,
                  'Message' => "Balance Sheet Updated successfully!"
              ], 200); // Status code here
              return redirect('/wealth-profile')->with('openModal',true);
          } else {
              Session::flash('message', 'Something went wrong!');
              Session::flash('alert-class', 'alert-danger');
              $response = response()->json([
                  'isSuccess' => false,
                  'Message' => "Something went wrong!"
              ], 201); // Status code here
              return redirect('/wealth-profile');
          }
      }*/
}
