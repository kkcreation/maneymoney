<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\WealthWish;
use App\Models\WealthWishImage;
use App\Requests\ActionPlanRequest;
use App\Services\WealthWishService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class WealthWishController extends Controller
{
    /**
     * @var WealthWishService
     */
    private $wealthWishService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(WealthWishService $wealthWishService)
    {
        $this->middleware('auth');
        $this->wealthWishService = $wealthWishService;
    }

    /**
     * Get wealth wish list by user id.
     *
     * @return mixed
     */
    public function wealthWishList()
    {
        $user = User::find(Auth::user()->id);
        return view('wealth-wish-list')->with(['wealthWishes' => $user->wealthWishes->sortBy('expected_achievement_date')]);
    }

    public function wealthWishCreate()
    {
        return view('wealth-wish-create');
    }

    public function wealthWishUpdate($id)
    {
        $wealthWish = WealthWish::find($id);
        return view('wealth-wish-update', ['wealthWish' => $wealthWish]);
    }

    public function wealthWishDetail($id)
    {
        $wealthWish = WealthWish::find($id);
        return view('wealth-wish-detail', ['wealthWish' => $wealthWish]);
    }

    /**
     * Update existing wealth wish.
     *
     * @return mixed
     */
    public function updateData(Request $request)
    {
        try {

            $wish = WealthWish::find($request->get('wealthwishid'));
            if (!$wish) {
                throw new ModelNotFoundException();
            }
            $durationInMonths = $this->wealthWishService->calculateExpectedDuration($request->get('expected_achievement_date'), $request->get('effective_date'));

            if ($wish->expected_amount != $request->get('expected_amount') && !empty($wish->calculated_payment)) {
                // Re-calculate action plan
                // annualRate, period, presentValue, futureValue, advancedPayments
                if ($wish->expected_annual_rate == 0) {
                    $wish->calculated_payment = ($request->get('expected_amount') - $wish->present_value) / $durationInMonths;
                } else {
                    $payment = $this->wealthWishService->pmtWithAnnualRate($wish->expected_annual_rate, $durationInMonths, $wish->present_value, $request->get('expected_amount'), 0);
                    $wish->calculated_payment = -($payment);
                }
            } else {
                if (($wish->expected_achievement_date != $request->get('expected_achievement_date') || $wish->effective_date != $request->get('effective_date')) && !empty($wish->calculated_payment)) {
                    // Re-calculate action plan
                    // annualRate, period, presentValue, futureValue, advancedPayments
                    if ($wish->expected_annual_rate == 0) {
                        $wish->calculated_payment = ($request->get('expected_amount') - $wish->present_value) / $durationInMonths;
                    } else {
                        $payment = $this->wealthWishService->pmtWithAnnualRate($wish->expected_annual_rate, $durationInMonths, $wish->present_value, $request->get('expected_amount'), 0);
                        $wish->calculated_payment = -($payment);
                    }
                }
            }
            $timeBound = $this->wealthWishService->defineTimeBound($durationInMonths);
            $wish->time_bound = $timeBound;
            $wish->category = $request->get('category');
            $wish->title = $request->get('title');
            $wish->description = $request->get('description');
            $wish->effective_date = $request->get('effective_date');
            $wish->expected_achievement_date = $request->get('expected_achievement_date');
            $wish->expected_amount = $request->get('expected_amount');
            $wish->amount_term = $request->get('amount_term');
            $wish->expected_duration = $durationInMonths;
            $wish->priority = $request->get('priority');
            $wish->update();

        } catch (\Exception $e) {
            return response()->json(['status' => 'exception', 'msg' => $e->getMessage()]);
        }
        return response()->json(['status' => "success", 'wealth_wish_id' => $wish->id]);
    }

    /**
     * Create new wealth wish.
     *
     * @return mixed
     */
    public function storeData(Request $request)
    {
        try {
            $durationInMonths = $this->wealthWishService->calculateExpectedDuration($request->get('expected_achievement_date'), $request->get('effective_date'));
            $timeBound = $this->wealthWishService->defineTimeBound($durationInMonths);

            $newWish = WealthWish::create([
                'user_id'                   => Auth::user()->id,
                'time_bound'                => $timeBound,
                'category'                  => $request->has('category') ? $request->get('category') : '',
                'title'                     => $request->get('title'),
                'description'               => $request->get('description'),
                'effective_date'            => $request->has('expected_achievement_date') ? $request->get('effective_date') : '',
                'expected_achievement_date' => $request->has('expected_achievement_date') ? $request->get('expected_achievement_date') : '',
                'expected_amount'           => $request->get('expected_amount'),
                'amount_term'               => $request->has('amount_term') ? $request->get('amount_term') : '',
                'expected_duration'         => $durationInMonths ?? 0,
                'priority'                  => $request->has('priority') ? $request->get('priority') : ''
            ]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'exception', 'msg' => $e->getMessage()]);
        }
        return response()->json(['status' => "success", 'wealth_wish_id' => $newWish->id]);
    }

    // We are submitting are image along with wealthwishid and with the help of wealth wish id we are updating our record
    public function storeImage(Request $request)
    {
        if ($request->file('file')) {

            $images = $request->file('file');
            $count = 0;
            foreach ($images as $img) {
                //here we are getting userid along with an image
                $wealthWishId = $request->wealthwishid;
                $imageName = $count . '_' . time();
                $imagePath = $imageName . '.' . $img->getClientOriginalExtension();
                // $originalName = $img->getClientOriginalName();

                if (!is_dir(public_path() . '/images/wealth-wish')) {
                    mkdir(public_path() . '/images/wealth-wish', 0777, true);
                }

                $img->move(public_path() . '/images/wealth-wish', $imagePath);

                // we are updating our image column with the help of user id
                $wealthWishImage = new WealthWishImage();
                $wealthWishImage->wealth_wish_id = $wealthWishId;
                $wealthWishImage->path = $imagePath;
                $wealthWishImage->name = $imageName;
                $wealthWishImage->save();
                $count++;
            }
            return response()->json(['status' => "success", 'wealthwishid' => $wealthWishId]);
        }
    }

    public function wealthWishImages($id)
    {
        /* Upload directory*/
        $wish = WealthWish::find($id);
        $wealthWishImages = $wish->wealthWishImages;
        $targetDir = public_path('/images/wealth-wish/');
        $dir = '/images/wealth-wish/';
        $fileList = [];
        foreach ($wealthWishImages as $wealthWishImage) {

            if (!is_dir($targetDir . $wealthWishImage->path)) {
                $size = filesize($targetDir . $wealthWishImage->path);
                $fileList[] = ['name' => $wealthWishImage->name, 'size' => $size, 'path' => $dir . $wealthWishImage->path];
            }
        }
        return json_encode($fileList);
        //return response()->json(['status' => "success"]);
    }

    public function deleteImage(Request $request)
    {
        $filename = $request->get('name');//gives orginal file name eg:abc.jpg
        $image = WealthWishImage::where('name', $filename)->first();
        if (!$image) {
            throw new ModelNotFoundException();
        }
        WealthWishImage::where('name', $filename)->delete();
        $targetDir = public_path('/images/wealth-wish/') . $image->path;
        unlink($targetDir);
        return "ok";
    }

    public function deleteData(Request $request)
    {
        $wealthWishID = $request->get('wealthwishid');
        $wealthWish = WealthWish::find($wealthWishID);
        $wealthWishImages = $wealthWish->wealthWishImages;
        // Clear all related wealth wish images
        if ($wealthWishImages) {
            foreach ($wealthWishImages as $wealthWishImage) {
                $targetDir = public_path('/images/wealth-wish/') . $wealthWishImage->path;
                unlink($targetDir);
                $wealthWishImage->delete();
            }
        }
        // Delete wealth wish
        WealthWish::where('id', $wealthWishID)->delete();
        return 'Delete Successfully';
    }

    /**
     * Update Action Plan of Wealth Wish.
     *
     * @return mixed
     */
    public function updateActionPlan(ActionPlanRequest $request)
    {
        $input = $request->validated();
        try {
            $wish = WealthWish::find($input['wealthwishid']);
            if (!$wish) {
                throw new ModelNotFoundException();
            }
            if ($request->has('annualrate')) {
                $wish->expected_annual_rate = $input['annualrate'];
            }else {
                $wish->expected_annual_rate = 0;
            }
            $wish->present_value = $input['presentvalue'];
            $wish->asset_id = $input['tool'];
            // $wish->advanced_payment = $request->get('advanced_payment');
            $wish->calculated_payment = $input['payment'];
            $wish->update();

            Session::flash('message', 'Action Plan Saved successfully!');
            Session::flash('alert-class', 'alert-success');

            return redirect('/wealth-wish-detail/' . $wish->id);

        } catch (\Exception $e) {
            Session::flash('message', 'Something went wrong!' . $e->getMessage());
            Session::flash('alert-class', 'alert-danger');

            return redirect('/wealth-wish-detail/' . $wish->id);
        }
    }
}
