<?php

namespace App\Http\Requests;

use App\Utility\Response\Traits\StandardResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class Request extends FormRequest
{
    use StandardResponse;

    /**
     * Generate error response for validation
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $detail = [];
        foreach (collect($validator->messages())->toArray() as $key => $value) {
            $detail[$key] = $value[0];
        }

        throw new HttpResponseException(
            $this->jsonFailResponse('Validation Failed.', 422, ['error' => ['detail' => $detail]])
        );
    }

    /**
     * Group error message
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*' => 'There\'s something wrong with :attribute',
        ];
    }
}
