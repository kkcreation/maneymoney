<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessCreateOmiseCustomerId implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var $userId
     */
    public $userId;

    /**
     * Create a new job instance.
     *
     * @param $userId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var $user */
        $user = User::where('id', $this->userId)
            ->first();

        /** create Omise customer ID */
        if ($user->omise_customer_id === NULL) {
            try {
                $omiseCustomerId = \OmiseCustomer::create(
                    [
                        'email' => $user->email,
                        'description' => 'The User ID: ' . $user->id,
                    ]
                );

                $user->omise_customer_id = $omiseCustomerId['id'];
                $user->save();
            } catch (\Exception $exception) {
                $this->info('Failed Updating User ID:' . $user);
            }
        }
    }
}
