<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AMCFund extends Model
{
    use HasFactory;
    protected $table = 'amc_funds';

    protected $fillable = [
        'proj_id',
        'proj_abbr_name',
        'last_upd_date',
        'unique_id'
    ];
}
