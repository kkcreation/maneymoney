<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;
    protected $table = 'assets';

    protected $fillable = [
        'type',
        'name',
        'description',
    ];

    public function userAssets()
    {
        return $this->hasMany(UserAsset::class);
    }
}
