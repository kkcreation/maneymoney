<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    use HasFactory;
    protected $table = 'debts';

    protected $fillable = [
        'type',
        'name',
        'description',
    ];

    public function userDebts()
    {
        return $this->hasMany(UserDebt::class);
    }
}
