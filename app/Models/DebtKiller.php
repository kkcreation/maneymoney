<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DebtKiller extends Model
{
    use HasFactory;
    protected $table = 'debt_killers';

    protected $fillable = [
        'user_id',
        'debt_id',
        'creditor',
        'total_paid',
        'outstanding_balance',
        'expected_payment',
        'minimum_payment',
        'actual_payment',
        'annual_interest_rate',
        'statement_date',
        'due_date',
        'start_clearance_date',
        'expected_clearance_date',
        'expected_duration',
        'maximum_duration',
        'actual_duration',
        'total_interest_amount',
        'type',
        'notes',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function debt()
    {
        return $this->belongsTo(Debt::class);
    }
}
