<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;
    protected $table = 'expenses';

    protected $fillable = [
        'type',
        'name',
        'description',
    ];

    public function userExpenses()
    {
        return $this->hasMany(UserExpense::class);
    }
}
