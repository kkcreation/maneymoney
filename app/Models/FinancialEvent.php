<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialEvent extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'asset_id',
        'wealth_wish_id',
        'event_title',
        'event_start',
        'event_end',
        'event_tag',
        'event_action',
        'currency',
        'conversion_rate',
        'unit',
        'price',
        'fee',
        'color'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
