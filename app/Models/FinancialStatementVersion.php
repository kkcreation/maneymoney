<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FinancialStatementVersion extends Model
{
    use SoftDeletes, HasFactory;
    protected $table = 'financial_statement_versions';

    protected $fillable = [
        'submission_date',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function userIncome(){
        return $this->hasMany(UserIncome::class);
    }

    public function userExpense(){
        return $this->hasMany(UserExpense::class);
    }

    public function userAssets(){
        return $this->hasMany(UserAsset::class);
    }

    public function userDebts(){
        return $this->hasMany(UserDebt::class);
    }
}
