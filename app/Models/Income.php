<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    use HasFactory;
    protected $table = 'income';

    protected $fillable = [
        'type',
        'name',
        'description',
    ];

    public function userIncome()
    {
        return $this->hasMany(UserIncome::class);
    }
}
