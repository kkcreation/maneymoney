<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailLog extends Model
{
    protected $table = 'mail_logs';

    protected $fillable = [
        'booking_id',
        'mail_type',
        'message_id',
        'variables',
    ];

    protected $casts = [
        'variables' => 'array',
    ];
}
