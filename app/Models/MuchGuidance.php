<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MuchGuidance extends Model
{
    use SoftDeletes, HasFactory;
    protected $table = 'much_guidance';

    protected $fillable = [
        'type',
        'translation_key',
        'headline',
        'content',
        'attachment'
    ];
}
