<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Payment
 * @package App\Modules\Payment\Models
 */
class Payment extends Model
{

    use SoftDeletes, LogsActivity;

    protected $table = 'payments';

    protected $fillable = [
        'user_id',
        'payment_method',
        'charge_id',
        'status',
        'amount',
        'current_state'
    ];

    protected $casts = [
        'amount' => 'float',
    ];

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function getActivitylogOptions(): LogOptions
    {
        // TODO: Implement getActivitylogOptions() method.
    }
}
