<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'dob',
        'gender',
        'marital_status',
        'first_name',
        'last_name',
        'role',
        'omise_customer_id',
        'language_preference'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userAssets(){
        return $this->hasMany(UserAsset::class);
    }

    public function userDebts(){
        return $this->hasMany(UserDebt::class);
    }

    public function userIncome(){
        return $this->hasMany(UserIncome::class);
    }

    public function userExpenses(){
        return $this->hasMany(UserExpense::class);
    }

    public function wealthWishes(){
        return $this->hasMany(WealthWish::class);
    }

    public function investmentMemo(){
        return $this->hasMany(FinancialEvent::class);
    }

    public function debtKillers(){
        return $this->hasMany(DebtKiller::class);
    }

    public function financialStatementVersions()
    {
        return $this->hasMany(FinancialStatementVersion::class);
    }
}
