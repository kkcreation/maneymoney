<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAsset extends Model
{
    use SoftDeletes, HasFactory;
    protected $table = 'user_asset_records';

    protected $fillable = [
        'amount',
        'user_id',
        'asset_id',
        'key',
        'version_id'
    ];

    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }

    public function version()
    {
        return $this->belongsTo(FinancialStatementVersion::class);
    }
}
