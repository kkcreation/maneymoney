<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDebt extends Model
{
    use SoftDeletes, HasFactory;
    protected $table = 'user_debt_records';

    protected $fillable = [
        'amount',
        'user_id',
        'debt_id',
        'key',
        'version_id'
    ];

    public function debt()
    {
        return $this->belongsTo(Debt::class);
    }

    public function version()
    {
        return $this->belongsTo(FinancialStatementVersion::class);
    }
}
