<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserExpense extends Model
{
    use SoftDeletes, HasFactory;
    protected $table = 'user_expense_records';

    protected $fillable = [
        'amount',
        'user_id',
        'expense_id',
        'key',
        'version_id'
    ];

    public function expense()
    {
        return $this->belongsTo(Expense::class);
    }

    public function version()
    {
        return $this->belongsTo(FinancialStatementVersion::class);
    }
}
