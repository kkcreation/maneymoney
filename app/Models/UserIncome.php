<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserIncome extends Model
{
    use SoftDeletes, HasFactory;
    protected $table = 'user_income_records';

    protected $fillable = [
        'amount',
        'user_id',
        'income_id',
        'key',
        'version_id'
    ];

    public function income()
    {
        return $this->belongsTo(Income::class);
    }

    public function version()
    {
        return $this->belongsTo(FinancialStatementVersion::class);
    }
}
