<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WealthWish extends Model
{
    use HasFactory;
    protected $table = 'wealth_wishes';

    protected $fillable = [
        'user_id',
        'time_bound',
        'category',
        'title',
        'description',
        'effective_date',
        'expected_achievement_date',
        'actual_achievement_date',
        'expected_amount',
        'actual_amount',
        'amount_term',
        'expected_duration',
        'actual_duration',
        'priority'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wealthWishImages(){
        return $this->hasMany(WealthWishImage::class);
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }
}
