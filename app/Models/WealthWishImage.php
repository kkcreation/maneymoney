<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WealthWishImage extends Model
{
    use HasFactory;
    protected $table = 'wealth_wish_images';

    protected $fillable = [
        'wealth_wish_id',
        'name',
        'path'
    ];

    public function wealthWish()
    {
        return $this->belongsTo(WealthWish::class);
    }
}
