<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    public static $permissions = [
        'wealth-health-check' => ['customer', 'vip', 'admin'],
        'debt-killer'         => ['vip', 'admin'],
        'wealth-protection'   => ['vip', 'admin'],
        'wealth-accumulation' => ['vip', 'admin'],
        'manage-member'       => ['admin']
    ];


    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::before(function ($user, $ability) {
            if ($user->role === 'admin') {
                return true;
            }
        });

        foreach (self::$permissions as $permission => $roles) {
            Gate::define($permission, function (User $user) use ($roles) {
                foreach ($roles as $role) {
                    if ($user->role === $role) {
                        return true;
                    }
                }
            });
        }
        //
    }
}
