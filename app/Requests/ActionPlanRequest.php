<?php

namespace App\Requests;

use App\Http\Requests\Request;
use App\Utility\Response\Traits\StandardResponse;


class ActionPlanRequest extends Request
{
    use StandardResponse;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wealthwishid'     => 'required|exists:wealth_wishes,id',
            'tool'             => 'required|exists:assets,id',
            'annualrate'   => 'nullable|numeric|min:0',
            'presentvalue'   => 'nullable|numeric',
            'payment'     => 'required|numeric',
        ];
    }

    /**
     * This method is executed before the validation rules are actually evaluated
     *
     * @param $validator
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            //
        });
    }
}
