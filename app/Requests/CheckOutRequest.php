<?php

namespace App\Requests;

use App\Http\Requests\Request;
use App\Utility\Response\Traits\StandardResponse;


class CheckOutRequest extends Request
{
    use StandardResponse;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_token' => 'required',
            'user_id'     => 'sometimes|exists:users,id',
            'distinct_id'   => 'sometimes',
            'save_card'     => 'nullable|boolean',
        ];
    }

    /**
     * This method is executed before the validation rules are actually evaluated
     *
     * @param $validator
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            //
        });
    }
}
