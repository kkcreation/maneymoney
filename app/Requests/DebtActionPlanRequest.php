<?php

namespace App\Requests;

use App\Http\Requests\Request;
use App\Utility\Response\Traits\StandardResponse;


class DebtActionPlanRequest extends Request
{
    use StandardResponse;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'debtkillerid'     => 'required|exists:debt_killers,id',
            'type'             => 'required|in:fixed_duration,fixed_payment',
           /* 'annualrate'   => 'required|numeric|min:0',*/
            'expectedduration'   => 'nullable|numeric',
            'expectedpayment'     => 'required|numeric',
            'totalinterest'     => 'required|numeric'
        ];
    }

    /**
     * This method is executed before the validation rules are actually evaluated
     *
     * @param $validator
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            //
        });
    }
}
