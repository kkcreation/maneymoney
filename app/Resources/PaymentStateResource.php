<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentStateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'payment_id'     => $this->id,
            'payment_method' => $this->payment_method,
            'charge_id'      => $this->charge_id,
            'amount'         => $this->amount,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
        ];
    }
}
