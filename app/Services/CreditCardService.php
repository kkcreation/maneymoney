<?php

namespace App\Services;

use App\Models\User;

class CreditCardService
{
    /**
     * Gets a list of all cards belongs to this customer.
     *
     * @param User $user
     * @return \OmiseCardList
     */
    public function getAllUserCreditCards(User $user): \OmiseCardList
    {
        return \OmiseCustomer::retrieve($user->omise_customer_id)->cards();
    }

    /**
     * Retrieve a card
     *
     * @param User $user
     * @param $card
     * @return \OmiseCard
     */
    public function findById(User $user, $creditCard)
    {
        return \OmiseCustomer::retrieve($user->omise_customer_id)->cards()
            ->retrieve($creditCard);
    }

    /**
     * Attach a card to a customer.
     *
     * @param User $user
     * @param $card
     */
    public function addCreditCardToCustomer(User $user, $creditCard)
    {
        $customer = \OmiseCustomer::retrieve($user->omise_customer_id);
        $customer->update([
            'card' => (string)$creditCard
        ]);
    }

    /**
     * Update a card.
     *
     * @param User $user
     * @param $card
     * @param $data
     */
    public function updateCreditCard(User $user, $creditCard, $data)
    {
        $customer = \OmiseCustomer::retrieve($user->omise_customer_id);
        $creditCard = $customer->cards()->retrieve((string)$creditCard);
        $creditCard->update([
            'expiration_month' => $data['expiration_month'],
            'expiration_year'  => $data['expiration_year'],
            'name'             => $data['name'],
            'postal_code'      => $data['postal_code'] ?? '-'
        ]);
    }

    /**
     * Delete a card.
     *
     * @param User $user
     * @param $card
     * @return bool|null
     */
    public function deleteCreditCard(User $user, $creditCard)
    {
        $customer = \OmiseCustomer::retrieve($user->omise_customer_id);
        $creditCard = $customer->cards()->retrieve($creditCard);
        $creditCard->destroy();

        return $creditCard->isDestroyed();
    }

    /**
     * Get latest customer card.
     *
     * @param User $user
     * @return mixed
     */
    public function getLatestCustomerCreditCard(User $user)
    {
        $customer = \OmiseCustomer::retrieve($user->omise_customer_id)->cards();
        $customerCreditCards = collect(collect($customer)['data']);
        return $customerCreditCards->last();
    }
}
