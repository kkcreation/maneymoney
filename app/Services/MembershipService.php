<?php

namespace App\Services;

use App\Models\User;

class MembershipService
{
    /**
     * Update membership status with user id
     *
     * @param $userId
     * @param $status
     * @return mixed
     */
    public function updateMembershipStatusWithUserId($userId, $status)
    {
        $user = User::where('id', $userId)->first();
        $user->status = $status;
        $user->save();

        return $user;
    }
}
