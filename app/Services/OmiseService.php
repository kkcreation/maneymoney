<?php

namespace App\Services;

use Jenssegers\Agent\Agent;
use OmiseCharge;

class OmiseService
{

    /**
     * Send request to omise to create transaction
     *
     * @param $paymentData
     * @param $gift
     * @return OmiseCharge
     * @throws \OmiseException
     */
    public function makeTransaction($paymentData)
    {
        $paymentToken = $paymentData['payment_token'];
        $savedCard = $paymentData['save_card'] ?? null;
        $distinctId = $paymentData['distinct_id'];
        $paymentAmount = $paymentData['payment_amount'];
        // Credit card method
        if (str_contains($paymentToken, 'tokn_')) {
            if (empty($savedCard) || $savedCard === FALSE || $savedCard === null) {
                return $this->payWithCreditCard($distinctId, $paymentToken, $paymentAmount);
            } else {
                // add card
                $this->addCardToCustomer($paymentToken);
                // retrieve card id
                $cardId = $this->getLastestCardIdByCustomer();
                // charge
                return $this->payWithSavedCreditCard($distinctId, $cardId, $paymentAmount);
            }
        } elseif ((str_contains($paymentToken, 'src_'))) { // Source method e.g. promptpay, alipay
            return $this->payWithSource($distinctId, $paymentToken, $paymentAmount);
        } elseif ((str_contains($paymentToken, 'card_'))) {
            return $this->payWithSavedCreditCard($distinctId, $paymentToken, $paymentAmount);
        } else {
            throw new \OmiseException('Payment method not allowed');
        }
    }

    /**
     * Pay with credit card method
     *
     * @param $distinctId
     * @param $paymentToken
     * @param $gift
     * @return OmiseCharge
     */
    private function payWithCreditCard($paymentToken, $paymentAmount)
    {
        $url          = config('app.url');
        $agent        = new Agent();
        $userId       = Auth::user()->id;
        $chargeDetail = [
            'description' => "UserID : {$userId}",
            'card'        => $paymentToken,
            'amount'      => $this->omiseAmountFormat($paymentAmount),
            'currency'    => 'THB',
            'return_uri'  => $url . "/payments/verify?user_id={$userId}",
            'metadata'    => [
                'user_id'  => $userId,
                'client_info' => [
                    'user-agent' => $agent->getUserAgent(),
                    'device'     => $agent->device(),
                    'platform'   => $agent->platform(),
                    'browser'    => $agent->browser()
                ],
                'ip'          => $_SERVER['HTTP_CF_CONNECTING_IP'] ?? request()->ip(),
            ]
        ];

        return OmiseCharge::create($chargeDetail);
    }

    /**
     * Pay with source method e.g. alipay, promptpay
     *
     * @param $distinctId
     * @param $paymentToken
     * @param $gift
     * @return OmiseCharge
     */
    private function payWithSource($paymentToken, $paymentAmount)
    {
        $url          = config('app.url');
        $agent        = new Agent();
        $userId       = Auth::user()->id;
        $chargeDetail = [
            'description' => "UserID : {$userId}",
            'source'      => $paymentToken,
            'amount'      => $this->omiseAmountFormat($paymentAmount),
            'currency'    => 'THB',
            'return_uri'  => $url . "/payments/verify?user_id={$userId}",
            'metadata'    => [
                'user_id'  => $userId,
                'gift_id'  => $gift['id'],
                'client_info' => [
                    'user-agent' => $agent->getUserAgent(),
                    'device'     => $agent->device(),
                    'platform'   => $agent->platform(),
                ],
                'ip'          => $_SERVER['HTTP_CF_CONNECTING_IP'] ?? request()->ip(),
            ]
        ];

        return OmiseCharge::create($chargeDetail);
    }

    /**
     * Convert amount to omise format (omise need satang THB format)
     *
     * @param $value
     * @return float|int
     */
    private function omiseAmountFormat($value)
    {
        return $value * 100;
    }

    /**
     * Attach a card to a customer.
     *
     * @param $card
     */
    private function addCardToCustomer($card)
    {
        $customer = \OmiseCustomer::retrieve(auth()->user()->omise_customer_id);
        $customer->update([
            'card' => (string)$card
        ]);
    }

    /**
     * Get card id by token id.
     *
     * @param $tokenId
     * @return mixed
     */
    private function getCardIdByTokenId($tokenId)
    {
        $token = \OmiseToken::retrieve($tokenId);
        return $token['card']['id'];
    }

    /**
     * Get latest customer card id.
     *
     * @return mixed
     */
    private function getLastestCardIdByCustomer()
    {
        $customer = \OmiseCustomer::retrieve(auth()->user()->omise_customer_id)->cards();
        $customerCards = collect(collect($customer)['data']);
        return $customerCards->last()['id'];
    }

    /**
     * Pay with saved credit-card.
     *
     * @param $distinctId
     * @param $cardId
     * @param $gift
     * @return OmiseCharge
     */
    private function payWithSavedCreditCard($distinctId, $cardId, $gift)
    {
        $url          = config('app.url');
        $agent        = new Agent();
        $chargeDetail = [
            'description' => "GiftID : {$gift['unique_id']}",
            'card'        => $cardId,
            'customer'    => auth()->user()->omise_customer_id,
            'amount'      => $this->omiseAmountFormat($gift['total_paid']),
            'currency'    => 'THB',
            'return_uri'  => $url . "/creditpayments/verify?unique_id={$gift['unique_id']}",
            'metadata'    => [
                'unique_id'   => $gift['unique_id'],
                'gift_id'  => $gift['id'],
                'client_info' => [
                    'user-agent' => $agent->getUserAgent(),
                    'device'     => $agent->device(),
                    'platform'   => $agent->platform(),
                    'browser'    => $agent->browser()
                ],
                'distinct_id' => $distinctId,
                'ip'          => $_SERVER['HTTP_CF_CONNECTING_IP'] ?? request()->ip(),
            ]
        ];

        return OmiseCharge::create($chargeDetail);
    }


}
