<?php


namespace App\Services;

use App\Models\Payment;
use App\Resources\PaymentStateResource;
use Illuminate\Support\Facades\Auth;
use OmiseCharge;

class PaymentService
{
    /**
     * Create transaction
     *
     * @param $transaction
     * @return Payment
     */
    public function recordTransaction($transaction)
    {
        $payment                 = new Payment();
        $payment->user_id     = $transaction['metadata']['user_id'];
        $payment->payment_method = !empty($transaction['card']) ? 'credit_card' : $transaction['source']['type'];
        $payment->charge_id      = $transaction['id'];
        $payment->status         = $transaction['status'];
        $payment->amount         = $transaction['amount'] / 100; // Convert omiseAmountFormat to NormalFormat
        $payment->save();

        return $payment;
    }

    /**
     * Update transaction status with unique id
     *
     * @param $charge_id
     * @param $status
     * @return
     */
    public function updateTransactionStatusWithChargeId($charge_id, $status)
    {
        $payment         = Payment::where('charge_id', $charge_id)->first();
        $payment->status = $status;
        $payment->save();

        return $payment;
    }

    /**
     * Freeze raw current state to prevent changing
     *
     * @param $payment
     */
    public function recordCurrentState($payment)
    {
        $payment                = Payment::find($payment->id);
        $payment->current_state = json_encode(new PaymentStateResource($payment));
        $payment->save();
    }

    /**
     * Verify payment status
     *
     * In this case, this process should return true even status equal to pending
     * Because we use omise webhook to update payment status
     * So, we can always return true...
     *
     * @param $userId
     * @return bool
     */
    public function verifyPayment()
    {
        // This line should be always true. Because It pass the validator already.

        // Query with eloquent instead of using relation model to use full function
        $payment = Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        // Get latest status from omise server
        $transaction = OmiseCharge::retrieve($payment->charge_id);

        // @TODO : This condition is not complete yet because we can not handle in case of waiting banking to approve payment
        if ($transaction['status'] === 'successful' || $transaction['status'] === 'pending') {
            return true;
        }

        return false;
    }

    /**
     * Mapping error status from omise to display error message
     *
     * @param $failureCode
     * @return mixed
     */
    public function mapTransactionErrorMessage($failureCode)
    {
        $messages = [
            'confirmed_amount_mismatch' => 'Final amount from payment channel does not match original amount.',
            'failed_fraud_check'        => 'Card was marked as fraudulent.',
            'failed_processing'         => 'General payment processing failure.',
            'insufficient_balance'      => 'Insufficient funds in the account or the card has reached the credit limit.',
            'insufficient_fund'         => 'Insufficient funds in the account or the card has reached the credit limit.',
            'invalid_account_number'    => 'Valid account for payment method not found.',
            'invalid_account'           => 'Valid account for payment method not found.',
            'invalid_security_code'     => 'Security code invalid or card did not pass preauthorization.',
            'payment_cancelled'         => 'Payment cancelled by payer.',
            'payment_rejected'          => 'Payment rejected by issuer.',
            'stolen_or_lost_card'       => 'Card stolen or lost.',
            'timeout'                   => 'Payer did not take action before charge expiration.',
        ];

        return $messages[$failureCode];
    }

    /**
     * Health check payment transaction when customer use `PromptPay`
     * or other payment methods which need to check in time range eg. countdown, timer...
     *
     * @param $uniqueId
     * @return bool
     */
    public function checkStatus()
    {
        // Query with eloquent instead of using relation model to use full function
        $payment = Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        // Get latest status from omise server
        $transaction = OmiseCharge::retrieve($payment->charge_id);

        if ($transaction['status'] === 'successful') {
            return true;
        }

        return false;
    }

    /**
     * Get charge by charge id.
     *
     * @param $chargeId
     * @return mixed
     */
    public function findChargeByChargeId($chargeId)
    {
        return Payment::where('charge_id', $chargeId)->first();
    }

}
