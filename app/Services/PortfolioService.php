<?php

namespace App\Services;

use App\Models\User;

class PortfolioService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getPortfolioInfo($investmentMemo)
    {
        $groupedInvestmentMemo = $investmentMemo->groupBy(['wealth_wish_id', 'event_title'])->values()->all();
        $collection = collect();
        foreach($groupedInvestmentMemo as $key => $value) {
            $data = $groupedInvestmentMemo[$key]->each(function ($item) {
                $data = collect();
                if ($item->where('event_action', 'buy')->first()) {
                    $totalInUnit = $item->where('event_action', 'buy')->sum('unit');
                    $totalOutUnit = $item->where('event_action', 'sell')->sum('unit');
                    $totalBalanceUnit = $totalInUnit - $totalOutUnit;
                    $totalInValue = $item->where('event_action', 'buy')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                    $totalOutValue = $item->where('event_action', 'sell')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                } else if ($item->where('event_action', 'deposit')->first()) {
                    $totalInUnit = $item->where('event_action', 'deposit')->sum('unit');
                    $totalOutUnit = $item->where('event_action', 'withdraw')->sum('unit');
                    $totalBalanceUnit = $totalInUnit - $totalOutUnit;
                    $totalInValue = $item->where('event_action', 'deposit')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                    $totalOutValue = $item->where('event_action', 'withdraw')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                }
                $costPerUnit = divnum($totalInUnit, $totalOutUnit);
                $data->put('total_in_unit', round($totalInUnit, 4));
                $data->put('total_out_unit', round($totalOutUnit, 4));
                $data->put('total_balance_unit', round($totalBalanceUnit, 4));
                $data->put('total_in_value', round($totalInValue, 0));
                $data->put('total_cost_value', round($totalInValue - $totalOutValue, 0));
                $data->put('cost_per_unit', round($costPerUnit, 4));
                $item->put('summary', $data);
            });
            $data = $data->filter(function ($item) {
                if ($item->get('summary')->get('total_balance_unit') <= 0.0001) {
                    return false;
                } else {
                    return true;
                }
            });
            $collection->push($data);
        }
        return $collection;
    }

    public function getInvestmentSummary($investmentMemo)
    {
        $groupedInvestmentMemo = $investmentMemo->groupBy(['wealth_wish_id', 'event_title'])->values()->all();
            $data = $groupedInvestmentMemo[0]->each(function ($item) {
                $data = collect();
                if ($item->where('event_action', 'buy')->first()) {
                    $totalInUnit = $item->where('event_action', 'buy')->sum('unit');
                    $totalOutUnit = $item->where('event_action', 'sell')->sum('unit');
                    $totalBalanceUnit = $totalInUnit - $totalOutUnit;
                    $totalInValue = $item->where('event_action', 'buy')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                    $totalOutValue = $item->where('event_action', 'sell')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                } else if ($item->where('event_action', 'deposit')->first()) {
                    $totalInUnit = $item->where('event_action', 'deposit')->sum('unit');
                    $totalOutUnit = $item->where('event_action', 'withdraw')->sum('unit');
                    $totalBalanceUnit = $totalInUnit - $totalOutUnit;
                    $totalInValue = $item->where('event_action', 'deposit')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                    $totalOutValue = $item->where('event_action', 'withdraw')->sum(function ($value) {
                        return $value['unit'] * $value['price'];
                    });
                }
                $costPerUnit = divnum($totalInUnit, $totalOutUnit);
                $data->put('total_in_unit', round($totalInUnit, 4));
                $data->put('total_out_unit', round($totalOutUnit, 4));
                $data->put('total_balance_unit', round($totalBalanceUnit, 4));
                $data->put('total_in_value', round($totalInValue, 0));
                $data->put('total_cost_value', round($totalInValue - $totalOutValue, 0));
                $data->put('cost_per_unit', round($costPerUnit, 4));
                $item->put('summary', $data);
            });
            $data = $data->filter(function ($item) {
                if ($item->get('summary')->get('total_balance_unit') <= 0.0001) {
                    return false;
                } else {
                    return true;
                }
            });
        return $data;
    }
}
