<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;

class SECService
{
    /**
     * Get project abbr name
     *
     */
    public function getProjectAbbrName($name)
    {
        $header = [
            'Content-Type'              => 'application/json',
            'Cache-Control'             => 'no-cache',
            'Ocp-Apim-Subscription-Key' => 'f345c1571acb4e4ba6ff5955f48371a8'
        ];
        $attributes = [
            'name' => $name,
        ];

        $url = 'https://api.sec.or.th/FundFactsheet/fund/class_fund';
        $client = new \GuzzleHttp\Client();
        $request = $client->post($url, ['headers' => $header, 'body' => json_encode($attributes)]);
        $response = json_decode($request->getBody());
        return $response;
    }

    /**
     * Get fund info
     *
     */
    public function getFundInfo($name)
    {
        $header = [
            'Content-Type'              => 'application/json',
            'Cache-Control'             => 'no-cache',
            'Ocp-Apim-Subscription-Key' => 'f345c1571acb4e4ba6ff5955f48371a8'
        ];
        $attributes = [
            'name' => $name,
        ];

        $url = 'https://api.sec.or.th/FundFactsheet/fund';
        $client = new \GuzzleHttp\Client();
        $request = $client->post($url, ['headers' => $header, 'body' => json_encode($attributes)]);
        $response = json_decode($request->getBody());
        return $response;
    }

    /**
     * Get fund classes
     *
     */
    public function getFundClasses($projectID)
    {
        $header = [
            'Content-Type'              => 'application/json',
            'Cache-Control'             => 'no-cache',
            'Ocp-Apim-Subscription-Key' => 'f345c1571acb4e4ba6ff5955f48371a8'
        ];

        $url = 'https://api.sec.or.th/FundFactsheet/fund/'.$projectID.'/class_fund';
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url, ['headers' => $header]);
        $response = json_decode($request->getBody());
        return $response;
    }

    /**
     * Get fund daily NAV
     *
     */
    public function getFundDailyNAV($projectID, $navDate)
    {
        $header = [
            'Content-Type'              => 'application/json',
            'Cache-Control'             => 'no-cache',
            'Ocp-Apim-Subscription-Key' => '3e2ee747b76b4a74831e8ee3e8bf4420'
        ];

        $url = 'https://api.sec.or.th/FundDailyInfo/'.$projectID.'/dailynav/'.$navDate;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url, ['headers' => $header]);
        $response = json_decode($request->getBody());
        return $response;
    }

    /**
     * Get all AMC
     *
     */
    public function getAllAMC()
    {
        $header = [
            'Content-Type'              => 'application/json',
            'Cache-Control'             => 'no-cache',
            'Ocp-Apim-Subscription-Key' => 'f345c1571acb4e4ba6ff5955f48371a8'
        ];

        $url = 'https://api.sec.or.th/FundFactsheet/fund/amc';
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url, ['headers' => $header]);
        $response = json_decode($request->getBody());
        return $response;
    }

    /**
     * Get amc funds
     *
     */
    public function getAMCFunds($uniqueID)
    {
        $header = [
            'Content-Type'              => 'application/json',
            'Cache-Control'             => 'no-cache',
            'Ocp-Apim-Subscription-Key' => 'f345c1571acb4e4ba6ff5955f48371a8'
        ];

        $url = 'https://api.sec.or.th/FundFactsheet/fund/amc/'.$uniqueID;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url, ['headers' => $header]);
        $response = json_decode($request->getBody());
        return $response;
    }
}
