<?php

namespace App\Services;

use App\Models\Asset;
use App\Models\Debt;
use App\Models\Expense;
use App\Models\Income;
use App\Models\User;
use App\Models\UserAsset;
use App\Models\UserDebt;
use App\Models\UserExpense;
use App\Models\UserIncome;
use Carbon\Carbon;
use Illuminate\Support\Str;

class WealthService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function wealthHealthCheck($date): array
    {
        $version = $this->getFinancialStatementVersion($date);
        if ($version->first()) {
            $versionId = $version->first()->id;
            $user = User::find($this->user->id);
            $userIncome = $user->userIncome->where('version_id', $versionId);
            $userExpenses = $user->userExpenses->where('version_id', $versionId);
            $userAssets = $user->userAssets->where('version_id', $versionId);
            $userDebts = $user->userDebts->where('version_id', $versionId);
            $data = [];
            $data['survival_ratio'] = $this->survivalRatio($userIncome, $userExpenses);
            $data['wealth_ratio'] = $this->wealthRatio($userIncome, $userExpenses);
            $data['debt_to_asset'] = $this->debtToAsset($userDebts, $userAssets);
            $data['investment_to_net_wealth'] = $this->investmentToNetWealth($userDebts, $userAssets);
            $data['installment_to_income'] = $this->installmentToIncome($userExpenses, $userIncome);
            $data['saving_to_income'] = $this->savingToIncome($userExpenses, $userIncome);
            $data['expense_to_income'] = $this->expenseToIncome($userExpenses, $userIncome);
            $data['insurance_premium_to_income'] = $this->insurancePremiumToIncome($userExpenses, $userIncome);
            $data['reserved_money_to_expense'] = $this->reservedMoneyToExpense($userAssets, $userExpenses);
        } else {
            $data = [];
        }

        return $data;
    }

    private function survivalRatio($userIncome, $userExpenses): array
    {
        $survivalRatio = [];
        $activeIncome = $userIncome->where('income.type', 'active_income');
        $passiveIncome = $userIncome->where('income.type', 'passive_income');
        $survivalRatio['active_income'] = $activeIncome;
        $survivalRatio['passive_income'] = $passiveIncome;
        $survivalRatio['income'] = $userIncome->sum('amount');
        $survivalRatio['expense'] = $userExpenses->whereIn('expense.type', ['fixed_expense','non_fixed_expense'])->sum('amount');
        $survivalRatio['value'] = number_format(divnum($userIncome->sum('amount'), $userExpenses->whereIn('expense.type', ['fixed_expense','non_fixed_expense'])->sum('amount')), 2);
        $survivalRatio['benchmark'] = config('hellomuch.wealth_check.survival_ratio.benchmark');
        return $survivalRatio;
    }

    private function wealthRatio($userIncome, $userExpenses): array
    {
        $passiveIncome = $userIncome->where('income.type', 'passive_income');
        $wealthRatio = [];
        $wealthRatio['passive_income'] = $passiveIncome;
        $wealthRatio['expense'] = $userExpenses->whereIn('expense.type', ['fixed_expense','non_fixed_expense'])->sum('amount');
        $wealthRatio['value'] = number_format(divnum($passiveIncome->sum('amount'), $userExpenses->whereIn('expense.type', ['fixed_expense','non_fixed_expense'])->sum('amount')), 2);
        $wealthRatio['benchmark'] = config('hellomuch.wealth_check.wealth_ratio.benchmark');
        return $wealthRatio;
    }

    private function debtToAsset($userDebts, $userAssets): array
    {
        $debtToAsset = [];
        $debtToAsset['debt'] = $userDebts->sum('amount');
        $debtToAsset['asset'] = $userAssets->sum('amount');
        $debtToAsset['value'] = round(floatval((divnum($userDebts->sum('amount'), $userAssets->sum('amount'))) * 100), 2);
        $debtToAsset['benchmark'] = config('hellomuch.wealth_check.debt_to_asset.benchmark');
        return $debtToAsset;
    }

    private function investmentToNetWealth($userDebts, $userAssets): array
    {
        $netWealth = $userAssets->sum('amount') - $userDebts->sum('amount');
        $investmentAsset = $userAssets->where('asset.type', 'investment_asset')->sum('amount');
        $investmentToNetWealth = [];
        $formattedNumber = round(floatval((divnum($investmentAsset, $netWealth)) * 100), 2);
        $investmentToNetWealth['investment_asset'] = $investmentAsset;
        $investmentToNetWealth['net_wealth'] = $netWealth;
        $investmentToNetWealth['value'] = $formattedNumber;
        $investmentToNetWealth['benchmark'] = config('hellomuch.wealth_check.investment_to_wealth.benchmark');
        return $investmentToNetWealth;
    }

    private function installmentToIncome($userExpenses, $userIncome): array
    {
        $installmentToIncome = [];
        $installment = $userExpenses->whereIn('expense.name',
            [
                'real_estate_installment',
                'vehicle_installment',
                'product_installment',
                'informal_debt_installment',
                'personal_loan_installment',
                'student_loan_installment',
                'cooperative_loan_installment'
            ]
        )->sum('amount');
        $installmentToIncome['installment'] = $installment;
        $installmentToIncome['value'] = number_format(divnum($installmentToIncome['installment'], $userIncome->sum('amount')), 2);
        $installmentToIncome['benchmark'] = config('hellomuch.wealth_check.installment_to_income.benchmark');

        return $installmentToIncome;
    }

    private function savingToIncome($userExpenses, $userIncome): array
    {
        $savingToIncome = [];
        $saving = $userExpenses->whereIn('expense.type',
            [
                'savings',
                'investment',
            ]
        )->sum('amount');
        $savingToIncome['saving_investment'] = $saving;
        $savingToIncome['value'] = number_format(divnum($savingToIncome['saving_investment'], $userIncome->sum('amount')), 2);
        $savingToIncome['benchmark'] = config('hellomuch.wealth_check.saving_to_income.benchmark');
        return $savingToIncome;
    }

    private function expenseToIncome($userExpenses, $userIncome): array
    {
        $expenseToIncome = [];
        $nonFixedExpense = $userExpenses->whereIn('expense.type',
            [
                'non_fixed_expense',
            ]
        )->sum('amount');
        $fixedExpense = $userExpenses->where('expense.type', 'fixed_expense');
        $specificFixedExpense = $fixedExpense->whereIn('expense.name',
            [
                'real_estate_rental',
                'insurance_premium',
                'other'
            ]
        )->sum('amount');


        $expenseToIncome['personal_expense'] = $nonFixedExpense + $specificFixedExpense;
        $expenseToIncome['value'] = number_format(divnum($expenseToIncome['personal_expense'], $userIncome->sum('amount')), 2);
        $expenseToIncome['benchmark'] = config('hellomuch.wealth_check.expense_to_income.benchmark');
        return $expenseToIncome;
    }

    private function insurancePremiumToIncome($userExpenses, $userIncome): array
    {
        $insurancePremiumToIncome = [];
        $insurancePremium = $userExpenses->whereIn('expense.name',
            [
                'insurance_premium',
            ]
        )->sum('amount');
        $insurancePremiumToIncome['insurance_premium'] = $insurancePremium;
        $insurancePremiumToIncome['value'] = number_format(divnum($insurancePremiumToIncome['insurance_premium'], $userIncome->sum('amount')), 2);
        $insurancePremiumToIncome['benchmark'] = config('hellomuch.wealth_check.insurance_premium_to_income.benchmark');
        return $insurancePremiumToIncome;
    }

    private function reservedMoneyToExpense($userAssets, $userExpenses): array
    {
        $reservedMoneyToExpense = [];
        $reservedMoney = $userAssets->whereIn('asset.name',
            [
                'reserved_money',
            ]
        )->sum('amount');
        $reservedMoneyToExpense['reserved_money'] = $reservedMoney;
        $reservedMoneyToExpense['expense'] = round($userExpenses->whereIn('expense.type', ['fixed_expense','non_fixed_expense'])->sum('amount'), -3);
        $reservedMoneyToExpense['value'] = number_format(divnum($reservedMoneyToExpense['reserved_money'], $reservedMoneyToExpense['expense']), 2);
        $reservedMoneyToExpense['benchmark'] = config('hellomuch.wealth_check.reserved_money_to_expense.benchmark');
        $reservedMoneyToExpense['reserved_money_percentage'] = number_format((floatval($reservedMoneyToExpense['value']) / 6.0) * 100, 2);
        return $reservedMoneyToExpense;
    }

    public function insertOrUpdateVersion(User $user, $submissionDate)
    {
        // $date = Carbon::createFromFormat('Y-m-d', $submissionDate)->format('Y-m');
        $filterVersions = $user->financialStatementVersions->filter(function ($q) use ($submissionDate) {
            return Str::startsWith($q['submission_date'], $submissionDate);
        });
        if ($filterVersions->count() > 0) {
            return 'update';
        } else { // INSERT
            return 'insert';
        }
    }

    /**
     * Generate user income's default list
     *
     * @param User $user
     * @param $newVersion
     * @param null $submissionDate
     * @return void
     */
    public function generateUserIncome(User $user, $newVersion)
    {
        $allIncome = Income::all();
        foreach ($allIncome as $income) {
            $userIncome = new UserIncome();
            $userIncome->version_id = $newVersion->id;
            $userIncome->user_id = $user->id;
            $userIncome->income_id = $income->id;
            $userIncome->key = $income->type . '_' . $income->name;
            $userIncome->amount = 0.00;
            $userIncome->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $userIncome->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $userIncome->save();
        }
    }

    /**
     * Generate user expense's default list
     *
     * @param User $user
     * @param $newVersion
     * @param null $submissionDate
     * @return void
     */
    public function generateUserExpense(User $user, $newVersion)
    {
        $allExpense = Expense::all();
        foreach ($allExpense as $expense) {
            $userExpense = new UserExpense();
            $userExpense->version_id = $newVersion->id;
            $userExpense->user_id = $user->id;
            $userExpense->expense_id = $expense->id;
            $userExpense->key = $expense->type . '_' . $expense->name;
            $userExpense->amount = 0.00;
            $userExpense->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $userExpense->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $userExpense->save();
        }
    }

    /**
     * Generate user asset's default list
     *
     * @param User $user
     * @param $newVersion
     * @param null $submissionDate
     * @return void
     */
    public function generateUserAsset(User $user, $newVersion)
    {
        $allAsset = Asset::all();
        foreach ($allAsset as $asset) {
            $userAsset = new UserAsset();
            $userAsset->version_id = $newVersion->id;
            $userAsset->user_id = $user->id;
            $userAsset->asset_id = $asset->id;
            $userAsset->key = $asset->type . '_' . $asset->name;
            $userAsset->amount = 0.00;
            $userAsset->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $userAsset->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $userAsset->save();
        }
    }

    /**
     * Generate user debt's default list
     *
     * @param User $user
     * @param $newVersion
     * @param null $submissionDate
     * @return void
     */
    public function generateUserDebt(User $user, $newVersion)
    {
        $allDebt = Debt::all();
        foreach ($allDebt as $debt) {
            $userDebt = new UserDebt();
            $userDebt->version_id = $newVersion->id;
            $userDebt->user_id = $user->id;
            $userDebt->debt_id = $debt->id;
            $userDebt->key = $debt->type . '_' . $debt->name;
            $userDebt->amount = 0.00;
            $userDebt->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $userDebt->updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $userDebt->save();
        }
    }

    public function getFinancialStatementVersion($submissionDate)
    {
        //$date = Carbon::createFromFormat('Y-m-d', $submissionDate)->format('Y-m');
        $version = $this->user->financialStatementVersions->filter(function ($q) use ($submissionDate) {
            return Str::startsWith($q['submission_date'], $submissionDate);
        });
        return $version;
    }
}
