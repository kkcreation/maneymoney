<?php

namespace App\Services;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class WealthWishService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function calculateExpectedDuration($expectedAchievementDate, $effectiveDate = null): int
    {
        if (isset($effectiveDate)) {
            $startDate = $effectiveDate;
        } else {
            $startDate = Carbon::now();
        }
        $expectedAchievementDate = Carbon::parse($expectedAchievementDate);
        return $expectedAchievementDate->diffInMonths($startDate);
    }

    public function defineTimeBound($durationInMonths)
    {
        $durationInYears = $durationInMonths / 12.00;
        $timeBound = null;
        if ($durationInYears >= 0 && $durationInYears < 3) {
            $timeBound = 'short_term';
        } else if ($durationInYears >= 3 && $durationInYears <= 7) {
            $timeBound = 'mid_term';
        } else if ($durationInYears > 7) {
            $timeBound = 'long_term';
        }
        return $timeBound;
    }

    public function pmtWithAnnualRate($annualRate, $period, $presentValue, $futureValue, $advancedPayments)
    {
        $monthlyRate = $annualRate / 1200;
        $t1 = 1 + $monthlyRate;
        $t2 = pow($t1, $period);
        $t3 = pow($t1, ($period - $advancedPayments));
        return ($presentValue - ($futureValue / $t2)) / (((1 - (1 / ($t3))) / $monthlyRate) + $advancedPayments);
    }
}
