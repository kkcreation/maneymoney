<?php

namespace App\Utility\ActivityLog\Traits;

use Illuminate\Database\Eloquent\Model;

trait CustomActivityLog
{
    /**
     *  Comparing old values and new values to log
     *
     * @param Model $model
     * @param array $oldValues
     * @param bool $beforeSave
     * @return array
     */
    public function detectsChanges(Model $model, array $oldValues, bool $beforeSave = false)
    {
        $original = $oldValues;
        $changed  = $beforeSave ? $model->getDirty() : $model->getChanges();
        $changes  = [];

        // Remove updated_at from array when compare
        if (!empty($original['updated_at'])) {
            unset($original['updated_at']);
        }

        if (!empty($changed['updated_at'])) {
            unset($changed['updated_at']);
        }

        foreach ($changed as $key => $value) {
            // Return only changed value and ignore updated_at column
            if ($original[$key] != $value) {
                $changes[$key] = [
                    'original' => $original[$key],
                    'changes'  => $value,
                ];
            }
        }

        return $changes;
    }
}
