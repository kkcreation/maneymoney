<?php

namespace App\Utility\Language\Traits;

use App\Models\User;

trait UserLanguage
{

    /**
     * Update user language
     *
     * @param $lang
     */
    public function updateUserLanguage($userId, $lang)
    {
        $user                      = User::find($userId);
        $user->language_preference = $lang;
        $user->save();
    }
}
