<?php

namespace App\Utility\Random\Traits;

trait IssuerUniqueIdentity
{
    static $ALPHABET_NUMBERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    static $PATTERN = '/^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*)$/'; // 1 digit, 1 uppercase, 1 lowercase

    /**
     * Dynamic generate unique string
     *
     * @param $model
     * @param string $uniqueField
     * @param string|null $uniqueStringPattern This should be string pattern to use in `sprintf` method
     * @param int $length
     * @return false|string
     */
    public function generateUniqueString($model, string $uniqueField, string $uniqueStringPattern = null, int $length = 6)
    {
        do {
            $uniqueString = substr(str_shuffle(self::$ALPHABET_NUMBERS), 0, $length);

            // Concat string pattern and random unique string
            if ($uniqueStringPattern) {
                $uniqueString = sprintf($uniqueStringPattern, $uniqueString);
            }

        } while ($model::where($uniqueField, $uniqueString)->exists() || preg_match(self::$PATTERN, $uniqueString) === 0);

        return $uniqueString;
    }
}
