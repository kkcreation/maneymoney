<?php

namespace App\Utility\Response\Traits;

trait StandardResponse
{
    /**
     * Return standard successful response for RLAX
     *
     * @param string|null $message
     * @param int $httpCode
     * @param array|null $optionals
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonSuccessfulResponse(string $message = null, int $httpCode = 200, array $optionals = null)
    {
        $data = [
            'success' => true,
            'code'    => $httpCode,
            'message' => __($message) ?? null,
        ];

        if ($optionals) {
            $data = array_merge($data, $optionals);
        }

        return response()->json($data, $httpCode);
    }

    /**
     * Return standard failed response for RLAX
     *
     * @param string|null $message
     * @param int $httpCode
     * @param array|null $optionals
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonFailResponse(string $message = null, int $httpCode = 400, array $optionals = null)
    {
        $data = [
            'success' => false,
            'code'    => $httpCode,
            'message' => __($message) ?? null,
        ];

        if ($optionals) {
            $data = array_merge($data, $optionals);
        }

        return response()->json($data, $httpCode);
    }
}
