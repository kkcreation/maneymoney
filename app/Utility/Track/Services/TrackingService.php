<?php

namespace App\Utility\Track\Services;

class TrackingService
{
    /**
     * Get the client IP address.
     *
     * @return string|null
     */
    public function getRealClientIp()
    {
        return config('app.env') === 'production' ? $_SERVER['HTTP_CF_CONNECTING_IP'] : request()->ip() ?? '';
    }
}
