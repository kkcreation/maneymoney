<?php
return [
        'attributes' => [
            'time_bound' => [
                'en' => 'Time Bound',
                'th' => 'กรอบเวลา',
                'color' => [
                    'short_term' => 'warning',
                    'long_term' => 'danger',
                ]
            ],
            'creditor' => [
                'en' => 'Creditor',
                'th' => 'เจ้าหนี้',
            ],
            'outstanding_balance' => [
                'en' => 'Balance (THB)',
                'th' => 'ยอดหนี้คงเหลือ (บาท)',
            ],
            'effective_date' => [
                'en' => 'Start Date',
                'th' => 'วันที่เริ่ม'
            ],
            'statement_date' => [
                'en' => 'Statement Date',
                'th' => 'วันสรุปยอด'
            ],
            'due_date' => [
                'en' => 'Due Date',
                'th' => 'วันครบกำหนดชำระ'
            ],
            'start_clearance_date' => [
                'en' => 'Start Clearance Date',
                'th' => 'วันเริ่มจัดการหนี้'
            ],
            'expected_clearance_date' => [
                'en' => 'Expected Clearance Date',
                'th' => 'วันปิดบัญชีหนี้ (คาดการณ์)'
            ],
            'expected_duration' => [
                'en' => 'Duration',
                'th' => 'ระยะเวลาผ่อนชำระ'
            ],
            'expected_payment' => [
                'en' => 'Expected Payment (THB/Month)',
                'th' => 'ยอดผ่อนชำระ (บาท/เดือน)'
            ],
            'annual_interest_rate' => [
                'en' => 'Annual Interest Rate (%)',
                'th' => 'อัตราดอกเบี้ย (% ต่อปี)'
            ],
            'type' => [
                'en' => 'Plan Type',
                'th' => 'ประเภทแผน'
            ],
            'notes' => [
                'en' => 'Notes',
                'th' => 'บันทึกเดือนความจำ'
            ]
        ],
        'en' => [
            'time_bound' => [
                'short_term' => 'Short Term Debt',
                'long_term' => 'Long Term Debt'
            ],
            'type' => [
                'fixed_duration' => 'Fixed Duration',
                'fixed_payment' => 'Fixed Payment',
            ],
            'status' => [
                'active' => 'Active',
                'inactive' => 'Inactive',
                'completed' => 'Completed'
            ],
        ],
        'th' => [
            'time_bound' => [
                'short_term' => 'หนี้สินระยะสั้น',
                'long_term' => 'หนี้สินระยะยาว'
            ],
            'type' => [
                'fixed_duration' => 'กำหนดระยะเวลา',
                'fixed_payment' => 'กำหนดจำนวนผ่อนชำระ',
            ],
            'status' => [
                'active' => 'ดำเนินการ',
                'inactive' => 'หยุดดำเนินการ',
                'completed' => 'เสร็จสมบูรณ์'
            ],
        ]
];
