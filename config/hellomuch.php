<?php
return [
    'role' => [
        'admin' => 'Administrator',
        'member' => 'Regular',
        'customer' => 'Standard',
        'vip' => 'Prime'
    ],
    'membership_payment_interval' => [
        'none' => 'None',
        'monthly' => 'Monthly',
        'half_yearly' => 'Half-Yearly',
        'yearly' => 'Yearly',
        'lifetime' => 'Lifetime'
    ],
    'marital_status' => [
        'single' => 'Single',
        'married' => 'Married',
        'widowed' => 'Widowed',
        'separated' => 'Separated',
        'divorced' => 'Divorced'
    ],
    'gender' => [
        'male' => 'Male',
        'female' => 'Female',
        'other' => 'Other'
    ],
    'wealth_check' => [
        'survival_ratio' => [
            'benchmark' => [
                'index' => 1,
                'unit' => 'unit',
                'sign' => '>=',
                'label' => '>= 1'
            ]
        ],
        'wealth_ratio' => [
            'benchmark' => [
                'index' => 1.5,
                'unit' => 'unit',
                'sign' => '>=',
                'label' => '>= 1.5'
            ]
        ],
        'debt_to_asset' => [
            'benchmark' => [
                'index' => 50,
                'unit' => 'percentage',
                'sign' => '<=',
                'label' => '<= 50%'
            ]
        ],
        'investment_to_wealth' => [
            'benchmark' => [
                'index' => 50,
                'unit' => 'percentage',
                'sign' => '>=',
                'label' => '>= 50%'
            ]
        ],
        'installment_to_income' => [
            'benchmark' => [
                'index' => 40,
                'unit' => 'percentage',
                'sign' => '<=',
                'label' => '<= 40%'
            ]
        ],
        'saving_to_income' => [
            'benchmark' => [
                'index_min' => 10,
                'index_suggested' => 20,
                'unit' => 'percentage',
                'sign' => '>=',
                'label' => '>= 10%'
            ]
        ],
        'expense_to_income' => [
            'benchmark' => [
                'index' => 50,
                'unit' => 'percentage',
                'sign' => '<=',
                'label' => '<= 50%'
            ]
        ],
        'insurance_premium_to_income' => [
            'benchmark' => [
                'index_min' => 10,
                'index_max' => 15,
                'unit' => 'percentage',
                'sign' => 'between',
                'label' => '10% - 15%'
            ]
        ],
        'reserved_money_to_expense' => [
            'benchmark' => [
                'index' => 6,
                'unit' => 'months',
                'sign' => '>=',
                'label' => '>= 6 months'
            ]
        ],
    ]
];
