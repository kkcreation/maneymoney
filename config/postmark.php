<?php

return [
    'client_id' => env('POSTMARK_TOKEN'),
    'sender' => env('POSTMARK_MAIL_SENDER', 'admin@hellomuch.com'),
    'webhook_token' => env('POSTMARK_WEBHOOK_TOKEN'),
    'preheader' => [],
    'reset_password' => [
        'en' => [
            'id'     => 19605669,
        ],
        'th' => [
            'id' => 19888900,
        ]
    ]
];
