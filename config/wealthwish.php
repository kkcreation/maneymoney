<?php
return [
        'attributes' => [
            'time_bound' => [
                'en' => 'Time Bound',
                'th' => 'กรอบเวลา',
                'color' => [
                    'short_term' => 'danger',
                    'mid_term' => 'warning',
                    'long_term' => 'success',
                ]
            ],
            'title' => [
                'en' => 'Goal',
                'th' => 'เป้าหมาย',
            ],
            'description' => [
                'en' => 'Inspiration',
                'th' => 'แรงบันดาลใจ',
            ],
            'category' => [
                'en' => 'Category',
                'th' => 'ประเภท',
                'icon' => [
                    'distribution' => 'bx-body',
                    'retirement' => 'bx-money',
                    'donation' => 'bxs-donate-heart',
                    'adoption' => 'bxs-baby-carriage',
                    'medical_care' => 'bx-plus-medical',
                    'parents_treat' => 'bx-body',
                    'children_treat' => 'bx-wallet-alt',
                    'couple_treat' => 'bx-heart',
                    'higher_education' => 'bxs-graduation',
                    'debt_free' => 'bx-credit-card',
                    'wedding' => 'bx-diamond',
                    'honeymoon' => 'bxs-heart',
                    'land' => 'bx-landscape',
                    'condominium' => 'bx-building-house',
                    'house' => 'bx-home-heart',
                    'car' => 'bxs-car',
                    'domestic_traveling' => 'bxs-hotel',
                    'oversea_traveling' => 'bxs-plane-take-off',
                    'immigration' => 'bx-pin',
                    'emergency_fund' => 'bxs-info-circle',
                    'shopping' => 'bxs-purchase-tag',
                    'passive_income' => 'bx-business',
                    'other_financial_goal' => 'bxs-bookmark-star'
                ]
            ],
            'priority' => [
                'en' => 'Priority',
                'th' => 'ลำดับความสำคัญ',
                'color' => [
                    'really_matter' => 'primary',
                    'matter' => 'warning',
                    'not_really_matter' => 'secondary',
                ]
            ],
            'effective_date' => [
                'en' => 'Start Date',
                'th' => 'วันที่เริ่ม'
            ],
            'expected_achievement_date' => [
                'en' => 'Due Date',
                'th' => 'กำหนดเวลา'
            ],
            'expected_duration' => [
                'en' => 'Duration',
                'th' => 'ระยะเวลา'
            ],
            'expected_amount' => [
                'en' => 'Expected Amount',
                'th' => 'จำนวนเงิน'
            ],
            'amount_term' => [
                'en' => 'Amount Term',
                'th' => 'หน่วย'
            ]
        ],
        'en' => [
            'time_bound' => [
                'short_term' => 'Short Term Goal',
                'mid_term' => 'Mid Term Goal',
                'long_term' => 'Long Term Goal'
            ],
            'category' => [
                'distribution' => 'Distribution',
                'retirement' => 'Retirement',
                'donation' => 'Donation',
                'adoption' => 'Adoption',
                'medical_care' => 'Medical Care',
                'parents_treat' => 'Parents Treat',
                'children_treat' => 'Children Treat',
                'couple_treat' => 'Couple Treat',
                'higher_education' => 'Higher Education',
                'debt_free' => 'Debt Free',
                'wedding' => 'Wedding',
                'honeymoon' => 'Honeymoon',
                'land' => 'Land',
                'condominium' => 'Condominium',
                'house' => 'House',
                'car' => 'Car',
                'domestic_traveling' => 'Domestic Traveling',
                'oversea_traveling' => 'Oversea Traveling',
                'immigration' => 'Immigrate',
                'emergency_fund' => 'Emergency Fund',
                'shopping' => 'Shopping',
                'passive_income' => 'Passive Income',
                'other_financial_goal' => 'Other Financial Goal'
            ],
            'category_action' => [
                'distribution' => 'Plan for Distribution',
                'retirement' => 'Plan for Retirement',
                'donation' => 'Do Donation',
                'adoption' => 'Do Adoption',
                'medical_care' => 'Plan for Medical Care',
                'parents_treat' => 'Treat Parents',
                'children_treat' => 'Treat a Child / Children',
                'couple_treat' => 'Treat a Couple',
                'higher_education' => 'Plan for Higher Education',
                'debt_free' => 'Get Out of Debt',
                'wedding' => 'Plan for Wedding',
                'honeymoon' => 'Go Honeymoon',
                'land' => 'Own a Land',
                'condominium' => 'Buy New Condominium',
                'house' => 'Buy New House',
                'car' => 'Buy New Car',
                'domestic_traveling' => 'Travel Domestically',
                'oversea_traveling' => 'Travel Oversea',
                'immigration' => 'Immigrate to Other Country',
                'emergency_fund' => 'Collect Emergency Fund',
                'shopping' => 'Go Shopping',
                'passive_income' => 'Produce Passive Income',
                'other_financial_goal' => 'Plan for Other Financial Goal'
            ],
            'priority' => [
                'really_matter' => 'Really Matter',
                'matter' => 'Matter',
                'not_really_matter' => 'Not Really Matter'
            ],
            'amount_term' => [
                'monthly' => 'Monthly',
                'yearly' => 'Yearly',
                'fixed' => 'Total'
            ]
        ],
        'th' => [
            'time_bound' => [
                'short_term' => 'เป้าหมายระยะสั้น',
                'mid_term' => 'เป้าหมายระยะกลาง',
                'long_term' => 'เป้าหมายระยะยาว'
            ],
            'category' => [
                'distribution' => 'มรดกตกทอด',
                'retirement' => 'การเกษียณอายุ',
                'donation' => 'การให้ / บริจาค',
                'adoption' => 'การอุปการะ',
                'medical_care' => 'การรักษาพยาบาล',
                'parents_treat' => 'การดูแลพ่อ-แม่',
                'children_treat' => 'การดูแลลูก-หลาน',
                'couple_treat' => 'การดูแลแฟน / สามี-ภรรยา / คู่ชีวิต',
                'higher_education' => 'การศึกษาต่อ',
                'debt_free' => 'การปลดหนี้',
                'wedding' => 'การแต่งงาน',
                'honeymoon' => 'การฮันนีมูน',
                'land' => 'ที่ดิน',
                'condominium' => 'คอนโดมิเนียม',
                'house' => 'บ้าน',
                'car' => 'รถยนต์',
                'domestic_traveling' => 'การท่องเที่ยวในประเทศ',
                'oversea_traveling' => 'การท่องเที่ยวต่างประเทศ',
                'immigration' => 'การย้ายถิ่นที่อยู่',
                'emergency_fund' => 'กองทุนเงินสำรองฉุกเฉิน',
                'shopping' => 'การช้อปปิ้ง',
                'passive_income' => 'รายได้จากสินทรัพย์',
                'other_financial_goal' => 'เป้าหมายทางการเงินอื่นๆ'
            ],
            'category_action' => [
                'distribution' => 'วางแผนมรดก',
                'retirement' => 'วางแผนเพื่อการเกษียณอายุ',
                'donation' => 'ให้ / บริจาค',
                'adoption' => 'รับอุปการะ',
                'medical_care' => 'วางแผนเพื่อรักษาพยาบาล',
                'parents_treat' => 'ดูแลพ่อ-แม่',
                'children_treat' => 'ดูแลลูก-หลาน',
                'couple_treat' => 'ดูแลแฟน / คู่สามี-ภรรยา / คู่ชีวิต',
                'higher_education' => 'วางแผนเพื่อการศึกษาต่อ',
                'debt_free' => 'ปลดหนี้',
                'wedding' => 'วางแผนแต่งงาน',
                'honeymoon' => 'วางแผนฮันนีมูน',
                'land' => 'ซื้อที่ดิน',
                'condominium' => 'ซื้อคอนโดมิเนียมใหม่',
                'house' => 'ซื้อบ้านใหม่',
                'car' => 'ซื้อรถคันใหม่',
                'domestic_traveling' => 'ท่องเที่ยวในประเทศ',
                'oversea_traveling' => 'ท่องเที่ยวต่างประเทศ',
                'immigration' => 'ย้ายถิ่นที่อยู่',
                'emergency_fund' => 'เก็บเงินสำรองฉุกเฉิน',
                'shopping' => 'ช้อปปิ้ง',
                'passive_income' => 'สร้างรายได้จากสินทรัพย์',
                'other_financial_goal' => 'วางแผนเพื่อเป้าหมายการเงินอื่นๆ'
            ],
            'priority' => [
                'really_matter' => 'สำคัญอย่างมาก',
                'matter' => 'สำคัญ',
                'not_really_matter' => 'ไม่ค่อยสำคัญ'
            ],
            'amount_term' => [
                'monthly' => 'ต่อเดือน',
                'yearly' => 'ต่อปี',
                'fixed' => 'เงินก้อน'
            ]
        ]
];
