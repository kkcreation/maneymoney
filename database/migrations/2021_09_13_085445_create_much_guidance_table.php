<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMuchGuidanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('much_guidance', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type',['definition','answer','formula','other'])->nullable();
            $table->string('translation_key');
            $table->string('headline');
            $table->text('content');
            $table->string('attachment')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('much_guidance');
    }
}
