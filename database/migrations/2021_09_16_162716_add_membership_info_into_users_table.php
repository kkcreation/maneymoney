<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMembershipInfoIntoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->datetime('membership_expired_at')->after('email_verified_at')->nullable();
            $table->datetime('membership_latest_renewed_at')->after('membership_expired_at')->nullable();
            $table->enum('membership_payment_interval',['none','monthly','half_yearly','yearly','lifetime'])->after('membership_latest_renewed_at')->default('none');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('membership_expired_at');
            $table->dropColumn('membership_latest_renewed_at');
            $table->dropColumn('membership_payment_interval');
        });
    }
}
