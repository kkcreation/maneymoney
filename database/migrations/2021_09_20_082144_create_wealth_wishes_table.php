<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWealthWishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wealth_wishes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->enum('time_bound', ['short_term', 'mid_term', 'long_term']);
            $table->enum('category',
                [
                    'distribution', 'retirement', 'donation', 'adoption', 'medical_care',
                    'parents_treat', 'children_treat', 'couple_treat',
                    'higher_education', 'debt_free', 'wedding', 'honeymoon',
                    'land', 'condominium', 'house', 'car',
                    'domestic_traveling', 'oversea_traveling', 'immigration',
                    'emergency_fund', 'shopping', 'passive_income', 'other_financial_goal'
                ]);
            $table->string('title');
            $table->text('description')->nullable();
            $table->date('expected_achievement_date');
            $table->date('actual_achievement_date')->nullable();
            $table->decimal('expected_amount', 15, 2);
            $table->decimal('actual_amount', 15, 2)->default(0);
            $table->enum('amount_term', ['monthly', 'yearly', 'fixed']);
            $table->decimal('expected_duration', 7, 2);
            $table->decimal('actual_duration', 7, 2)->default(0);
            $table->enum('priority', ['really_matter', 'matter', 'not_really_matter']);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wealth_wishes');
    }
}
