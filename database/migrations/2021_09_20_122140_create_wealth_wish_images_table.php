<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWealthWishImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wealth_wish_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('wealth_wish_id')->index();
            $table->string('name');
            $table->string('path');
            $table->timestamps();
            $table->foreign('wealth_wish_id')->references('id')->on('wealth_wishes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wealth_wish_images');
    }
}
