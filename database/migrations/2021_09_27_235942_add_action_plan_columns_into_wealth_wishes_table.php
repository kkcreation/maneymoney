<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActionPlanColumnsIntoWealthWishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wealth_wishes', function (Blueprint $table) {
            $table->decimal('expected_annual_rate',6)->after('priority')->nullable();
            $table->decimal('present_value',15)->after('expected_annual_rate')->nullable();
            $table->decimal('advanced_payment',15)->after('present_value')->nullable();
            $table->decimal('calculated_payment',15)->after('advanced_payment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wealth_wishes', function (Blueprint $table) {
            $table->dropColumn('expected_annual_rate');
            $table->dropColumn('present_value');
            $table->dropColumn('advanced_payment');
            $table->dropColumn('calculated_payment');
        });
    }
}
