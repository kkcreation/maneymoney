<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedInteger('wealth_wish_id')->nullable();
            $table->unsignedInteger('asset_id')->nullable();
            $table->string('event_title'); // free text (memo)
            $table->string('event_tag'); // specific
            $table->enum('event_action',['buy','sell','deposit','withdraw']);
            $table->enum('currency',['fiat','stable_coin']);
            $table->date('event_start');
            $table->date('event_end');
            $table->decimal('conversion_rate',25,10)->nullable();
            $table->decimal('unit',25,10)->nullable();
            $table->decimal('price',25,10)->default(0);
            $table->decimal('fee',25,10)->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('wealth_wish_id')->references('id')->on('wealth_wishes');
            $table->foreign('asset_id')->references('id')->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_events');
    }
}
