<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorIntoFinancialEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('financial_events', function (Blueprint $table) {
            $table->enum('color', ['bg-danger', 'bg-success', 'bg-primary', 'bg-warning', 'bg-secondary'])->default('bg-secondary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('financial_events', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
