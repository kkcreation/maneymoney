<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebtKillersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debt_killers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedInteger('debt_id')->nullable();
            $table->string('creditor');
            $table->decimal('total_paid',13)->default(0);
            $table->decimal('outstanding_balance',13)->default(0);
            $table->decimal('expected_payment',13)->default(0);
            $table->decimal('minimum_payment',13)->default(0);
            $table->decimal('actual_payment',13)->nullable()->default(0);
            $table->decimal('annual_interest_rate',6)->default(0);
            $table->integer('statement_date');
            $table->integer('due_date');
            $table->date('start_clearance_date');
            $table->date('expected_clearance_date');
            $table->decimal('expected_duration', 7)->default(0);
            $table->decimal('maximum_duration', 7)->default(0);
            $table->decimal('actual_duration', 7)->nullable()->default(0);
            $table->decimal('total_interest_amount',13)->default(0);
            $table->enum('type',['fixed_duration','fixed_payment'])->default('fixed_duration');
            $table->text('notes')->nullable();
            $table->enum('status',['active','inactive','completed'])->default('active');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('debt_id')->references('id')->on('debts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debt_killers');
    }
}
