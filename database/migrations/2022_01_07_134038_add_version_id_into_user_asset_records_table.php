<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVersionIdIntoUserAssetRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_asset_records', function (Blueprint $table) {
            $table->unsignedInteger('version_id')->nullable();
            $table->foreign('version_id')->references('id')->on('financial_statement_versions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_asset_records', function (Blueprint $table) {
            $table->dropColumn('version_id');
            $table->dropForeign('user_asset_records_version_id_foreign');
        });
    }
}
