<?php

namespace Database\Seeders;

use App\Models\Asset;
use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Liquid Asset
        $cash = new Asset();
        $cash->type = 'liquid_asset';
        $cash->name = 'cash';
        $cash->description = 'Cash';
        $cash->description_th = 'เงินสด';
        $cash->save();

        $current = new Asset();
        $current->type = 'liquid_asset';
        $current->name = 'current';
        $current->description = 'Current Deposit';
        $current->description_th = 'เงินฝากบัญชีกระแสรายวัน';
        $current->save();

        $saving = new Asset();
        $saving->type = 'liquid_asset';
        $saving->name = 'savings';
        $saving->description = 'Savings';
        $saving->description_th = 'เงินฝากออมทรัพย์';
        $saving->save();

        $cooperative = new Asset();
        $cooperative->type = 'liquid_asset';
        $cooperative->name = 'cooperative';
        $cooperative->description = 'Cooperative Deposit';
        $cooperative->description_th = 'เงินฝากสหกรณ์';
        $cooperative->save();

        $fixed = new Asset();
        $fixed->type = 'liquid_asset';
        $fixed->name = 'fixed';
        $fixed->description = 'Fixed Deposit';
        $fixed->description_th = 'เงินฝากประจำ';
        $fixed->save();

        $reservedMoney = new Asset();
        $reservedMoney->type = 'liquid_asset';
        $reservedMoney->name = 'reserved_money';
        $reservedMoney->description = 'Reserved Money';
        $reservedMoney->description_th = 'เงินเก็บสำรองฉุกเฉิน';
        $reservedMoney->save();

        $other = new Asset();
        $other->type = 'liquid_asset';
        $other->name = 'other';
        $other->description = 'Other';
        $other->description_th = 'อื่นๆ';
        $other->save();

        // Investment Asset
        $savingLottery = new Asset();
        $savingLottery->type = 'investment_asset';
        $savingLottery->name = 'savings_lottery';
        $savingLottery->description = 'Savings Lottery';
        $savingLottery->description_th = 'สลากออมทรัพย์ (สลากออมสิน / สลาก ธกส.)';
        $savingLottery->save();

        $bond = new Asset();
        $bond->type = 'investment_asset';
        $bond->name = 'bond';
        $bond->description = 'Bond';
        $bond->description_th = 'พันธบัตร';
        $bond->save();

        $debenture = new Asset();
        $debenture->type = 'investment_asset';
        $debenture->name = 'debenture';
        $debenture->description = 'Debenture';
        $debenture->description_th = 'หุ้นกู้';
        $debenture->save();

        $ordinaryStock = new Asset();
        $ordinaryStock->type = 'investment_asset';
        $ordinaryStock->name = 'ordinary_stock';
        $ordinaryStock->description = 'Ordinary Stock';
        $ordinaryStock->description_th = 'หุ้นสามัญ';
        $ordinaryStock->save();

        $preferredStock = new Asset();
        $preferredStock->type = 'investment_asset';
        $preferredStock->name = 'preferred_stock';
        $preferredStock->description = 'Preferred Stock';
        $preferredStock->description_th = 'หุ้นสหกรณ์';
        $preferredStock->save();

        $mutualFund = new Asset();
        $mutualFund->type = 'investment_asset';
        $mutualFund->name = 'mutual_fund';
        $mutualFund->description = 'Mutual Fund (DIY / SSF / LTF / RMF)';
        $mutualFund->description_th = 'กองทุนรวม (DIY / SSF / LTF / RMF)';
        $mutualFund->save();

        $ssf = new Asset();
        $ssf->type = 'investment_asset';
        $ssf->name = 'social_security_fund';
        $ssf->description = 'Social Security Fund (Retirement Pension)';
        $ssf->description_th = 'กองทุนประกันสังคม (บำนาญชราภาพ)';
        $ssf->save();

        $pvd = new Asset();
        $pvd->type = 'investment_asset';
        $pvd->name = 'provident_fund';
        $pvd->description = 'Provident Fund (PVD)';
        $pvd->description_th = 'กองทุนสำรองเลี้ยงชีพ (PVD)';
        $pvd->save();

        $gpf = new Asset();
        $gpf->type = 'investment_asset';
        $gpf->name = 'gov_pension_fund';
        $gpf->description = 'Government Pension Fund (GPF)';
        $gpf->description_th = 'กองทุน กบข.';
        $gpf->save();

        $nsf = new Asset();
        $nsf->type = 'investment_asset';
        $nsf->name = 'national_savings_fund';
        $nsf->description = 'National Savings Fund (NSF)';
        $nsf->description_th = 'กองทุนการออมแห่งชาติ กอช.';
        $nsf->save();

        $annuity = new Asset();
        $annuity->type = 'investment_asset';
        $annuity->name = 'annuity';
        $annuity->description = 'Annuity';
        $annuity->description_th = 'ประกันบำนาญ';
        $annuity->save();

        $insurance = new Asset();
        $insurance->type = 'investment_asset';
        $insurance->name = 'insurance';
        $insurance->description = 'Insurance (Cash Surrender Value)';
        $insurance->description_th = 'กรมธรรม์ประกันชีวิต';
        $insurance->save();

        $commodity = new Asset();
        $commodity->type = 'investment_asset';
        $commodity->name = 'commodity';
        $commodity->description = 'Gold / Silver / etc.';
        $commodity->description_th = 'สินค้าโภคภัณฑ์ (ทอง / เงิน / อื่นๆ)';
        $commodity->save();

        $realEstate = new Asset();
        $realEstate->type = 'investment_asset';
        $realEstate->name = 'real_estate';
        $realEstate->description = 'Real Estate (For Sale / Rent)';
        $realEstate->description_th = 'อสังหาริมทรัพย์ (เพื่อขาย / ให้เช่า)';
        $realEstate->save();

        $cryptocurrency = new Asset();
        $cryptocurrency->type = 'investment_asset';
        $cryptocurrency->name = 'cryptocurrency';
        $cryptocurrency->description = 'Cryptocurrency';
        $cryptocurrency->description_th = 'คริปโตเคอเรนซี่';
        $cryptocurrency->save();

        $otherInvestmentAsset = new Asset();
        $otherInvestmentAsset->type = 'investment_asset';
        $otherInvestmentAsset->name = 'other';
        $otherInvestmentAsset->description = 'Other';
        $otherInvestmentAsset->description_th = 'อื่นๆ';
        $otherInvestmentAsset->save();

        // Personal Asset
        $realEstateForLiving = new Asset();
        $realEstateForLiving->type = 'personal_asset';
        $realEstateForLiving->name = 'living_real_estate';
        $realEstateForLiving->description = 'Real Estate (For Living)';
        $realEstateForLiving->description_th = 'อสังหาริมทรัพย์ (เพื่ออยู่อาศัย)';
        $realEstateForLiving->save();

        $vehicle = new Asset();
        $vehicle->type = 'personal_asset';
        $vehicle->name = 'vehicle';
        $vehicle->description = 'Vehicle (Car / Motorbike / Other)';
        $vehicle->description_th = 'ยานพาหนะ (รถยนต์ / มอเตอร์ไซค์ / อื่นๆ)';
        $vehicle->save();

        $jewel = new Asset();
        $jewel->type = 'personal_asset';
        $jewel->name = 'jewel';
        $jewel->description = 'Jewelry';
        $jewel->description_th = 'เครื่องประดับ';
        $jewel->save();

        $homeAppliances = new Asset();
        $homeAppliances->type = 'personal_asset';
        $homeAppliances->name = 'home_appliances';
        $homeAppliances->description = 'Home Appliances';
        $homeAppliances->description_th = 'เครื่องใช้ในบ้าน';
        $homeAppliances->save();

        $collectibles = new Asset();
        $collectibles->type = 'personal_asset';
        $collectibles->name = 'collectibles';
        $collectibles->description = 'Collectibles (Antique Collectibles / Painting / etc.)';
        $collectibles->description_th = 'ของสะสม (ของเก่า / ภาพวาด / อื่นๆ)';
        $collectibles->save();

        $otherPersonalAsset = new Asset();
        $otherPersonalAsset->type = 'personal_asset';
        $otherPersonalAsset->name = 'other';
        $otherPersonalAsset->description = 'Other';
        $otherPersonalAsset->description_th = 'อื่นๆ';
        $otherPersonalAsset->save();
    }
}
