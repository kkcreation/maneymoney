<?php

namespace Database\Seeders;

use App\Models\Debt;
use Illuminate\Database\Seeder;

class DebtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Short Term Debt
        $informal = new Debt();
        $informal->type = 'short_term_debt';
        $informal->name = 'informal_debt';
        $informal->description = 'Informal Debt';
        $informal->description_th = 'หนี้นอกระบบ';
        $informal->save();

        $creditCard = new Debt();
        $creditCard->type = 'short_term_debt';
        $creditCard->name = 'credit_card';
        $creditCard->description = 'Credit Card Debt';
        $creditCard->description_th = 'หนี้บัตรเครดิต';
        $creditCard->save();

        $installment = new Debt();
        $installment->type = 'short_term_debt';
        $installment->name = 'installment_debt';
        $installment->description = 'Installment Debt';
        $installment->description_th = 'หนี้ผ่อนซื้อสินค้า';
        $installment->save();

        $cashCredit = new Debt();
        $cashCredit->type = 'short_term_debt';
        $cashCredit->name = 'cash_credit_debt';
        $cashCredit->description = 'Cash Credit Debt';
        $cashCredit->description_th = 'หนี้บัตรกดเงินสด';
        $cashCredit->save();

        $asset = new Debt();
        $asset->type = 'short_term_debt';
        $asset->name = 'asset_debt';
        $asset->description = 'Asset Debt';
        $asset->description_th = 'หนี้จากสินทรัพย์';
        $asset->save();

        $other = new Debt();
        $other->type = 'short_term_debt';
        $other->name = 'other';
        $other->description = 'Other Debt';
        $other->description_th = 'หนี้ระยะสั้นอื่นๆ';
        $other->save();

        // Long Term Debt
        $realEstate = new Debt();
        $realEstate->type = 'long_term_debt';
        $realEstate->name = 'real_estate_debt';
        $realEstate->description = 'Real Estate Loan (Land / House / Condo)';
        $realEstate->description_th = 'หนี้จากอสังหาริมทรัพย์ (ที่ดิน / บ้าน / คอนโด)';
        $realEstate->save();

        $vehicle = new Debt();
        $vehicle->type = 'long_term_debt';
        $vehicle->name = 'vehicle_debt';
        $vehicle->description = 'Vehicle Leasing (Car / Motorbike / etc.)';
        $vehicle->description_th = 'หนี้ยานพาหนะ (รถยนต์ / มอเตอร์ไซค์ / อื่นๆ)';
        $vehicle->save();

        $personal = new Debt();
        $personal->type = 'long_term_debt';
        $personal->name = 'personal_loan';
        $personal->description = 'Personal Loan';
        $personal->description_th = 'หนี้สินเชื่อส่วนบุคคล';
        $personal->save();

        $student = new Debt();
        $student->type = 'long_term_debt';
        $student->name = 'student_loan';
        $student->description = 'Student Loan';
        $student->description_th = 'หนี้กู้ยืมเพื่อการศึกษา (กยศ. / กรอ. / อื่นๆ)';
        $student->save();

        $cooperative = new Debt();
        $cooperative->type = 'long_term_debt';
        $cooperative->name = 'cooperative_loan';
        $cooperative->description = 'Cooperative Loan';
        $cooperative->description_th = 'หนี้สหกรณ์';
        $cooperative->save();

        $assetLong = new Debt();
        $assetLong->type = 'long_term_debt';
        $assetLong->name = 'asset_debt';
        $assetLong->description = 'Asset Debt';
        $assetLong->description_th = 'หนี้จากสินทรัพย์';
        $assetLong->save();

        $otherLong = new Debt();
        $otherLong->type = 'long_term_debt';
        $otherLong->name = 'other';
        $otherLong->description = 'Other Debt';
        $otherLong->description_th = 'หนี้ระยะยาวอื่นๆ';
        $otherLong->save();
    }
}
