<?php

namespace Database\Seeders;

use App\Models\Expense;
use Illuminate\Database\Seeder;

class ExpenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Savings
        $ss = new Expense();
        $ss->type = 'savings';
        $ss->name = 'social_security_payment';
        $ss->description = 'Social Security';
        $ss->description_th = 'ประกันสังคม';
        $ss->save();

        $savings = new Expense();
        $savings->type = 'savings';
        $savings->name = 'savings';
        $savings->description = 'Savings';
        $savings->description_th = 'เงินออม';
        $savings->save();

        $reserved = new Expense();
        $reserved->type = 'savings';
        $reserved->name = 'reserved_money';
        $reserved->description = 'Reserved Money';
        $reserved->description_th = 'เงินเก็บสำรองฉุกเฉิน';
        $reserved->save();


        // Investment
        $pvd = new Expense();
        $pvd->type = 'investment';
        $pvd->name = 'provident_fund';
        $pvd->description = 'Provident Fund';
        $pvd->description_th = 'กองทุนสำรองเลี้ยงชีพ';
        $pvd->save();

        $gp = new Expense();
        $gp->type = 'investment';
        $gp->name = 'gov_pension_fund';
        $gp->description = 'Government Pension Fund';
        $gp->description_th = 'กองทุน กบข.';
        $gp->save();

        $ns = new Expense();
        $ns->type = 'investment';
        $ns->name = 'national_savings_fund';
        $ns->description = 'National Savings Fund';
        $ns->description_th = 'กองทุนการออมแห่งชาติ';
        $ns->save();

        $cooperative = new Expense();
        $cooperative->type = 'investment';
        $cooperative->name = 'cooperative';
        $cooperative->description = 'Savings and Credit Cooperative';
        $cooperative->description_th = 'สหกรณ์ออมทรัพย์';
        $cooperative->save();

        $annuity = new Expense();
        $annuity->type = 'investment';
        $annuity->name = 'annuity';
        $annuity->description = 'Annuity';
        $annuity->description_th = 'ประกันบำนาญ';
        $annuity->save();

        $mutualFund = new Expense();
        $mutualFund->type = 'investment';
        $mutualFund->name = 'mutual_fund';
        $mutualFund->description = 'Mutual Fund';
        $mutualFund->description_th = 'กองทุนรวม';
        $mutualFund->save();

        $bond = new Expense();
        $bond->type = 'investment';
        $bond->name = 'bond';
        $bond->description = 'Bond';
        $bond->description_th = 'พันธบัตร / ตราสารหนี้';
        $bond->save();

        $stock = new Expense();
        $stock->type = 'investment';
        $stock->name = 'stock';
        $stock->description = 'Stock';
        $stock->description_th = 'ตราสารทุน (หุ้น)';
        $stock->save();

        $gold = new Expense();
        $gold->type = 'investment';
        $gold->name = 'gold';
        $gold->description = 'Gold';
        $gold->description_th = 'ทองคำ';
        $gold->save();

        $crypto = new Expense();
        $crypto->type = 'investment';
        $crypto->name = 'cryptocurrency';
        $crypto->description = 'Cryptocurrency';
        $crypto->description_th = 'คริปโตเคอเรนซี่';
        $crypto->save();

        $business = new Expense();
        $business->type = 'investment';
        $business->name = 'business';
        $business->description = 'Business';
        $business->description_th = 'ธุรกิจ';
        $business->save();

        $other = new Expense();
        $other->type = 'investment';
        $other->name = 'other';
        $other->description = 'Other';
        $other->description_th = 'อื่นๆ';
        $other->save();

        // Fixed Expense
        $realEstateInstallment = new Expense();
        $realEstateInstallment->type = 'fixed_expense';
        $realEstateInstallment->name = 'real_estate_installment';
        $realEstateInstallment->description = 'Real Estate Installment (House / Condo for living)';
        $realEstateInstallment->description_th = 'ค่าผ่อนบ้าน / คอนโด เพื่ออยู่อาศัย';
        $realEstateInstallment->save();

        $realEstateRental = new Expense();
        $realEstateRental->type = 'fixed_expense';
        $realEstateRental->name = 'real_estate_rental';
        $realEstateRental->description = 'Real Estate Rental (House / Condo for living)';
        $realEstateRental->description_th = 'ค่าเช่าบ้าน / คอนโด เพื่ออยู่อาศัย';
        $realEstateRental->save();

        $vehicleInstallment = new Expense();
        $vehicleInstallment->type = 'fixed_expense';
        $vehicleInstallment->name = 'vehicle_installment';
        $vehicleInstallment->description = 'Vehicle Installment (Car / Motorbike / etc.)';
        $vehicleInstallment->description_th = 'ค่าผ่อนรถ / มอเตอร์ไซค์ / ยานพาหนะอื่นๆ';
        $vehicleInstallment->save();

        $productInstallment = new Expense();
        $productInstallment->type = 'fixed_expense';
        $productInstallment->name = 'product_installment';
        $productInstallment->description = 'Product / Service Installment';
        $productInstallment->description_th = 'ค่าผ่อนสินค้า / บริการ';
        $productInstallment->save();

        $informalDebtInstallment = new Expense();
        $informalDebtInstallment->type = 'fixed_expense';
        $informalDebtInstallment->name = 'informal_debt_installment';
        $informalDebtInstallment->description = 'Informal Debt Installment';
        $informalDebtInstallment->description_th = 'ค่าผ่อนหนี้นอกระบบ';
        $informalDebtInstallment->save();

        $personalLoanInstallment = new Expense();
        $personalLoanInstallment->type = 'fixed_expense';
        $personalLoanInstallment->name = 'personal_loan_installment';
        $personalLoanInstallment->description = 'Personal Loan Installment';
        $personalLoanInstallment->description_th = 'ค่าผ่อนสินเชื่อส่วนบุคคล';
        $personalLoanInstallment->save();

        $studentLoanInstallment = new Expense();
        $studentLoanInstallment->type = 'fixed_expense';
        $studentLoanInstallment->name = 'student_loan_installment';
        $studentLoanInstallment->description = 'Student Loan Installment';
        $studentLoanInstallment->description_th = 'ค่าผ่อนหนี้กู้ยืมเพื่อการศึกษา (กยศ. / กรอ. / อื่นๆ)';
        $studentLoanInstallment->save();

        $cooperativeLoanInstallment = new Expense();
        $cooperativeLoanInstallment->type = 'fixed_expense';
        $cooperativeLoanInstallment->name = 'cooperative_loan_installment';
        $cooperativeLoanInstallment->description = 'Cooperative Loan Installment';
        $cooperativeLoanInstallment->description_th = 'ค่าผ่อนหนี้สหกรณ์';
        $cooperativeLoanInstallment->save();

        $insurancePremium = new Expense();
        $insurancePremium->type = 'fixed_expense';
        $insurancePremium->name = 'insurance_premium';
        $insurancePremium->description = 'Insurance Premium (Monthly)';
        $insurancePremium->description_th = 'ค่าเบี้ยประกัน';
        $insurancePremium->save();

        $otherFixedExpense = new Expense();
        $otherFixedExpense->type = 'fixed_expense';
        $otherFixedExpense->name = 'other';
        $otherFixedExpense->description = 'Other';
        $otherFixedExpense->description_th = 'อื่นๆ';
        $otherFixedExpense->save();

        // Non-fixed Expense
        $food = new Expense();
        $food->type = 'non_fixed_expense';
        $food->name = 'food_drink';
        $food->description = 'Food & Drink';
        $food->description_th = 'อาหารและเครื่องดื่ม';
        $food->save();

        $utilities = new Expense();
        $utilities->type = 'non_fixed_expense';
        $utilities->name = 'bill_utilities';
        $utilities->description = 'Bill & Utilities (Electricity / Water / Internet / Phone / Gas / etc.)';
        $utilities->description_th = 'สาธารณูปโภค (ไฟฟ้า / น้ำประปา / อินเตอร์เน็ต / โทรศัพท์ / แก๊ส / อื่นๆ)';
        $utilities->save();

        $travel = new Expense();
        $travel->type = 'non_fixed_expense';
        $travel->name = 'travel';
        $travel->description = 'Travel (Gasoline / Vehicle Maintenance / Public Transportation / Parking Fee / Toll Fee / Car Care / etc.)';
        $travel->description_th = 'ค่าเดินทาง (น้ำมัน / บำรุงรักษารถ / รถสาธารณะ / ที่จอดรถ / ทางด่วน / ล้างรถ / อื่นๆ)';
        $travel->save();

        $education = new Expense();
        $education->type = 'non_fixed_expense';
        $education->name = 'education';
        $education->description = 'Education (Course Fee / Book / etc.)';
        $education->description_th = 'การศึกษา (คอร์สเรียน / หนังสือ / อื่นๆ)';
        $education->save();

        $entertainment = new Expense();
        $entertainment->type = 'non_fixed_expense';
        $entertainment->name = 'entertainment';
        $entertainment->description = 'Entertainment (Movie / Games / etc.)';
        $entertainment->description_th = 'นันทนาการ (ดูหนัง / เกมส์ / อื่นๆ)';
        $entertainment->save();

        $giving = new Expense();
        $giving->type = 'non_fixed_expense';
        $giving->name = 'giving_donation';
        $giving->description = 'Giving & Donation (Charity / Wedding / Funeral / etc.)';
        $giving->description_th = 'การให้ & บริจาค (การกุศล / เงินใส่ซองช่วยงาน / อื่นๆ)';
        $giving->save();

        $wellness = new Expense();
        $wellness->type = 'non_fixed_expense';
        $wellness->name = 'wellness';
        $wellness->description = 'Health & Fitness (Supplement / Sport / Spa / Medical / etc.)';
        $wellness->description_th = 'สุขภาพ & ฟิตเนส (ผลิตภัณฑ์เสริมอาหาร / กีฬา / สปา / การรักษาพยาบาล / อื่นๆ)';
        $wellness->save();

        $family = new Expense();
        $family->type = 'non_fixed_expense';
        $family->name = 'family';
        $family->description = 'Family (Parents / House Maintenance / Baby & Children / Pet / Home Services)';
        $family->description_th = 'ครอบครัว (พ่อ-แม่ / ปรับปรุงบ้าน / เด็ก & ทารก / สัตว์เลี้ยง / โฮมเซอร์วิส)';
        $family->save();

        $fee = new Expense();
        $fee->type = 'non_fixed_expense';
        $fee->name = 'fee';
        $fee->description = 'Fee & Service Charge';
        $fee->description_th = 'ค่าธรรมเนียม & ค่าบริการ';
        $fee->save();

        $subscription = new Expense();
        $subscription->type = 'non_fixed_expense';
        $subscription->name = 'shopping';
        $subscription->description = 'Shopping (Personal Items / etc.)';
        $subscription->description_th = 'ช้อปปิ้ง (ของใช้ส่วนตัว / อื่นๆ)';
        $subscription->save();

        $friend = new Expense();
        $friend->type = 'non_fixed_expense';
        $friend->name = 'friend_couple';
        $friend->description = 'Friend & Couple';
        $friend->description_th = 'เพื่อน & คนรัก';
        $friend->save();

        $traveling = new Expense();
        $traveling->type = 'non_fixed_expense';
        $traveling->name = 'traveling';
        $traveling->description = 'Traveling';
        $traveling->description_th = 'การท่องเที่ยว';
        $traveling->save();

        $otherNonFixedExpense = new Expense();
        $otherNonFixedExpense->type = 'non_fixed_expense';
        $otherNonFixedExpense->name = 'other';
        $otherNonFixedExpense->description = 'Other';
        $otherNonFixedExpense->description_th = 'อื่นๆ';
        $otherNonFixedExpense->save();
    }
}
