<?php

namespace Database\Seeders;

use App\Models\FinancialStatementVersion;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class FinancialStatementVersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user) {
            if ($user->id != 1) {
                $newVersion = new FinancialStatementVersion();
                $newVersion->user_id = $user->id;
                $newVersion->submission_date = Carbon::now()->format('Y-m') . '-01';
                $newVersion->save();

                foreach ($user->userIncome as $item) {
                    $item->version_id = $newVersion->id;
                    $item->save();
                }

                foreach ($user->userExpenses as $item) {
                    $item->version_id = $newVersion->id;
                    $item->save();
                }

                foreach ($user->userAssets as $item) {
                    $item->version_id = $newVersion->id;
                    $item->save();
                }

                foreach ($user->userDebts as $item) {
                    $item->version_id = $newVersion->id;
                    $item->save();
                }
            }
        }
    }
}
