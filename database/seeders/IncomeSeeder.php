<?php

namespace Database\Seeders;

use App\Models\Income;
use Illuminate\Database\Seeder;

class IncomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Active Income
        $salary = new Income();
        $salary->type = 'active_income';
        $salary->name = 'salary';
        $salary->description = 'Salary';
        $salary->description_th = 'เงินเดือน';
        $salary->save();

        $wage = new Income();
        $wage->type = 'active_income';
        $wage->name = 'wage';
        $wage->description = 'Wage';
        $wage->description_th = 'ค่าจ้าง';
        $wage->save();

        $sale = new Income();
        $sale->type = 'active_income';
        $sale->name = 'selling_profit';
        $sale->description = 'Product / Service Profit';
        $sale->description_th = 'กำไรจากการขายสินค้า / บริการ';
        $sale->save();

        $trading = new Income();
        $trading->type = 'active_income';
        $trading->name = 'trading_profit';
        $trading->description = 'Trading Profit';
        $trading->description_th = 'กำไรจากการเทรด';
        $trading->save();

        $overTime = new Income();
        $overTime->type = 'active_income';
        $overTime->name = 'over_time';
        $overTime->description = 'Over Time (OT)';
        $overTime->description_th = 'ค่าล่วงเวลา';
        $overTime->save();

        $bonus = new Income();
        $bonus->type = 'active_income';
        $bonus->name = 'bonus';
        $bonus->description = 'Bonus';
        $bonus->description_th = 'โบนัส';
        $bonus->save();

        $commission = new Income();
        $commission->type = 'active_income';
        $commission->name = 'commission';
        $commission->description = 'Commission';
        $commission->description_th = 'คอมมิชชัน';
        $commission->save();

        $other = new Income();
        $other->type = 'active_income';
        $other->name = 'other';
        $other->description = 'Other';
        $other->description_th = 'อื่นๆ';
        $other->save();

        // Passive Income
        $interest = new Income();
        $interest->type = 'passive_income';
        $interest->name = 'interest';
        $interest->description = 'Interest';
        $interest->description_th = 'ดอกเบี้ย';
        $interest->save();

        $dividend = new Income();
        $dividend->type = 'passive_income';
        $dividend->name = 'dividend';
        $dividend->description = 'Dividend';
        $dividend->description_th = 'เงินปันผล';
        $dividend->save();

        $rentalFee = new Income();
        $rentalFee->type = 'passive_income';
        $rentalFee->name = 'rental_fee';
        $rentalFee->description = 'Rental Fee';
        $rentalFee->description_th = 'ค่าเช่า';
        $rentalFee->save();

        $business = new Income();
        $business->type = 'passive_income';
        $business->name = 'business_profit';
        $business->description = 'Business Profit';
        $business->description_th = 'กำไรจากธุรกิจ';
        $business->save();

        $capitalGain = new Income();
        $capitalGain->type = 'passive_income';
        $capitalGain->name = 'capital_gain';
        $capitalGain->description = 'Capital Gain';
        $capitalGain->description_th = 'กำไรส่วนต่างราคา';
        $capitalGain->save();

        $otherPassive = new Income();
        $otherPassive->type = 'passive_income';
        $otherPassive->name = 'other';
        $otherPassive->description = 'Other';
        $otherPassive->description_th = 'อื่นๆ';
        $otherPassive->save();
    }
}
