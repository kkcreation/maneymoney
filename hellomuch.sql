-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: minia_laravel
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `assets` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('liquid_asset','investment_asset','personal_asset') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_th` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (1,'liquid_asset','cash','Cash','เงินสด','2021-08-13 13:21:45','2021-08-13 13:21:45'),(2,'liquid_asset','current','Current Deposit','เงินฝากบัญชีกระแสรายวัน','2021-08-13 13:21:45','2021-08-13 13:21:45'),(3,'liquid_asset','savings','Savings','เงินฝากออมทรัพย์','2021-08-13 13:21:45','2021-08-13 13:21:45'),(4,'liquid_asset','cooperative','Cooperative Deposit','เงินฝากสหกรณ์','2021-08-13 13:21:45','2021-08-13 13:21:45'),(5,'liquid_asset','fixed','Fixed Deposit','เงินฝากประจำ','2021-08-13 13:21:45','2021-08-13 13:21:45'),(6,'liquid_asset','reserved_money','Reserved Money','เงินเก็บสำรองฉุกเฉิน','2021-08-13 13:21:45','2021-08-13 13:21:45'),(7,'liquid_asset','other','Other','อื่นๆ','2021-08-13 13:21:45','2021-08-13 13:21:45'),(8,'investment_asset','savings_lottery','Savings Lottery','สลากออมทรัพย์ (สลากออมสิน / สลาก ธกส.)','2021-08-13 13:21:45','2021-08-13 13:21:45'),(9,'investment_asset','bond','Bond','พันธบัตร','2021-08-13 13:21:45','2021-08-13 13:21:45'),(10,'investment_asset','debenture','Debenture','หุ้นกู้','2021-08-13 13:21:45','2021-08-13 13:21:45'),(11,'investment_asset','ordinary_stock','Ordinary Stock','หุ้นสามัญ','2021-08-13 13:21:45','2021-08-13 13:21:45'),(12,'investment_asset','preferred_stock','Preferred Stock','หุ้นสหกรณ์','2021-08-13 13:21:45','2021-08-13 13:21:45'),(13,'investment_asset','mutual_fund','Mutual Fund (DIY / SSF / LTF / RMF)','กองทุนรวม (DIY / SSF / LTF / RMF)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(14,'investment_asset','social_security_fund','Social Security Fund (Retirement Pension)','กองทุนประกันสังคม (บำนาญชราภาพ)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(15,'investment_asset','provident_fund','Provident Fund (PVD)','กองทุนสำรองเลี้ยงชีพ (PVD)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(16,'investment_asset','gov_pension_fund','Government Pension Fund (GPF)','กองทุน กบข.','2021-08-13 13:21:46','2021-08-13 13:21:46'),(17,'investment_asset','national_savings_fund','National Savings Fund (NSF)','กองทุนการออมแห่งชาติ กอช.','2021-08-13 13:21:46','2021-08-13 13:21:46'),(18,'investment_asset','annuity','Annuity','ประกันบำนาญ','2021-08-13 13:21:46','2021-08-13 13:21:46'),(19,'investment_asset','insurance','Insurance (Cash Surrender Value)','กรมธรรม์ประกันชีวิต (มูลค่าเวนคืน)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(20,'investment_asset','commodity','Gold / Silver / etc.','สินค้าโภคภัณฑ์ (ทอง / เงิน / อื่นๆ)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(21,'investment_asset','real_estate','Real Estate (For Sale / Rent)','อสังหาริมทรัพย์ (เพื่อขาย / ให้เช่า)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(22,'investment_asset','cryptocurrency','Cryptocurrency','คริปโตเคอเรนซี่','2021-08-13 13:21:46','2021-08-13 13:21:46'),(23,'investment_asset','other','Other','อื่นๆ','2021-08-13 13:21:46','2021-08-13 13:21:46'),(24,'personal_asset','living_real_estate','Real Estate (For Living)','อสังหาริมทรัพย์ (เพื่ออยู่อาศัย)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(25,'personal_asset','vehicle','Vehicle (Car / Motorbike / Other)','ยานพาหนะ (รถยนต์ / มอเตอร์ไซค์ / อื่นๆ)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(26,'personal_asset','jewel','Jewelry','เครื่องประดับ','2021-08-13 13:21:46','2021-08-13 13:21:46'),(27,'personal_asset','home_appliances','Home Appliances','เครื่องใช้ในบ้าน','2021-08-13 13:21:46','2021-08-13 13:21:46'),(28,'personal_asset','collectibles','Collectibles (Antique Collectibles / Painting / etc.)','ของสะสม (ของเก่า / ภาพวาด / อื่นๆ)','2021-08-13 13:21:46','2021-08-13 13:21:46'),(29,'personal_asset','other','Other','อื่นๆ','2021-08-13 13:21:46','2021-08-13 13:21:46');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debts`
--

DROP TABLE IF EXISTS `debts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `debts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('short_term_debt','long_term_debt') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_th` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debts`
--

LOCK TABLES `debts` WRITE;
/*!40000 ALTER TABLE `debts` DISABLE KEYS */;
INSERT INTO `debts` VALUES (1,'short_term_debt','informal_debt','Informal Debt','หนี้นอกระบบ','2021-08-13 13:37:10','2021-08-13 13:37:10'),(2,'short_term_debt','credit_card','Credit Card Debt','หนี้บัตรเครดิต','2021-08-13 13:37:11','2021-08-13 13:37:11'),(3,'short_term_debt','installment_debt','Installment Debt','หนี้ผ่อนซื้อสินค้า','2021-08-13 13:37:11','2021-08-13 13:37:11'),(4,'short_term_debt','cash_credit_debt','Cash Credit Debt','หนี้บัตรกดเงินสด','2021-08-13 13:37:11','2021-08-13 13:37:11'),(5,'short_term_debt','asset_debt','Asset Debt','หนี้จากสินทรัพย์','2021-08-13 13:37:11','2021-08-13 13:37:11'),(6,'short_term_debt','other','Other Debt','หนี้ระยะสั้นอื่นๆ','2021-08-13 13:37:11','2021-08-13 13:37:11'),(7,'long_term_debt','real_estate_debt','Real Estate Loan (Land / House / Condo)','หนี้จากอสังหาริมทรัพย์ (ที่ดิน / บ้าน / คอนโด)','2021-08-13 13:37:11','2021-08-13 13:37:11'),(8,'long_term_debt','vehicle_debt','Vehicle Leasing (Car / Motorbike / etc.)','หนี้ยานพาหนะ (รถยนต์ / มอเตอร์ไซค์ / อื่นๆ)','2021-08-13 13:37:11','2021-08-13 13:37:11'),(9,'long_term_debt','personal_loan','Personal Loan','หนี้สินเชื่อส่วนบุคคล','2021-08-13 13:37:11','2021-08-13 13:37:11'),(10,'long_term_debt','student_loan','Student Loan','หนี้กู้ยืมเพื่อการศึกษา (กยศ. / กรอ. / อื่นๆ)','2021-08-13 13:37:11','2021-08-13 13:37:11'),(11,'long_term_debt','cooperative_loan','Cooperative Loan','หนี้สหกรณ์','2021-08-13 13:37:11','2021-08-13 13:37:11'),(12,'long_term_debt','asset_debt','Asset Debt','หนี้จากสินทรัพย์','2021-08-13 13:37:11','2021-08-13 13:37:11'),(13,'long_term_debt','other','Other Debt','หนี้ระยะยาวอื่นๆ','2021-08-13 13:37:11','2021-08-13 13:37:11');
/*!40000 ALTER TABLE `debts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `expenses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('fixed_expense','non_fixed_expense','savings','investment') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_th` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenses`
--

LOCK TABLES `expenses` WRITE;
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
INSERT INTO `expenses` VALUES (1,'savings','social_security_payment','Social Security','ประกันสังคม','2021-08-14 08:02:44','2021-08-14 08:02:44'),(2,'savings','savings','Savings','เงินออม','2021-08-14 08:02:44','2021-08-14 08:02:44'),(3,'savings','reserved_money','Reserved Money','เงินเก็บสำรองฉุกเฉิน','2021-08-14 08:02:44','2021-08-14 08:02:44'),(4,'investment','provident_fund','Provident Fund','กองทุนสำรองเลี้ยงชีพ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(5,'investment','gov_pension_fund','Government Pension Fund','กองทุน กบข.','2021-08-14 08:02:44','2021-08-14 08:02:44'),(6,'investment','national_savings_fund','National Savings Fund','กองทุนการออมแห่งชาติ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(7,'investment','cooperative','Savings and Credit Cooperative','สหกรณ์ออมทรัพย์','2021-08-14 08:02:44','2021-08-14 08:02:44'),(8,'investment','annuity','Annuity','ประกันบำนาญ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(9,'investment','mutual_fund','Mutual Fund','กองทุนรวม','2021-08-14 08:02:44','2021-08-14 08:02:44'),(10,'investment','bond','Bond','พันธบัตร / ตราสารหนี้','2021-08-14 08:02:44','2021-08-14 08:02:44'),(11,'investment','stock','Stock','ตราสารทุน (หุ้น)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(12,'investment','gold','Gold','ทองคำ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(13,'investment','cryptocurrency','Cryptocurrency','คริปโตเคอเรนซี่','2021-08-14 08:02:44','2021-08-14 08:02:44'),(14,'investment','business','Business','ธุรกิจ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(15,'investment','other','Other','อื่นๆ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(16,'fixed_expense','real_estate_installment','Real Estate Installment (House / Condo for living)','ค่าผ่อนบ้าน / คอนโด เพื่ออยู่อาศัย','2021-08-14 08:02:44','2021-08-14 08:02:44'),(17,'fixed_expense','real_estate_rental','Real Estate Rental (House / Condo for living)','ค่าเช่าบ้าน / คอนโด เพื่ออยู่อาศัย','2021-08-14 08:02:44','2021-08-14 08:02:44'),(18,'fixed_expense','vehicle_installment','Vehicle Installment (Car / Motorbike / etc.)','ค่าผ่อนรถ / มอเตอร์ไซค์ / ยานพาหนะอื่นๆ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(19,'fixed_expense','product_installment','Product / Service Installment','ค่าผ่อนสินค้า / บริการ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(20,'fixed_expense','informal_debt_installment','Informal Debt Installment','ค่าผ่อนหนี้นอกระบบ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(21,'fixed_expense','personal_loan_installment','Personal Loan Installment','ค่าผ่อนสินเชื่อส่วนบุคคล','2021-08-14 08:02:44','2021-08-14 08:02:44'),(22,'fixed_expense','student_loan_installment','Student Loan Installment','ค่าผ่อนหนี้กู้ยืมเพื่อการศึกษา (กยศ. / กรอ. / อื่นๆ)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(23,'fixed_expense','cooperative_loan_installment','Cooperative Loan Installment','ค่าผ่อนหนี้สหกรณ์','2021-08-14 08:02:44','2021-08-14 08:02:44'),(24,'fixed_expense','insurance_premium','Insurance Premium (Monthly)','ค่าเบี้ยประกัน','2021-08-14 08:02:44','2021-08-14 08:02:44'),(25,'fixed_expense','other','Other','อื่นๆ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(26,'non_fixed_expense','food_drink','Food & Drink','อาหาร','2021-08-14 08:02:44','2021-08-14 08:02:44'),(27,'non_fixed_expense','bill_utilities','Bill & Utilities (Electricity / Water / Internet / Phone / Gas / etc.)','สาธารณูปโภค (ไฟฟ้า / น้ำประปา / อินเตอร์เน็ต / โทรศัพท์ / แก๊ส / อื่นๆ)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(28,'non_fixed_expense','travel','Travel (Gasoline / Vehicle Maintenance / Public Transportation / Parking Fee / Toll Fee / Car Care / etc.)','ค่าเดินทาง (น้ำมัน / บำรุงรักษารถ / รถสาธารณะ / ที่จอดรถ / ทางด่วน / ล้างรถ / อื่นๆ)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(29,'non_fixed_expense','education','Education (Course Fee / Book / etc.)','การศึกษา (คอร์สเรียน / หนังสือ / อื่นๆ)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(30,'non_fixed_expense','entertainment','Entertainment (Movie / Games / etc.)','นันทนาการ (ดูหนัง / เกมส์ / อื่นๆ)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(31,'non_fixed_expense','giving_donation','Giving & Donation (Charity / Wedding / Funeral / etc.)','การให้ & บริจาค (การกุศล / เงินใส่ซองช่วยงาน / อื่นๆ )','2021-08-14 08:02:44','2021-08-14 08:02:44'),(32,'non_fixed_expense','wellness','Health & Fitness (Supplement / Sport / Spa / Medical / etc.)','สุขภาพ & ฟิตเนส (ผลิตภัณฑ์เสริมอาหาร / กีฬา / สปา / การรักษาพยาบาล / อื่นๆ)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(33,'non_fixed_expense','family','Family (Parents / House Maintenance / Baby & Children / Pet / Home Services)','ครอบครัว (พ่อ-แม่ / ปรับปรุงบ้าน / เด็ก & ทารก / สัตว์เลี้ยง / โฮมเซอร์วิส)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(34,'non_fixed_expense','fee','Fee & Service Charge','ค่าธรรมเนียม & ค่าบริการ','2021-08-14 08:02:44','2021-08-14 08:02:44'),(35,'non_fixed_expense','shopping','Shopping (Personal Items / etc.)','ช้อปปิ้ง (ของใช้ส่วนตัว / อื่นๆ)','2021-08-14 08:02:44','2021-08-14 08:02:44'),(36,'non_fixed_expense','friend_couple','Friend & Couple','เพื่อน & คนรัก','2021-08-14 08:02:44','2021-08-14 08:02:44'),(37,'non_fixed_expense','traveling','Traveling','การท่องเที่ยว','2021-08-14 08:02:44','2021-08-14 08:02:44'),(38,'non_fixed_expense','other','Other','อื่นๆ','2021-08-14 08:02:44','2021-08-14 08:02:44');
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income`
--

DROP TABLE IF EXISTS `income`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `income` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('passive_income','active_income') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_th` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income`
--

LOCK TABLES `income` WRITE;
/*!40000 ALTER TABLE `income` DISABLE KEYS */;
INSERT INTO `income` VALUES (1,'active_income','salary','Salary','เงินเดือน','2021-08-13 14:10:54','2021-08-13 14:10:54'),(2,'active_income','wage','Wage','ค่าจ้าง','2021-08-13 14:10:54','2021-08-13 14:10:54'),(3,'active_income','selling_profit','Product / Service Profit','กำไรจากการขายสินค้า / บริการ','2021-08-13 14:10:54','2021-08-13 14:10:54'),(4,'active_income','trading_profit','Trading Profit','กำไรจากการเทรด','2021-08-13 14:10:54','2021-08-13 14:10:54'),(5,'active_income','over_time','Over Time (OT)','ค่าล่วงเวลา','2021-08-13 14:10:54','2021-08-13 14:10:54'),(6,'active_income','bonus','Bonus','โบนัส','2021-08-13 14:10:54','2021-08-13 14:10:54'),(7,'active_income','commission','Commission','คอมมิชชัน','2021-08-13 14:10:54','2021-08-13 14:10:54'),(8,'active_income','other','Other','อื่นๆ','2021-08-13 14:10:54','2021-08-13 14:10:54'),(9,'passive_income','interest','Interest','ดอกเบี้ย','2021-08-13 14:10:54','2021-08-13 14:10:54'),(10,'passive_income','dividend','Dividend','เงินปันผล','2021-08-13 14:10:54','2021-08-13 14:10:54'),(11,'passive_income','rental_fee','Rental Fee','ค่าเช่า','2021-08-13 14:10:54','2021-08-13 14:10:54'),(12,'passive_income','business_profit','Business Profit','กำไรจากธุรกิจ','2021-08-13 14:10:54','2021-08-13 14:10:54'),(13,'passive_income','capital_gain','Capital Gain','กำไรส่วนต่างราคา','2021-08-13 14:10:54','2021-08-13 14:10:54'),(14,'passive_income','other','Other','อื่นๆ','2021-08-13 14:10:54','2021-08-13 14:10:54');
/*!40000 ALTER TABLE `income` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2021_08_12_052646_add_dob_into_users_table',2),(6,'2021_08_12_055724_add_avatar_into_users_table',2),(11,'2021_08_13_101846_create_assets_table',3),(12,'2021_08_13_101930_create_debts_table',3),(13,'2021_08_13_101958_create_income_table',3),(14,'2021_08_13_102018_create_expenses_table',3),(15,'2021_08_13_105215_create_user_asset_records_table',4),(16,'2021_08_13_105234_create_user_debt_records_table',4),(17,'2021_08_13_112129_add_gender_into_users_table',5),(18,'2021_08_13_112226_add_marital_status_into_users_table',5),(19,'2021_08_13_125933_add_key_into_user_asset_records_table',6),(20,'2021_08_13_125943_add_key_into_user_debt_records_table',6),(21,'2021_08_13_134242_create_user_income_records_table',7),(22,'2021_08_13_134333_create_user_expense_records_table',7),(23,'2021_08_15_083415_add_full_name_into_users_table',8),(24,'2021_08_25_075828_add_description_th_into_income_table',9),(25,'2021_08_25_080054_add_description_th_into_expenses_table',9),(26,'2021_08_25_080113_add_description_th_into_assets_table',9),(27,'2021_08_25_080129_add_description_th_into_debts_table',9);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_asset_records`
--

DROP TABLE IF EXISTS `user_asset_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_asset_records` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `asset_id` int unsigned NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_asset_records_user_id_index` (`user_id`),
  KEY `user_asset_records_asset_id_index` (`asset_id`),
  CONSTRAINT `user_asset_records_asset_id_foreign` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`),
  CONSTRAINT `user_asset_records_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_asset_records`
--

LOCK TABLES `user_asset_records` WRITE;
/*!40000 ALTER TABLE `user_asset_records` DISABLE KEYS */;
INSERT INTO `user_asset_records` VALUES (1,1,1,0.00,'liquid_asset_cash','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(2,1,2,0.00,'liquid_asset_current','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(3,1,3,0.00,'liquid_asset_savings','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(4,1,4,0.00,'liquid_asset_cooperative','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(5,1,5,0.00,'liquid_asset_fixed','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(6,1,6,300000.00,'liquid_asset_reserved_money','2021-08-23 11:17:25','2021-08-27 08:57:44',NULL),(7,1,7,0.00,'liquid_asset_other','2021-08-23 11:17:25','2021-08-24 17:20:07',NULL),(8,1,8,0.00,'investment_asset_savings_lottery','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(9,1,9,0.00,'investment_asset_bond','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(10,1,10,0.00,'investment_asset_debenture','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(11,1,11,0.00,'investment_asset_ordinary_stock','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(12,1,12,0.00,'investment_asset_preferred_stock','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(13,1,13,19000.00,'investment_asset_mutual_fund','2021-08-23 11:17:25','2021-08-26 10:44:51',NULL),(14,1,14,50422.80,'investment_asset_social_security_fund','2021-08-23 11:17:25','2021-08-24 17:20:07',NULL),(15,1,15,5400.00,'investment_asset_provident_fund','2021-08-23 11:17:25','2021-08-24 02:33:51',NULL),(16,1,16,0.00,'investment_asset_gov_pension_fund','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(17,1,17,0.00,'investment_asset_national_savings_fund','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(18,1,18,44134.48,'investment_asset_annuity','2021-08-23 11:17:25','2021-08-24 17:20:07',NULL),(19,1,19,38020.00,'investment_asset_insurance','2021-08-23 11:17:25','2021-08-24 17:20:07',NULL),(20,1,20,1000.00,'investment_asset_commodity','2021-08-23 11:17:25','2021-08-24 02:43:30',NULL),(21,1,21,0.00,'investment_asset_real_estate','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(22,1,22,0.00,'investment_asset_cryptocurrency','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(23,1,23,0.00,'investment_asset_other','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(24,1,24,5500000.00,'personal_asset_living_real_estate','2021-08-23 11:17:25','2021-08-24 17:20:07',NULL),(25,1,25,350000.00,'personal_asset_vehicle','2021-08-23 11:17:25','2021-08-24 02:33:51',NULL),(26,1,26,0.00,'personal_asset_jewel','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(27,1,27,300000.00,'personal_asset_home_appliances','2021-08-23 11:17:25','2021-08-24 17:20:07',NULL),(28,1,28,0.00,'personal_asset_collectibles','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(29,1,29,0.00,'personal_asset_other','2021-08-23 11:17:25','2021-08-23 11:17:25',NULL),(30,2,1,0.00,'liquid_asset_cash','2021-08-23 11:18:05','2021-08-23 11:18:05',NULL),(31,2,2,0.00,'liquid_asset_current','2021-08-23 11:18:05','2021-08-23 11:18:05',NULL),(32,2,3,0.00,'liquid_asset_savings','2021-08-23 11:18:05','2021-08-23 11:18:05',NULL),(33,2,4,0.00,'liquid_asset_cooperative','2021-08-23 11:18:05','2021-08-23 11:18:05',NULL),(34,2,5,0.00,'liquid_asset_fixed','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(35,2,6,0.00,'liquid_asset_other','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(36,2,7,0.00,'liquid_asset_reserved_money','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(37,2,8,0.00,'investment_asset_savings_lottery','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(38,2,9,0.00,'investment_asset_bond','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(39,2,10,0.00,'investment_asset_debenture','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(40,2,11,0.00,'investment_asset_ordinary_stock','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(41,2,12,0.00,'investment_asset_preferred_stock','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(42,2,13,0.00,'investment_asset_mutual_fund','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(43,2,14,0.00,'investment_asset_social_security_fund','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(44,2,15,0.00,'investment_asset_provident_fund','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(45,2,16,0.00,'investment_asset_gov_pension_fund','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(46,2,17,0.00,'investment_asset_national_savings_fund','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(47,2,18,0.00,'investment_asset_annuity','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(48,2,19,0.00,'investment_asset_insurance','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(49,2,20,0.00,'investment_asset_commodity','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(50,2,21,0.00,'investment_asset_real_estate','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(51,2,22,0.00,'investment_asset_cryptocurrency','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(52,2,23,0.00,'investment_asset_other','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(53,2,24,0.00,'personal_asset_living_real_estate','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(54,2,25,0.00,'personal_asset_vehicle','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(55,2,26,0.00,'personal_asset_jewel','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(56,2,27,0.00,'personal_asset_home_appliances','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(57,2,28,0.00,'personal_asset_collectibles','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL),(58,2,29,0.00,'personal_asset_other','2021-08-23 11:18:06','2021-08-23 11:18:06',NULL);
/*!40000 ALTER TABLE `user_asset_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_debt_records`
--

DROP TABLE IF EXISTS `user_debt_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_debt_records` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `debt_id` int unsigned NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_debt_records_user_id_index` (`user_id`),
  KEY `user_debt_records_debt_id_index` (`debt_id`),
  CONSTRAINT `user_debt_records_debt_id_foreign` FOREIGN KEY (`debt_id`) REFERENCES `debts` (`id`),
  CONSTRAINT `user_debt_records_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_debt_records`
--

LOCK TABLES `user_debt_records` WRITE;
/*!40000 ALTER TABLE `user_debt_records` DISABLE KEYS */;
INSERT INTO `user_debt_records` VALUES (1,1,1,0.00,'short_term_debt_informal_debt','2021-08-23 11:17:32','2021-08-23 11:17:32',NULL),(2,1,2,171219.92,'short_term_debt_credit_card','2021-08-23 11:17:32','2021-08-24 17:20:07',NULL),(3,1,3,0.00,'short_term_debt_installment_debt','2021-08-23 11:17:32','2021-08-23 11:17:32',NULL),(4,1,4,128000.00,'short_term_debt_cash_credit_debt','2021-08-23 11:17:32','2021-08-24 17:20:07',NULL),(5,1,5,0.00,'short_term_debt_asset_debt','2021-08-23 11:17:32','2021-08-23 11:17:32',NULL),(6,1,6,0.00,'short_term_debt_other','2021-08-23 11:17:32','2021-08-23 11:17:32',NULL),(7,1,7,4900000.00,'long_term_debt_real_estate_debt','2021-08-23 11:17:32','2021-08-26 10:45:28',NULL),(8,1,8,37405.00,'long_term_debt_vehicle_debt','2021-08-23 11:17:32','2021-08-24 17:20:07',NULL),(9,1,9,74572.61,'long_term_debt_personal_loan','2021-08-23 11:17:32','2021-08-24 17:20:07',NULL),(10,1,10,382837.36,'long_term_debt_student_loan','2021-08-23 11:17:32','2021-08-24 17:20:07',NULL),(11,1,11,0.00,'long_term_debt_cooperative_loan','2021-08-23 11:17:32','2021-08-23 11:17:32',NULL),(12,1,12,0.00,'long_term_debt_asset_debt','2021-08-23 11:17:32','2021-08-23 11:17:32',NULL),(13,1,13,0.00,'long_term_debt_other','2021-08-23 11:17:32','2021-08-23 11:17:32',NULL),(14,2,1,0.00,'short_term_debt_informal_debt','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(15,2,2,0.00,'short_term_debt_credit_card','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(16,2,3,0.00,'short_term_debt_installment_debt','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(17,2,4,0.00,'short_term_debt_cash_credit_debt','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(18,2,5,0.00,'short_term_debt_asset_debt','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(19,2,6,0.00,'short_term_debt_other','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(20,2,7,0.00,'long_term_debt_real_estate_debt','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(21,2,8,0.00,'long_term_debt_vehicle_debt','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(22,2,9,0.00,'long_term_debt_personal_loan','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(23,2,10,0.00,'long_term_debt_student_loan','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(24,2,11,0.00,'long_term_debt_cooperative_loan','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(25,2,12,0.00,'long_term_debt_asset_debt','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL),(26,2,13,0.00,'long_term_debt_other','2021-08-23 11:18:11','2021-08-23 11:18:11',NULL);
/*!40000 ALTER TABLE `user_debt_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_expense_records`
--

DROP TABLE IF EXISTS `user_expense_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_expense_records` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `expense_id` int unsigned NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_expense_records_user_id_index` (`user_id`),
  KEY `user_expense_records_expense_id_index` (`expense_id`),
  CONSTRAINT `user_expense_records_expense_id_foreign` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`),
  CONSTRAINT `user_expense_records_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_expense_records`
--

LOCK TABLES `user_expense_records` WRITE;
/*!40000 ALTER TABLE `user_expense_records` DISABLE KEYS */;
INSERT INTO `user_expense_records` VALUES (1,1,1,750.00,'savings_social_security_payment','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(2,1,2,0.00,'savings_savings','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(3,1,3,1000.00,'savings_reserved_money','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(4,1,4,1350.00,'investment_provident_fund','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(5,1,5,0.00,'investment_gov_pension_fund','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(6,1,6,0.00,'investment_national_savings_fund','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(7,1,7,0.00,'investment_cooperative','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(8,1,8,3394.96,'investment_annuity','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(9,1,9,3325.00,'investment_mutual_fund','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(10,1,10,0.00,'investment_bond','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(11,1,11,0.00,'investment_stock','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(12,1,12,175.00,'investment_gold','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(13,1,13,0.00,'investment_cryptocurrency','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(14,1,14,0.00,'investment_business','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(15,1,15,0.00,'investment_other','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(16,1,16,22900.00,'fixed_expense_real_estate_installment','2021-08-23 11:17:19','2021-08-26 10:44:22',NULL),(17,1,17,0.00,'fixed_expense_real_estate_rental','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(18,1,18,7481.00,'fixed_expense_vehicle_installment','2021-08-23 11:17:19','2021-08-26 10:44:22',NULL),(19,1,19,0.00,'fixed_expense_product_installment','2021-08-23 11:17:19','2021-08-24 05:27:40',NULL),(20,1,20,0.00,'fixed_expense_informal_debt_installment','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(21,1,21,10000.00,'fixed_expense_personal_loan_installment','2021-08-23 11:17:19','2021-08-26 10:44:22',NULL),(22,1,22,2500.00,'fixed_expense_student_loan_installment','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(23,1,23,0.00,'fixed_expense_cooperative_loan_installment','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(24,1,24,4000.00,'fixed_expense_insurance_premium','2021-08-23 11:17:19','2021-08-26 10:44:22',NULL),(25,1,25,0.00,'fixed_expense_other','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(26,1,26,5000.00,'non_fixed_expense_food_drink','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(27,1,27,2500.00,'non_fixed_expense_bill_utilities','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(28,1,28,1500.00,'non_fixed_expense_travel','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(29,1,29,0.00,'non_fixed_expense_education','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(30,1,30,0.00,'non_fixed_expense_entertainment','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(31,1,31,500.00,'non_fixed_expense_giving_donation','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(32,1,32,5000.00,'non_fixed_expense_wellness','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(33,1,33,3500.00,'non_fixed_expense_family','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(34,1,34,1800.00,'non_fixed_expense_fee','2021-08-23 11:17:19','2021-08-23 15:15:32',NULL),(35,1,35,10000.00,'non_fixed_expense_shopping','2021-08-23 11:17:19','2021-08-27 08:57:10',NULL),(36,1,36,0.00,'non_fixed_expense_friend_couple','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(37,1,37,0.00,'non_fixed_expense_traveling','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(38,1,38,0.00,'non_fixed_expense_other','2021-08-23 11:17:19','2021-08-23 11:17:19',NULL),(39,2,1,0.00,'savings_social_security_payment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(40,2,2,0.00,'savings_savings','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(41,2,3,0.00,'savings_reserved_money','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(42,2,4,0.00,'investment_provident_fund','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(43,2,5,0.00,'investment_gov_pension_fund','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(44,2,6,0.00,'investment_national_savings_fund','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(45,2,7,0.00,'investment_cooperative','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(46,2,8,0.00,'investment_annuity','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(47,2,9,0.00,'investment_mutual_fund','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(48,2,10,0.00,'investment_bond','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(49,2,11,0.00,'investment_stock','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(50,2,12,0.00,'investment_gold','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(51,2,13,0.00,'investment_cryptocurrency','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(52,2,14,0.00,'investment_business','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(53,2,15,0.00,'investment_other','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(54,2,16,0.00,'fixed_expense_real_estate_installment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(55,2,17,0.00,'fixed_expense_real_estate_rental','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(56,2,18,0.00,'fixed_expense_vehicle_installment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(57,2,19,0.00,'fixed_expense_product_installment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(58,2,20,0.00,'fixed_expense_informal_debt_installment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(59,2,21,0.00,'fixed_expense_personal_loan_installment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(60,2,22,0.00,'fixed_expense_student_loan_installment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(61,2,23,0.00,'fixed_expense_cooperative_loan_installment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(62,2,24,0.00,'fixed_expense_insurance_premium','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(63,2,25,0.00,'fixed_expense_other','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(64,2,26,0.00,'non_fixed_expense_food_drink','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(65,2,27,0.00,'non_fixed_expense_bill_utilities','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(66,2,28,0.00,'non_fixed_expense_travel','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(67,2,29,0.00,'non_fixed_expense_education','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(68,2,30,0.00,'non_fixed_expense_entertainment','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(69,2,31,0.00,'non_fixed_expense_giving_donation','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(70,2,32,0.00,'non_fixed_expense_wellness','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(71,2,33,0.00,'non_fixed_expense_family','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(72,2,34,0.00,'non_fixed_expense_fee','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(73,2,35,0.00,'non_fixed_expense_shopping','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(74,2,36,0.00,'non_fixed_expense_friend_couple','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(75,2,37,0.00,'non_fixed_expense_traveling','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL),(76,2,38,0.00,'non_fixed_expense_other','2021-08-23 11:18:00','2021-08-23 11:18:00',NULL);
/*!40000 ALTER TABLE `user_expense_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_income_records`
--

DROP TABLE IF EXISTS `user_income_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_income_records` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `income_id` int unsigned NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_income_records_user_id_index` (`user_id`),
  KEY `user_income_records_income_id_index` (`income_id`),
  CONSTRAINT `user_income_records_income_id_foreign` FOREIGN KEY (`income_id`) REFERENCES `income` (`id`),
  CONSTRAINT `user_income_records_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_income_records`
--

LOCK TABLES `user_income_records` WRITE;
/*!40000 ALTER TABLE `user_income_records` DISABLE KEYS */;
INSERT INTO `user_income_records` VALUES (1,1,1,50000.00,'active_income_salary','2021-08-23 11:17:12','2021-08-26 10:44:22',NULL),(2,1,2,0.00,'active_income_wage','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(3,1,3,50000.00,'active_income_selling_profit','2021-08-23 11:17:12','2021-08-26 10:51:51',NULL),(4,1,4,0.00,'active_income_trading_profit','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(5,1,5,0.00,'active_income_over_time','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(6,1,6,0.00,'active_income_bonus','2021-08-23 11:17:12','2021-08-24 17:21:44',NULL),(7,1,7,0.00,'active_income_commission','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(8,1,8,17000.00,'active_income_other','2021-08-23 11:17:12','2021-08-23 15:15:32',NULL),(9,1,9,0.00,'passive_income_interest','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(10,1,10,0.00,'passive_income_dividend','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(11,1,11,0.00,'passive_income_rental_fee','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(12,1,12,50000.00,'passive_income_business_profit','2021-08-23 11:17:12','2021-08-26 10:51:51',NULL),(13,1,13,0.00,'passive_income_capital_gain','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(14,1,14,0.00,'passive_income_other','2021-08-23 11:17:12','2021-08-23 11:17:12',NULL),(15,2,1,0.00,'active_income_salary','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(16,2,2,0.00,'active_income_wage','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(17,2,3,0.00,'active_income_selling_profit','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(18,2,4,0.00,'active_income_trading_profit','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(19,2,5,0.00,'active_income_over_time','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(20,2,6,0.00,'active_income_bonus','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(21,2,7,0.00,'active_income_commission','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(22,2,8,0.00,'active_income_other','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(23,2,9,0.00,'passive_income_interest','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(24,2,10,0.00,'passive_income_dividend','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(25,2,11,0.00,'passive_income_rental_fee','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(26,2,12,0.00,'passive_income_business_profit','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(27,2,13,0.00,'passive_income_capital_gain','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL),(28,2,14,0.00,'passive_income_other','2021-08-23 11:17:54','2021-08-23 11:17:54',NULL);
/*!40000 ALTER TABLE `user_income_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` enum('male','female','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` enum('single','married','widowed','separated','divorced') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'kunsirat.kps','Kunsirat','Kanchanaphasit','kunsirat.kps@gmail.com','1993-07-08','female','single','/images/1628748117.jpeg',NULL,'$2y$10$i2M3NPWugJIhbSklwCxzZe/jYyQbNBpwCtEF1MmhB7G09pbBPAm6y',NULL,'2021-08-12 06:01:57','2021-08-25 04:53:10'),(2,'shinyymina','Rapheelak','Srisawad','minapupu1998@gmail.com','1998-03-31','female','single','/images/1629648971.jpg',NULL,'$2y$10$iwYQaanY/EZmnTEjnI1Dd.U0E1rfjH8SEoqfSYtFfNZc7nlunnEym',NULL,'2021-08-22 16:16:11','2021-08-22 18:18:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-29  4:32:40
