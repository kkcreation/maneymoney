/*
Template Name: Minia - Admin & Dashboard Template
Author: Themesbrand
Website: https://themesbrand.com/
Contact: themesbrand@gmail.com
File: Calendar init js
*/

!function ($) {
    "use strict";

    var CalendarPage = function () {
    };

    CalendarPage.prototype.init = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        if($('meta[name="lang"]').attr('content') == 'en'){
            var addEventTitle = 'Add New Memo';
        }else{
            var addEventTitle = 'บันทึกรายการใหม่';
        }

        if($('meta[name="lang"]').attr('content') == 'en'){
            var editEventTitle = 'Edit Memo';
        }else{
            var editEventTitle = 'แก้ไขบันทึก';
        }
        var addEvent = $("#event-modal");
        var modalTitle = $("#modal-title");
        var formEvent = $("#form-event");
        var selectedEvent = null;
        var newEventData = null;
        var forms = document.getElementsByClassName('needs-validation');
        var selectedEvent = null;
        var newEventData = null;
        var eventObject = null;
        /* initialize the calendar */

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        //    var Draggable = FullCalendarInteraction.Draggable;
        //    var externalEventContainerEl = document.getElementById('external-events');
        // init dragable
        /*  new Draggable(externalEventContainerEl, {
              itemSelector: '.external-event',
              eventData: function (eventEl) {
                  return {
                      title: eventEl.innerText,
                      className: $(eventEl).data('class')
                  };
              }
          });*/
        /*    var defaultEvents;
            $.getJSON('/financial-calendar?start=2021-09-26T00%3A00%3A00%2B07%3A00&end=2021-11-07T00%3A00%3A00%2B07%3A00',function(json){
                defaultEvents = json;
            });*/
        /* var defaultEvents = $.ajax({
             url: 'financial-calendar',
             method: 'GET',
             failure: function() {
                 alert('there was an error while fetching events!');
             },*/
        //    color: 'yellow',   // a non-ajax option
        //    textColor: 'black' // a non-ajax option
        /*});*/
        /*var defaultEvents = [
            {
                title: 'All Day Event',
                start: new Date(y, m, 1)
            },
            {
                title: 'Long Event',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2),
                className: 'bg-warning'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false,
                className: 'bg-info'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d + 4, 16, 0),
                allDay: false,
                className: 'bg-primary'
            },
            {
                title: 'Meeting',
                start: new Date(y, m, d, 10, 30),
                allDay: false,
                className: 'bg-success'
            },
            {
                title: 'Lunch',
                start: new Date(y, m, d, 12, 0),
                end: new Date(y, m, d, 14, 0),
                allDay: false,
                className: 'bg-danger'
            },
            {
                title: 'Birthday Party',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                allDay: false,
                className: 'bg-success'
            },
            {
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://google.com/',
                className: 'bg-dark'
            }];*/

        //   var draggableEl = document.getElementById('external-events');
        var calendarEl = document.getElementById('calendar');

        function addNewEvent(info) {
            $("#fetch-price").hide();
            addEvent.modal('show');
            formEvent.removeClass("was-validated");
            formEvent[0].reset();
            $("#event-title").val();
            modalTitle.text(addEventTitle);
            newEventData = info;
            selectedEvent = false;
        }

        var calendar = new FullCalendar.Calendar(calendarEl, {
            eventSources: [{
                id: 1,
                url: '/events',
                extraParams: function () {
                    return {
                        cachebuster: new Date().valueOf()
                    };
                }
            }],
            plugins: ['bootstrap', 'interaction', 'dayGrid', 'timeGrid'],
            editable: true,
            droppable: true,
            selectable: false,
            defaultView: 'dayGridMonth',
            themeSystem: 'bootstrap',
            eventLimit: 3,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,listMonth'
            },
            eventClick: function (info) {
                addEvent.modal('show');
                formEvent[0].reset();
                selectedEvent = info.event;
                var asset_id = selectedEvent.extendedProps.asset_id;
                var wealth_wish_id = selectedEvent.extendedProps.wealth_wish_id;
                var action = selectedEvent.extendedProps.action;
                $("#event-asset option[value=" + asset_id + "]").prop('selected', 'selected');
                $("#event-wealth-wish option[value=" + wealth_wish_id + "]").prop('selected', 'selected');
                $("#event-action option[value=" + action + "]").prop('selected', 'selected');
                $("#event-title").val(selectedEvent.title);
                $("#event-tag").val(selectedEvent.extendedProps.tag);
                $("#conversion-rate").val(selectedEvent.extendedProps.conversion_rate);
                $("#unit").val(selectedEvent.extendedProps.unit);
                $("#price").val(selectedEvent.extendedProps.price);
                $("#fee").val(selectedEvent.extendedProps.fee);
                $("#fetch-price").show();
                if ($("#event-action").val() === 'buy') {
                    $("#fetch-price").removeClass();
                    $("#fetch-price").addClass('mdi mdi-download text-success');
                } else if ($("#event-action").val() === 'sell') {
                    $("#fetch-price").removeClass();
                    $("#fetch-price").addClass('mdi mdi-upload text-danger');
                } else if ($("#event-action").val() === 'deposit') {
                    $("#fetch-price").removeClass();
                    $("#fetch-price").addClass('mdi mdi-download text-primary');
                } else if ($("#event-action").val() === 'withdraw') {
                    $("#fetch-price").removeClass();
                    $("#fetch-price").addClass('mdi mdi-upload text-secondary');
                }

                newEventData = null;
                modalTitle.text(editEventTitle);
                newEventData = null;
            },
            dateClick: function (info) {
                addNewEvent(info);
            },
            eventDrop: function (info) {
                //   alert(info.event.title + " was dropped on " + info.event.start.toISOString());
                var event_start = moment(info.event.start).format('YYYY-MM-DDTHH:mm:ss');
                var event_end = moment(info.event.start).format('YYYY-MM-DDTHH:mm:ss');
                $.ajax({
                    url: '/manage-events',
                    data: {
                        id: info.event.id,
                        event_start: event_start,
                        event_end: event_end,
                        type: 'edit-date'
                    },
                    type: "POST",
                    success: function (data) {
                        calendar.refetchEvents();
                    }
                });

            }
        });
        calendar.render();

        /*Add new event*/
        // Form to add new event

        $(formEvent).on('submit', function (ev) {
            ev.preventDefault();
            var $option = $('#event-category').find('option:selected');
            var value = $option.val();
            var updatedTitle = $("#event-title").val();
            var updatedAction = $("#event-action").val();
            var updatedPrice = $("#price").val();
            var updatedTag = $("#event-tag").val(); //
            var updatedUnit = $("#unit").val(); //
            if ($("#conversion-rate").val()) {
                var updatedConversionRate = $("#conversion-rate").val(); //
            } else {
                var updatedConversionRate = 0;
            }
            if ($("#conversion-rate").val()) {
                var updatedFee = $("#fee").val(); //
            } else {
                var updatedFee = 0;
            }

            var updatedAsset = $("#event-asset").val();
            var updatedWealthWish = $("#event-wealth-wish").val();

            // validation
            if (forms[0].checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                forms[0].classList.add('was-validated');
            } else {
                if (selectedEvent) {
                    var event_start = moment(selectedEvent.start).format('YYYY-MM-DDTHH:mm:ss');
                    var event_end = moment(selectedEvent.start).format('YYYY-MM-DDTHH:mm:ss');
                    var oldTitle = selectedEvent.title;
                    if (updatedTitle !== oldTitle) {
                        if ($("#event-asset").val() === '13' || $("#event-asset").val() === '15') {
                            var navDate = moment(selectedEvent.start).format('YYYY-MM-DD');
                            $.ajax({
                                url: '/fund-info',
                                data: {
                                    name: updatedTitle,
                                    nav_date: navDate,
                                },
                                type: "POST",
                                success: function (data) {
                                    ev.preventDefault();
                                    var newPrice = 0;
                                    if ($("#event-action").val() === 'buy') {
                                        newPrice = data.sell_price;
                                    } else if ($("#event-action").val() === 'sell') {
                                        newPrice = data.buy_price;
                                    }
                                    $.ajax({
                                        url: '/manage-events',
                                        data: {
                                            id: selectedEvent.id,
                                            event_start: event_start,
                                            event_end: event_end,
                                            event_title: updatedTitle, // Asset Name
                                            event_tag: updatedTag, // Memo
                                            asset_id: updatedAsset,
                                            wealth_wish_id: updatedWealthWish,
                                            event_action: updatedAction,
                                            conversion_rate: updatedConversionRate,
                                            price: newPrice,
                                            unit: updatedUnit,
                                            fee: updatedFee,
                                            type: 'edit'
                                        },
                                        type: "POST",
                                        success: function (data) {
                                            calendar.refetchEvents();
                                        }
                                    });
                                }
                            });
                        } else {
                            // Change
                            $.ajax({
                                url: '/manage-events',
                                data: {
                                    id: selectedEvent.id,
                                    event_start: event_start,
                                    event_end: event_end,
                                    event_title: updatedTitle,
                                    event_tag: updatedTag,
                                    asset_id: updatedAsset,
                                    wealth_wish_id: updatedWealthWish,
                                    event_action: updatedAction,
                                    conversion_rate: updatedConversionRate,
                                    price: updatedPrice,
                                    unit: updatedUnit,
                                    fee: updatedFee,
                                    type: 'edit'
                                },
                                type: "POST",
                                success: function (data) {
                                    calendar.refetchEvents();
                                }
                            });
                        }
                    } else {
                        $.ajax({
                            url: '/manage-events',
                            data: {
                                id: selectedEvent.id,
                                event_start: event_start,
                                event_end: event_end,
                                event_title: updatedTitle,
                                event_tag: updatedTag,
                                asset_id: updatedAsset,
                                wealth_wish_id: updatedWealthWish,
                                event_action: updatedAction,
                                conversion_rate: updatedConversionRate,
                                price: updatedPrice,
                                unit: updatedUnit,
                                fee: updatedFee,
                                type: 'edit'
                            },
                            type: "POST",
                            success: function (data) {
                                calendar.refetchEvents();
                            }
                        });
                    }

                } else {
                    var event_start = moment(newEventData.date).format('YYYY-MM-DDTHH:mm:ss');
                    var event_end = moment(newEventData.date).format('YYYY-MM-DDTHH:mm:ss');

                    $.ajax({
                        url: '/manage-events',
                        data: {
                            event_title: updatedTitle,
                            event_start: event_start,
                            event_end: event_end,
                            event_tag: updatedTag,
                            asset_id: updatedAsset,
                            wealth_wish_id: updatedWealthWish,
                            event_action: updatedAction,
                            conversion_rate: updatedConversionRate,
                            price: updatedPrice,
                            unit: updatedUnit,
                            fee: updatedFee,
                            type: 'create'
                        },
                        type: "POST",
                        success: function (data) {
                            calendar.refetchEvents();
                        }
                    });
                }
                addEvent.modal('hide');
            }
        });

        $("#btn-delete-event").on('click', function (e) {
            if (selectedEvent) {
                $.ajax({
                    url: '/manage-events',
                    data: {
                        id: selectedEvent.id,
                        type: 'delete'
                    },
                    type: "POST",
                    success: function (data) {
                        calendar.refetchEvents();
                    }
                });
                /*   selectedEvent.remove();
                   selectedEvent = null;*/
                addEvent.modal('hide');
            }
        });
        $("#event-action").on('change', function (ev) {
            $("#fetch-price").show();
            if ($("#event-action").val() === 'buy') {
                $("#fetch-price").removeClass();
                $("#fetch-price").addClass('mdi mdi-download text-success');
            } else if ($("#event-action").val() === 'sell') {
                $("#fetch-price").removeClass();
                $("#fetch-price").addClass('mdi mdi-upload text-danger');
            } else if ($("#event-action").val() === 'deposit') {
                $("#fetch-price").removeClass();
                $("#fetch-price").addClass('mdi mdi-download text-primary');
            } else if ($("#event-action").val() === 'withdraw') {
                $("#fetch-price").removeClass();
                $("#fetch-price").addClass('mdi mdi-upload text-secondary');
            }
            if (selectedEvent) {
                if ($("#event-asset").val() === '13' || $("#event-asset").val() === '15') {
                    var fundName = $("#event-title").val();
                    var navDate = moment(selectedEvent.start).format('YYYY-MM-DD');
                    $.ajax({
                        url: '/fund-info',
                        data: {
                            name: fundName,
                            nav_date: navDate,
                        },
                        type: "POST",
                        success: function (data) {
                            ev.preventDefault();
                            if ($("#event-action").val() === 'buy') {
                                $('#price').val(data.sell_price);
                            } else if ($("#event-action").val() === 'sell') {
                                $('#price').val(data.buy_price);
                            }
                        }
                    });
                }
            } else {
                var fundName = $("#event-title").val();
                if (fundName !== '') {
                    if ($("#event-asset").val() === '13' || $("#event-asset").val() === '15') {
                        var navDate = moment(newEventData.date).format('YYYY-MM-DD');
                        $.ajax({
                            url: '/fund-info',
                            data: {
                                name: fundName,
                                nav_date: navDate,
                            },
                            type: "POST",
                            success: function (data) {
                                ev.preventDefault();
                                if ($("#event-action").val() === 'buy') {
                                    $('#price').val(data.sell_price);
                                } else if ($("#event-action").val() === 'sell') {
                                    $('#price').val(data.buy_price);
                                }
                            }
                        });
                    }
                }
            }
        });
        $("#fetch-price").on('click', function (ev) {
            if (selectedEvent) {
                if ($("#event-asset").val() === '13' || $("#event-asset").val() === '15') {
                    var fundName = $("#event-title").val();
                    var navDate = moment(selectedEvent.start).format('YYYY-MM-DD');
                    $.ajax({
                        url: '/fund-info',
                        data: {
                            name: fundName,
                            nav_date: navDate,
                        },
                        type: "POST",
                        success: function (data) {
                            ev.preventDefault();
                            if ($("#event-action").val() === 'buy') {
                                $('#price').val(data.sell_price);
                            } else if ($("#event-action").val() === 'sell') {
                                $('#price').val(data.buy_price);
                            }
                        }
                    });
                }
            } else {
                var fundName = $("#event-title").val();
                if (fundName !== '') {
                    if ($("#event-asset").val() === '13' || $("#event-asset").val() === '15') {
                        var navDate = moment(newEventData.date).format('YYYY-MM-DD');
                        $.ajax({
                            url: '/fund-info',
                            data: {
                                name: fundName,
                                nav_date: navDate,
                            },
                            type: "POST",
                            success: function (data) {
                                ev.preventDefault();
                                if ($("#event-action").val() === 'buy') {
                                    $('#price').val(data.sell_price);
                                } else if ($("#event-action").val() === 'sell') {
                                    $('#price').val(data.buy_price);
                                }
                            }
                        });
                    }
                }
            }
        });

        var path = 'autocomplete';
        $('input.typeahead').typeahead({
            minLength: 3,
            source: function (query, process) {
                return $.get(path, {tag: query}, function (data) {
                    return process(data);
                });
            },
            updater: function (item) {
                //$("#fetch-price").click();
                if (selectedEvent) {
                    if ($("#event-asset").val() === '13' || $("#event-asset").val() === '15') {
                        //var fundName = $("#event-title").val();
                        var navDate = moment(selectedEvent.start).format('YYYY-MM-DD');
                        $.ajax({
                            url: '/fund-info',
                            data: {
                                name: item,
                                nav_date: navDate,
                            },
                            type: "POST",
                            success: function (data) {
                                if ($("#event-action").val() === 'buy') {
                                    $('#price').val(data.sell_price);
                                } else if ($("#event-action").val() === 'sell') {
                                    $('#price').val(data.buy_price);
                                }
                            }
                        });
                    }
                } else {
                    if ($("#event-asset").val() === '13' || $("#event-asset").val() === '15') {
                        var navDate = moment(newEventData.date).format('YYYY-MM-DD');
                        $.ajax({
                            url: '/fund-info',
                            data: {
                                name: item,
                                nav_date: navDate,
                            },
                            type: "POST",
                            success: function (data) {
                                if ($("#event-action").val() === 'buy') {
                                    $('#price').val(data.sell_price);
                                } else if ($("#event-action").val() === 'sell') {
                                    $('#price').val(data.buy_price);
                                }
                            }
                        });
                    }
                }
                //dont forget to return the item to reflect them into input
                return item;
            }
        });
        /* typeaheadCtrl.on('typeahead:initialized', function (event, data) {
             // After initializing, hide the progress icon.
             $('.tt-hint').css('background-image', '');
         });*/

// Show progress icon while loading.
        // $('.tt-hint').css('background-image', 'url("/images/ajax-loader.gif")');

        /*  $("#btn-new-event").on('click', function (e) {
              addNewEvent({date: new Date(), allDay: true});
          });*/

    },
        //init
        $.CalendarPage = new CalendarPage, $.CalendarPage.Constructor = CalendarPage
}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.CalendarPage.init()
    }(window.jQuery);
