/*
Template Name: Minia - Admin & Dashboard Template
Author: Themesbrand
Website: https://themesbrand.com/
Contact: themesbrand@gmail.com
File: Form advanced Js File
*/


// Chocies Select plugin
document.addEventListener('DOMContentLoaded', function () {
	var genericExamples = document.querySelectorAll('[data-trigger]');
	for (i = 0; i < genericExamples.length; ++i) {
	  var element = genericExamples[i];
	  new Choices(element, {
		placeholderValue: 'This is a placeholder set in the config',
		searchPlaceholderValue: 'This is a search placeholder',
	  });
	}
});

// flatpickr

/*flatpickr('#datepicker-basic');*/

flatpickr('#membership_latest_renewed_at', {
    enableTime: true,
    dateFormat: "Y-m-d H:i"
 /* minDate: "today",
  maxDate: new Date().fp_incr(36500) // 100 years from now*/
});

flatpickr('#membership_expired_at', {
    enableTime: true,
    dateFormat: "Y-m-d H:i",
});
