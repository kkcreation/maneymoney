/*
Template Name: Minia - Admin & Dashboard Template
Author: Themesbrand
Website: https://themesbrand.com/
Contact: themesbrand@gmail.com
File: Echarts Init Js File
*/

// get colors array from the string
function getChartColorsArray(chartId) {
    var colors = $(chartId).attr('data-colors');
    var colors = JSON.parse(colors);
    return colors.map(function (value) {
        var newValue = value.replace(' ', '');
        if (newValue.indexOf('--') != -1) {
            var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
            if (color) return color;
        } else {
            return newValue;
        }
    })
}

// Doughnut Chart

$.ajax({
    url: '/portfolio-info',
    type: "GET",
    success: function (data) {
        $.each(data, function (key, item) {
            var fundName = [];
            var fundPortion = [];
            var totalCostValue = 0;
            $.each(item, function (key, item) {
                totalCostValue += item.summary.total_cost_value;
                fundPortion.push({
                    name: key,
                    value: item.summary.total_cost_value
                })
                fundPortion.sort(function (a, b) {
                    return a.value - b.value;
                });
                fundName = fundPortion.map(function (item) {
                    return item.name;
                });
                var wealth_wish_id = item[0].wealth_wish_id
                var doughnutColors = getChartColorsArray("#goal-" + wealth_wish_id);
                var dom = document.getElementById("goal-" + wealth_wish_id);
                var myChart = echarts.init(dom);
                var app = {};
                option = null;

                option = {
                  /*  tooltip: {
                        trigger: 'item',
                        formatter: '{a} <br/>{b}: {c} ({d}%)'
                    },*/
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}"
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'left',
                        // textStyle: {color: '#858d98'},
                        data: fundName,
                    },
                    color: doughnutColors, //['#5156be', '#ffbf53', '#fd625e', '#4ba6ef', '#2ab57d'],
                    series: [
                        {
                            name: 'Info:',
                            type: 'pie',
                            radius: ['40%', '60%'],
                            markPoint: {
                                tooltip: { show: false },
                                label: {
                                    show: true,
                                    formatter: '{b}',
                                    color: 'black',
                                    fontSize: 20,
                                },
                                data: [{
                                    name: (totalCostValue/1000).toFixed(2).toLocaleString()+' K',
                                    value: '-',
                                    symbol: 'circle',
                                    itemStyle: { color: 'transparent' },
                                    x: '50%',
                                    y: '50%',
                                }],
                            },
                            labelLine: {
                                length: 30
                            },
                            label: {
                                //formatter: '{b}\n{d}% {c} THB ',
                                formatter: '{abg|} {per|{c} THB} {d|{d}%}',
                                emphasis: {
                                    show: true,
                                    textStyle: {
                                        fontSize: '20',
                                        fontWeight: 'bold'
                                    }
                                },
                                rich: {
                                    b: {
                                        color: '#6E7079',
                                        lineHeight: 22,
                                        fontWeight: 'bold',
                                        align: 'center'
                                    },
                                    hr: {
                                        borderColor: '#8C8D8E',
                                        width: '100%',
                                        borderWidth: 1,
                                        height: 0
                                    },
                                    per: {
                                        color: '#fff',
                                        backgroundColor: '#4C5058',
                                        padding: [3, 4],
                                        borderRadius: 4
                                    },
                                    c: {
                                        align: 'center'
                                    },
                                }
                            },
                            /*  label: {
                                  normal: {
                                      show: false,
                                      position: 'center'
                                  },
                                  emphasis: {
                                      show: true,
                                      textStyle: {
                                          fontSize: '30',
                                          fontWeight: 'bold'
                                      }
                                  }
                              },*/
                            /*label: {
                                formatter: '{b|{b}}{abg|}\n{hr|}\n {per|{d}%} {c} THB ',
                                backgroundColor: '#F6F8FC',
                                borderColor: '#8C8D8E',
                                borderWidth: 1,
                                borderRadius: 4,
                                rich: {
                                    b: {
                                        color: '#6E7079',
                                        lineHeight: 22,
                                        fontWeight: 'bold',
                                        align: 'center'
                                    },
                                    hr: {
                                        borderColor: '#8C8D8E',
                                        width: '100%',
                                        borderWidth: 1,
                                        height: 0
                                    },
                                    per: {
                                        color: '#fff',
                                        backgroundColor: '#4C5058',
                                        padding: [3, 4],
                                        borderRadius: 4
                                    }
                                }
                            },*/
                        //    avoidLabelOverlap: false,
                        /*    label: {
                                normal: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    show: true,
                                    textStyle: {
                                        fontSize: '30',
                                        fontWeight: 'bold'
                                    }
                                }
                            },*/
                            data: fundPortion,
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                ;
                if (option && typeof option === "object") {
                    myChart.setOption(option, true);
                }
            });
        });
    }
});
/*$.ajax({
    url: '/portfolio-info',
    type: "GET",
    success: function (data) {
        var fundName = [];
        var fundPortion = [];
        $.each(data, function (key, item) {
            //alert(key)
            //fundName.push(key);
            fundPortion.push({
                name:key,
                value:item.summary.total_cost_value
            })
            fundPortion.sort(function (a, b) {
                return a.value - b.value;
            });
            fundName = fundPortion.map(function(item){
                return item.name;
            });
            var wealth_wish_id = item[0].wealth_wish_id
            var pieColors = getChartColorsArray("#goal-"+wealth_wish_id);
            //var pieColors = getRandomColor();
            var dom = document.getElementById("goal-"+wealth_wish_id);
            var myChart = echarts.init(dom);
            var app = {};
            option = null;
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: fundName,
                    textStyle: {color: '#858d98'}
                },
                color: pieColors, //['#fd625e', '#2ab57d', '#4ba6ef', '#ffbf53', '#5156be'],
                series: [
                    {
                        name: 'Fund info',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: fundPortion,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            ;
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
        });
    }
});*/

/*

setInterval(function () {
    option.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;
    myChart.setOption(option, true);
},2000);
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}*/
