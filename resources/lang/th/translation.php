<?php

return [
    "Menu" => "เมนู",
    "Dashboard" => "แดชบอร์ด",
    "Back_Dashboard" => "กลับไปยังหน้าแดชบอร์ด",
    "Wealth_Wish" => "แบบร่างความฝัน",
    "Wealth_Health" => "บอร์ดสุขภาพการเงิน",
    "Wealth_Dashboard" => "ตรวจเช็คสุขภาพการเงิน",
    "Apps" => "แอพ",
    "Investment_Calendar" => "ปฏิทินการลงทุน",
    "Chat" => "การสนทนา",
    "Email" => "อีเมล์",
    "Inbox" => "กล่องข้อความ",
    "Read_Email" => "เมล์อ่านแล้ว",
    "Invoices" => "ใบแจ้งหนี้",
    "Invoice_List" => "รายการใบแจ้งหนี้",
    "Invoice_Detail" => "รายละเอียดใบแจ้งหนี้",
    "Contacts" => "ผู้ติดต่อ",
    "User_Grid" => "กริดผู้ใช้ระบบ",
    "User_List" => "ลิสต์ผู้ใช้ระบบ",
    "Profile" => "โปรไฟล์",
    "Profile_Details" => "รายละเอียดโปรไฟล์",
    "Authentication" => "การรับรองสิทธิ์",
    "Login" => "ลงชื่อเข้าใช้",
    "Register" => "ลงทะเบียน",
    "Recover_Password" => "กู้คืนรหัสผ่าน",
    "Lock_Screen" => "หน้าต่างล็อค",
    "Confirm_Mail" => "เมล์ยืนยัน",
    "Email_Verification" => "การยืนยันอีเมล์",
    "Two_Step_Verification" => "การยืนยันสองขั้นตอน",
    "Pages" => "หน้า",
    "Starter_Page" => "Starter Page",
    "Maintenance" => "Maintenance",
    "Coming_Soon" => "เร็วๆนี้",
    "Timeline" => "ไทม์ไลน์",
    "FAQs" => "FAQs",
    "Pricing" => "Pricing",
    "Error_404" => "Error 404",
    "Error_403" => "Error 403",
    "403_Message" => "ขออภัย คุณไม่ได้รับสิทธิ์ในการเข้าถึงฟีเจอร์นี้",
    "Error_500" => "Error 500",
    "Horizontal" => "Horizontal",
    "Elements" => "Elements",
    "Components" => "ส่วนประกอบ",
    "Alerts" => "แจ้งเตือน",
    "Buttons" => "ปุ่ม",
    "Cards" => "Cards",
    "Carousel" => "Carousel",
    "Dropdowns" => "Dropdowns",
    "Grid" => "กริด",
    "Images" => "รูปภาพ",
    "Modals" => "Modals",
    "Offcanvas" => "Offcanvas",
    "Progress_Bars" => "Progress Bars",
    "Tabs_n_Accordions" => "Tabs & Accordions",
    "Typography" => "Typography",
    "Video" => "วิดีโอ",
    "General" => "ทั่วไป",
    "Colors" => "Colors",
    "Extended" => "ส่วนขยาย",
    "Lightbox" => "Lightbox",
    "Range_Slider" => "Range Slider",
    "SweetAlert_2" => "SweetAlert 2",
    "Session_Timeout" => "Session Timeout",
    "Rating" => "เรทติ้ง",
    "Notifications" => "การแจ้งเตือน",
    "Forms" => "ฟอร์ม",
    "Basic_Elements" => "Basic Elements",
    "Validation" => "Validation",
    "Form_Validation" => "Form Validation",
    "Advanced_Plugins" => "Advanced Plugins",
    "Editors" => "Editors",
    "File_Upload" => "อัพโหลดไฟล์",
    "Wizard" => "Wizard",
    "Mask" => "Mask",
    "Tables" => "ตาราง",
    "Bootstrap_Basic" => "Bootstrap Basic",
    "DataTables" => "DataTables",
    "Responsive" => "Responsive",
    "Editable" => "Editable",
    "Charts" => "แผนภูมิ",
    "Apexcharts" => "Apexcharts",
    "Echarts" => "Echarts",
    "Chartjs" => "Chartjs",
    "Jquery_Knob" => "Jquery Knob",
    "Sparkline" => "Sparkline",
    "Icons" => "ไอคอน",
    "Boxicons" => "Boxicons",
    "Material_Design" => "Material Design",
    "Dripicons" => "Dripicons",
    "Font_Awesome_5" => "Font Awesome 5",
    "Maps" => "แผนที่",
    "Google" => "Google",
    "Vector" => "Vector",
    "Leaflet" => "Leaflet",
    "Multi_Level" => "หลายชั้น",
    "Level_1_1" => "Level 1 1",
    "Level_1_2" => "Level 1 2",
    "Level_2_1" => "Level 2 1",
    "Level_2_2" => "Level 2 2",
    "Unlimited_Access" => "ใช้งานไม่จำกัด",
    "Upgrade_your_plan_from_a_Free_trial,_to_select_‘Prime_Plan’" => "ปลดล็อคฟีเจอร์ข่วยวางแผนการเงินสุดพิเศษ สมัครสมาชิก<br> <b>Much Prime</b><br>เพียง <b>99 บาท/เดือน</b>",
    "Upgrade_Now" => "อัพเกรดตอนนี้",
    "Search" => "Search...",
    "Notifications" => "Notifications",
    "Unread" => "Unread",
    "James_Lemire" => "James Lemire",
    "It_will_seem_like_simplified_English" => "It will seem like simplified English",
    "1_hours_ago" => "1 hours ago",
    "Your_order_is_placed" => "Your order is placed",
    "If_several_languages_coalesce_the_grammar" => "If several languages ​​coalesce the grammar",
    "3_min_ago" => "3 min ago",
    "Your_item_is_shipped" => "Your item is shipped",
    "If_several_languages_coalesce_the_grammar" => "If several languages ​​coalesce the grammar",
    "3_min_ago" => "3 min ago",
    "Salena_Layfield" => "Salena Layfield",
    "As_a_skeptical_Cambridge_friend_of_mine_occidental" => "As a skeptical Cambridge friend of mine occidental",
    "1_hours_ago" => "1 hours ago",
    "View_More" => "View More",
    "Shawn_L" => "Shawn L",
    "Profile" => "โปรไฟล์",
    "Billing" => "Billing",
    "Settings" => "Settings",
    "Lock_screen" => "Lock screen",
    "Logout" => "Logout",
    "Extra_pages" => "Extra pages",
    "Utility" => "Utility",
    "Wealth_Check" => "ตรวจเช็คสุขภาพการเงิน",
    "Wealth_Profile" => "งบการเงินส่วนบุคคล",
    "Wealth_Onboarding" => "เตรียมความพร้อม",
    "User_Profile" => "ข้อมูลผู้ใช้งาน",
    "First_Name" => "ชื่อ",
    "Last_Name" => "นามสกุล",
    "Gender" => "เพศ",
    "Marital_Status" => "สถานภาพ",
    "Personal_Statement" => "งบรายรับ-รายจ่าย",
    "Personal_Balance_Sheet" => "งบแสดงสถานะทางการเงิน",
    "Fill_all_information_below" => "โปรดกรอกข้อมูลให้ครบถ้วน",
    "Income" => "รายรับ",
    "Expense" => "รายจ่าย",
    "Savings_Investment" => "ออม & ลงทุน",
    "Income_Uppercase" => "รายรับ",
    "Expense_Uppercase" => "รายจ่าย",
    "Savings_Investment_Uppercase" => "ออม & ลงทุน",
    "Net_Cash_Flow_Uppercase" => "กระแสเงินสดสุทธิ",
    "Active_Income" => "รายได้จากการทำงาน",
    "Passive_Income" => "รายได้จากสินทรัพย์",
    "Fixed_Expense" => "ค่าใช้จ่ายคงที่",
    "Non_Fixed_Expense" => "ค่าใช้จ่ายผันแปร",
    "Savings_Cost" => "ค่าใช้จ่ายการออม",
    "Investment_Cost" => "ค่าใช้จ่ายการลงทุน",
    "Asset" => "สินทรัพย์",
    "Debt" => "หนี้สิน",
    "Asset_Uppercase" => "สินทรัพย์",
    "Debt_Uppercase" => "หนี้สิน",
    "Net_Wealth_Uppercase" => "ความมั่งคั่งสุทธิ",
    "Liquid_Asset" => "สินทรัพย์สภาพคล่อง",
    "Personal_Asset" => "สินทรัพย์ส่วนตัว",
    "Investment_Asset" => "สินทรัพย์เพื่อการลงทุน",
    "Continue" => "( ต่อ )",
    "Short_Term_Debt" => "หนี้สินระยะสั้น",
    "Long_Term_Debt" => "หนี้สินระยะยาว",
    "Benchmark" => "เกณฑ์มาตรฐาน",
    "Survival_Ratio" => "อัตราส่วนความอยู่รอด",
    "Wealth_Ratio" => "อัตราส่วนความมั่งคั่ง",
    "Debt_to_Asset" => "หนี้สิน : ทรัพย์สิน",
    "Investment_to_Wealth" => "การลงทุน : ความมั่งคั่ง",
    "Much_Guide" => "คำแนะนำ",
    "Save_Changes" => "บันทึก",
    "Confirm_Changes" => "ยืนยัน",
    "Cash_Flow_Balance" => "สมดุลกระแสเงินสด (รายเดือน)",
    "Reserved_Money" => "เงินเก็บสำรองฉุกเฉิน",
    "Reserved_Money_Amount" => "จำนวนเงินสำรองที่คุณมี",
    "Suggested_Amount" => "จำนวนเงินที่แนะนำ",
    "Personal_Expense" => "ค่าใช้จ่ายส่วนตัว",
    "Insurance_Premium" => "ค่าเบี้ยประกัน",
    "Risk_Transfer" =>    "การโอนย้ายความเสี่ยง",
    "Debt_Installment" => "ค่าผ่อนชำระหนี้",
    "Cash_Flow" => "กระแสเงินสดสุทธิ (สภาพคล่อง)",
    "Actual" => "ค่าจริงที่ได้",
    "Min" => "ค่าต่ำสุดแนะนำ",
    "Max" => "ค่าสูงสุดแนะนำ",
    "Suggested" => "ค่ามาตรฐานแนะนำ",
    "Previous" => "ก่อนหน้า",
    "Next" => "ต่อไป",
    "Wealth_Overview" => "วิเคราะห์สุขภาพการเงิน",
    "Update" => "แก้ไข",
    "Membership" => "ข้อมูลสมาชิก",
    "Membership_Type" => "ประเภทสมาชิก",
    "Payment_Interval" => "รูปแบบการจ่าย",
    "Membership_Expiry" => "หมดอายุสมาชิก",
    "Latest_Renewal" => "ต่ออายุสมาชิกล่าสุด",
    "Confirm_Update" => "ยืนยันการเปลี่ยนแปลง!",
    "Close" => "ปิดหน้าต่าง",
    "Free" => "ฟรี",
    "Wealth_Wish_Maker" => "สร้างแบบร่างความฝัน",
    "Budget_Planner" => "วางแผนงบการเงินล่วงหน้า",
    "Financial_State_Tracker" => "บันทึกงบการเงินส่วนบุคคล",
    "Wealth_Check_Up" => "ตรวจเช็คสุขภาพการเงิน",
    "Wealth_Care" => "รับคำแนะนำการดูแลการเงินส่วนบุคคล",
    "Debt_Handler" => "สร้างแบบแผนจัดการหนี้สิน",
    "Wealth_Protect_Plus" => "สร้างแบบแผนโอนย้ายความเสี่ยง",
    "Investment_Monitor" => "ติดตามพอร์ตการลงทุน",
    "Month" => "เดือน",
    "_Monthly" => "เดือน",
    "Monthly" => "รายเดือน",
    "Yearly" => "รายปี",
    "Much_Starter" => "สำหรับผู้เริ่มต้น ทดลองใช้งานฟีเจอร์เริ่มต้น HelloMuch! <u>แบบไม่มีค่าใช้จ่าย</u>",
    "Much_Standard" => "สำหรับผู้ติดตาม ที่ต้องการ<u>คำแนะนำและตัวช่วยพื้นฐาน</u> เพื่อบริหารจัดการการเงินส่วนบุคคล",
    "Much_Prime" => "สำหรับผู้ติดตาม ที่ต้องการ<u>คำแนะนำและตัวช่วยพิเศษ</u> เพื่อบริหารจัดการการเงินส่วนบุคคล",
    "Suggested" => "แนะนำ",
    "Your_Current_Plan" => "แผนของคุณตอนนี้",
    "Choose_Plan" => "เลือกแผนนี้",
    "Pricing_Subtitle" => "เริ่มต้นดูแลสุขภาพการเงินของคุณกับ HelloMuch!",
    "Upgrade_Plan" => "อัพเกรดแผนของคุณ",
    "Tell_Us_Your_Wish" => "ระบุความฝันของคุณ",
    "Year" => "ปี",
    "Drop_File_Here" => "วางรูปตรงนี้หรือกดเพื่ออัพโหลด",
    "Placeholder_Wish_Title" => "เช่น สร้างบ้านในฝัน, เก็บเงินเกษียณ, ฯลฯ",
    "Create" => "เพิ่ม",
    "Confirm_Delete" => "ยืนยันการลบ",
    "List" => "รายการ",
    "Timeline" =>  "ไทม์ไลน์",
    "Wish_List" => "รายการความฝัน",
    "Wish_Timeline" =>  "ไทม์ไลน์ความฝัน",
    "Formula" => "สูตรคำนวณ",
    "Action_Plan" => "แผนดำเนินการ",
    "Normal_Savings" => "ออมแบบปกติ",
    "Wish_Value" => "มูลค่าความฝัน",
    "Period" => "ระยะเวลา (เดือน)",
    "Current_Savings" => "เงินเก็บปัจจุบัน",
    "Annual_Rate" => "อัตราเติบโต (% ต่อปี)",
    "DCA" => "จำนวนเงินออมลงทุน (ต่อเดือน)",
    "Calculate" => "คำนวณ",
    "Save_Plan" => "บันทึกแผน",
    "Tool" => "เครื่องมือลงทุน",
    "Very_Poor" => "แย่มาก",
    "Poor" => "แย่",
    "Average" => "พอใช้",
    "Good" => "ดี",
    "Excellent" => "ดีมาก",
    "Wealth_Creation" => "สร้างความมั่งคั่ง",
    "Wealth_Protection" => "ปกป้องความมั่งคั่ง",
    "Wealth_Accumulation" => "สะสมความมั่งคั่ง",
    "Wealth_Distribution" => "ส่งมอบความมั่งคั่ง",
    "Wealth_Special" => "++ พิเศษ",
    "Pinned" => "ปักหมุด",
    "Admin" => "แอดมิน",
    "Money_Forecast" => "งบการเงินล่วงหน้า",
    "Debt_Killer" => "วางแผนจัดการหนี้สิน",
    "Risk_Awareness" => "รับรู้ความเสี่ยง",
    "Protection_Planner" => "วางแผนจัดการความเสี่ยง",
    "Investment_Memo" => "บันทึกการลงทุน",
    "Portfolio" => "พอร์ตการลงทุน",
    "Legacy_Planner" => "วางแผนมรดก",
    "DCA_Total" => "จำนวนเงินออมลงทุน (แนะนำ)",
    "Wealth_Analysis" => "ผลวิเคราะห์สุขภาพการเงิน",
    "Wealth_Analysis_Desc" => "ผลวิเคราะห์ & คำแนะนำในการดูแลสุขภาพการเงินของคุณพร้อมแล้ว!",
    "Wealth_Analysis_Pls_Create" => "ผลวิเคราะห์ & คำแนะนำในการดูแลสุขภาพการเงินของคุณยังไม่พร้อม!",
    "See_Result" => "ดูผลลัพธ์",
    "Create_New_Financial_Stm" => "สร้างงบการเงินใหม่",
    "Get_Instruction" => "คำแนะนำเพิ่มเติม",
    "Back_Wealth_Analysis" => "กลับไปยัง ผลวิเคราะห์",
    "Wealth_Analysis_Title" => "ผลวิเคราะห์ HelloMuch!",
    "Wealth_Care_Instruction" => "คำแนะนำ HelloMuch!",
    "Debt_To_Asset" => "ภาระหนี้สิน",
    "Total_Investment_Asset" => "มูลค่าการลงทุนรวม",
    "Debt_Type" => "ประเภทหนี้สิน",
    "Debt_List" => "รายการหนี้สิน",
    "Debt_To_Kill" => "เพิ่มรายการหนี้สิน",
    "Placeholder_Debt_Creditor" => "เช่น ธนาคารไทยพาณิชย์ (SCB)",
    "Total_Debt_Interest" => "ดอกเบี้ยรวม",
    "Debt_Edit" => "แก้ไขรายละเอียดหนี้สิน",
    "Wish_Edit" => "แก้ไขรายละเอียดความฝัน",
    "Create_New" => "สร้างใหม่",
    "Actual_Tool" => "เครื่องมือการลงทุน",
    "Target_Wealth_Wish" => "เป้าหมาย",
    "Undefined" => "ไม่ระบุ",
    "Select" => "เลือก",
    "Action" => "ประเภทธุรกรรม",
    "Deposit" => "ฝาก",
    "Withdraw" => "ถอน",
    "Buy" => "ซื้อ",
    "Sell" => "ขาย",
    "Asset_Name" => "ชื่อแท็กลงทุน",
    "Price" => "ราคาต่อหน่วย",
    "Unit" => "จำนวน",
    "Conversion_Rate" => "อัตราแลกเปลี่ยน (ไม่บังคับ)",
    "Fee" => "ค่าธรรมเนียม (ไม่บังคับ)",
    "Memo" => "บันทึกรายละเอียด",
    "Delete" => "ลบทิ้ง",
    "Add_Event" => "บันทึกรายการใหม่",
    "Edit_Event" => "แก้ไขบันทึก",
    "Percent_Annually" => "% ต่อปี",
    "Error" => "พบข้อผิดพลาด"
];
?>
