@extends('layouts.master')

@section('title') @lang('translation.User_List') @endsection

@section('css')

<!-- DataTables -->
<link href="{{ URL::asset('/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Responsive datatable examples -->
<link href="{{ URL::asset('/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') @lang('translation.Contacts') @endslot
@slot('title') @lang('translation.User_List') @endslot
@endcomponent

<div class="row align-items-center">
    <div class="col-md-6">
        <div class="mb-3">
            <h5 class="card-title">Contact List <span class="text-muted fw-normal ms-2">({{ $authMemberList->count() }})</span></h5>
        </div>
    </div>

   {{-- <div class="col-md-6">
        <div class="d-flex flex-wrap align-items-center justify-content-end gap-2 mb-3">
            <div>
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link active" href="apps-contacts-list" data-bs-toggle="tooltip" data-bs-placement="top" title="List"><i class="bx bx-list-ul"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="apps-contacts-grid" data-bs-toggle="tooltip" data-bs-placement="top" title="Grid"><i class="bx bx-grid-alt"></i></a>
                    </li>
                </ul>
            </div>
            <div>
                <a href="#" class="btn btn-light"><i class="bx bx-plus me-1"></i> Add New</a>
            </div>

            <div class="dropdown">
                <a class="btn btn-link text-muted py-1 font-size-16 shadow-none dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="bx bx-dots-horizontal-rounded"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-end">
                    <li><a class="dropdown-item" href="#">Action</a></li>
                    <li><a class="dropdown-item" href="#">Another action</a></li>
                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
            </div>
        </div>

    </div>--}}
</div>
<!-- end row -->

<div class="table-responsive mb-4">
    <table class="table align-middle datatable dt-responsive table-check nowrap" style="border-collapse: collapse; border-spacing: 0 8px; width: 100%;">
        <thead>
            <tr>
                <th scope="col" style="width: 50px;">
                    <div class="form-check font-size-16">
                        #
                      {{--  <input type="checkbox" class="form-check-input" id="checkAll">
                        <label class="form-check-label" for="checkAll"></label>--}}
                    </div>
                </th>
                <th scope="col">Username</th>
                <th scope="col">Name</th>
                <th scope="col">Age</th>
                <th scope="col">Email</th>
                <th scope="col">Membership</th>
                <th style="width: 80px; min-width: 80px;">Action</th>
            </tr>
        </thead>
        <tbody>
        @php $count = 1; @endphp
        @foreach($authMemberList as $member)
            <tr>
                <th scope="row">
                    <div class="form-check font-size-16">
                        {{  $count++ }}
                    </div>
                </th>
                <td>
                    <img src="{{ isset($member->avatar) ? asset($member->avatar) : asset('/assets/images/users/avatar-1.jpg') }}" alt="" class="avatar-sm rounded-circle me-2">
                    <a href="#" class="text-body">{{ $member->name }}</a>
                </td>
                <td>
                    <a href="#" class="text-body">{{ $member->first_name.' '.$member->last_name }}</a>
                </td>
                <td>{{ $member->dob ? Carbon\Carbon::parse($member->dob)->age : null }}</td>
                <td>{{ $member->email }}</td>
                <td>
                    <div class="d-flex gap-2">
                        <a href="#" class="badge badge-soft-primary">{{ config('hellomuch.role.'.$member->role) }}</a>
                    </div>
                </td>
                <td>
                    <a class="btn btn-outline-primary btn-sm edit" title="Edit"
                       href="member-info/{{ $member->id }}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <!-- end table -->
</div>
<!-- end table responsive -->

@endsection

@section('script')

<!-- Required datatable js -->
<script src="{{ URL::asset('/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

<!-- Responsive examples -->
<script src="{{ URL::asset('/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<!-- init js -->
<script src="{{ URL::asset('/assets/js/pages/datatable-pages.init.js') }}"></script>

@endsection
