@extends('layouts.master')

@section('title') @lang('translation.Debt_Killer') @endsection
<meta name="csrf-token" content="{{ csrf_token() }}"/>
@section('css')

    <!-- dropzone css -->
    <link href="{{ URL::asset('/assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
 {{--   <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <!-- datepicker css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.css') }}">
   {{-- <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
   {{-- <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
          type="text/css"/>--}}
    <style>
        .dropzone {
            box-shadow: 0px 2px 20px 0px #f2f2f2;
            border-radius: 10px;
        }
    </style>
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Debt_Killer') @endslot
        @slot('title') @lang('translation.Debt_To_Kill') @endslot
    @endcomponent
    @if (session('message'))
        <div class="alert {{ session('alert-class') }} alert-border-left alert-dismissible fade show" role="alert">
            <i class="mdi mdi-check-all me-3 align-middle"></i>{{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="bx bx-plus me-1"></i> @lang('translation.Debt_To_Kill')</h4>
                    {{-- <p class="card-title-desc">DropzoneJS is an open source library
                         that provides drag’n’drop file uploads with image previews.
                     </p>--}}
                </div>
                <div class="card-body">
                {{-- <div class="form-wrapper py-5">--}}
                <!-- form starts -->
                    <form action="{{ route('form.debtdata') }}" name="debtcreateform" id="debtcreateform" method="POST"
                          class="dropzone">

                        @csrf
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">@lang('translation.Debt_Type')</label>
                                        <select name="debttype"  class="form-select">
                                            @php $shortTermDebt = \App\Models\Debt::whereIn('type',['short_term_debt'])->get(); @endphp
                                            <option class="dropdown-header text-secondary" disabled>---------- @lang('translation.Short_Term_Debt') ----------</option>
                                            @foreach($shortTermDebt as $value)
                                                <option value="{{ $value->id }}">
                                                    {{ App::getLocale() == 'en' ? $value->description : $value->description_th }}
                                                </option>
                                            @endforeach
                                            @php $longTermDebt = \App\Models\Debt::whereIn('type',['long_term_debt'])->get(); @endphp
                                            <option class="dropdown-header text-secondary" disabled>---------- @lang('translation.Long_Term_Debt') ----------</option>
                                            @foreach($longTermDebt as $value)
                                                <option value="{{ $value->id }}">
                                                    {{ App::getLocale() == 'en' ? $value->description : $value->description_th }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label
                                            for="debtkiller-creditor">{{ config('debtkiller.attributes.creditor.'.app()->getLocale()) }}</label>
                                        <input name="creditor" type="text" class="form-control"
                                               id="creditor"
                                               value="" placeholder="@lang('translation.Placeholder_Debt_Creditor')" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('debtkiller.attributes.outstanding_balance.'.app()->getLocale()) }}</label>
                                        <input type="number" name="outstanding_balance" id="outstanding_balance"
                                               class="form-control" step="0.01" placeholder="eg. 100,000" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('debtkiller.attributes.annual_interest_rate.'.app()->getLocale()) }}</label>
                                        <input type="number" class="form-control" name="annual_interest_rate" id="annual_interest_rate" step="0.01"
                                               placeholder="eg. 18" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('debtkiller.attributes.expected_duration.'.app()->getLocale()) }}</label>
                                        <input type="number" class="form-control" name="expected_duration" id="expected_duration"
                                               placeholder="eg. 48" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('debtkiller.attributes.start_clearance_date.'.app()->getLocale()) }}</label>
                                        <input type="text" class="form-control" name="start_clearance_date" id="start_clearance_date"
                                               placeholder="eg. {{ \Carbon\Carbon::now()->format('Y-m-d') }}" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('debtkiller.attributes.statement_date.'.app()->getLocale()) }}</label>
                                        <input type="number" class="form-control" name="statement_date" id="statement_date"
                                               placeholder="eg. 23" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('debtkiller.attributes.due_date.'.app()->getLocale()) }}</label>
                                        <input type="number" class="form-control" name="due_date" id="due_date"
                                               placeholder="eg. 13" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label
                                            for="wealthwish-description">{{ config('debtkiller.attributes.notes.'.app()->getLocale()) }}</label>
                                        <textarea name="notes" class="form-control"
                                                  id="notes" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <br>
                            <button type="submit" class="btn btn-md btn-primary">@lang('translation.Create')</button>
                        </div>
                    </form>
                    <!-- form end -->
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')
    <!-- datepicker js -->
    <script src="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/debt-killer-create.init.js') }}"></script>
@endsection
