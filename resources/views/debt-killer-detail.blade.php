@extends('layouts.master')

@section('title') @lang('translation.Debt_Killer') @endsection
@section('css')

    <!-- dropzone css -->
    <link href="{{ URL::asset('/assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
    {{--   <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <!-- datepicker css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.css') }}">
    {{-- <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    {{-- <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
           type="text/css"/>--}}
    <style>
        .carousel-inner img {
            max-height: 350px;
        }

        .timeline .timeline-date {
            top: -30px !important;
        }

        @media (max-width: 767.98px) {
            .timeline .timeline-date {
                left: initial;
                right: 15px;
                top: -30px;
            }
        }
    </style>
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Debt_Killer') @endslot
        @slot('title') @lang('translation.Action_Plan') @endslot
    @endcomponent
    @if (session('message'))
        <div class="alert {{ session('alert-class') }} alert-border-left alert-dismissible fade show" role="alert">
            <i class="mdi mdi-check-all me-3 align-middle"></i>{{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6 col-xl-6">
            <div class="card h-100">
                @php
                    $debtKillerService = new \App\Services\DebtKillerService(Auth::user());
                        $durationInMonths = $debtKiller->expected_duration;
                        $timeBound = $debtKillerService->defineTimeBound($durationInMonths);
                @endphp
                <div class="card-body">
                    <div class="timeline timeline-box">
                        <div
                            class="timeline-date bg-danger text-center rounded">
                            <h3 class="text-white mb-0">{{ Carbon\Carbon::parse($debtKiller->expected_clearance_date)->age + Carbon\Carbon::parse(Auth::user()->dob)->age }}</h3>
                            <p class="mb-0 text-white-50">@lang('translation.Year')</p>
                        </div>
                        <blockquote class="blockquote">
                            <h4 class="card-title">{{ $debtKiller->creditor }}</h4>
                            <h4>
                                <i class="bx bxs-purchase-tag"></i> {{ number_format($debtKiller->outstanding_balance,2,'.',',') }}
                            </h4>
                            <div class="gap-2 font-size-18">
                        <span class="badge badge-soft-secondary">
                       {{ App::getLocale() == 'en' ? $debtKiller->debt->description : $debtKiller->debt->description_th  }}
                   </span>
                                <span
                                    class="badge badge-soft-{{ config('debtkiller.attributes.time_bound.color.'.$timeBound) }}">
                       {{ config('debtkiller.'.app()->getLocale().'.time_bound.'.$timeBound) }}
                   </span>
                            </div>
                        </blockquote>
                        <p class="card-text text-secondary" style="text-align: center;">
                            <i>"{{ $debtKiller->notes }}"</i></p>
                    </div>
                    {{--    <h6 class="card-subtitle text-muted">{{ config('wealthwish.'.app()->getLocale().'.category.'. $wealthWish->category) }}</h6>--}}
                </div><!-- end card-body -->
            </div>

        </div>
        <div class="col-md-6 col-xl-6">
            <!-- FV = PV x ( 1 + i ) ^ n -->
            <div class="card h-100">
                <div class="card-body"><h4>@lang('translation.Action_Plan')</h4>
                    <form class="form" method="post"
                          action="{{ route('updateDebtActionPlan',['debtkillerid'=> $debtKiller->id ]) }}"
                          id="action-plan">
                        @csrf
                        <blockquote class="blockquote">
                            <h4 class="card-title text-danger">* @lang('translation.Total_Debt_Interest')
                                : <span id="totalinterest">{{ number_format($debtKiller->total_interest_amount ,2,'.',',') }}</span>
                            </h4>
                        </blockquote>
                        <input type="hidden" name="token" value="{{ $token ?? '' }}">
                        <input type="hidden" name="totalinterest" value="{{ $debtKiller->total_interest_amount }}">
                        <div class="form-group row">
                            <label for="outstandingbalance"
                                   class="col-md-5 col-form-label">{{ config('debtkiller.attributes.outstanding_balance.'.app()->getLocale()) }}
                                :</label>
                            <div class="col-md-7">
                                <input type="text" readonly class="form-control-plaintext" id="outstandingbalance"
                                       value="{{ number_format($debtKiller->outstanding_balance,2,'.',',') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="annualrate" class="col-md-5 col-form-label">{{ config('debtkiller.attributes.annual_interest_rate.'.app()->getLocale()) }}
                                :</label>
                            <div class="col-md-7">
                                <input type="text" readonly class="form-control-plaintext" id="annualrate"
                                       value="{{ $debtKiller->annual_interest_rate }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tool"
                                   class="col-md-5 col-form-label">{{ config('debtkiller.attributes.type.'.app()->getLocale()) }}
                                :</label>
                            <div class="col-md-7">
                                <select id="type" name="type" class="form-select"
                                        onchange="changePlanType(this.value);">
                                    @foreach(config('debtkiller.'.app()->getLocale().'.type') as $key => $value)
                                        <option
                                            value="{{ $key }}" {{ $key == $debtKiller->type ? 'selected' : ''  }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="expectedduration"
                                   class="col-md-5 col-form-label">{{ config('debtkiller.attributes.expected_duration.'.app()->getLocale()) }}
                                :</label>
                            <div class="col-md-7">
                                <input type="number" {{ $debtKiller->type == 'fixed_payment' ? 'readonly' : '' }} class="form-control" name="expectedduration" id="expectedduration"
                                       placeholder="months" value="{{ $debtKiller->expected_duration }}" min="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="expectedpayment"
                                   class="col-md-5 col-form-label">{{ config('debtkiller.attributes.expected_payment.'.app()->getLocale()) }}
                                :</label>
                            <div class="col-md-7">
                                <input type="number" {{ $debtKiller->type == 'fixed_duration' ? 'readonly' : '' }} class="form-control text-danger" name="expectedpayment"
                                       id="expectedpayment" placeholder="THB"
                                       value="{{ $debtKiller->expected_payment }}">
                            </div>
                        </div>
                    </form>
                    <div class="form-group row">
                        <p></p>
                        <div class="col-md-2"></div>
                        <div class="col-md-8" style="text-align: center;">
                            <button type="button" class="btn btn-md btn-outline-danger" onclick="calculatePMT()">
                                @lang('translation.Calculate')
                            </button>
                            <button type="button" class="btn btn-md btn-danger" onclick="formSubmit()"><i
                                    class="bx bxs-bolt"></i> @lang('translation.Save_Plan')
                            </button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->

@endsection

@section('script')
    <script>
        function calculatePMT() {
            var annualRate = $('#annualrate').val();
            var futureValue = $('#outstandingbalance').val();
            if ($('#type').val() === 'fixed_payment') {
            var period = 0;
            payment = $('#expectedpayment').val();
                period = periodWithPMT(annualRate, payment, parseFloat(futureValue.replace(/,/g, '')));
                $('#expectedduration').val(period.toFixed(2));
                totalinterest = (payment * period) - parseFloat(futureValue.replace(/,/g, ''));
                document.getElementById('totalinterest').innerHTML = totalinterest.toFixed(2);
                $("input[name='totalinterest']").val(totalinterest);
            } else {
                var payment = 0;
                if (annualRate == 0) {
                    var period = $('#expectedduration').val();
                    payment = (parseFloat(futureValue.replace(/,/g, '')) - 0) / period;
                    $('#expectedpayment').val(payment.toFixed(2));
                } else {
                    var period = $('#expectedduration').val();
                    var futureValue = $('#outstandingbalance').val();
                    payment = pmtWithAnnualRate(annualRate, period, parseFloat(futureValue.replace(/,/g, '')));
                    $('#expectedpayment').val(payment.toFixed(2));
                    totalinterest = (payment * period) - parseFloat(futureValue.replace(/,/g, ''));
                    document.getElementById('totalinterest').innerHTML = totalinterest.toFixed(2);
                    $("input[name='totalinterest']").val(totalinterest);
                }
            }
        }

        function pmtWithAnnualRate(annualRate, period, futureValue) {
            monthlyRate = annualRate / 1200
            t1 = 1 + monthlyRate
            t2 = Math.pow(t1, period)
            return futureValue / ((1 - (1 / (t2))) / monthlyRate);
        }

        function periodWithPMT(annualRate, payment, futureValue) {
            monthlyRate = annualRate / 1200
            t1 = 1 + monthlyRate
            t2 = 1 / (1 - ( (monthlyRate * futureValue) / payment ))

            return Math.log(t2) / Math.log(t1);
        }

        function formSubmit() {
            /** Re-calculate Payment */
            calculatePMT();
            $("#action-plan").submit();
        }

        function changePlanType(value) {
            if (value === 'fixed_payment') {
                $("#expectedpayment").attr("readonly", false);
                $("#expectedduration").attr("readonly", true);
            } else {
                $("#expectedduration").attr("readonly", false);
                $("#expectedpayment").attr("readonly", true);
            }
        }
    </script>

@endsection
