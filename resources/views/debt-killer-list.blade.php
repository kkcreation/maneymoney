@extends('layouts.master')

@section('title') @lang('translation.Debt_Killer') @endsection
<meta name="csrf-token" content="{{ csrf_token() }}"/>
@section('css')

    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- Responsive datatable examples -->
    <link href="{{ URL::asset('/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}"
          rel="stylesheet" type="text/css"/>

    <!-- glightbox css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/glightbox/css/glightbox.min.css') }}">

@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Wealth_Creation') @endslot
        @slot('title') @lang('translation.Debt_Killer') @endslot
    @endcomponent
    @if (session('message'))
        <div class="alert {{ session('alert-class') }} alert-border-left alert-dismissible fade show" role="alert">
            <i class="mdi mdi-check-all me-3 align-middle"></i>{{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <div class="flex-shrink-0">
                        <ul class="nav justify-content-end nav-pills card-header-pills" role="tablist">
                            {{--   <ul class="nav justify-content-end nav-tabs-custom rounded card-header-tabs" role="tablist">--}}
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#debtlist" role="tab">
                                    <span class="d-block d-sm-none"><i class="bx bx-list-ol"></i></span>
                                    <span class="d-none d-sm-block">@lang('translation.List')</span>
                                </a>
                            </li>
                         {{--   <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#debtkillertimeline" role="tab">
                                    <span class="d-block d-sm-none"><i class="bx bxs-time"></i></span>
                                    <span class="d-none d-sm-block">@lang('translation.Timeline')</span>
                                </a>
                            </li>--}}
                        </ul>
                    </div>
                </div><!-- end card header -->
                <div class="card-body">
                    <div class="tab-content text-muted">
                        <div class="tab-pane active" id="debtlist" role="tabpanel">
                            <div class="row" id="debt-list">
                                <div class="row align-items-center">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">
                                        <div style="text-align: center; padding:20px;">
                                            <h5>@lang('translation.Debt_Installment')</h5>
                                            <h4 class="card-title text-danger">
                                                <i class="mdi mdi-cash-lock"></i>
                                                {{ number_format(auth()->user()->debtKillers->sum('expected_payment'),2,'.',',') }}
                                                / @lang('translation._Monthly')
                                            </h4>
                                            <h4 class="card-title text-secondary">
                                                [ @lang('translation.Total_Debt_Interest') ~ {{ number_format(auth()->user()->debtKillers->sum('total_interest_amount'),2,'.',',') }} ]
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <h5 class="card-title">@lang('translation.Debt_List') <span
                                                    class="text-muted fw-normal ms-2">( {{  $debtKillers ? $debtKillers->count() : '0' }} )</span>
                                            </h5>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="d-flex flex-wrap align-items-center justify-content-end gap-2 mb-3">
                                            <div>
                                                <a href="debt-killer-create" class="btn btn-light"><i
                                                        class="bx bx-plus me-1"></i> @lang('translation.Debt_To_Kill')
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- end row -->

                                <div class="table-responsive mb-4">
                                    <table
                                        class="table align-middle datatable dt-responsive table-check table-bordered table-sm font-size-14"
                                        style="border-collapse: collapse; width: 100%;">
                                        <thead class="table-secondary">
                                        <tr>
                                            <th scope="col" style="width: 50px;">
                                                #
                                                {{--<div class="form-check font-size-16">
                                                    <input type="checkbox" class="form-check-input" id="checkAll">
                                                    <label class="form-check-label" for="checkAll"></label>
                                                </div>--}}
                                            </th>
                                            <th scope="col">{{ config('debtkiller.attributes.creditor.'.app()->getLocale()) }}</th>
                                            <th scope="col">@lang('translation.Debt_Type')</th>
                                            <th scope="col">{{ config('debtkiller.attributes.outstanding_balance.'.app()->getLocale()) }}</th>
                                            <th scope="col">{{ config('debtkiller.attributes.expected_payment.'.app()->getLocale()) }}</th>
                                            {{--   <th scope="col">{{ config('wealthwish.attributes.time_bound.'.app()->getLocale()) }}</th>--}}
                                            <th scope="col"
                                                colspan="3">{{ config('debtkiller.attributes.expected_duration.'.app()->getLocale()) }}
                                                (@lang('translation.Year'))
                                            </th>
                                            <th scope="col">{{ config('debtkiller.attributes.expected_clearance_date.'.app()->getLocale()) }}</th>
                                           {{-- <th scope="col">{{ config('debtkiller.attributes.statement_date.'.app()->getLocale()) }}</th>--}}
                                            <th scope="col">{{ config('debtkiller.attributes.due_date.'.app()->getLocale()) }}</th>
                                            <th scope="col">{{ config('debtkiller.attributes.annual_interest_rate.'.app()->getLocale()) }}</th>
                                            <th style="width: 80px; min-width: 80px;">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            @php $count = 1; @endphp
                                            @foreach($debtKillers as $debt)
                                                {{-- <th scope="row">
                                                     <div class="form-check font-size-16">
                                                         <input type="checkbox" class="form-check-input"
                                                                id="contacusercheck1">
                                                         <label class="form-check-label" for="contacusercheck1"></label>
                                                     </div>
                                                 </th>--}}
                                                <th scope="row">
                                                    {{ $count }}
                                                </th>
                                                <td>
                                                    <a href="#" class="text-body">{{ $debt->creditor }}</a>
                                                </td>
                                                <td>{{ App::getLocale() == 'en' ? $debt->debt->description : $debt->debt->description_th }}</td>
                                                <td>{{ $debt->outstanding_balance }}</td>
                                                @php
                                                    $debtKillerService = new \App\Services\DebtKillerService(Auth::user());
                                                        $durationInMonths = $debt->expected_duration;
                                                        $timeBound = $debtKillerService->defineTimeBound($durationInMonths);
                                                @endphp
                                                <td class="text-{{ config('debtkiller.attributes.time_bound.color.'.$timeBound) }}">{{ $debt->expected_payment }}</td>
                                                <td>
                                                    <div class="d-flex gap-2">
                                                        <span
                                                            class="badge badge-soft-{{ config('debtkiller.attributes.time_bound.color.'.$timeBound) }}">
                                                            {{ config('debtkiller.'.app()->getLocale().'.time_bound.'.$timeBound) }}
                                                        </span>
                                                    </div>
                                                </td>
                                                {{--  <td>{{ round(($wish->expected_duration / 12.00) * 2) / 2 }}</td>--}}
                                                @if(floor($durationInMonths / 12) == 0)
                                                    <td style="text-align:center;">-</td>
                                                @else
                                                    <td style="text-align:center;">{{ floor($durationInMonths / 12) }}Y
                                                    </td>
                                                @endif
                                                @if($durationInMonths % 12 == 0)
                                                    <td style="text-align:center;">-</td>
                                                @else
                                                    <td style="text-align:center;">{{ $durationInMonths % 12 }}M</td>
                                                @endif
                                                <td>{{ $debt->expected_clearance_date }}</td>
                                               {{-- <td>{{ $debt->statement_date }}</td>--}}
                                                <td>{{ $debt->due_date }}</td>
                                                <td>{{ $debt->annual_interest_rate }}</td>
                                                <td>
                                                    <a class="btn btn-outline-secondary btn-sm edit" title="View Detail"
                                                       href="debt-killer-detail/{{ $debt->id }}">
                                                        <i class="fas fa-arrow-alt-circle-right"></i>
                                                    </a>
                                                    <a class="btn btn-outline-secondary btn-sm edit" title="Edit"
                                                       href="debt-killer-update/{{ $debt->id }}">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </a>
                                                    <a class="btn btn-outline-secondary btn-sm edit" title="Delete"
                                                       href="javascript: void(0);" data-bs-toggle="modal"
                                                       data-bs-target=".confirmModal" data-debt-id="{{ $debt->id }}">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </a>
                                                    {{--  <div class="dropdown">
                                                          <button
                                                              class="btn btn-link font-size-16 shadow-none py-0 text-muted dropdown-toggle"
                                                              type="button" data-bs-toggle="dropdown"
                                                              aria-expanded="false">
                                                              <i class="bx bx-dots-horizontal-rounded"></i>
                                                          </button>
                                                          <ul class="dropdown-menu dropdown-menu-end">
                                                              <li><a class="dropdown-item" href="wealth-wish-update/{{ $wish->id }}">Edit</a></li>
                                                              <li><a class="dropdown-item" href="wealth-wish-delete/{{ $wish->id }}">Delete</a>
                                                          </ul>
                                                      </div>--}}
                                                </td>
                                        </tr>
                                        @php $count++; @endphp
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <!-- end table -->
                                </div>
                                <!-- end table responsive -->
                            </div>
                        </div>
                     {{--   <div class="tab-pane" id="debtkillertimeline" role="tabpanel">
                            <div class="row" id="debt-killer-timeline">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">@lang('translation.Debt_Killer_Timeline')</h4>
                                        </div><!-- end card header -->

                                        <div class="card-body">

                                            <div class="row justify-content-center">
                                                <div class="col-xl-10">
                                                    <div class="timeline">
                                                        <div class="timeline-container">
                                                            <div class="timeline-end">
                                                                <p>Start</p>
                                                            </div>
                                                            <div class="timeline-continue">
                                                                @php $count = 1; @endphp
                                                                @foreach($wealthWishes as $wish)
                                                                    @php
                                                                        $wealthWishService = new \App\Services\WealthWishService(Auth::user());
                                                                            $durationInMonths = $wealthWishService->calculateExpectedDuration($wish->expected_achievement_date,$wish->effective_date);
                                                                            $timeBound = $wealthWishService->defineTimeBound($durationInMonths);
                                                                    @endphp
                                                                    @if($count % 2 == 1)
                                                                        <div class="row timeline-right">
                                                                            <div class="col-md-6">
                                                                                <div class="timeline-icon">
                                                                                    <i class="bx {{ config('wealthwish.attributes.category.icon.'.$wish->category) }} text-primary h2 mb-0"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="timeline-box">
                                                                                    <div
                                                                                        class="timeline-date bg-primary text-center rounded">
                                                                                        @php
                                                                                            $atAge = floor($durationInMonths / 12) + Carbon\Carbon::parse(Auth::user()->dob)->age;
                                                                                        @endphp
                                                                                        <h3 class="text-white mb-0">{{ $atAge }}</h3>
                                                                                        <p class="mb-0 text-white-50">@lang('translation.Year')</p>
                                                                                    </div>
                                                                                    <div class="event-content">
                                                                                        <div class="timeline-text">
                                                                                            <a href="/wealth-wish-detail/{{ $wish->id }}">
                                                                                                <h3 class="font-size-18">{{ $wish->title }}</h3>
                                                                                            </a>
                                                                                            <p class="mb-0 mt-2 pt-1 text-muted">
                                                                                                {{ $wish->description }}</p>
                                                                                            <p></p>
                                                                                            <div
                                                                                                class="gap-2 font-size-18">
                                                                                                <a href="#"
                                                                                                   class="badge badge-soft-primary">{{ number_format($wish->expected_amount,2,'.',',') }}
                                                                                                    / {{ config('wealthwish.'.app()->getLocale().'.amount_term.'.$wish->amount_term) }}</a>
                                                                                                <a href="#"
                                                                                                   class="badge badge-soft-primary">{{ config('wealthwish.'.app()->getLocale().'.category.'.$wish->category) }}</a>
                                                                                            </div>
                                                                                            <div
                                                                                                class="d-flex flex-wrap align-items-start event-img mt-3 gap-2">
                                                                                                @foreach($wish->wealthWishImages as $image)
                                                                                                    <a href="{{ URL::asset('/images/wealth-wish/'.$image->path) }}"
                                                                                                       class="image-popup">
                                                                                                        <img
                                                                                                            src="{{ URL::asset('/images/wealth-wish/'.$image->path) }}"
                                                                                                            alt=""
                                                                                                            class="img-fluid rounded"
                                                                                                            style="max-height:60px; width:auto;">
                                                                                                    </a>
                                                                                                @endforeach
                                                                                            </div>
                                                                                            <div class="mt-3">
                                                                                                <i class="bx bx-time"
                                                                                                   title="Effective Date"></i> {{ $wish->effective_date }}
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                        <div class="row timeline-left">
                                                                            <div class="col-md-6 d-md-none d-block">
                                                                                <div class="timeline-icon">
                                                                                    <i class="bx {{ config('wealthwish.attributes.category.icon.'.$wish->category) }} text-primary h2 mb-0"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="timeline-box">
                                                                                    <div
                                                                                        class="timeline-date bg-primary text-center rounded">
                                                                                        @php
                                                                                            $atAge = floor($durationInMonths / 12) + Carbon\Carbon::parse(Auth::user()->dob)->age;
                                                                                        @endphp
                                                                                        <h3 class="text-white mb-0">{{ $atAge }}</h3>
                                                                                        <p class="mb-0 text-white-50">@lang('translation.Year')</p>
                                                                                    </div>
                                                                                    <div class="event-content">
                                                                                        <div class="timeline-text">
                                                                                            <a href="/wealth-wish-detail/{{ $wish->id }}">
                                                                                                <h3 class="font-size-18">{{ $wish->title }}</h3>
                                                                                            </a>
                                                                                            <p class="mb-0 mt-2 pt-1 text-muted">
                                                                                                {{ $wish->description }}</p>
                                                                                            <p></p>
                                                                                            <div
                                                                                                class="gap-2 font-size-18">
                                                                                                <a href="#"
                                                                                                   class="badge badge-soft-primary">{{ number_format($wish->expected_amount,2,'.',',') }}
                                                                                                    / {{ config('wealthwish.'.app()->getLocale().'.amount_term.'.$wish->amount_term) }}</a>
                                                                                                <a href="#"
                                                                                                   class="badge badge-soft-primary">{{ config('wealthwish.'.app()->getLocale().'.category.'.$wish->category) }}</a>
                                                                                            </div>
                                                                                            <div
                                                                                                class="d-flex flex-wrap align-items-start event-img mt-3 gap-2">
                                                                                                @foreach($wish->wealthWishImages as $image)
                                                                                                    <a href="{{ URL::asset('/images/wealth-wish/'.$image->path) }}"
                                                                                                       class="image-popup">
                                                                                                        <img
                                                                                                            src="{{ URL::asset('/images/wealth-wish/'.$image->path) }}"
                                                                                                            alt=""
                                                                                                            class="img-fluid rounded"
                                                                                                            style="max-height:60px; width:auto;">
                                                                                                    </a>
                                                                                                @endforeach
                                                                                            </div>
                                                                                            <div class="mt-3">
                                                                                                <i class="bx bx-time"
                                                                                                   title="Effective Date"></i> {{ $wish->effective_date }}
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 d-md-block d-none">
                                                                                <div class="timeline-icon">
                                                                                    <i class="bx {{ config('wealthwish.attributes.category.icon.'.$wish->category) }} text-primary h2 mb-0"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                    @php $count++; @endphp
                                                                @endforeach
                                                            </div>
                                                            <div class="timeline-start">
                                                                <p>End</p>
                                                            </div>
                                                            <div class="timeline-launch">
                                                                <div class="timeline-box">
                                                                    <div class="timeline-text">
                                                                        <h3 class="font-size-18">Rest In Peace</h3>
                                                                        <i class="bx bx-ghost text-primary h2 mb-0"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end card body -->
                                    </div>
                                    <!-- end card -->
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <!-- Modal -->
    <div id="deleteConfirmationModal" class="modal fade confirmModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="mb-3">
                            <i class="bx bx-check-circle display-4 text-success"></i>
                        </div>
                        <h5>@lang('translation.Confirm_Delete')</h5>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <input type="hidden" value="" name="debtid">
                    <button type="button" class="btn btn-light w-md"
                            data-bs-dismiss="modal">@lang('translation.Close')</button>
                    <button type="button" class="btn btn-danger w-md" data-bs-dismiss="modal"
                            onclick="confirmDelete()">
                        @lang('translation.Confirm_Changes')
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
@endsection
@section('script')

    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Responsive examples -->
    <script src="{{ URL::asset('/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script
        src="{{ URL::asset('/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatable-pages.init.js') }}"></script>

    <!-- glightbox js -->
    <script src="{{ URL::asset('/assets/libs/glightbox/js/glightbox.min.js') }}"></script>

    <!-- lightbox init -->
    <script src="{{ URL::asset('/assets/js/pages/lightbox.init.js') }}"></script>

    <script>
        function confirmDelete() {
            var debtkillerid = $('input[name="debtid"]').val();
            let token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'post',
                url: '{{ url('/deletedebtdata') }}',
                data: {debtkillerid: debtkillerid, _token: token},
                dataType: 'html',
                success: function (data) {
                    window.location.href = '/debt-killer-list';
                }
            })
        }

        $('#deleteConfirmationModal').on('show.bs.modal', function (e) {

            //get data-id attribute of the clicked element
            var debtId = $(e.relatedTarget).data('debt-id');

            //populate the textbox
            $(e.currentTarget).find('input[name="debtid"]').val(debtId);
        });

    </script>
@endsection
