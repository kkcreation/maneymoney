@extends('layouts.master')

@section('title') @lang('translation.File_Upload') @endsection

@section('css')

    <!-- dropzone css -->
    <link href="{{ URL::asset('/assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Forms @endslot
        @slot('title') File Upload @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Dropzone</h4>
                    <p class="card-title-desc">DropzoneJS is an open source library
                        that provides drag’n’drop file uploads with image previews.
                    </p>
                </div>
                <div class="card-body">

                    <div>
                        <form action="{{ route('dropzone.store') }}" method="post" enctype="multipart/form-data"
                              id="image-upload" class="dropzone">
                            @csrf
                            <div class="fallback">
                                <input name="file" type="file" multiple="multiple">
                            </div>
                            <div class="dz-message needsclick">
                                <div class="mb-3">
                                    <i class="display-4 text-muted bx bx-cloud-upload"></i>
                                </div>

                                <h5>Drop files here or click to upload.</h5>
                            </div>
                        </form>
                    </div>

                    {{-- <div class="text-center mt-4">
                         <button type="submit" class="btn btn-primary waves-effect waves-light">Send
                             Files</button>
                     </div>--}}
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')

    <!-- dropzone js -->
    <script src="{{ URL::asset('/assets/libs/dropzone/min/dropzone.min.js') }}"></script>
    <script>
        Dropzone.options.imageUpload = {
            addRemoveLinks: true,
            autoProcessQueue: false,
            parallelUploads: 3,
            maxFiles: 3,
            maxFilesize: 3,
            acceptedFiles: ".jpeg,.jpg,.png,.gif"
        };
    </script>
@endsection
