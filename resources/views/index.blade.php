@extends('layouts.master')

@section('title') @lang('translation.Dashboard') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Dashboard') @endslot
        @slot('title') @lang('translation.Dashboard') @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header">
                    @if(app()->getLocale() == 'en')
                        <h4 class="card-title">Welcome K.{{ Auth::user()->first_name }} ..</h4>
                        <p class="card-title-desc"><i class="bx bx-right-arrow"></i> For your perfect start of <code class="highlighter-rouge">financial planning</code>, we recommend you to begin studying <code class="highlighter-rouge"> Financial Pyramid </code> attentively and
                            adhere to this model as <code class="highlighter-rouge"> your roadmap of wealth</code>. <br><b class="text-secondary">HelloMuch</b> will <code class="highlighter-rouge">walk you through</code> those steps with our <code class="highlighter-rouge"> amazing tools</code> so that you can reach your own <code class="highlighter-rouge">financial goals</code> effectively.</p>
                    @else
                    <h4 class="card-title">ยินดีต้อนรับ คุณ {{ Auth::user()->first_name }} ..</h4>
                        <p class="card-title-desc"><i class="bx bx-right-arrow"></i> เพื่อการเริ่มต้น <code class="highlighter-rouge">วางแผนการเงิน</code> ที่สมบูรณ์แบบ เราขอแนะนำให้คุณเริ่มต้นศึกษา<code class="highlighter-rouge"> พีระมิดการเงิน </code> อย่างตั้งใจ
                            และใช้โมเดลนี้เป็นต้นแบบ <code class="highlighter-rouge">เส้นทางสู่ความมั่งคั่ง</code> <br><b class="text-secondary">HelloMuch</b> จะเป็นเพื่อนที่ <code class="highlighter-rouge">คอยใส่ใจและนำทาง</code> ให้คุณทำความเข้าใจในแต่ละขั้นตอนด้วย <code class="highlighter-rouge">เครื่องมือสนับสนุนสุดพิเศษ </code> ของเรา เพื่อบรรลุ <code class="highlighter-rouge">เป้าหมายทางการเงิน </code> ของคุณเอง</p>
                    @endif
                </div><!-- end card header -->

                <div class="card-body">
                    <div>
                        @if(app()->getLocale() == 'en')
                            <img src="{{ URL::asset('/assets/images/financial-pyramid-en.png') }}" class="img-fluid"
                                 alt="Responsive image">
                        @else
                            <img src="{{ URL::asset('/assets/images/financial-pyramid-th.png') }}" class="img-fluid"
                                 alt="Responsive image">
                        @endif
                    </div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div><!-- end row -->
@endsection
