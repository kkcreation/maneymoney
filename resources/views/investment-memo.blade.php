@extends('layouts.master')

@section('title') @lang('translation.Investment_Memo') @endsection
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="lang" content="{{ App::getLocale() }}">

@section('css')
    <link href="{{ URL::asset('/assets/libs/@fullcalendar/core/main.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/assets/libs/@fullcalendar/daygrid/main.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/assets/libs/@fullcalendar/bootstrap/main.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('/assets/libs/@fullcalendar/timegrid/main.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Wealth_Accumulation') @endslot
        @slot('title') @lang('translation.Investment_Memo') @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">

            <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            {{--  <div class="d-grid">
                                  <button class="btn font-16 btn-primary" id="btn-new-event"><i class="mdi mdi-plus-circle-outline"></i> Create
                                      New Event</button>
                              </div>

                              <div id="external-events" class="mt-2">
                                  <br>
                                  <p class="text-muted">Drag and drop your event or click in the calendar</p>
                                  <div class="external-event fc-event text-success bg-soft-success" data-class="bg-success">
                                      <i class="mdi mdi-checkbox-blank-circle font-size-11 me-2"></i>New Event Planning
                                  </div>
                                  <div class="external-event fc-event text-info bg-soft-info" data-class="bg-info">
                                      <i class="mdi mdi-checkbox-blank-circle font-size-11 me-2"></i>Meeting
                                  </div>
                                  <div class="external-event fc-event text-warning bg-soft-warning" data-class="bg-warning">
                                      <i class="mdi mdi-checkbox-blank-circle font-size-11 me-2"></i>Generating Reports
                                  </div>
                                  <div class="external-event fc-event text-danger bg-soft-danger" data-class="bg-danger">
                                      <i class="mdi mdi-checkbox-blank-circle font-size-11 me-2"></i>Create New theme
                                  </div>
                                  <div class="external-event fc-event text-dark bg-soft-dark" data-class="bg-dark">
                                      <i class="mdi mdi-checkbox-blank-circle font-size-11 me-2"></i>Team Meeting
                                  </div>
                              </div>--}}

                            <div class="row justify-content-center mt-5">
                                <div class="col-lg-12 col-sm-6">
                                    <img src="assets/images/undraw-calendar.svg" alt="" class="img-fluid d-block">
                                </div>
                            </div>

                        </div>
                    </div>
                </div> <!-- end col-->

                <div class="col-xl-9 col-lg-8">
                    <div class="card">
                        <div class="card-body">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div> <!-- end col -->

            </div>

            <div style='clear:both'></div>


            <!-- Add New Event MODAL -->
            <div class="modal fade" id="event-modal" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header py-3 px-4 border-bottom-0">
                            <h5 class="modal-title" id="modal-title">Event</h5>

                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>

                        </div>
                        <div class="modal-body p-4">
                            <form class="needs-validation" name="event-form" id="form-event" novalidate>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Tool')</label>
                                            <select class="form-control form-select" name="asset_id" id="event-asset">
                                                <option selected>--@lang('translation.Select')--</option>
                                                @php $liquidAssets = \App\Models\Asset::whereIn('type',['liquid_asset'])->get(); @endphp
                                                <option class="dropdown-header text-secondary" disabled>---------- @lang('translation.Liquid_Asset') ----------</option>
                                                @foreach($liquidAssets as $asset)
                                                    <option value="{{ $asset->id }}">
                                                        {{ App::getLocale() == 'en' ? $asset->description : $asset->description_th }}
                                                    </option>
                                                @endforeach
                                                @php $investmentAssets = \App\Models\Asset::whereIn('type',['investment_asset'])->get(); @endphp
                                                <option class="dropdown-header text-secondary" disabled>---------- @lang('translation.Investment_Asset') ----------</option>
                                                @foreach($investmentAssets as $asset)
                                                    <option value="{{ $asset->id }}">
                                                        {{ App::getLocale() == 'en' ? $asset->description : $asset->description_th }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Please select a valid asset</div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Target_Wealth_Wish')</label>
                                            <select class="form-control form-select" name="wealth_wish_id"
                                                    id="event-wealth-wish">
                                                <option value="" selected> --@lang('translation.Select')--</option>
                                                @foreach(Auth::user()->wealthWishes as $wealthWish)
                                                    <option
                                                        value="{{ $wealthWish->id }}">{{ $wealthWish->title }}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Please select a valid wealth wish</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Action')</label>
                                            <select class="form-control form-select" name="event_action"
                                                    id="event-action">
                                                <option selected>--@lang('translation.Select')--</option>
                                                <option value="deposit"><i class="mdi mdi-download text-primary"></i> @lang('translation.Deposit')</option>
                                                <option value="withdraw"><i class="mdi mdi-upload text-secondary"></i> @lang('translation.Withdraw')</option>
                                                <option value="buy"><i class="mdi mdi-download text-success"></i> @lang('translation.Buy')</option>
                                                <option value="sell"><i class="mdi mdi-upload text-danger"></i> @lang('translation.Sell')</option>
                                            </select>
                                            <div class="invalid-feedback">Please provide a valid event action</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Asset_Name')</label>
                                            <input class="typeahead form-control" placeholder="" type="text"
                                                   name="event_title" data-provide="typeahead" id="event-title" required autocomplete="off" value=""/>
                                            <div class="invalid-feedback">Please provide a valid event tag</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Price') <i class=""
                                                                               id="fetch-price"
                                                                               data-bs-toggle="tooltip"
                                                                               data-bs-placement="top"
                                                                               title="Click here to fetch NAV of the day"></i></label>
                                            <input class="form-control" placeholder="" type="number"
                                                   name="price" id="price" step="0.00001" required value=""/>
                                            <div class="invalid-feedback">Please provide a valid price</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Unit')</label>
                                            <input class="form-control" placeholder="" type="number"
                                                   name="unit" id="unit" step="0.00000000000001" required value=""/>
                                            <div class="invalid-feedback">Please provide a valid unit</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Conversion_Rate') </label>
                                            <input class="form-control" placeholder=""
                                                   type="number" name="conversion_rate" id="conversion-rate"
                                                   step="0.00001" value=""/>
                                            <div class="invalid-feedback">Please provide a valid conversion rate</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Fee')</label>
                                            <input class="form-control" placeholder="" type="number"
                                                   name="fee" id="fee" step="0.00000000000001" value=""/>
                                            <div class="invalid-feedback">Please provide a valid fee</div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Memo')</label>
                                            <input class="form-control" placeholder="" type="text"
                                                   name="event_tag" id="event-tag" required value=""/>
                                            <div class="invalid-feedback">Please provide a valid memo</div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row mt-2">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-danger" id="btn-delete-event">@lang('translation.Delete')
                                        </button>
                                    </div>
                                    <div class="col-6 text-end">
                                        <button type="button" class="btn btn-light me-1" data-bs-dismiss="modal">@lang('translation.Close')
                                        </button>
                                        <button type="submit" class="btn btn-success" id="btn-save-event">@lang('translation.Save_Changes')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> <!-- end modal-content-->
                </div> <!-- end modal dialog-->
            </div>
            <!-- end modal-->

        </div>
    </div>

@endsection

@section('script')

  {{--  <script src="{{ URL::asset('/assets/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/feather-icons/feather.min.js') }}"></script>--}}
    <!-- pace js -->
    <script src="{{ URL::asset('/assets/libs/pace-js/pace.min.js') }}"></script>

    <script src="{{ URL::asset('/assets/libs/@fullcalendar/core/main.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/@fullcalendar/bootstrap/main.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/@fullcalendar/daygrid/main.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/@fullcalendar/timegrid/main.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/@fullcalendar/interaction/main.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <!-- Calendar init -->
    <script src="{{ URL::asset('/assets/js/pages/financial-calendar.init.js') }}"></script>

@endsection
