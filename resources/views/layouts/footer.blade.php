<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © HelloMuch!
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    Design & Develop by <a href="https://www.facebook.com/maneymoney.ofcl" class="text-decoration-underline">ManeyMoney</a>
                </div>
            </div>
        </div>
    </div>
</footer>
