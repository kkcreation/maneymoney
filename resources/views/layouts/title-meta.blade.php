<meta charset="utf-8" />
<title>@@title | HelloMuch! </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="HelloMuch! Your Wealth Care Companion" name="description" />
<meta content="ManeyMoney" name="author" />
<!-- App favicon -->
<link rel="shortcut icon" href="assets/images/mnmn-logo.svg">
