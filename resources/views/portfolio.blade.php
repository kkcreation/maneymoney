@extends('layouts.master')

@section('title') @lang('translation.Portfolio') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Wealth_Accumulation') @endslot
        @slot('title') @lang('translation.Portfolio') @endslot
    @endcomponent
    <div class="row text-center">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1"><i class="mdi mdi-bank-plus"></i> @lang('translation.Total_Investment_Asset')</h4>
                </div><!-- end card header -->

                <div class="card-body bg-soft-secondary">
                    <h3 class="mt-1 text-secondary">
                        @php
                            $totalInvestmentAsset = 0;
                            $totalExpected = 0;
                            $averagePercentage = 0;
                        @endphp
                        @foreach($investmentMemo as $key => $item)
                            @php
                                $wealthWish = \App\Models\WealthWish::find($key);
                                $portfolioService = new \App\Services\PortfolioService(Auth::user());
                                $data = $portfolioService->getInvestmentSummary($item);
                                $grandTotalCostValue = 0;
                                foreach($data->toArray() as $value){
                                    $grandTotalCostValue += $value['summary']['total_cost_value'];
                                }
                                $totalInvestmentAsset += $grandTotalCostValue;
                                $totalExpected += $wealthWish->expected_amount;
                                $averagePercentage = round(divnum($totalInvestmentAsset, $totalExpected) * 100,2);
                            @endphp
                        @endforeach
                    {{ number_format($totalInvestmentAsset,0,'.',',') }} / {{ number_format($totalExpected,0,'.',',') }}
                    </h3>{{ $averagePercentage }}%
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>
    <div class="row">
    {{-- <div class="col-xl-6">
         <div class="card">
             <div class="card-header">
                 <h4 class="card-title mb-0">Doughnut Chart</h4>
             </div>
             <div class="card-body">
                 <div id="doughnut-chart" data-colors='["#5156be", "#ffbf53", "#fd625e", "#4ba6ef", "#2ab57d"]' class="e-charts"></div>
             </div>
         </div>
         <!-- end card -->
     </div>--}}
    <!-- end col -->
        @foreach($investmentMemo as $key => $item)
            @php
                $wealthWish = \App\Models\WealthWish::find($key);
                $portfolioService = new \App\Services\PortfolioService(Auth::user());
                $data = $portfolioService->getInvestmentSummary($item);
                $grandTotalCostValue = 0;
                foreach($data->toArray() as $value){
                    $grandTotalCostValue += $value['summary']['total_cost_value'];
                }
                $progressPercentage = round(divnum($grandTotalCostValue, $wealthWish->expected_amount) * 100,2);
            @endphp
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                <a href="/wealth-wish-detail/{{ $key }}">
                                    <h4 class="mb-0 text-size-18"><i
                                            class="bx {{ config('wealthwish.attributes.category.icon.'.$wealthWish->category) }} text-primary"></i> {{ $wealthWish->title }}
                                    </h4>
                                </a>
                                    <div class="mt-2 text-secondary gap-2 font-size-16">
                                    @if(App::getLocale() == 'en')
                                            <span class="badge badge-soft-secondary">{{ $wealthWish->asset->description }}</span> <span class="badge bg-success">{{ $wealthWish->expected_annual_rate }}@lang('translation.Percent_Annually') </span>
                                        @else
                                            <span class="badge badge-soft-secondary">{{ $wealthWish->asset->description_th }}</span> <span class="badge bg-success">{{ $wealthWish->expected_annual_rate }}@lang('translation.Percent_Annually') </span>
                                        @endif
                                    </div>
                            </div>
                            <div class="col-md-4">
                                <h4 class="mt-1">
                                    <i class="bx bxs-purchase-tag"></i> {{ number_format($wealthWish->expected_amount,2,'.',',') }}
                                </h4>
                                <div class="progress progress-xl">
                                    <div class="progress-bar bg-success" role="progressbar"
                                         style="width: {{$progressPercentage}}%;"
                                         aria-valuenow="{{ $grandTotalCostValue }}" aria-valuemin="0"
                                         aria-valuemax="{{ $wealthWish->expected_amount }}">{{$progressPercentage}}%
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="goal-{{ $key }}"
                             data-colors='["#fd625e","#ff938f", "#2ab57d","#77cca4", "#4ba6ef", "#85c1f4", "#ffbf53", "#ffd28a", "#5156be", "#8589d2", "#74788d", "#dcdde2"]'
                             class="e-charts"></div>
                    </div>
                </div>
                <!-- end card -->
            </div>
    @endforeach
    <!-- end col -->
    </div>
    <!-- end row -->

@endsection


@section('script')
    <!-- echarts js -->
    <script src="{{ URL::asset('/assets/libs/echarts/echarts.min.js') }}"></script>
    <!-- echarts init -->
    <script src="{{ URL::asset('/assets/js/pages/portfolio.init.js') }}"></script>

@endsection
