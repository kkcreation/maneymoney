@extends('layouts.master')

@section('title') @lang('translation.Wizard') @endsection

@section('css')

<!-- twitter-bootstrap-wizard css -->
<link rel="stylesheet" href="{{ URL::asset('/assets/libs/twitter-bootstrap-wizard/prettify.css') }}">

@endsection

@section('content')
    @if (session('message'))
        <div class="alert {{ session('alert-class') }} alert-border-left alert-dismissible fade show" role="alert">
            <i class="mdi mdi-check-all me-3 align-middle"></i>{{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
@component('components.breadcrumb')
@slot('li_1') @lang('translation.Profile') @endslot
@slot('title') @lang('translation.Profile') @endslot
@endcomponent

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-0">@lang('translation.User_Profile')</h4>
            </div>
            <div class="card-body">
                <div id="progrss-wizard" class="twitter-bs-wizard">
                    <ul class="twitter-bs-wizard-nav nav nav-pills nav-justified">
                        <li class="nav-item">
                            <a href="#progress-profile-details" class="nav-link" data-toggle="tab">
                                <div class="step-icon" data-bs-toggle="tooltip" data-bs-placement="top" title="Profile Details">
                                    <i class="bx bx-list-ul"></i>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- wizard-nav -->

                 <!--   <div id="bar" class="progress mt-4">
                        <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"></div>
                    </div> -->
                    <div class="tab-content twitter-bs-wizard-tab-content">
                        <div class="tab-pane" id="progress-profile-details">
                            <div class="text-center mb-4">
                                <h5>@lang('translation.Profile_Details')</h5>
                                <p class="card-title-desc">@lang('translation.Fill_all_information_below')</p>
                            </div>
                            <form method="post" action="{{ route('updateProfile',['id'=> Auth::user()->id]) }}" id="user-profile">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token ?? '' }}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="progresspill-firstname-input">@lang('translation.First_Name')</label>
                                            <input name="first_name" type="text" class="form-control" id="progresspill-firstname-input"
                                                   value="{{ Auth::user()->first_name }}" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="progresspill-lastname-input">@lang('translation.Last_Name')</label>
                                            <input name="last_name" type="text" class="form-control" id="progresspill-lastname-input" value="{{ Auth::user()->last_name }}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Gender')</label>
                                            <select name="gender"  class="form-select">
                                                @foreach(config('hellomuch.gender') as $key => $value)
                                                    <option value="{{ $key }}" {{ $key == Auth::user()->gender ? 'selected' : ''  }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('translation.Marital_Status')</label>
                                            <select name="marital_status"  class="form-select">
                                                @foreach(config('hellomuch.marital_status') as $key => $value)
                                                <option value="{{ $key }}" {{ $key == Auth::user()->marital_status ? 'selected' : ''  }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                         <!--       <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <label for="progresspill-address-input">Address</label>
                                            <textarea id="progresspill-address-input" class="form-control" rows="2"></textarea>
                                        </div>
                                    </div>
                                </div> -->
                         <!--   </form>
                            <ul class="pager wizard twitter-bs-wizard-pager-link">
                                <li class="next"><a href="javascript: void(0);" class="btn btn-primary" onclick="nextTab()">Next <i class="bx bx-chevron-right ms-1"></i></a></li>
                            </ul> -->
                               <!-- <button class="btn btn-primary w-100 waves-effect waves-light" data-bs-toggle="modal" data-bs-target=".confirmModal">Update</button> -->
                            </form>
                            <ul class="pager wizard twitter-bs-wizard-pager-link">
                             <!--   <li class="previous"><a href="javascript: void(0);" class="btn btn-primary" onclick="nextTab()"><i class="bx bx-chevron-left me-1"></i> Previous</a></li> -->
                                <li class="float-end"><a href="javascript: void(0);" class="btn btn-primary" data-bs-toggle="modal" data-bs-target=".confirmModal">Save
                                        Changes</a></li>
                            </ul>
                        </div>
                       <!-- <div class="tab-pane" id="progress-personal-income-expense">
                            <div>
                                <div class="text-center mb-4">
                                    <h5>Personal Income-Expense</h5>
                                    <p class="card-title-desc">Fill all information below</p>
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-pancard-input" class="form-label">PAN Card</label>
                                                <input type="text" class="form-control" id="progresspill-pancard-input">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-vatno-input" class="form-label">VAT/TIN No.</label>
                                                <input type="text" class="form-control" id="progresspill-vatno-input">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-cstno-input" class="form-label">CST No.</label>
                                                <input type="text" class="form-control" id="progresspill-cstno-input">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-servicetax-input" class="form-label">Service Tax No.</label>
                                                <input type="text" class="form-control" id="progresspill-servicetax-input">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-companyuin-input" class="form-label">Company UIN</label>
                                                <input type="text" class="form-control" id="progresspill-companyuin-input">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-declaration-input" class="form-label">Declaration</label>
                                                <input type="text" class="form-control" id="progresspill-declaration-input">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <ul class="pager wizard twitter-bs-wizard-pager-link">
                                    <li class="previous"><a href="javascript: void(0);" class="btn btn-primary" onclick="nextTab()"><i class="bx bx-chevron-left me-1"></i> Previous</a></li>
                                    <li class="next"><a href="javascript: void(0);" class="btn btn-primary" onclick="nextTab()">Next <i class="bx bx-chevron-right ms-1"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane" id="progress-personal-balance-sheet">
                            <div>
                                <div class="text-center mb-4">
                                    <h5>Personal Balance Sheet</h5>
                                    <p class="card-title-desc">Fill all information below</p>
                                </div>
                                <form>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-namecard-input" class="form-label">Name on Card</label>
                                                <input type="text" class="form-control" id="progresspill-namecard-input">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label class="form-label">Credit Card Type</label>
                                                <select class="form-select">
                                                    <option selected>Select Card Type</option>
                                                    <option value="AE">American Express</option>
                                                    <option value="VI">Visa</option>
                                                    <option value="MC">MasterCard</option>
                                                    <option value="DI">Discover</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-cardno-input" class="form-label">Credit Card Number</label>
                                                <input type="text" class="form-control" id="progresspill-cardno-input">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-card-verification-input" class="form-label">Card Verification Number</label>
                                                <input type="text" class="form-control" id="progresspill-card-verification-input">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-3">
                                                <label for="progresspill-expiration-input" class="form-label">Expiration Date</label>
                                                <input type="text" class="form-control" id="progresspill-expiration-input">
                                            </div>
                                        </div>

                                    </div>
                                </form>
                                <ul class="pager wizard twitter-bs-wizard-pager-link">
                                    <li class="previous"><a href="javascript: void(0);" class="btn btn-primary" onclick="nextTab()"><i class="bx bx-chevron-left me-1"></i> Previous</a></li>
                                    <li class="float-end"><a href="javascript: void(0);" class="btn btn-primary" data-bs-toggle="modal" data-bs-target=".confirmModal">Save
                                            Changes</a></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>
<!-- end row -->

<!-- End Page-content -->
<!-- Modal -->
<div class="modal fade confirmModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="mb-3">
                        <i class="bx bx-check-circle display-4 text-success"></i>
                    </div>
                    <h5>Confirm Save Changes</h5>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-light w-md" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary w-md" data-bs-dismiss="modal" onclick="formSubmit()">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal -->
@endsection

@section('script')

<!-- twitter-bootstrap-wizard js -->
<script src="{{ URL::asset('/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/twitter-bootstrap-wizard/prettify.js') }}"></script>

<!-- form wizard init -->
<script src="{{ URL::asset('/assets/js/pages/form-wizard.init.js') }}"></script>
<script>
   function formSubmit(){
       $( "#user-profile" ).submit();
   }
</script>
@endsection
