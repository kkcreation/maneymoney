@extends('layouts.master')

@section('title') @lang('translation.Wealth_Dashboard') @endsection

@section('css')

    <!-- plugin css -->
    <link href="{{ URL::asset('/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}"
          rel="stylesheet" type="text/css"/>
    <!-- datepicker css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.css') }}">
    <!-- flatpickr plugin -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/plugins/monthSelect/style.css') }}">

@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Pinned') @endslot
        @slot('title') @lang('translation.Wealth_Dashboard') @endslot
    @endcomponent
    @php
        if($data){
            $income = round($data['survival_ratio']['income'],0);
            $debtInstallment = round($data['installment_to_income']['installment'],0);
            $expense = round($data['expense_to_income']['personal_expense'],0);
            $insurancePremium = round($data['insurance_premium_to_income']['insurance_premium'],0);
            $savingInvestment = round($data['saving_to_income']['saving_investment'],0);
            $cashflow = $income - ($debtInstallment + $expense + $savingInvestment);
            $savingPercentage = round((divnum($data['saving_to_income']['saving_investment'], $data['survival_ratio']['income'])) * 100,2);
            $installmentPercentage = round((divnum($data['installment_to_income']['installment'], $data['survival_ratio']['income'])) * 100,2);
            $personalExpensePercentage = round((divnum(($data['expense_to_income']['personal_expense'] - $data['insurance_premium_to_income']['insurance_premium']), $data['survival_ratio']['income'])) * 100, 2);
            $insurancePremiumPercentage = round((divnum($data['insurance_premium_to_income']['insurance_premium'], $data['survival_ratio']['income'])) * 100, 2);
            $cashflowPercentage = number_format((divnum($cashflow, $income)) * 100,2);
            $investmentToNetWealth = round($data['investment_to_net_wealth']['value'],2);
            $investmentToNetWealthBenchmark = round($data['investment_to_net_wealth']['benchmark']['index'],0);

            $personalExpenseBenchmark = $data['expense_to_income']['benchmark']['index'];
            $personalExpenseScore = 0;
            if ($personalExpensePercentage <= $personalExpenseBenchmark) {
                $personalExpenseScore = 100;
            }

            $insurancePremiumBenchmarkMin = $data['insurance_premium_to_income']['benchmark']['index_min'];
            $insurancePremiumBenchmarkMax = $data['insurance_premium_to_income']['benchmark']['index_max'];

            $insurancePremiumScore = 0;
            if ($insurancePremiumPercentage >= $insurancePremiumBenchmarkMin && $insurancePremiumPercentage <= $insurancePremiumBenchmarkMax) {
                $insurancePremiumScore = 100;
            }

            $debtInstallmentBenchmark = $data['installment_to_income']['benchmark']['index'];
            $debtInstallmentScore = 0;
            if ($installmentPercentage <= $debtInstallmentBenchmark) {
                $debtInstallmentScore = 100;
            }

            $savingToIncomeBenchmarkMin = $data['saving_to_income']['benchmark']['index_min'];
            $savingToIncomeBenchmarkSuggested = $data['saving_to_income']['benchmark']['index_suggested'];
            $savingInvestmentScore = $savingPercentage * (100 / $savingToIncomeBenchmarkMin);
            if ($savingPercentage > $savingToIncomeBenchmarkMin) {
                $savingInvestmentScore = 100;
            }
            $investmentToNetWealthScore = $investmentToNetWealth * (100 / $investmentToNetWealthBenchmark);
            if ($investmentToNetWealthScore > 100) {
                $investmentToNetWealthScore = 100;
            } else if ($investmentToNetWealthScore < 0) {
                $investmentToNetWealthScore = 0;
            }

            $debtToAsset = round($data['debt_to_asset']['value'],2);
            $debtToAssetBenchmark = round($data['debt_to_asset']['benchmark']['index'],0);
            $debtToAssetScore = 0;
            if ($debtToAsset <= $debtToAssetBenchmark) {
                $debtToAssetScore = 100;
            }

            $reservedMoneyRatio = $data['reserved_money_to_expense']['value'];
            $reservedMoneyPercentage = $data['reserved_money_to_expense']['reserved_money_percentage'];
            $reservedMoneyScore = $reservedMoneyPercentage > 100 ? 100 : $reservedMoneyPercentage;
            $survivalScore = ($data['survival_ratio']['value'] / 1.0) * 100;
            if($survivalScore > 100){
                $survivalScore = 100;
            }

            $wealthScore = ($data['wealth_ratio']['value'] / 1.5) * 100;
            if($wealthScore > 100){
                $wealthScore = 100;
            }
            $overallWealthScore = number_format((($personalExpenseScore + $insurancePremiumScore + $debtInstallmentScore + $reservedMoneyScore + $savingInvestmentScore + $investmentToNetWealthScore + $debtToAssetScore) + ($survivalScore) + ($wealthScore)) / 9.0, 2);
            $netCashflow = $data['survival_ratio']['income'] - ($data['installment_to_income']['installment'] + $data['expense_to_income']['personal_expense'] + $data['saving_to_income']['saving_investment']);
            $netWealth = $data['investment_to_net_wealth']['net_wealth'];
            }
    @endphp
    <div class="row">
        <div class="col-lg-8">
            <!-- card -->
            <div class="card bg-primary text-white shadow-primary card-h-100">
                <!-- card body -->
                <div class="card-body p-0">
                    <div id="carouselExampleCaptions" class="carousel slide text-center widget-carousel"
                         data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="text-center p-3">
                                    <i class="mdi mdi-heart-pulse widget-box-1-icon"></i>
                                    {{-- <i data-feather="activity"></i>--}}
                                    {{--     <div class="avatar-md m-auto">
                                                     <span
                                                         class="avatar-title rounded-circle bg-soft-light text-white font-size-24">
                                                         <i class="mdi mdi-heart-pulse"></i>
                                                     </span>
                                         </div>--}}
                                    <p class="text-white-50 font-size-13"> @if($data) @lang('translation.Wealth_Analysis_Desc') @else @lang('translation.Wealth_Analysis_Pls_Create') @endif </p>
                                    <h4 class="mt-3 lh-base fw-normal text-white">
                                        @if($data)
                                        <button type="button" class="btn btn-light btn-sm"
                                                data-bs-toggle="modal"
                                                data-bs-target="#wealthAnalysis">@lang('translation.See_Result')
                                            <i
                                                class="fas fa-arrow-alt-circle-right text-success"></i></button>
                                        @else
                                            <a href="/wealth-profile/{{ $date }}" type="button" class="btn btn-light btn-sm">@lang('translation.Create_New_Financial_Stm')
                                                <i
                                                    class="fas fa-arrow-alt-circle-right text-success"></i></a>
                                        @endif
                                        {{-- <a class="btn btn-sm text-white"
                                            data-bs-toggle="modal"
                                            data-bs-target="#wealthAnalysis"
                                            title="@lang('translation.See_Result')"
                                            href="debt-killer-detail/1">
                                             <i class="fas fa-arrow-alt-circle-right"></i> @lang('translation.Wealth_Analysis')
                                         </a>--}}
                                    </h4>
                                    {{--  <button type="button" class="btn btn-light btn-sm"
                                              data-bs-toggle="modal"
                                              data-bs-target="#wealthAnalysis">@lang('translation.See_Result')
                                          <i
                                              class="mdi mdi-arrow-right ms-1"></i></button>--}}
                                </div>
                            </div>
                            <!-- end carousel-item -->
                        {{--  <div class="carousel-item">
                              <div class="text-center p-4">
                                  <i class="mdi mdi-ethereum widget-box-1-icon"></i>
                                  <div class="avatar-md m-auto">
                                      <span
                                          class="avatar-title rounded-circle bg-soft-light text-white font-size-24">
                                          <i class="mdi mdi-heart-pulse"></i>
                                      </span>
                                  </div>
                                  <h4 class="mt-3 lh-base fw-normal text-white"><b>ManeyMoney</b> News</h4>
                                  <p class="text-white-50 font-size-13"> Alternative coin prices fell sharply amid
                                      the global sell-off in
                                      equities. Negative news over the past week has dampened sentiment
                                      for bitcoin. </p>
                                  <button type="button" class="btn btn-light btn-sm">View details <i
                                          class="mdi mdi-arrow-right ms-1"></i></button>
                              </div>
                          </div>--}}
                        <!-- end carousel-item -->
                        </div>
                        <!-- end carousel-inner -->

                    {{-- <div class="carousel-indicators carousel-indicators-rounded">
                         <button type="button" data-bs-target="#carouselExampleCaptions"
                                 data-bs-slide-to="0" class="active"
                                 aria-current="true" aria-label="Slide 1"></button>
                         <button type="button" data-bs-target="#carouselExampleCaptions"
                                 data-bs-slide-to="1" aria-label="Slide 2"></button>
                     </div>--}}
                    <!-- end carousel-indicators -->
                    </div>
                    <!-- end carousel -->
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
        <div class="col-lg-4">
            <div class="mb-3">
                <div class="input-group">
                    <div class="input-group-text">
                        <i class="bx bxs-calendar text-primary font-size-24"></i>
                    </div>
                    <input class="form-control text-center text-primary flatpickr-input" type="text" name="submission_date"
                           id="submission-date"
                           placeholder="SUBMISSION DATE"
                           value="{{ $date }}"
                           required data-input> <!-- input is mandatory -->
                </div>
            </div>
            <div class="mb-3">
                <div class="input-group">
                    <div class="input-group-text">
                        <i class="mdi mdi-bullseye-arrow text-primary font-size-24"></i>
                    </div>
                    <input class="form-control text-center text-primary fw-bold font-size-20" type="text"
                           value="{{ $overallWealthScore ?? 0}}%"
                           required data-input readonly><!-- input is mandatory -->
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    @if($data)
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header align-items-center d-flex">
                        <h4 class="card-title mb-0 flex-grow-1">@lang('translation.Wealth_Profile')</h4>
                        <div class="flex-shrink-0">
                            <ul class="nav justify-content-end nav-pills card-header-pills" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#personalstatement"
                                       role="tab">
                                        <span class="d-block d-sm-none"><i class="bx bx-money"></i></span>
                                        <span
                                            class="d-none d-sm-block">@lang('translation.Personal_Statement')</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#balancesheet" role="tab">
                                        <span class="d-block d-sm-none"><i class="bx bxs-bank"></i></span>
                                        <span
                                            class="d-none d-sm-block">@lang('translation.Personal_Balance_Sheet')</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div><!-- end card header -->

                    <div class="card-body">
                        <!-- Tab panes -->
                        <div class="tab-content text-muted">
                            <div class="tab-pane active" id="personalstatement" role="tabpanel">
                                <div class="row" id="personal-statement">
                                    <div class="col-lg-3 col-md-12">
                                        <div class="card bg-success border-success text-white-50">
                                            <div class="card-body">
                                                <h6 class="mb-4 text-white"><i
                                                        class="mdi mdi-cash-plus me-3"></i>@lang('translation.Income_Uppercase')
                                                </h6>
                                                <p class="card-text"
                                                   id="income-summary">{{ number_format($data['survival_ratio']['income'],2) }}</p>
                                            </div>
                                        </div>
                                    </div><!-- end col -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="card bg-danger border-danger text-white-50">
                                            <div class="card-body">
                                                <h6 class="mb-4 text-white"><i
                                                        class="mdi mdi-cash-minus me-3"></i>@lang('translation.Expense_Uppercase')
                                                </h6>
                                                <p class="card-text"
                                                   id="expense-summary">{{ number_format($data['survival_ratio']['expense'],2) }}</p>
                                            </div>
                                        </div>
                                    </div><!-- end col -->
                                    <div class="col-lg-3 col-md-6">
                                        <div class="card bg-warning border-warning text-white-50">
                                            <div class="card-body ">
                                                <h6 class="mb-4 text-white"><i
                                                        class="mdi mdi-cash-lock me-3"></i>@lang('translation.Savings_Investment_Uppercase')
                                                </h6>
                                                <p class="card-text"
                                                   id="saving-summary">{{ number_format($data['saving_to_income']['saving_investment'],2) }}</p>
                                            </div>
                                        </div>
                                    </div><!-- end col -->
                                    <div class="col-lg-3 col-md-12">
                                        <div class="card text-black-50">
                                            <div class="card-body ">
                                                <h6 class="mb-4 text-black"><i
                                                        class="mdi mdi-cash-check me-3"></i>@lang('translation.Net_Cash_Flow_Uppercase')
                                                </h6>
                                                @if($netCashflow < 0)
                                                    <h5 class="card-text text-danger"
                                                        id="cash-flow-summary">{{ number_format($netCashflow,2) }}</h5>
                                                @else
                                                    <h5 class="card-text text-success" id="cash-flow-summary">
                                                        +{{ number_format($netCashflow,2) }}</h5>
                                                @endif

                                            </div>
                                        </div>
                                    </div><!-- end col -->
                                    <div class="col-lg-6 col-md-12">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Survival_Ratio')<hr></span>
                                                        <h4 class="mb-3">
                                <span class="counter-value"
                                      data-target="{{ $data['survival_ratio']['value'] }}">0</span>
                                                        </h4>
                                                    </div>
                                                    <div class="col-5">
                                                        @php
                                                            $survivalRatioAnalysis = null;
                                                        @endphp
                                                        @if($data['survival_ratio']['value'] >= 2)
                                                            <button
                                                                class="btn btn-success waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-heart-eyes bx-sm bx-tada-hover label-icon text-white"></i> @lang('translation.Excellent')
                                                            </button>
                                                            @php
                                                                $survivalRatioAnalysis = __('translation.Excellent');
                                                                $survivalRatioStyle = 'success';
                                                                $survivalMathSymbol = '>';
                                                            @endphp
                                                        @elseif($data['survival_ratio']['value'] >= 1 && $data['survival_ratio']['value'] < 2)
                                                            <button
                                                                class="btn btn-success waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-alt bx-sm bx-tada-hover label-icon text-white"></i> @lang('translation.Good')
                                                            </button>
                                                            @php
                                                                $survivalRatioAnalysis = __('translation.Good');
                                                                $survivalRatioStyle = 'success';
                                                                $survivalMathSymbol = '>=';
                                                            @endphp
                                                        @elseif($data['survival_ratio']['value'] < 1)
                                                            <button
                                                                class="btn btn-danger waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-confused bx-sm bx-tada-hover label-icon text-white"></i> @lang('translation.Poor')
                                                            </button>
                                                            @php
                                                                $survivalRatioAnalysis = __('translation.Poor');
                                                                $survivalRatioStyle = 'danger';
                                                                $survivalMathSymbol = '<';
                                                            @endphp
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                <span
                                                    class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['survival_ratio']['benchmark']['label'] }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col -->

                                    <div class="col-lg-6 col-md-12">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Wealth_Ratio')<hr></span>
                                                        <h4 class="mb-3">
                                                        <span class="counter-value"
                                                              data-target="{{ $data['wealth_ratio']['value'] }}">0</span>
                                                        </h4>
                                                    </div>
                                                    <div class="col-5">
                                                        @php $wealthRatioAnalysis = null; @endphp
                                                        @if($data['wealth_ratio']['value'] >= 2)
                                                            <button
                                                                class="btn btn-success waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-heart-eyes bx-sm bx-tada-hover label-icon text-white"></i> @lang('translation.Excellent')
                                                            </button>
                                                            @php
                                                                $wealthRatioAnalysis = __('translation.Excellent');
                                                                $wealthRatioStyle = 'success';
                                                                $wealthMathSymbol = '>';
                                                            @endphp
                                                        @elseif($data['wealth_ratio']['value'] >= 1.5 && $data['wealth_ratio']['value'] < 2)
                                                            <button
                                                                class="btn btn-success waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-alt bx-sm bx-tada-hover label-icon text-white"></i> @lang('translation.Good')
                                                            </button>
                                                            @php
                                                                $wealthRatioAnalysis = __('translation.Good');
                                                                $wealthRatioStyle = 'success';
                                                                $wealthMathSymbol = '>=';
                                                            @endphp
                                                        @elseif($data['wealth_ratio']['value'] >= 0.5 && $data['wealth_ratio']['value'] < 1.5)
                                                            <button
                                                                class="btn btn-warning waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-smile bx-sm bx-tada-hover label-icon text-white"></i> @lang('translation.Average')
                                                            </button>
                                                            @php
                                                                $wealthRatioAnalysis = __('translation.Average');
                                                                $wealthRatioStyle = 'warning';
                                                                $wealthMathSymbol = '<';
                                                            @endphp
                                                        @elseif($data['wealth_ratio']['value'] < 0.5)
                                                            <button
                                                                class="btn btn-danger waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-confused bx-sm bx-tada-hover label-icon text-white"></i> @lang('translation.Poor')
                                                            </button>
                                                            @php
                                                                $wealthRatioAnalysis = __('translation.Poor');
                                                                $wealthRatioStyle = 'danger';
                                                                $wealthMathSymbol = '<';
                                                            @endphp
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                <span
                                                    class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['wealth_ratio']['benchmark']['label']  }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col-->
                                </div>
                            </div>
                            <div class="tab-pane" id="balancesheet" role="tabpanel">
                                <div class="row" id="balance-sheet">
                                    <div class="col-lg-4 col-md-6">
                                        <div class="card bg-primary border-primary text-white-50">
                                            <div class="card-body">
                                                <h6 class="mb-4 text-white"><i
                                                        class="mdi mdi-bank-plus me-3"></i>@lang('translation.Asset_Uppercase')
                                                </h6>
                                                <p class="card-text"
                                                   id="asset-summary">{{ number_format($data['debt_to_asset']['asset'],2) }}</p>
                                            </div>
                                        </div>
                                    </div><!-- end col -->
                                    <div class="col-lg-4 col-md-6">
                                        <div class="card bg-secondary border-secondary text-white-50">
                                            <div class="card-body">
                                                <h6 class="mb-4 text-white"><i
                                                        class="mdi mdi-bank-minus me-3"></i>@lang('translation.Debt_Uppercase')
                                                </h6>
                                                <p class="card-text"
                                                   id="debt-summary">{{ number_format($data['debt_to_asset']['debt'],2) }}</p>
                                            </div>
                                        </div>
                                    </div><!-- end col -->
                                    <div class="col-lg-4 col-md-12">
                                        <div class="card text-black-50">
                                            <div class="card-body ">
                                                <h6 class="mb-4 text-black"><i
                                                        class="mdi mdi-bank-check me-3"></i>@lang('translation.Net_Wealth_Uppercase')
                                                </h6>
                                                @if($netWealth < 0)
                                                    <h5 class="card-text text-danger"
                                                        id="net-wealth-summary">{{ number_format($netWealth,2) }}</h5>
                                                @else
                                                    <h5 class="card-text text-success" id="net-wealth-summary">
                                                        +{{ number_format($netWealth,2) }}</h5>
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- end col -->
                                    <div class="col-lg-6 col-md-12">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Investment_to_Wealth')<hr></span>
                                                        <h4 class="mb-3">
                                <span class="counter-value"
                                      data-target="{{ $data['investment_to_net_wealth']['value'] }}">0</span>%
                                                        </h4>
                                                    </div>
                                                    <div class="col-5">
                                                        @php $investmentAssetAnalysis = null; @endphp
                                                        @if($data['investment_to_net_wealth']['value'] <= 0)
                                                            <button
                                                                class="btn btn-secondary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-confused bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Poor')
                                                            </button>
                                                            @php
                                                                $investmentAssetAnalysis = __('translation.Poor');
                                                                $investmentToNetWealthMathSymbol = '<';
                                                                $investmentToNetWealthStyle = 'secondary';
                                                            @endphp
                                                        @elseif($data['investment_to_net_wealth']['value'] < 25 && $data['investment_to_net_wealth']['value'] >= 0)
                                                            <button
                                                                class="btn btn-primary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-smile bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Average')
                                                            </button>
                                                            @php
                                                                $investmentAssetAnalysis = __('translation.Average');
                                                                $investmentToNetWealthMathSymbol = '<';
                                                                  $investmentToNetWealthStyle = 'primary';
                                                            @endphp
                                                        @elseif($data['investment_to_net_wealth']['value'] < 50 && $data['investment_to_net_wealth']['value'] >= 25)
                                                            <button
                                                                class="btn btn-primary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-alt bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Good')
                                                            </button>
                                                            @php
                                                                $investmentAssetAnalysis = __('translation.Good');
                                                                $investmentToNetWealthMathSymbol = '<';
                                                                  $investmentToNetWealthStyle = 'primary';
                                                            @endphp
                                                        @elseif($data['investment_to_net_wealth']['value'] >= 50)
                                                            <button
                                                                class="btn btn-primary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-heart-eyes bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Excellent')
                                                            </button>
                                                            @php
                                                                $investmentAssetAnalysis = __('translation.Excellent');
                                                                $investmentToNetWealthMathSymbol = '>=';
                                                                $investmentToNetWealthStyle = 'primary';
                                                            @endphp
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                <span
                                                    class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['investment_to_net_wealth']['benchmark']['label']  }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col -->
                                    <div class="col-lg-6 col-md-12">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-7">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Debt_to_Asset')<hr></span>
                                                        <h4 class="mb-3">
                                                        <span class="counter-value"
                                                              data-target="{{ $data['debt_to_asset']['value'] }}">0</span>%
                                                        </h4>
                                                    </div>
                                                    <div class="col-5">
                                                        @php $debToAssetAnalysis = null; @endphp
                                                        @if($data['debt_to_asset']['value'] <= 25)
                                                            <button
                                                                class="btn btn-primary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-heart-eyes bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Excellent')
                                                            </button>
                                                            @php
                                                                $debtToAssetAnalysis = __('translation.Excellent');
                                                                $debtToAssetStyle = 'primary';
                                                            @endphp
                                                        @elseif($data['debt_to_asset']['value'] <= 50 && $data['debt_to_asset']['value'] > 25)
                                                            <button
                                                                class="btn btn-primary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-happy-alt bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Good')
                                                            </button>
                                                            @php
                                                                $debtToAssetAnalysis = __('translation.Good');
                                                                $debtToAssetStyle = 'primary';
                                                            @endphp
                                                        @elseif($data['debt_to_asset']['value'] <= 75 && $data['debt_to_asset']['value'] > 50)
                                                            <button
                                                                class="btn btn-primary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-smile bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Average')
                                                            </button>
                                                            @php
                                                                $debtToAssetAnalysis = __('translation.Average');
                                                                $debtToAssetStyle = 'primary';
                                                            @endphp
                                                        @elseif($data['debt_to_asset']['value'] <= 100 && $data['debt_to_asset']['value'] > 75)
                                                            <button
                                                                class="btn btn-secondary waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-confused bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Poor')
                                                            </button>
                                                            @php
                                                                $debtToAssetAnalysis = __('translation.Poor');
                                                                $debtToAssetStyle = 'secondary';
                                                            @endphp
                                                        @elseif($data['debt_to_asset']['value'] > 100)
                                                            <button
                                                                class="btn btn-dark waves-effect btn-label waves-light btn-lg">
                                                                <i class="bx bx-tired bx-sm bx-tada-hover label-icon text-white"></i>@lang('translation.Very_Poor')
                                                            </button>
                                                            @php
                                                                $debtToAssetAnalysis = __('translation.Very_Poor');
                                                                $debtToAssetStyle = 'dark';
                                                            @endphp
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                <span
                                                    class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['debt_to_asset']['benchmark']['label']  }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col -->
                                </div>
                            </div>
                            {{--    <div class="row">
                                    <div class="col-lg-3 col-md-6">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-8">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Survival_Ratio')<hr></span>
                                                        <h4 class="mb-3">
                                        <span class="counter-value"
                                              data-target="{{ $data['survival_ratio']['value'] }}">0</span>
                                                        </h4>
                                                    </div>
                                                    <div class="col-4">
                                                        @if($data['survival_ratio']['value'] >= 2)
                                                          <i class="bx bx-happy-heart-eyes bx-lg bx-tada-hover label-icon text-success"></i>
                                                        @elseif($data['survival_ratio']['value'] >= 1 && $data['survival_ratio']['value'] < 2)
                                                            <i class="bx bx-happy-alt bx-lg bx-tada-hover label-icon text-success"></i>
                                                        @elseif($data['survival_ratio']['value'] < 1)
                                                            <i class="bx bx-confused bx-lg bx-tada-hover label-icon text-danger"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                    <span class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['survival_ratio']['benchmark']['label'] }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col -->
                                    <div class="col-lg-3 col-md-6">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-8">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Wealth_Ratio')<hr></span>
                                                        <h4 class="mb-3">
                                                            <span class="counter-value" data-target="{{ $data['wealth_ratio']['value'] }}">0</span>
                                                        </h4>
                                                    </div>
                                                    <div class="col-4">
                                                        @if($data['wealth_ratio']['value'] >= 2)
                                                            <i class="bx bx-happy-heart-eyes bx-lg bx-tada-hover label-icon text-success"></i>
                                                        @elseif($data['wealth_ratio']['value'] >= 1 && $data['wealth_ratio']['value'] < 2)
                                                          <i class="bx bx-happy-alt bx-lg bx-tada-hover label-icon text-success"></i>
                                                        @elseif($data['wealth_ratio']['value'] >= 0.5 && $data['wealth_ratio']['value'] < 1)
                                                         <i class="bx bx-smile bx-md bx-tada-hover label-icon text-warning"></i>
                                                        @elseif($data['wealth_ratio']['value'] < 0.5)
                                                           <i class="bx bx-confused bx-lg bx-tada-hover label-icon text-danger"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                    <span class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['wealth_ratio']['benchmark']['label']  }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col-->
                                    <div class="col-lg-3 col-md-6">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-8">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Investment_to_Wealth')<hr></span>
                                                        <h4 class="mb-3">
                                        <span class="counter-value"
                                              data-target="{{ $data['investment_to_net_wealth']['value'] }}">0</span>%
                                                        </h4>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="col-4">
                                                            @if($data['investment_to_net_wealth']['value'] <= 0)
                                                                <i class="bx bx-confused bx-lg bx-tada-hover label-icon text-secondary"></i>
                                                            @elseif($data['investment_to_net_wealth']['value'] < 25 && $data['investment_to_net_wealth']['value'] >= 0)
                                                                <i class="bx bx-smile bx-lg bx-tada-hover label-icon text-primary"></i>
                                                            @elseif($data['investment_to_net_wealth']['value'] < 50 && $data['investment_to_net_wealth']['value'] >= 25)
                                                                <i class="bx bx-happy-alt bx-lg bx-tada-hover label-icon text-primary"></i>
                                                            @elseif($data['investment_to_net_wealth']['value'] >= 50)
                                                                <i class="bx bx-happy-heart-eyes bx-lg bx-tada-hover label-icon text-primary"></i>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                    <span class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['investment_to_net_wealth']['benchmark']['label']  }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col -->
                                    <div class="col-lg-3 col-md-6">
                                        <!-- card -->
                                        <div class="card card-h-100">
                                            <!-- card body -->
                                            <div class="card-body">
                                                <div class="row align-items-center">
                                                    <div class="col-8">
                                                        <span class="text-muted mb-3 lh-2 d-block">@lang('translation.Debt_to_Asset')<hr></span>
                                                        <h4 class="mb-3">
                                                            <span class="counter-value" data-target="{{ $data['debt_to_asset']['value'] }}">0</span>%
                                                        </h4>
                                                    </div>
                                                    <div class="col-4">
                                                        @if($data['debt_to_asset']['value'] <= 25)
                                                           <i class="bx bx-happy-heart-eyes bx-lg bx-tada-hover label-icon text-primary"></i>
                                                        @elseif($data['debt_to_asset']['value'] <= 50 && $data['debt_to_asset']['value'] > 25)
                                                           <i class="bx bx-happy-alt bx-lg bx-tada-hover label-icon text-primary"></i>
                                                        @elseif($data['debt_to_asset']['value'] <= 75 && $data['debt_to_asset']['value'] > 50)
                                                            <i class="bx bx-smile bx-lg bx-tada-hover label-icon text-primary"></i>
                                                        @elseif($data['debt_to_asset']['value'] <= 100 && $data['debt_to_asset']['value'] > 75)
                                                            <i class="bx bx-confused bx-lg bx-tada-hover label-icon text-secondary"></i>
                                                        @elseif($data['debt_to_asset']['value'] > 100)
                                                            <i class="bx bx-tired bx-lg bx-tada-hover label-icon text-dark"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="text-nowrap">
                                                    <span class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ $data['debt_to_asset']['benchmark']['label']  }}</span>
                                                </div>
                                            </div><!-- end card body -->
                                        </div><!-- end card -->
                                    </div><!-- end col -->
                                </div>--}}
                        </div>
                    </div><!-- end card-body -->
                </div><!-- end card -->
            </div><!-- end col -->
        </div><!-- end row-->
        <div class="row">
            <div class="col-xl-5">
                <!-- card -->
                <div class="card card-h-100">
                    <!-- card body -->
                    <div class="card-body">
                        <div class="d-flex flex-wrap align-items-center mb-4">
                            <h5 class="card-title me-2">@lang('translation.Cash_Flow_Balance')</h5>
                        </div>

                        <div class="row align-items-center">
                            <div class="col-sm align-self-center">
                                <div class="row g-0">
                                    <div class="col-12">
                                        <div id="cash-flow-balance"
                                             data-colors='["#a8aada", "#777aca", "#5156be","#2ab57d"]'
                                             class="apex-charts">

                                        </div>
                                    </div>
                                </div>
                                {{--  <div class="row g-0">
                                      <div class="col-6">
                                          <div>
                                              <p class="mb-2 text-muted text-uppercase font-size-11">@lang('translation.Income')</p>
                                              <h4>฿ {{ number_format($data['survival_ratio']['income'],0,',') }}</h4>
                                          </div>
                                          <hr>
                                      </div>
                                      <div class="col-6">
                                          <div>
                                              <p class="mb-2 text-muted text-uppercase font-size-11">@lang('translation.Cash_Flow')</p>
                                              <h4>
                                                  ฿ {{ number_format($data['survival_ratio']['income'] - ($data['installment_to_income']['installment'] + $data['expense_to_income']['personal_expense'] + $data['saving_to_income']['saving_investment']),0,',') }}</h4>
                                          </div>
                                          <hr>
                                      </div>
                                  </div>--}}
                                <hr>
                                <div class="row g-0">
                                    <div class="col-12">
                                        <div id="cashflow-balance" class="apex-charts">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end card -->
            <!-- end col -->
            <div class="col-xl-7">
                <div class="row">
                    <div class="col-xl-12">
                        <!-- card -->
                        <div class="card card-h-100">
                            <!-- card body -->
                            <div class="card-body">
                                <div class="d-flex flex-wrap align-items-center mb-4">
                                    <h5 class="card-title me-2">@lang('translation.Reserved_Money')</h5>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm">
                                        <div id="reserved-money-overview" data-colors='["#5156be", "#34c38f"]'
                                             class="apex-charts">
                                        </div>
                                    </div>
                                    <div class="col-sm align-self-center">
                                        <div class="mt-4 mt-sm-0">
                                            <p class="mb-1">@lang('translation.Reserved_Money_Amount')</p>
                                            <h4>
                                                ฿ {{ number_format($data['reserved_money_to_expense']['reserved_money'],0,',') }}</h4>
                                            <div class="mt-4 mt-sm-0">
                                                <div>
                                                    <hr>
                                                    <p class="mb-2"><i
                                                            class="mdi mdi-circle align-middle font-size-10 me-2 text-success"></i> @lang('translation.Suggested_Amount')
                                                    </p>
                                                    <h6><span class="text-muted font-size-14 fw-normal">฿ {{ number_format($data['reserved_money_to_expense']['expense'],0,',') }} x 6 >></span>
                                                        ฿ {{ number_format($data['reserved_money_to_expense']['expense'] * 6,0,',') }}
                                                    </h6>
                                                    <hr>
                                                    <span
                                                        class="ms-1 text-muted font-size-13">@lang('translation.Benchmark')</span>
                                                    <span
                                                        class="badge bg-soft-success text-success">{{ App::getLocale() == 'en' ? $data['reserved_money_to_expense']['benchmark']['label'] : '>= 6 เท่าของรายจ่าย' }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="wealthAnalysis"
                             tabindex="-1" role="dialog"
                             aria-labelledby="wealthAnalysisTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="wealthAnalysisTitle">
                                            <img
                                                src="{{ URL::asset('/assets/images/mnmn-logo.svg') }}"
                                                alt=""
                                                height="25"> @lang('translation.Wealth_Analysis_Title')
                                        </h5>
                                        <button type="button" class="btn-close"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                    </div>
                                    @if(app()->getLocale() == 'en')
                                        @php
                                            $minimumSavingsNeeded = round(auth()->user()->wealthWishes->where('amount_term','fixed')->sum('calculated_payment'),2);
                                                if($savingInvestment < $minimumSavingsNeeded){
                                                    $textSavingsGap = 'which is lower than savings suggested.';
                                                    $savingsGapPercentage = number_format(divnum($savingInvestment - $minimumSavingsNeeded, $savingInvestment) * 100, 2);
                                                    $savingsGapStyle = 'danger';
                                                    $savingsGap = $savingInvestment - $minimumSavingsNeeded;
                                                } else {
                                                    $textSavingsGap = 'which is higher than savings suggested.';
                                                    $savingsGapPercentage = number_format(divnum($savingInvestment - $minimumSavingsNeeded, $savingInvestment) * 100, 2);
                                                    $savingsGapStyle = 'success';
                                                    $savingsGap = $savingInvestment - $minimumSavingsNeeded;
                                                }
                                                $dcaPercentage = round(divnum($minimumSavingsNeeded,$income) * 100,2);
                                                $savingStandard = round(divnum($savingToIncomeBenchmarkSuggested * $income, 100), 2);
                                                $expectedIncomeBySavings = round(divnum($minimumSavingsNeeded * 100, 20),2);
                                                $expectedIncomeBySavings30 = round(divnum($minimumSavingsNeeded * 100, 30),2);
                                        if($dcaPercentage >= $savingToIncomeBenchmarkSuggested){
                                            $additionalText = 'Your total income, however, must be boosted-up by ';
                                        }else{
                                            $additionalText= '';
                                        }
                                        @endphp
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <p><i>Hi K. {{ Auth::user()->first_name }}!</i>
                                            </p>
                                            <p>We are glad to deliver your wealth health
                                                check
                                                result...</p>
                                            <div class="card border border-primary">
                                                <div
                                                    class="card-header bg-transparent border-primary">
                                                    <h5 class="my-0 text-primary"><i
                                                            class="mdi mdi-bullseye-arrow me-3"></i>Your
                                                        overall wealth score: <span
                                                            class="badge badge-soft-primary text-primary font-size-16">{{ $overallWealthScore }}%</span>
                                                        {{-- <b class="text-secondary">%</b>--}}
                                                    </h5>
                                                </div>
                                            </div>
                                            <div
                                                class="card bg-transparent border-{{ $savingsGapStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $savingsGapStyle }}">
                                                    <h5 class="my-0 text-{{ $savingsGapStyle }}">
                                                        <i
                                                            class="mdi mdi-cash-lock me-3"></i>Savings
                                                        Needed:
                                                        <span
                                                            class="badge bg-{{ $savingsGapStyle }} font-size-16">
                                                                                <b>
                                                                                    {{ number_format(auth()->user()->wealthWishes->where('amount_term','fixed')->sum('calculated_payment'),2,'.',',') }} THB / @lang('translation._Monthly')
                                                                                </b>
                                                                            </span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <p class="card-text">Your current
                                                        savings &
                                                        investment per month is
                                                        <span
                                                            class="badge bg-soft-{{ $savingsGapStyle }} text-{{ $savingsGapStyle }} font-size-14">
                                                                                {{ number_format($savingInvestment,2) }} THB
                                                                            </span>
                                                        <span
                                                            class="badge bg-{{ $savingsGapStyle }} font-size-14">
                                                                                {{  number_format($savingsGap,2) }} THB
                                                                            </span>
                                                        <b class="text-{{ $savingsGapStyle }}">
                                                            {{ $textSavingsGap }}
                                                        </b>
                                                    </p>
                                                    <p>
                                                        For achieving
                                                        <b class="text-{{ $savingsGapStyle }}">
                                                            <a class="text-{{ $savingsGapStyle }}"
                                                               href="/wealth-wish-list">
                                                                <u>WEALTH WISHES</u>.
                                                            </a>
                                                        </b>{{ $additionalText }}
                                                        @if(!empty($additionalText))
                                                            <span
                                                                class="badge bg-soft-{{ $savingsGapStyle }} text-{{ $savingsGapStyle }} font-size-14">
                                                                                {{ $expectedIncomeBySavings30 - $income <= 0 ? 0 : number_format($expectedIncomeBySavings30 - $income,2) }} - {{ number_format($expectedIncomeBySavings - $income,2) }} THB
                                                                            </span> in order to remain <b>savings &
                                                                investment level</b> at
                                                            <span
                                                                class="badge bg-{{ $savingsGapStyle }} font-size-14">
                                                                                    {{ $savingToIncomeBenchmarkSuggested }} - 30 %</span>
                                                            with balance portions of other
                                                            spending
                                                            aspects including Personal
                                                            Expenses,
                                                            Debt Installments and Insurance
                                                            Premium.
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @php
                                            $minimumSavingsNeeded = round(auth()->user()->wealthWishes->where('amount_term','fixed')->sum('calculated_payment'),2);
                                                  if($savingInvestment < $minimumSavingsNeeded){
                                                      $textSavingsGap = 'ซึ่งน้อยกว่าจำนวนเงินเก็บออมที่แนะนำ ';
                                                      $savingsGapPercentage = number_format(divnum($savingInvestment - $minimumSavingsNeeded, $savingInvestment) * 100, 2);
                                                      $savingsGapStyle = 'danger';
                                                      $savingsGap = $savingInvestment - $minimumSavingsNeeded;
                                                  } else {
                                                      $textSavingsGap = 'ซึ่งมากกว่าจำนวนเงินเก็บออมที่แนะนำ ';
                                                      $savingsGapPercentage = number_format(divnum($savingInvestment - $minimumSavingsNeeded, $savingInvestment) * 100, 2);
                                                      $savingsGapStyle = 'success';
                                                      $savingsGap = $savingInvestment - $minimumSavingsNeeded;
                                                  }
                                                  $dcaPercentage = round(divnum($minimumSavingsNeeded,$income) * 100,2);
                                                  $savingStandard = round(divnum($savingToIncomeBenchmarkSuggested * $income, 100), 2);
                                                  $expectedIncomeBySavings = round(divnum($minimumSavingsNeeded * 100, 20),2);
                                                  $expectedIncomeBySavings30 = round(divnum($minimumSavingsNeeded * 100, 30),2);
                                          if($dcaPercentage >= $savingToIncomeBenchmarkSuggested){
                                              $additionalText = 'อย่างไรก็ดีควรเพิ่มรายรับโดยประมาณ ';
                                          }else{
                                              $additionalText= '';
                                          }
                                        @endphp
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <p><i>สวัสดี คุณ {{ Auth::user()->first_name }}
                                                    !</i></p>
                                            <p>
                                                เรายินดีที่จะส่งมอบผลการตรวจสุขภาพการเงินของคุณ...</p>
                                            <div class="card border border-primary">
                                                <div
                                                    class="card-header bg-transparent border-primary">
                                                    <h5 class="my-0 text-primary"><i
                                                            class="mdi mdi-bullseye-arrow me-3"></i>คะแนนความมั่งคั่งของคุณ:
                                                        <span
                                                            class="badge badge-soft-primary text-primary font-size-16">{{ $overallWealthScore }}%</span>
                                                        {{-- <b class="text-secondary">%</b>--}}
                                                    </h5>
                                                </div>
                                            </div>
                                            <div
                                                class="card bg-transparent border-{{ $savingsGapStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $savingsGapStyle }}">
                                                    <h5 class="my-0 text-{{ $savingsGapStyle }}">
                                                        <i
                                                            class="mdi mdi-cash-lock me-3"></i>เงินเก็บออมขั้นต่ำที่แนะนำ:
                                                        <span
                                                            class="badge bg-{{ $savingsGapStyle }} font-size-16"><b>
                                                                                    {{ number_format(auth()->user()->wealthWishes->where('amount_term','fixed')->sum('calculated_payment'),2,'.',',') }} บาท / @lang('translation._Monthly')
                                                                                </b>
                                                                            </span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <p class="card-text">
                                                        จำนวนเงินเก็บออมและลงทุนปัจจุบันของคุณคือ
                                                        <span
                                                            class="badge bg-soft-{{ $savingsGapStyle }} text-{{ $savingsGapStyle }} font-size-14">
                                                                                {{ number_format($savingInvestment,2) }} บาท
                                                                            </span>
                                                        <b class="text-{{ $savingsGapStyle }}"> {{ $textSavingsGap }} </b><span
                                                            class="badge bg-{{ $savingsGapStyle }} font-size-14">
                                                                                {{ number_format($savingsGap,2) }} บาท
                                                                            </span></p>
                                                    <p>เพื่อการพิชิต
                                                        <b class="text-{{ $savingsGapStyle }}">
                                                            <a class="text-{{ $savingsGapStyle }}"
                                                               href="/wealth-wish-list">
                                                                <u>รายการความฝัน</u>ของคุณ
                                                            </a>
                                                        </b>{{ $additionalText }}
                                                        @if(!empty($additionalText))
                                                            <span
                                                                class="badge bg-soft-{{ $savingsGapStyle }} text-{{ $savingsGapStyle }} font-size-14">
                                                                                    {{ $expectedIncomeBySavings30 - $income <= 0 ? 0 : number_format($expectedIncomeBySavings30 - $income,2) }} - {{ number_format($expectedIncomeBySavings - $income,2) }} บาท
                                                                                </span> เพื่อรักษา<b>ระดับการเก็บออมและลงทุนต่อเดือน</b>
                                                            ที่ <span
                                                                class="badge bg-{{ $savingsGapStyle }} font-size-14">
                                                                                    {{ $savingToIncomeBenchmarkSuggested }} - 30 %</span>
                                                            และคงไว้ซึ่งสัดส่วนที่เหมาะสม
                                                            สำหรับการจัดการค่าใช้จ่ายในด้านอื่นๆ
                                                            ได้แก่ ค่าใช้จ่ายส่วนตัว,
                                                            การผ่อนชำระหนี้ฯ และ
                                                            การชำระเบี้ยประกันฯ
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light"
                                                data-bs-dismiss="modal">@lang('translation.Close')</button>
                                        <button class="btn btn-primary"
                                                data-bs-target="#personalStatementAnalysis"
                                                data-bs-toggle="modal"
                                                data-bs-dismiss="modal">@lang('translation.Next')</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <div class="modal fade" id="personalStatementAnalysis"
                             tabindex="-1" role="dialog"
                             aria-labelledby="personalStatementAnalysisTitle"
                             aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="personalStatementAnalysisTitle">
                                            <img
                                                src="{{ URL::asset('/assets/images/mnmn-logo.svg') }}"
                                                alt=""
                                                height="25"> @lang('translation.Wealth_Analysis_Title')
                                        </h5>
                                        <button type="button" class="btn-close"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                    </div>
                                    @if(app()->getLocale() == 'en')
                                        @php
                                            if($netCashflow < 0){
                                                $textNetCashflow = 'cannot cover your monthly expenses with lacking ';
                                                $NetCashflowStyle = 'danger';
                                            } else {
                                                $textNetCashflow = 'can cover your monthly expenses with remaining ';
                                                $NetCashflowStyle = 'success';
                                            }

                                            if($data['survival_ratio']['value'] < 1){
                                                $textSurvivalRatio = 'lower than the benchmark';
                                                $diffPercentage = number_format(($data['survival_ratio']['value'] - 1) * 100, 2);
                                                $diffPercentageStyle = 'danger';
                                            } else {
                                                $textSurvivalRatio = 'higher than the benchmark';
                                                $diffPercentage = number_format(($data['survival_ratio']['value'] - 1) * 100, 2);
                                                $diffPercentageStyle = 'success';
                                            }
                                            if($data['wealth_ratio']['value'] < 1.5){
                                                $textWealthRatio = 'lower than the benchmark';
                                                $diffWealthPercentage = number_format(($data['wealth_ratio']['value'] - 1.5) * 100, 2);
                                                $diffWealthPercentageStyle = 'danger';
                                         } else {
                                                $textWealthRatio = 'higher than the benchmark';
                                                $diffWealthPercentage = number_format(($data['wealth_ratio']['value'] - 1.5) * 100, 2);
                                                $diffWealthPercentageStyle = 'success';
                                         }

                                        $wealthCover = number_format(($data['wealth_ratio']['value'] / 1.0) * 100,2);
                                         if($wealthCover < 100){
                                             $textPassiveIncomeAnalysis = 'We are sad to say that your Passive income is still not enough';
                                             $wealthCoverStyle = 'danger';
                                             $onlyPassive = 'only';
                                         }else{
                                              $textPassiveIncomeAnalysis = 'We are glad to say that your Passive income is already enough';
                                              $wealthCoverStyle = 'success';
                                               $onlyPassive = '';
                                         }
                                        @endphp
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <div
                                                class="card bg-transparent border-{{ $diffPercentageStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $diffPercentageStyle }}">
                                                    <h5 class="my-0 text-{{ $diffPercentageStyle }}">
                                                        Survival Ratio: <span
                                                            class="badge bg-{{ $survivalRatioStyle }} font-size-16"><b>{{ $data['survival_ratio']['value'] }}</b></span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">Your survival
                                                        index
                                                        represents <b
                                                            class="text-{{ $survivalRatioStyle }} font-size-16"><b>{{ $survivalRatioAnalysis }}</b></b>
                                                        survival standard</h5>
                                                    <p class="card-text">Your total income (
                                                        Active
                                                        + Passive
                                                        income ) {{ $textNetCashflow }}<span
                                                            class="badge bg-soft-{{ $NetCashflowStyle }} text-{{ $NetCashflowStyle }} font-size-14">{{ number_format($netCashflow,2) }}
                                                                    THB</span> of <b
                                                            class="text-{{ $NetCashflowStyle }}">cashflow</b>
                                                        ::
                                                        <span
                                                            class="badge bg-{{ $diffPercentageStyle }} font-size-14">{{ $diffPercentage }}%</span><b
                                                            class="text-{{ $diffPercentageStyle }}"> {{ $textSurvivalRatio }} </b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div
                                                class="card bg-transparent border-{{ $wealthRatioStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $wealthRatioStyle }}">
                                                    <h5 class="my-0 text-{{ $wealthRatioStyle }}">
                                                        Wealth Ratio: <span
                                                            class="badge bg-{{ $wealthRatioStyle }} font-size-16"><b>{{ $data['wealth_ratio']['value'] }}</b></span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">Your wealth index
                                                        represents <b
                                                            class="text-{{ $wealthRatioStyle }} font-size-16"><b>{{ $wealthRatioAnalysis }}</b></b>
                                                        financial freedom standard</h5>
                                                    <p>{{ $textPassiveIncomeAnalysis }}. It
                                                        can
                                                        cover {{ $onlyPassive ?? '' }} <span
                                                            class="badge bg-{{ $wealthRatioStyle }} font-size-14">{{ $wealthCover }}%</span>
                                                        of your
                                                        monthly
                                                        expenses. ::
                                                        <span
                                                            class="badge bg-{{ $wealthRatioStyle }} font-size-14">{{ $diffWealthPercentage }}%</span><b
                                                            class="text-{{ $wealthRatioStyle }}"> {{ $textWealthRatio }} </b>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @php
                                            if($netCashflow < 0){
                                                $textNetCashflow = 'ไม่สามารถรองรับค่าใช้จ่ายรายเดือนได้ โดยขาดสภาพคล่องอยู่ ';
                                                $NetCashflowStyle = 'danger';
                                            } else {
                                                $textNetCashflow = 'สามารถรองรับค่าใช้จ่ายรายเดือนได้ โดยเหลือสภาพคล่องอยู่ ';
                                                $NetCashflowStyle = 'success';
                                            }

                                            if($data['survival_ratio']['value'] < 1){
                                                $textSurvivalRatio = 'ต่ำกว่าเกณฑ์มาตรฐาน';
                                                $diffPercentage = number_format(($data['survival_ratio']['value'] - 1) * 100, 2);
                                                $diffPercentageStyle = 'danger';
                                            } else {
                                                $textSurvivalRatio = 'สูงกว่าเกณฑ์มาตรฐาน';
                                                $diffPercentage = number_format(($data['survival_ratio']['value'] - 1) * 100, 2);
                                                $diffPercentageStyle = 'success';
                                            }
                                            if($data['wealth_ratio']['value'] < 1.5){
                                                $textWealthRatio = 'ต่ำกว่าเกณฑ์มาตรฐาน';
                                                $diffWealthPercentage = number_format(($data['wealth_ratio']['value'] - 1.5) * 100, 2);
                                                $diffWealthPercentageStyle = 'danger';
                                         } else {
                                                $textWealthRatio = 'สูงกว่าเกณฑ์มาตรฐาน';
                                                $diffWealthPercentage = number_format(($data['wealth_ratio']['value'] - 1.5) * 100, 2);
                                                $diffWealthPercentageStyle = 'success';
                                         }

                                        $wealthCover = number_format(($data['wealth_ratio']['value'] / 1.0) * 100,2);
                                         if($wealthCover < 100){
                                             $textPassiveIncomeAnalysis = 'เราเสียใจที่ต้องบอกกับคุณว่า รายได้จากสินทรัพย์ของคุณยังคงไม่เพียงพอต่อการครอบครองอิสรภาพทางการเงิน';
                                             $wealthCoverStyle = 'danger';
                                             $onlyPassive = 'เพียง';
                                         }else{
                                              $textPassiveIncomeAnalysis = 'เรายินดีที่จะบอกกับคุณว่า รายได้จากสินทรัพย์ของคุณนั้นเพียงพอต่อการครอบครองอิสรภาพทางการเงิน';
                                              $wealthCoverStyle = 'success';
                                              $onlyPassive = '';
                                         }
                                        @endphp
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <div
                                                class="card bg-transparent border-{{ $diffPercentageStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $diffPercentageStyle }}">
                                                    <h5 class="my-0 text-{{ $diffPercentageStyle }}">
                                                        @lang('translation.Survival_Ratio')
                                                        : <span
                                                            class="badge bg-{{ $survivalRatioStyle }} font-size-16"><b>{{ $data['survival_ratio']['value'] }}</b></span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">ผลวิเคราะห์:
                                                        <b
                                                            class="text-{{ $survivalRatioStyle }} font-size-16"><b>{{ $survivalRatioAnalysis }}</b></b>
                                                        ในด้านของความอยู่รอด</h5>
                                                    <p class="card-text">รายรับรวมของคุณ (
                                                        จากการทำงาน
                                                        + จากสินทรัพย์
                                                        ) {{ $textNetCashflow }}<span
                                                            class="badge bg-soft-{{ $NetCashflowStyle }} text-{{ $NetCashflowStyle }} font-size-14">{{ number_format($netCashflow,2) }}
                                                                                บาท</span>
                                                    </p>
                                                    <p>
                                                        <b class="text-{{ $diffPercentageStyle }}">{{ $textSurvivalRatio }}</b>
                                                        <span
                                                            class="badge bg-{{ $diffPercentageStyle }} font-size-14">{{ $diffPercentage }}%
                                                                            </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div
                                                class="card bg-transparent border-{{ $wealthRatioStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $wealthRatioStyle }}">
                                                    <h5 class="my-0 text-{{ $wealthRatioStyle }}">
                                                        @lang('translation.Wealth_Ratio') :
                                                        <span
                                                            class="badge bg-{{ $wealthRatioStyle }} font-size-16"><b>{{ $data['wealth_ratio']['value'] }}</b></span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">ผลวิเคราะห์:
                                                        <b
                                                            class="text-{{ $wealthRatioStyle }} font-size-16"><b>{{ $wealthRatioAnalysis }}</b></b>
                                                        ในด้านของอิสรภาพทางการเงิน</h5>
                                                    <p>{{ $textPassiveIncomeAnalysis }}
                                                        เนื่องจากสามารถรองรับ{{ $onlyPassive ?? '' }}
                                                        <span
                                                            class="badge bg-{{ $wealthRatioStyle }} font-size-14">{{ $wealthCover }}%</span>
                                                        ของค่าใช้จ่ายรวมต่อเดือนของคุณ
                                                    </p>
                                                    <p>
                                                        <b class="text-{{ $wealthRatioStyle }}">{{ $textWealthRatio }}</b>
                                                        <span
                                                            class="badge bg-{{ $wealthRatioStyle }} font-size-14">
                                                                                {{ $diffWealthPercentage }}%
                                                                            </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light"
                                                data-bs-dismiss="modal">@lang('translation.Close')</button>
                                        <button class="btn btn-primary"
                                                data-bs-target="#wealthAnalysis"
                                                data-bs-toggle="modal"
                                                data-bs-dismiss="modal">@lang('translation.Previous')</button>
                                        <button class="btn btn-primary"
                                                data-bs-target="#balanceSheetAnalysis"
                                                data-bs-toggle="modal"
                                                data-bs-dismiss="modal">@lang('translation.Next')</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <div class="modal fade" id="balanceSheetAnalysis"
                             tabindex="-1" role="dialog"
                             aria-labelledby="balanceSheetAnalysisTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="balanceSheetAnalysisTitle"><img
                                                src="{{ URL::asset('/assets/images/mnmn-logo.svg') }}"
                                                alt=""
                                                height="25"> @lang('translation.Wealth_Analysis_Title')
                                        </h5>
                                        <button type="button" class="btn-close"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                    </div>
                                    @if(app()->getLocale() == 'en')
                                        @php
                                            if($data['investment_to_net_wealth']['value'] < 50){
                                                $textInvestmentToNetWealth = 'lower than the benchmark';
                                                $diffInvestmentToNetWealthPercentage = number_format($data['investment_to_net_wealth']['value'] - 50, 2);
                                            } else {
                                                $textInvestmentToNetWealth = 'higher than the benchmark';
                                                $diffInvestmentToNetWealthPercentage = number_format($data['investment_to_net_wealth']['value'] - 50, 2);
                                            }

                                            if($data['debt_to_asset']['value'] > 50){
                                                $textDebtAnalysis = 'We are sad to say that your Debt is overload';
                                                $textDebtToAsset = 'lower than the benchmark';
                                                $diffDebtToAssetPercentage = number_format(50 - $data['debt_to_asset']['value'], 2);
                                            } else {
                                                $textDebtAnalysis = 'We are glad to say that your Debt is under control';
                                                $textDebtToAsset = 'higher than the benchmark';
                                                $diffDebtToAssetPercentage = number_format(50 - $data['debt_to_asset']['value'], 2);
                                            }
                                        @endphp
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <div
                                                class="card bg-transparent border-{{ $investmentToNetWealthStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $investmentToNetWealthStyle }}">
                                                    <h5 class="my-0 text-{{ $investmentToNetWealthStyle }}">
                                                        Investment to Net Wealth:
                                                        <span
                                                            class="badge bg-{{ $investmentToNetWealthStyle }} font-size-16">
                                                                                <b>
                                                                                    {{ $data['investment_to_net_wealth']['value'] }}%
                                                                                </b>
                                                                            </span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">Your investment
                                                        percentage represents
                                                        <b class="text-{{ $investmentToNetWealthStyle }} font-size-16">
                                                            {{ $investmentAssetAnalysis }}
                                                        </b> financial accumulation standard
                                                    </h5>
                                                    <p class="card-text">
                                                        Your accumulated amount of
                                                        investment assets
                                                        is approximately
                                                        <span
                                                            class="badge bg-soft-{{ $investmentToNetWealthStyle }} text-{{ $investmentToNetWealthStyle }} font-size-14">
                                                                                {{ number_format($data['investment_to_net_wealth']['investment_asset'],2) }} THB
                                                                            </span> out of
                                                        <b class="text-{{ $investmentToNetWealthStyle }}">{{ number_format($netWealth,2) }}</b>
                                                        ::
                                                        <span
                                                            class="badge bg-{{ $investmentToNetWealthStyle }} font-size-14">{{ $diffInvestmentToNetWealthPercentage }}%
                                                                            </span>
                                                        <b class="text-{{ $investmentToNetWealthStyle }}"> {{ $textInvestmentToNetWealth }}
                                                        </b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div
                                                class="card bg-transparent border-{{ $debtToAssetStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $debtToAssetStyle }}">
                                                    <h5 class="my-0 text-{{ $debtToAssetStyle }}">
                                                        Debt to Total Asset:
                                                        <span
                                                            class="badge bg-{{ $debtToAssetStyle }} font-size-16">
                                                                                <b>
                                                                                    {{ $data['debt_to_asset']['value'] }}%
                                                                                </b>
                                                                            </span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">Your debt
                                                        percentage
                                                        represents
                                                        <b
                                                            class="text-{{ $debtToAssetStyle }} font-size-16">{{ $debtToAssetAnalysis }}
                                                        </b> liabilities management standard
                                                    </h5>
                                                    <p>{{ $textDebtAnalysis }}. ::
                                                        <span
                                                            class="badge bg-{{ $debtToAssetStyle }} font-size-14">{{ $diffDebtToAssetPercentage }}%</span><b
                                                            class="text-{{ $debtToAssetStyle }}"> {{ $textDebtToAsset }} </b>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @php
                                            if($data['investment_to_net_wealth']['value'] < 50){
                                                $textInvestmentToNetWealth = 'ต่ำกว่าเกณฑ์มาตรฐาน';
                                                $diffInvestmentToNetWealthPercentage = number_format($data['investment_to_net_wealth']['value'] - 50, 2);
                                            } else {
                                                $textInvestmentToNetWealth = 'สูงกว่าเกณฑ์มาตรฐาน';
                                                $diffInvestmentToNetWealthPercentage = number_format($data['investment_to_net_wealth']['value'] - 50, 2);
                                            }

                                            if($data['debt_to_asset']['value'] > 50){
                                                $textDebtAnalysis = 'เราเสียใจที่ต้องบอกว่า ภาระหนี้สินรวมของคุณมากเกินเกณฑ์เหมาะสม';
                                                $textDebtToAsset = 'สูงกว่าเกณฑ์มาตรฐาน';
                                                $diffDebtToAssetPercentage = number_format(50 - $data['debt_to_asset']['value'], 2);
                                            } else {
                                                $textDebtAnalysis = 'เรายินดีที่จะบอกว่า ภาระหนี้สินรวมของคุณอยู่ในเกณฑ์เหมาะสม';
                                                $textDebtToAsset = 'ต่ำกว่าเกณฑ์มาตรฐาน';
                                                $diffDebtToAssetPercentage = number_format(50 - $data['debt_to_asset']['value'], 2);
                                            }
                                        @endphp
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <div
                                                class="card bg-transparent border-{{ $investmentToNetWealthStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $investmentToNetWealthStyle }}">
                                                    <h5 class="my-0 text-{{ $investmentToNetWealthStyle }}">
                                                        การลงทุนต่อความมั่งคั่งสุทธิ:
                                                        <span
                                                            class="badge bg-{{ $investmentToNetWealthStyle }} font-size-16">
                                                                                <b>
                                                                                    {{ $data['investment_to_net_wealth']['value'] }}%
                                                                                </b>
                                                                            </span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">ผลวิเคราะห์:
                                                        <b class="text-{{ $investmentToNetWealthStyle }} font-size-16">
                                                            {{ $investmentAssetAnalysis }}
                                                        </b> ในด้านการสะสมความมั่งคั่ง
                                                    </h5>
                                                    <p class="card-text">
                                                        จำนวนสินทรัพย์เพื่อการลงทุนของคุณคือ
                                                        <span
                                                            class="badge bg-soft-{{ $investmentToNetWealthStyle }} text-{{ $investmentToNetWealthStyle }} font-size-14">
                                                                                {{ number_format($data['investment_to_net_wealth']['investment_asset'],2) }} บาท
                                                                            </span> ต่อ
                                                        <b class="text-{{ $investmentToNetWealthStyle }}">ความมั่งคั่งสุทธิ
                                                            <span
                                                                class="badge bg-soft-{{ $investmentToNetWealthStyle }} text-{{ $investmentToNetWealthStyle }} font-size-14">
                                                                                {{ number_format($netWealth,2) }} บาท
                                                                            </span>
                                                        </b>
                                                    </p>
                                                    <p>
                                                        <b class="text-{{ $investmentToNetWealthStyle }}">{{ $textInvestmentToNetWealth }}</b>
                                                        <span
                                                            class="badge bg-{{ $investmentToNetWealthStyle }} font-size-14">
                                                                                {{ $diffInvestmentToNetWealthPercentage }}%
                                                                            </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div
                                                class="card bg-transparent border-{{ $debtToAssetStyle }}">
                                                <div
                                                    class="card-header bg-transparent border-{{ $debtToAssetStyle }}">
                                                    <h5 class="my-0 text-{{ $debtToAssetStyle }}">
                                                        ภาระหนี้สินต่อสินทรัพย์รวม:
                                                        <span
                                                            class="badge bg-{{ $debtToAssetStyle }} font-size-16">
                                                                                <b>
                                                                                    {{ $data['debt_to_asset']['value'] }}%
                                                                                </b>
                                                                            </span>
                                                    </h5>
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">ผลวิเคราะห์:
                                                        <b
                                                            class="text-{{ $debtToAssetStyle }} font-size-16">{{ $debtToAssetAnalysis }}
                                                        </b>
                                                        ในด้านการบริหารจัดการภาระหนี้สิน
                                                    </h5>
                                                    <p>{{ $textDebtAnalysis }}</p>
                                                    <p>
                                                        <b class="text-{{ $debtToAssetStyle }}"> {{ $textDebtToAsset }} </b>
                                                        <span
                                                            class="badge bg-{{ $debtToAssetStyle }} font-size-14">
                                                                                {{ $diffDebtToAssetPercentage }}%
                                                                            </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light"
                                                data-bs-dismiss="modal">@lang('translation.Close')</button>
                                        <button class="btn btn-primary"
                                                data-bs-target="#personalStatementAnalysis"
                                                data-bs-toggle="modal"
                                                data-bs-dismiss="modal">@lang('translation.Previous')</button>
                                        <button class="btn btn-primary"
                                                data-bs-target="#wealthCareInstruction"
                                                data-bs-toggle="modal"
                                                data-bs-dismiss="modal">@lang('translation.Next')</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <div class="modal fade" id="wealthCareInstruction" tabindex="-1"
                             role="dialog" aria-labelledby="wealthCareInstructionTitle"
                             aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="wealthCareInstructionTitle"><img
                                                src="{{ URL::asset('/assets/images/mnmn-logo.svg') }}"
                                                alt=""
                                                height="25"> @lang('translation.Wealth_Care_Instruction')
                                        </h5>
                                        <button type="button" class="btn-close"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                    </div>
                                    @php
                                        if(app()->getLocale()== 'en'){
                                            $allAspects = [
                                                'personal_expense' => [
                                                    'label'=>'Personal Expense ( <= 50% of total income )',
                                                    'score'=>$personalExpenseScore
                                                ],
                                                'debt_installment' => [
                                                    'label'=>'Debt Service Rate ( <= 40% of total income )',
                                                    'score'=>$debtInstallmentScore
                                                ],
                                                'saving_investment' => [
                                                    'label'=>'Savings & Investment ( >=10% of total income )',
                                                    'score'=>$savingInvestmentScore
                                                ],
                                                'insurance_premium' => [
                                                    'label'=>'Risk Transfer ( 10 - 15% of total income )',
                                                    'score'=>$insurancePremiumScore
                                                ],
                                                'reserved_money' => [
                                                    'label'=>'Reserved Money ( 6 months coverage of total expenses )',
                                                    'score'=>$reservedMoneyScore
                                                ],
                                                'investment_asset' => [
                                                    'label'=>'Investment to Net Wealth ( >= 50% of net wealth )',
                                                    'score'=>$investmentToNetWealthScore
                                                ],
                                                'debt_to_asset' => [
                                                    'label'=>'Debt to Assets ( <= 50% of total assets )',
                                                    'score'=>$debtToAssetScore
                                                ]
                                            ];
                                        } else {
                                            $allAspects = [
                                                'personal_expense' => [
                                                    'label'=>'ค่าใช้จ่ายส่วนตัว ( <= 50% ของรายรับรวม )',
                                                    'score'=>$personalExpenseScore
                                                ],
                                                'debt_installment' => [
                                                    'label'=>'การผ่อนชำระหนี้ ( <= 40% ของรายรับรวม ) )',
                                                    'score'=>$debtInstallmentScore
                                                ],
                                                'saving_investment' => [
                                                    'label'=>'การออมและการลงทุน ( >=10% ของรายรับรวม ) )',
                                                    'score'=>$savingInvestmentScore
                                                ],
                                                'insurance_premium' => [
                                                    'label'=>'การโอนย้ายความเสี่ยง ( 10 - 15% ของรายรับรวม )',
                                                    'score'=>$insurancePremiumScore
                                                ],
                                                'reserved_money' => [
                                                    'label'=>'เงินเก็บสำรองฉุกเฉิน ( 6 เท่าของค่าใช้จ่ายรวมต่อเดือน )',
                                                    'score'=>$reservedMoneyScore
                                                ],
                                                'investment_asset' => [
                                                    'label'=>'สินทรัพย์เพื่อการลงทุน ( >= 50% ของความมั่งคั่งสุทธิ )',
                                                    'score'=>$investmentToNetWealthScore
                                                ],
                                                'debt_to_asset' => [
                                                    'label'=>'ภาระหนี้สินรวม ( <= 50% ของทรัพย์สินรวม )',
                                                    'score'=>$debtToAssetScore
                                                ]
                                            ];
                                        }
                                        $goodAspects = [];
                                        $poorAspects = [];
                                        foreach($allAspects as $key => $value){
                                            if($value['score'] >= 100){
                                                $goodAspects [] = $value['label'];
                                            } else {
                                                $poorAspects[] = $value['label'];
                                            }
                                        }
                                    @endphp
                                    @if(app()->getLocale() == 'en')
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <!-- card -->
                                                    <div class="card card-h-100">
                                                        <!-- card body -->
                                                        <div class="card-body">
                                                            <div
                                                                class="row align-items-center">
                                                                <div class="col-sm">
                                                                    <div
                                                                        id="wealth-health-summary2"
                                                                        data-colors='["#5156be", "#34c38f"]'
                                                                        class="apex-charts">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                            </div>
                                            <p>However, we are proud to say that you are
                                                really good
                                                at
                                                managing your current income with perfect
                                                portion of
                                                following aspects:
                                            <ul class="metismenu list-unstyled">
                                                @foreach($goodAspects as $goodAspect)
                                                    <li>
                                                        <i class="mdi mdi-check-circle text-success font-size-18 me-2"></i>
                                                        {{ $goodAspect }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                            </p>

                                            <p>Whereas there are certain things needed to be
                                                concerned
                                                and fulfilled for your healthier personal
                                                finance:
                                            <ul class="metismenu list-unstyled">
                                                @foreach($poorAspects as $poorAspect)
                                                    <li>
                                                        <i class="mdi mdi-alert-circle text-danger font-size-18 me-2"></i>
                                                        {{ $poorAspect }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                            </p>
                                        </div>
                                    @else
                                        <div class="modal-body text-secondary"
                                             style="text-align: justify;">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <!-- card -->
                                                    <div class="card card-h-100">
                                                        <!-- card body -->
                                                        <div class="card-body">
                                                            <div
                                                                class="row align-items-center">
                                                                <div class="col-sm">
                                                                    <div
                                                                        id="wealth-health-summary2"
                                                                        data-colors='["#5156be", "#34c38f"]'
                                                                        class="apex-charts">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col -->
                                            </div>
                                            <p>
                                                อย่างไรก็ดีเราภูมิใจที่จะบอกกับคุณว่า
                                                คุณมีความสามารถในการจัดการรายรับปัจจุบันของคุณเอง
                                                และสามารถจัดสรรสัดส่วนทางการเงินต่างๆเหล่านี้ได้เป็นอย่างดี
                                                ประกอบไปด้วย:
                                            <ul class="metismenu list-unstyled">
                                                @foreach($goodAspects as $goodAspect)
                                                    <li>
                                                        <i class="mdi mdi-check-circle text-success font-size-18 me-2"></i>
                                                        {{ $goodAspect }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                            </p>

                                            <p>
                                                ในขณะเดียวกันยังคงมีบางสิ่งที่คุณจำเป็นจะต้องให้ความสำคัญ
                                                ปรับปรุงแก้ไขหรือต่อยอดให้สมบูรณ์
                                                เพื่อสุขภาพการเงินส่วนบุคคลที่ยอดเยี่ยมของคุณเอง
                                                ประกอบไปด้วย:
                                            <ul class="metismenu list-unstyled">
                                                @foreach($poorAspects as $poorAspect)
                                                    <li>
                                                        <i class="mdi mdi-alert-circle text-danger font-size-18 me-2"></i>
                                                        {{ $poorAspect }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                            </p>
                                        </div>
                                    @endif
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light"
                                                data-bs-dismiss="modal">@lang('translation.Close')</button>
                                        <button class="btn btn-primary"
                                                data-bs-target="#balanceSheetAnalysis"
                                                data-bs-toggle="modal"
                                                data-bs-dismiss="modal">@lang('translation.Previous')</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-xl-12">
                        <!-- card -->
                        <div class="card card-h-100">
                            <!-- card body -->
                            <div class="card-body">
                                <div class="d-flex flex-wrap align-items-center mb-4">
                                    <h5 class="card-title me-2">@lang('translation.Wealth_Overview')</h5>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm">
                                        <div id="wealth-health-summary" data-colors='["#5156be", "#34c38f"]'
                                             class="apex-charts">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
            </div>
            <!-- end col -->
        </div> <!-- end row-->
    @endif
    <!-- end row-->
@endsection

@section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

    <!-- Plugins js-->
    <script
        src="{{ URL::asset('/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script
        src="{{ URL::asset('/assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-world-mill-en.js') }}"></script>

    <!-- dashboard init -->
    <script src="{{ URL::asset('/assets/js/pages/dashboard.init.js') }}">
    </script>

    <!-- datepicker js -->
    <script src="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <!-- flatpickr plugin -->
    <script src="{{ URL::asset('/assets/libs/flatpickr/plugins/monthSelect/index.js') }}"></script>
    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/wealth-health-check.init.js') }}"></script>
    @if($data)
        <script>
            var income = @json($income);
            var debtInstallment = @json($debtInstallment);
            var expense = @json($expense);
            var insurancePremium = @json($insurancePremium);
            var savingInvestment = @json($savingInvestment);
            var cashflow = income - (debtInstallment + expense + savingInvestment);
            var savingPercentage = @json($savingPercentage);
            var installmentPercentage = @json($installmentPercentage);
            var personalExpensePercentage = @json($personalExpensePercentage);
            var insurancePremiumPercentage = @json($insurancePremiumPercentage);
            var cashflowPercentage = ((cashflow / income) * 100).toFixed(2);
            var investmentToNetWealth = @json($investmentToNetWealth);
            var investmentToNetWealthBenchmark = @json($investmentToNetWealthBenchmark);

            var personalExpenseBenchmark = @json($personalExpenseBenchmark);
            var personalExpenseScore = 0;
            if (personalExpensePercentage <= personalExpenseBenchmark) {
                personalExpenseScore = 100;
            }

            var insurancePremiumBenchmarkMin = @json($insurancePremiumBenchmarkMin);
            var insurancePremiumBenchmarkMax = @json($insurancePremiumBenchmarkMax);

            var insurancePremiumScore = 0;
            if (insurancePremiumPercentage >= insurancePremiumBenchmarkMin && insurancePremiumPercentage <= insurancePremiumBenchmarkMax) {
                insurancePremiumScore = 100;
            }

            var debtInstallmentBenchmark = @json($debtInstallmentBenchmark);
            var debtInstallmentScore = 0;
            if (installmentPercentage <= debtInstallmentBenchmark) {
                debtInstallmentScore = 100;
            }

            var savingToIncomeBenchmarkMin = @json($savingToIncomeBenchmarkMin);
            var savingToIncomeBenchmarkSuggested = @json($savingToIncomeBenchmarkSuggested);
            var savingInvestmentScore = savingPercentage * (100 / savingToIncomeBenchmarkMin);
            if (savingPercentage > savingToIncomeBenchmarkMin) {
                savingInvestmentScore = 100;
            }

            var investmentToNetWealthScore = investmentToNetWealth * (100 / investmentToNetWealthBenchmark);
            if (investmentToNetWealthScore > 100) {
                investmentToNetWealthScore = 100;
            } else if (investmentToNetWealthScore < 0) {
                investmentToNetWealthScore = 0;
            }

            var debtToAsset = @json($debtToAsset);
            var debtToAssetBenchmark = @json($debtToAssetBenchmark);
            var debtToAssetScore = 0;
            if (debtToAsset <= debtToAssetBenchmark) {
                debtToAssetScore = 100;
            }

            // var piechartColors = getChartColorsArray("#cash-flow-balance");
            var options = {
                series: [debtInstallment, expense - insurancePremium, insurancePremium, savingInvestment, cashflow],
                chart: {
                    width: 320,
                    type: 'pie',
                },
                legend: {
                    show: false
                },
                labels:
                    [
                        @json(__('translation.Debt_Installment')),
                        @json(__('translation.Personal_Expense')),
                        @json(__('translation.Insurance_Premium')) ,
                        @json(__('translation.Savings_Investment')),
                        @json(__('translation.Cash_Flow'))
                    ],
                colors: [
                    //'rgba(255,69,96,1)',
                    '#74788d',
                    //'rgba(254,176,25,1)',
                    '#fd625e',
                    //'rgba(0,143,251,1)',
                    '#4ba6ef',
                    //'rgba(0,227,150,1)',
                    '#ffbf53',
                    //'rgba(119,93,208,1)'],
                    '#5156be'
                    //'#2ab57d'
                ],
                stroke: {
                    width: 0,
                },
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }]
            };

            var chart = new ApexCharts(document.querySelector("#cash-flow-balance"), options);
            chart.render();

            var reservedMoneyRatio = @json($reservedMoneyRatio);
            var reservedMoneyPercentage = @json($reservedMoneyPercentage);
            var reservedMoneyScore = reservedMoneyPercentage > 100 ? 100 : reservedMoneyPercentage;

            var radialchartColors = getChartColorsArray("#reserved-money-overview");
            var options = {
                chart: {
                    height: 270,
                    type: 'radialBar',
                    offsetY: -10
                },
                plotOptions: {
                    radialBar: {
                        startAngle: -130,
                        endAngle: 130,
                        dataLabels: {
                            name: {
                                show: true,
                                fontSize: '14px'
                            },
                            value: {
                                offsetY: 20,
                                fontSize: '18px',
                                color: undefined,
                                formatter: function (val) {
                                    return val + "%";
                                }
                            },
                        }
                    }
                },
                colors: [radialchartColors[0]],
                fill: {
                    type: 'gradient',
                    gradient: {
                        shade: 'dark',
                        type: 'horizontal',
                        gradientToColors: [radialchartColors[1]],
                        shadeIntensity: 0.15,
                        inverseColors: false,
                        opacityFrom: 1,
                        opacityTo: 1,
                        stops: [20, 60]
                    },
                },
                stroke: {
                    dashArray: 4,
                },
                legend: {
                    show: false
                },
                series: [reservedMoneyPercentage],
                labels: [reservedMoneyRatio + " / 6"],
            }

            var chart = new ApexCharts(
                document.querySelector("#reserved-money-overview"),
                options
            );

            chart.render();

            var options = {
                series: [
                    {
                        name: @json(__('translation.Actual')),
                        data: [
                            {
                                x: @json(__('translation.Debt_Installment')),
                                y: installmentPercentage,
                                goals: [
                                    {
                                        name: @json(__('translation.Max')),
                                        value: debtInstallmentBenchmark,
                                        strokeWidth: 5,
                                        strokeColor: '#fd625e'
                                    }
                                ],
                                bar: '#33b2df'
                            },
                            {
                                x: @json(__('translation.Personal_Expense')),
                                y: personalExpensePercentage,
                                goals: [
                                    {
                                        name: @json(__('translation.Max')),
                                        value: personalExpenseBenchmark,
                                        strokeWidth: 5,
                                        strokeColor: '#fd625e'
                                    }
                                ]
                            },
                            {
                                x: @json(__('translation.Insurance_Premium')),
                                y: insurancePremiumPercentage,
                                goals: [
                                    {
                                        name: @json(__('translation.Min')),
                                        value: insurancePremiumBenchmarkMin,
                                        strokeWidth: 5,
                                        strokeColor: '#5156be'
                                    },
                                    {
                                        name: @json(__('translation.Max')),
                                        value: insurancePremiumBenchmarkMax,
                                        strokeWidth: 5,
                                        strokeColor: '#fd625e'
                                    }
                                ]
                            },
                            {
                                x: @json(__('translation.Savings_Investment')),
                                y: savingPercentage,
                                goals: [
                                    {
                                        name: @json(__('translation.Min')),
                                        value: savingToIncomeBenchmarkMin,
                                        strokeWidth: 5,
                                        strokeColor: '#5156be'
                                    },
                                    {
                                        name: @json(__('translation.Suggested')),
                                        value: savingToIncomeBenchmarkSuggested,
                                        strokeWidth: 5,
                                        strokeColor: '#2ab57d'
                                    },
                                ]
                            },
                        ]
                    }
                ],
                chart: {
                    height: 350,
                    type: 'bar'
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                    }
                },
                /*   colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
                       '#f48024', '#69d2e7'
                   ],*/
                colors: ['#eee'],
                xaxis: {
                    max: 50,
                    tickAmount: 5,
                    show: false
                },
                dataLabels: {
                    formatter: function (val, opt) {
                        const goals =
                            opt.w.config.series[opt.seriesIndex].data[opt.dataPointIndex]
                                .goals

                        if (goals && goals.length) {
                            return `${val} / ${goals[0].value}`
                        }
                        return val
                    }
                },
                legend: {
                    show: true,
                    showForSingleSeries: true,
                    customLegendItems: [@json(__('translation.Actual')), @json(__('translation.Min')), @json(__('translation.Suggested')), @json(__('translation.Max'))],
                    markers: {
                        fillColors: ['#eee', '#5156be', '#2ab57d', '#fd625e']
                    }
                }
            };

            var chart = new ApexCharts(document.querySelector("#cashflow-balance"), options);
            chart.render();

            var options = {
                series: [{
                    name: 'Scores',
                    data: [personalExpenseScore, debtInstallmentScore, savingInvestmentScore, insurancePremiumScore, reservedMoneyScore, investmentToNetWealthScore, debtToAssetScore],
                }],
                chart: {
                    height: 280,
                    type: 'radar',
                },
                dataLabels: {
                    enabled: true
                },
                plotOptions: {
                    radar: {
                        size: 100,
                        polygons: {
                            strokeColors: '#e9e9e9',
                            fill: {
                                colors: ['#f8f8f8', '#fff']
                            }
                        }
                    }
                },
                title: {
                    text: ''
                },
                colors: ['#5156be'],
                markers: {
                    size: 4,
                    colors: ['#fff'],
                    strokeColor: '#775DD0',
                    strokeWidth: 2,
                },
                tooltip: {
                    y: {
                        formatter: function (val) {
                            return val
                        }
                    }
                },
                xaxis: {
                    categories: [@json(__('translation.Personal_Expense')), @json(__('translation.Debt_Installment')), @json(__('translation.Savings_Investment')), @json(__('translation.Risk_Transfer')), @json(__('translation.Reserved_Money')), @json(__('translation.Investment_Asset')), @json(__('translation.Debt_To_Asset'))]
                },
                yaxis: {
                    tickAmount: 7,
                    labels: {
                        formatter: function (val, i) {
                            if (i % 2 === 0) {
                                return val
                            } else {
                                return ''
                            }
                        }
                    }
                }
            };

            var chart = new ApexCharts(document.querySelector("#wealth-health-summary"), options);
            chart.render();

            var options = {
                series: [{
                    name: 'Scores',
                    data: [personalExpenseScore, debtInstallmentScore, savingInvestmentScore, insurancePremiumScore, reservedMoneyScore, investmentToNetWealthScore, debtToAssetScore],
                }],
                chart: {
                    height: 250,
                    type: 'radar',
                },
                dataLabels: {
                    enabled: true
                },
                plotOptions: {
                    radar: {
                        size: 100,
                        polygons: {
                            strokeColors: '#e9e9e9',
                            fill: {
                                colors: ['#f8f8f8', '#fff']
                            }
                        }
                    }
                },
                title: {
                    text: ''
                },
                colors: ['#5156be'],
                markers: {
                    size: 4,
                    colors: ['#fff'],
                    strokeColor: '#775DD0',
                    strokeWidth: 2,
                },
                tooltip: {
                    y: {
                        formatter: function (val) {
                            return val
                        }
                    }
                },
                xaxis: {
                    categories: [@json(__('translation.Personal_Expense')), @json(__('translation.Debt_Installment')), @json(__('translation.Savings_Investment')), @json(__('translation.Risk_Transfer')), @json(__('translation.Reserved_Money')), @json(__('translation.Investment_Asset')), @json(__('translation.Debt_To_Asset'))]
                },
                yaxis: {
                    tickAmount: 7,
                    labels: {
                        formatter: function (val, i) {
                            if (i % 2 === 0) {
                                return val
                            } else {
                                return ''
                            }
                        }
                    }
                }
            };

            var chart = new ApexCharts(document.querySelector("#wealth-health-summary2"), options);
            chart.render();

            $('#submission-date').on('change', function () {
                liveSearch($(this).val());
            });

            function liveSearch(date) {
                let url = "/wealth-health-check/" + date;
                window.location = url;
            }
        </script>
    @else
        <script>
            $('#submission-date').on('change', function () {
                liveSearch($(this).val());
            });

            function liveSearch(date) {
                let url = "/wealth-health-check/" + date;
                window.location = url;
            }
        </script>
    @endif
@endsection
