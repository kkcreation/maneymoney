@extends('layouts.master')

@section('title') @lang('translation.Wizard') @endsection

@section('css')

    <!-- twitter-bootstrap-wizard css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/twitter-bootstrap-wizard/prettify.css') }}">
    <!-- datepicker css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.css') }}">
    <!-- flatpickr plugin -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/plugins/monthSelect/style.css') }}">

@endsection

@section('content')
    @if (session('message'))
        <div class="alert {{ session('alert-class') }} alert-border-left alert-dismissible fade show" role="alert">
            <i class="mdi mdi-check-all me-3 align-middle"></i>{{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Wealth_Creation') @endslot
        @slot('title') @lang('translation.Wealth_Profile') @endslot
    @endcomponent
    @php
           // Get latest version
           if($version){
               $latestFinancialStatementVersion = $version;
           }else{
               $latestFinancialStatementVersion = Auth::user()->financialStatementVersions->sortByDesc('submission_date')->first();
           }
    @endphp
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <form method="post"
                      action="{{ route('updateFinancialStatement',['id'=>Auth::user()->id]) }}"
                      id="financial-statement">
                    @csrf
                    <div class="card-header">
                        {{-- <span id="guide-button" class="btn border-dark float-end"
                               data-bs-toggle="offcanvas"
                               data-bs-target="#offcanvasBottom"
                               aria-controls="offcanvasBottom">
                                                <img src="assets/images/mnmn-logo.svg" alt="" height="20"> @lang('translation.Much_Guide')
                                            </span>--}}
                        @if($version)
                        <span class="float-end">
                            <a href="javascript: void(0);" class="btn btn-primary"
                               data-bs-toggle="modal"
                               data-bs-target=".confirmModal">@lang('translation.Save_Changes')
                            </a>
                        </span>
                        @else
                            <span class="float-end">
                            <a href="javascript: void(0);" class="btn btn-primary"
                               data-bs-toggle="modal"
                               data-bs-target=".confirmModal">@lang('translation.Create_New')
                            </a>
                        </span>
                            @endif
                        <h4 class="card-title mb-0">@lang('translation.Wealth_Onboarding')</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-text">
                                        <i class="bx bxs-calendar text-primary font-size-24"></i>
                                    </div>
                                    <input class="form-control text-center text-primary flatpickr-input" type="text" name="submission_date"
                                           id="submission-date"
                                           placeholder="SUBMISSION DATE"
                                           value="{{ $date }}"
                                           required data-input> <!-- input is mandatory -->
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        <div id="progrss-wizard" class="twitter-bs-wizard">
                            <ul class="twitter-bs-wizard-nav nav nav-pills nav-justified">
                                <li class="nav-item">
                                    <a href="#progress-personal-income-expense" class="nav-link" data-toggle="tab">
                                        <div class="step-icon" data-bs-toggle="tooltip" data-bs-placement="top"
                                             title="Personal Statement">
                                            <i class="bx bx-money"></i>
                                        </div>
                                    </a>
                                    {{--   <p class="text-primary">@lang('translation.Personal_Statement')</p>--}}
                                </li>
                                <li class="nav-item">
                                    <a href="#progress-personal-balance-sheet" class="nav-link" data-toggle="tab">
                                        <div class="step-icon" data-bs-toggle="tooltip" data-bs-placement="top"
                                             title="Personal Balance Sheet">
                                            <i class="bx bxs-bank"></i>
                                        </div>
                                    </a>
                                    {{--    <p class="text-primary">@lang('translation.Personal_Balance_Sheet')</p>--}}
                                </li>
                            </ul>
                            <!-- wizard-nav -->

                            {{-- <div id="bar" class="progress mt-4">
                                 <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"></div>
                             </div>--}}
                            <div class="tab-content twitter-bs-wizard-tab-content">
                                <div class="tab-pane" id="progress-personal-income-expense">
                                    <div>
                                        <div class="text-center mb-4">
                                            <h5>@lang('translation.Personal_Statement')</h5>
                                            <p class="card-title-desc">@lang('translation.Fill_all_information_below')</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-12">
                                                <div class="card bg-success border-success text-white-50">
                                                    <div class="card-body">
                                                        <h6 class="mb-4 text-white"><i
                                                                class="mdi mdi-cash-plus me-3"></i>@lang('translation.Income_Uppercase')
                                                        </h6>
                                                        <p class="card-text" id="income-summary">0.00</p>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card bg-danger border-danger text-white-50">
                                                    <div class="card-body">
                                                        <h6 class="mb-4 text-white"><i
                                                                class="mdi mdi-cash-minus me-3"></i>@lang('translation.Expense_Uppercase')
                                                        </h6>
                                                        <p class="card-text" id="expense-summary">0.00</p>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="col-lg-3 col-md-6">
                                                <div class="card bg-warning border-warning text-white-50">
                                                    <div class="card-body ">
                                                        <h6 class="mb-4 text-white"><i
                                                                class="mdi mdi-cash-lock me-3"></i>@lang('translation.Savings_Investment_Uppercase')
                                                        </h6>
                                                        <p class="card-text" id="saving-summary">0.00</p>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="col-lg-3 col-md-12">
                                                <div class="card text-black-50">
                                                    <div class="card-body ">
                                                        <h6 class="mb-4 text-black"><i
                                                                class="mdi mdi-cash-check me-3"></i>@lang('translation.Net_Cash_Flow_Uppercase')
                                                        </h6>
                                                        <p class="card-text" id="cash-flow-summary">0.00</p>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->
                                        </div>
                                        <div class="card-body">
                                            @if($version)
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-bs-toggle="tab" href="#income"
                                                       role="tab">
                                                        <span class="d-block d-sm-none"><i
                                                                class="mdi mdi-cash-plus text-success"></i> @lang('translation.Income')</span>
                                                        <span class="d-none d-sm-block"><i
                                                                class="mdi mdi-cash-plus text-success"></i> @lang('translation.Income')</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-bs-toggle="tab" href="#expense" role="tab">
                                                    <span class="d-block d-sm-none"><i
                                                            class="mdi mdi-cash-minus text-danger"></i> @lang('translation.Expense')</span>
                                                        <span class="d-none d-sm-block"><i
                                                                class="mdi mdi-cash-plus text-danger"></i> @lang('translation.Expense')</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-bs-toggle="tab" href="#savings-investment"
                                                       role="tab">
                                                    <span class="d-block d-sm-none"><i
                                                            class="mdi mdi-cash-lock text-warning"></i> @lang('translation.Savings_Investment')</span>
                                                        <span class="d-none d-sm-block"><i
                                                                class="mdi mdi-cash-plus text-warning"></i> @lang('translation.Savings_Investment')</span>
                                                    </a>
                                                </li>
                                            </ul>

                                                <div class="tab-content p-3 text-muted">
                                                <div class="tab-pane active" id="income" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Active_Income')</label>
                                                                @foreach(Auth::user()->userIncome->where('income.type','active_income')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="income-description"
                                                                             class="input-group-text">{{ App::getLocale() == 'en' ? $item->income->description : $item->income->description_th }}</div>
                                                                        <input name="{{ $item->key }}"
                                                                               id="income-amount"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? $item->income->description : $item->income->description_th }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalIncome()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Passive_Income')</label>
                                                                @foreach(Auth::user()->userIncome->where('income.type','passive_income')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="income-description"
                                                                             class="input-group-text">{{ App::getLocale() == 'en' ? $item->income->description : $item->income->description_th }}</div>
                                                                        <input name="{{ $item->key }}"
                                                                               id="income-amount"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? $item->income->description : $item->income->description_th }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalIncome()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="expense" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Fixed_Expense')</label>
                                                                @foreach(Auth::user()->userExpenses->where('expense.type','fixed_expense')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="expense-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->expense->description : $item->expense->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}
                                                                        </div>
                                                                        <input name="{{ $item->key }}"
                                                                               id="expense-amount"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalExpense()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Non_Fixed_Expense')</label>
                                                                @foreach(Auth::user()->userExpenses->where('expense.type','non_fixed_expense')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="expense-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->expense->description : $item->expense->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}
                                                                        </div>
                                                                        <input name="{{ $item->key }}"
                                                                               id="expense-amount"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalExpense()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="savings-investment" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Savings_Cost')</label>
                                                                @foreach(Auth::user()->userExpenses->whereIn('expense.type',['savings'])->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="expense-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->expense->description : $item->expense->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}
                                                                        </div>
                                                                        <input name="{{ $item->key }}"
                                                                               id="saving-invest-amount"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalSavingsInvestment()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                        <!--       onblur="numberWithCommas(this, this.value)"> -->
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label"></label>
                                                                @foreach(Auth::user()->userExpenses->whereIn('expense.type',['investment'])->where('version_id',$latestFinancialStatementVersion->id)->slice(8,12) as $item)
                                                                    <div class="input-group">
                                                                        <div id="expense-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->expense->description : $item->expense->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}
                                                                        </div>
                                                                        <input name="{{ $item->key }}"
                                                                               id="saving-invest-amount"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalSavingsInvestment()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                        <!--       onblur="numberWithCommas(this, this.value)"> -->
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Investment_Cost')</label>
                                                                @foreach(Auth::user()->userExpenses->whereIn('expense.type',['investment'])->where('version_id',$latestFinancialStatementVersion->id)->slice(0,8) as $item)
                                                                    <div class="input-group">
                                                                        <div id="expense-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->expense->description : $item->expense->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}
                                                                        </div>
                                                                        <input name="{{ $item->key }}"
                                                                               id="saving-invest-amount"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->expense->description,45) : Str::limit($item->expense->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalSavingsInvestment()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                        <!--       onblur="numberWithCommas(this, this.value)"> -->
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                    </div>
                                                </div>
                                            </div>
                                                <ul class="pager wizard twitter-bs-wizard-pager-link">
                                                <li class="next"><a href="javascript: void(0);" class="btn btn-primary"
                                                                    onclick="nextTab()">@lang('translation.Next') <i
                                                            class="bx bx-chevron-right ms-1"></i></a></li>
                                            </ul>
                                            @endif
                                        </div>
                                        {{--  <ul class="pager wizard twitter-bs-wizard-pager-link">
                                              <li class="next"><a href="javascript: void(0);" class="btn btn-primary"
                                                                  onclick="nextTab()">Next <i
                                                          class="bx bx-chevron-right ms-1"></i></a></li>
                                          </ul>--}}
                                    </div>
                                </div>
                                <div class="tab-pane" id="progress-personal-balance-sheet">
                                    <div>
                                        <div class="text-center mb-4">
                                            <h5>@lang('translation.Personal_Balance_Sheet')</h5>
                                            <p class="card-title-desc">@lang('translation.Fill_all_information_below')</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-md-6">
                                                <div class="card bg-primary border-primary text-white-50">
                                                    <div class="card-body">
                                                        <h6 class="mb-4 text-white"><i
                                                                class="mdi mdi-bank-plus me-3"></i>@lang('translation.Asset_Uppercase')
                                                        </h6>
                                                        <p class="card-text" id="asset-summary">0.00</p>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="col-lg-4 col-md-6">
                                                <div class="card bg-dark border-dark text-white-50">
                                                    <div class="card-body">
                                                        <h6 class="mb-4 text-white"><i
                                                                class="mdi mdi-bank-minus me-3"></i>@lang('translation.Debt_Uppercase')
                                                        </h6>
                                                        <p class="card-text" id="debt-summary">0.00</p>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="col-lg-4 col-md-12">
                                                <div class="card text-black-50">
                                                    <div class="card-body ">
                                                        <h6 class="mb-4 text-black"><i
                                                                class="mdi mdi-bank-check me-3"></i>@lang('translation.Net_Wealth_Uppercase')
                                                        </h6>
                                                        <p class="card-text" id="net-wealth-summary">0.00</p>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->
                                        </div>
                                        <div class="card-body">
                                            @if($version)
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-bs-toggle="tab" href="#asset"
                                                       role="tab">
                                                        <span class="d-block d-sm-none"><i
                                                                class="mdi mdi-bank-plus text-primary"></i> @lang('translation.Asset')</span>
                                                        <span class="d-none d-sm-block"><i
                                                                class="mdi mdi-bank-plus text-primary"></i> @lang('translation.Asset')</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-bs-toggle="tab" href="#debt" role="tab">
                                                        <span class="d-block d-sm-none"><i
                                                                class="mdi mdi-bank-minus text-dark"></i> @lang('translation.Debt')</span>
                                                        <span class="d-none d-sm-block"><i
                                                                class="mdi mdi-bank-minus text-dark"></i> @lang('translation.Debt')</span>
                                                    </a>
                                                </li>
                                            </ul>

                                            <div class="tab-content p-3 text-muted">
                                                <div class="tab-pane active" id="asset" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Liquid_Asset')</label>
                                                                @foreach(Auth::user()->userAssets->where('asset.type','liquid_asset')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="asset-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->asset->description : $item->asset->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}
                                                                        </div>
                                                                        <input id="asset-amount"
                                                                               name="{{ $item->key }}"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalAsset()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                        <!--       onblur="numberWithCommas(this, this.value)"> -->
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Personal_Asset')</label>
                                                                @foreach(Auth::user()->userAssets->where('asset.type','personal_asset')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="asset-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->asset->description : $item->asset->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}
                                                                        </div>
                                                                        <input id="asset-amount"
                                                                               name="{{ $item->key }}"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalAsset()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Investment_Asset')</label>
                                                                @foreach(Auth::user()->userAssets->where('asset.type','investment_asset')->where('version_id',$latestFinancialStatementVersion->id)->slice(0,8) as $item)
                                                                    <div class="input-group">
                                                                        <div id="asset-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->asset->description : $item->asset->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}
                                                                        </div>
                                                                        <input id="asset-amount"
                                                                               name="{{ $item->key }}"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalAsset()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Continue')</label>
                                                                @foreach(Auth::user()->userAssets->where('asset.type','investment_asset')->where('version_id',$latestFinancialStatementVersion->id)->slice(8,16) as $item)
                                                                    <div class="input-group">
                                                                        <div id="asset-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->asset->description : $item->asset->description_th }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}
                                                                        </div>
                                                                        <input id="asset-amount"
                                                                               name="{{ $item->key }}"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->asset->description,45) : Str::limit($item->asset->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalAsset()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="debt" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Short_Term_Debt')</label>
                                                                @foreach(Auth::user()->userDebts->where('debt.type','short_term_debt')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="debt-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->debt->description : $item->debt->description_th  }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->debt->description,45) : Str::limit($item->debt->description_th,45) }}
                                                                        </div>
                                                                        <input id="debt-amount"
                                                                               name="{{ $item->key }}"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->debt->description,45) : Str::limit($item->debt->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalDebt()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label for="progresspill-pancard-input"
                                                                       class="form-label">@lang('translation.Long_Term_Debt')</label>
                                                                @foreach(Auth::user()->userDebts->where('debt.type','long_term_debt')->where('version_id',$latestFinancialStatementVersion->id) as $item)
                                                                    <div class="input-group">
                                                                        <div id="debt-description"
                                                                             class="input-group-text"
                                                                             data-bs-toggle="tooltip"
                                                                             data-bs-placement="top"
                                                                             title="{{ App::getLocale() == 'en' ? $item->debt->description : $item->debt->description_th  }}">
                                                                            {{ App::getLocale() == 'en' ? Str::limit($item->debt->description,45) : Str::limit($item->debt->description_th,45) }}
                                                                        </div>
                                                                        <input id="debt-amount"
                                                                               name="{{ $item->key }}"
                                                                               type="number" step="0.01"
                                                                               class="form-control"
                                                                               placeholder="{{ App::getLocale() == 'en' ? Str::limit($item->debt->description,45) : Str::limit($item->debt->description_th,45) }}"
                                                                               value="{{ $item->amount }}"
                                                                               onblur="sumTotalDebt()"
                                                                               onchange="setTwoNumberDecimal(this)">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="pager wizard twitter-bs-wizard-pager-link">
                                                <li class="previous"><a href="javascript: void(0);"
                                                                        class="btn btn-primary"
                                                                        onclick="nextTab()"><i
                                                            class="bx bx-chevron-left me-1"></i> @lang('translation.Previous')
                                                    </a></li>
                                            </ul>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasBottom"
                             aria-labelledby="offcanvasBottomLabel">
                            <div class="offcanvas-header">
                                <h5 id="offcanvasBottomLabel">MiniMuch Wealth Guide</h5>
                                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                                        aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        {{--<div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown">
                                                Frequently Asked Question <i class="mdi mdi-chevron-down"></i>
                                            </button>--}}
                                        <div class="card">
                                            <div class="card-body">
                                                <span><h6>Frequently Asked Question:</h6></span>
                                                <span id="guide-question" class="btn btn-soft-primary"
                                                      onclick="whatAreAssets(this)">What are Assets ?</span>
                                                <span id="guide-question" class="btn btn-soft-primary"
                                                      onclick="typesOfAssets(this)">How many types of Assets ?</span>
                                                <span id="guide-question" class="btn btn-soft-primary"
                                                      onclick="whatAreDebts(this)">What are Debts ?</span>
                                                <span id="guide-question" class="btn btn-soft-primary"
                                                      onclick="typesOfDebts(this)">How many types of Debts ?</span>
                                            </div>
                                        </div>
                                        {{--</div>--}}
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="card" id="whatAreAssets">
                                            <div class="card-body">
                                                <h6>WHAT ARE ASSETS ?</h6>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry.
                                                Lorem Ipsum has been the industry's standard dummy text ever since the
                                                1500s, when an unknown printer took a galley of type and scrambled it to
                                                make a type specimen book. It has survived not only five centuries, but
                                                also
                                                the leap into electronic typesetting, remaining essentially unchanged.
                                                It
                                                was popularised in the 1960s with the release of Letraset sheets
                                                containing
                                                Lorem Ipsum passages, and more recently with desktop publishing software
                                                like Aldus PageMaker including versions of Lorem Ipsum.
                                            </div>
                                        </div>
                                        <div class="card" id="typesOfAssets">
                                            <div class="card-body">
                                                <h6>HOW MANY TYPES OF ASSETS ?</h6>
                                                There are 3 main types of assets
                                            </div>
                                        </div>
                                        <div class="card" id="whatAreDebts">
                                            <div class="card-body">
                                                <h6>WHAT ARE DEBTS ?</h6>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry.
                                                Lorem Ipsum has been the industry's standard dummy text ever since the
                                                1500s, when an unknown printer took a galley of type and scrambled it to
                                                make a type specimen book. It has survived not only five centuries, but
                                                also
                                                the leap into electronic typesetting, remaining essentially unchanged.
                                                It
                                                was popularised in the 1960s with the release of Letraset sheets
                                                containing
                                                Lorem Ipsum passages, and more recently with desktop publishing software
                                                like Aldus PageMaker including versions of Lorem Ipsum.
                                            </div>
                                        </div>
                                        <div class="card" id="typesOfDebts">
                                            <div class="card-body">
                                                <h6>HOW MANY TYPES OF DEBTS ?</h6>
                                                There are 2 main types of debts
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end card body -->
                </form>
            </div>
            <!-- end card -->
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->

    <!-- End Page-content -->
    <!-- Modal -->
    <div class="modal fade confirmModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="mb-3">
                            <i class="bx bx-check-circle display-4 text-success"></i>
                        </div>
                        <h5>@lang('translation.Confirm_Update')</h5>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-light w-md"
                            data-bs-dismiss="modal">@lang('translation.Close')</button>
                    <button type="button" class="btn btn-primary w-md" data-bs-dismiss="modal"
                            onclick="userFormSubmit()">
                        @lang('translation.Confirm_Changes')
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
    <div class="modal fade myModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" id="myModal">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="mb-3">
                            <i class="bx bx-check-circle display-4 text-success"></i>
                        </div>
                        <h5>Ready for Wealth Health Check!</h5>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-light w-md" data-bs-dismiss="modal"
                            onclick="location.href='wealth-profile'">Back to Review
                    </button>
                    <button type="button" class="btn btn-primary w-md" data-bs-dismiss="modal"
                            onclick="location.href='index'">Go to Dashboard
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <!-- twitter-bootstrap-wizard js -->
    <script src="{{ URL::asset('/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/twitter-bootstrap-wizard/prettify.js') }}"></script>

    <!-- form wizard init -->
    <script src="{{ URL::asset('/assets/js/pages/form-wizard.init.js') }}"></script>
    <!-- datepicker js -->
    <script src="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <!-- flatpickr plugin -->
    <script src="{{ URL::asset('/assets/libs/flatpickr/plugins/monthSelect/index.js') }}"></script>
    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/wealth-profile.init.js') }}"></script>
    <script>
        $(document).ready(function () {
            console.log("page ready!");
            sumTotalIncome();
            sumTotalExpense();
            sumTotalSavingsInvestment();
            sumTotalAsset();
            sumTotalDebt();
            $('#whatAreAssets').hide();
            $('#typesOfAssets').hide();
            $('#whatAreDebts').hide();
            $('#typesOfDebts').hide();
        });

        function whatAreAssets(element) {
            var questions = document.querySelectorAll('[id=guide-question]');
            for (var i = 0; i < questions.length; i++) {
                if (questions[i] == element) {
                    questions[i].style.color = '#fff';
                    questions[i].style.background = '#5156be';
                } else {
                    questions[i].style.color = '#5156be';
                    questions[i].style.backgroundColor = 'rgba(81,86,190,.1)';
                    questions[i].style.borderColor = 'transparent';
                }
            }

            $('#typesOfAssets').hide();
            $('#whatAreDebts').hide();
            $('#typesOfDebts').hide();
            $('#whatAreAssets').show();
        }

        function typesOfAssets(element) {
            var questions = document.querySelectorAll('[id=guide-question]');
            for (var i = 0; i < questions.length; i++) {
                if (questions[i] == element) {
                    questions[i].style.color = '#fff';
                    questions[i].style.background = '#5156be';
                } else {
                    questions[i].style.color = '#5156be';
                    questions[i].style.backgroundColor = 'rgba(81,86,190,.1)';
                    questions[i].style.borderColor = 'transparent';
                }
            }
            $('#whatAreAssets').hide();
            $('#whatAreDebts').hide();
            $('#typesOfDebts').hide();
            $('#typesOfAssets').show();
        }

        function whatAreDebts(element) {
            var questions = document.querySelectorAll('[id=guide-question]');
            for (var i = 0; i < questions.length; i++) {
                if (questions[i] == element) {
                    questions[i].style.color = '#fff';
                    questions[i].style.background = '#5156be';
                } else {
                    questions[i].style.color = '#5156be';
                    questions[i].style.backgroundColor = 'rgba(81,86,190,.1)';
                    questions[i].style.borderColor = 'transparent';
                }
            }
            $('#whatAreAssets').hide();
            $('#typesOfAssets').hide();
            $('#typesOfDebts').hide();
            $('#whatAreDebts').show();
        }

        function typesOfDebts(element) {
            var questions = document.querySelectorAll('[id=guide-question]');
            for (var i = 0; i < questions.length; i++) {
                if (questions[i] == element) {
                    questions[i].style.color = '#fff';
                    questions[i].style.background = '#5156be';
                } else {
                    questions[i].style.color = '#5156be';
                    questions[i].style.backgroundColor = 'rgba(81,86,190,.1)';
                    questions[i].style.borderColor = 'transparent';
                }
            }
            $('#whatAreAssets').hide();
            $('#typesOfAssets').hide();
            $('#whatAreDebts').hide();
            $('#typesOfDebts').show();
        }

        function numberWithCommas(element, x) {
            var numWithSeparator = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            element.value = numWithSeparator;
        }

        function sumTotalIncome() {
            var arr = document.querySelectorAll('[id=income-amount]');
            var tot = 0.00;
            for (var i = 0; i < arr.length; i++) {
                if (parseFloat(arr[i].value))
                    tot += parseFloat(arr[i].value);
            }
            tot = parseFloat(tot).toFixed(2);
            document.getElementById('income-summary').innerHTML = tot.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            // Calculate Cash Flow
            calculateCashFlow();
        }

        function sumTotalExpense() {
            var arr = document.querySelectorAll('[id=expense-amount]');
            var tot = 0.00;
            for (var i = 0; i < arr.length; i++) {
                if (parseFloat(arr[i].value))
                    tot += parseFloat(arr[i].value);
            }
            tot = parseFloat(tot).toFixed(2);
            document.getElementById('expense-summary').innerHTML = tot.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            // Calculate Cash Flow
            calculateCashFlow();
        }

        function sumTotalSavingsInvestment() {
            var arr = document.querySelectorAll('[id=saving-invest-amount]');
            var tot = 0.00;
            for (var i = 0; i < arr.length; i++) {
                if (parseFloat(arr[i].value))
                    tot += parseFloat(arr[i].value);
            }
            tot = parseFloat(tot).toFixed(2);
            document.getElementById('saving-summary').innerHTML = tot.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            // Calculate Cash Flow
            calculateCashFlow();
        }

        function sumTotalAsset() {
            var arr = document.querySelectorAll('[id=asset-amount]');
            var tot = 0.00;
            for (var i = 0; i < arr.length; i++) {
                if (parseFloat(arr[i].value))
                    tot += parseFloat(arr[i].value);
            }
            tot = parseFloat(tot).toFixed(2);
            document.getElementById('asset-summary').innerHTML = tot.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            // Calculate Net Wealth
            calculateNetWealth();
        }

        function sumTotalDebt() {
            var arr = document.querySelectorAll('[id=debt-amount]');
            var tot = 0.00;
            for (var i = 0; i < arr.length; i++) {
                if (parseFloat(arr[i].value))
                    tot += parseFloat(arr[i].value);
            }
            tot = parseFloat(tot).toFixed(2);
            document.getElementById('debt-summary').innerHTML = tot.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            // Calculate Net Wealth
            calculateNetWealth();
        }

        function setTwoNumberDecimal(element) {
            element.value = parseFloat(element.value).toFixed(2);
        }

        function calculateCashFlow() {
            var arrIncome = document.querySelectorAll('[id=income-amount]');
            var income = 0.00;
            for (var i = 0; i < arrIncome.length; i++) {
                if (parseFloat(arrIncome[i].value))
                    income += parseFloat(arrIncome[i].value);
            }
            var arrExpense = document.querySelectorAll('[id=expense-amount]');
            var expense = 0.00;
            for (var i = 0; i < arrExpense.length; i++) {
                if (parseFloat(arrExpense[i].value))
                    expense += parseFloat(arrExpense[i].value);
            }
            var arrSaving = document.querySelectorAll('[id=saving-invest-amount]');
            var savingsInvestment = 0.00;
            for (var i = 0; i < arrSaving.length; i++) {
                if (parseFloat(arrSaving[i].value))
                    savingsInvestment += parseFloat(arrSaving[i].value);
            }
            var cashflow = 0.00;
            cashflow = parseFloat(income - (expense + savingsInvestment)).toFixed(2);
            document.getElementById('cash-flow-summary').innerHTML = cashflow.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            document.getElementById('cash-flow-summary').style.fontWeight = '600';
            if (cashflow < 0) {
                document.getElementById('cash-flow-summary').style.color = 'red';
            } else {
                document.getElementById('cash-flow-summary').style.color = 'rgba(0,0,0,.5)';
            }
        }

        function calculateNetWealth() {
            var arrAsset = document.querySelectorAll('[id=asset-amount]');
            var asset = 0.00;
            for (var i = 0; i < arrAsset.length; i++) {
                if (parseFloat(arrAsset[i].value))
                    asset += parseFloat(arrAsset[i].value);
            }
            var arrDebt = document.querySelectorAll('[id=debt-amount]');
            var debt = 0.00;
            for (var i = 0; i < arrDebt.length; i++) {
                if (parseFloat(arrDebt[i].value))
                    debt += parseFloat(arrDebt[i].value);
            }
            var netWealth = 0.00;
            netWealth = parseFloat(asset - debt).toFixed(2);
            document.getElementById('net-wealth-summary').innerHTML = netWealth.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            document.getElementById('net-wealth-summary').style.fontWeight = '600';
            if (netWealth < 0) {
                document.getElementById('net-wealth-summary').style.color = 'red';
            } else {
                document.getElementById('net-wealth-summary').style.color = 'rgba(0,0,0,.5)';
            }
        }

        function userFormSubmit() {
            $("#financial-statement").submit();
            /*  $("#user-balance-sheet").submit();*/
        }

        /*  function userBalanceSheetFormSubmit() {
              $("#user-balance-sheet").submit();
          }*/
        $('#submission-date').on('change', function () {
            liveSearch($(this).val());
        });

        function liveSearch(date) {
            let url = "/wealth-profile/" + date;
            window.location = url;
        }
    </script>
    @if(session('openModal')==true)
        <script>
            $(function () {
                $('.myModal').modal('show');
            });
        </script>
    @endif
    <style>

        #income-amount, #expense-amount, #saving-invest-amount {
            text-align: right;
        }

        #income-description, #expense-description, #asset-description, #debt-description {
            width: 65%;
        }

        #guide-button {
            font-style: italic;
            border-color: #e9e9ef !important;
        }

        .flatpickr-wrapper {
            display: inline;
        }

        /*  #guide-question {
              color: #5156be;
              border-color: #e9e9ef !important;
          }

          #guide-question.active {
              background-color: #e9e9ef;
              color: #5156be;
              border-color: #e9e9ef !important;
          }*/

        .offcanvas-body {
            padding-top: 0px !important;
        }
    </style>
@endsection
