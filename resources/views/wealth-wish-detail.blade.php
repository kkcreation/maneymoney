@extends('layouts.master')

@section('title') @lang('translation.Wealth_Wish') @endsection
@section('css')

    <!-- dropzone css -->
    <link href="{{ URL::asset('/assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
    {{--   <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <!-- datepicker css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.css') }}">
    {{-- <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    {{-- <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
           type="text/css"/>--}}
    <style>
        .carousel-inner img {
            max-height: 350px;
        }

        .timeline .timeline-date {
            top: -30px !important;
        }

        @media (max-width: 767.98px) {
            .timeline .timeline-date {
                left: initial;
                right: 15px;
                top: -30px;
            }
        }
    </style>
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Wealth_Wish') @endslot
        @slot('title') @lang('translation.Action_Plan') @endslot
    @endcomponent
    @if (session('message'))
        <div class="alert {{ session('alert-class') }} alert-border-left alert-dismissible fade show" role="alert">
            <i class="mdi mdi-check-all me-3 align-middle"></i>{{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row" style="padding-bottom: 20px;">
        <div class="col-md-6 col-xl-6">
            <div class="card h-100">
                @php
                    $wealthWishService = new \App\Services\WealthWishService(Auth::user());
                        $durationInMonths = $wealthWishService->calculateExpectedDuration($wealthWish->expected_achievement_date,$wealthWish->effective_date);
                        $timeBound = $wealthWishService->defineTimeBound($durationInMonths);
                @endphp
                <div class="card-body">
                    <div class="timeline timeline-box">
                        <div
                            class="timeline-date bg-primary text-center rounded">
                            <h3 class="text-white mb-0">{{ Carbon\Carbon::parse($wealthWish->expected_achievement_date)->age + Carbon\Carbon::parse(Auth::user()->dob)->age }}</h3>
                            <p class="mb-0 text-white-50">@lang('translation.Year')</p>
                        </div>
                        <blockquote class="blockquote">
                            <h4 class="card-title">{{ $wealthWish->title }}</h4>
                            <h4>
                                <i class="bx bxs-purchase-tag"></i> {{ number_format($wealthWish->expected_amount,2,'.',',') }}
                            </h4>
                            <div class="gap-2 font-size-18">
                        <span class="badge badge-soft-secondary">
                       {{ config('wealthwish.'.app()->getLocale().'.category.'. $wealthWish->category) }}
                   </span>
                                <span
                                    class="badge badge-soft-{{ config('wealthwish.attributes.time_bound.color.'.$timeBound) }}">
                       {{ config('wealthwish.'.app()->getLocale().'.time_bound.'.$timeBound) }}
                   </span>
                                <span
                                    class="badge badge-soft-{{ config('wealthwish.attributes.priority.color.'.$wealthWish->priority) }}">
                                                            {{ config('wealthwish.'.app()->getLocale().'.priority.'.$wealthWish->priority)}}
                                                        </span>
                            </div>
                            <div class="mt-3 gap-2 font-size-14">
                            <p class="card-text text-secondary" style="text-align: center;">
                                {{ \Carbon\Carbon::parse($wealthWish->effective_date)->format('j-M-Y') }} <i class="mdi mdi-arrow-right-bold text-success"></i> {{ \Carbon\Carbon::parse($wealthWish->expected_achievement_date)->format('j-M-Y') }}
                            </p>
                            </div>
                        </blockquote>
                        <p class="card-text text-secondary" style="text-align: center;">
                            <i>"{{ $wealthWish->description }}"</i></p>
                    </div>
                    {{--    <h6 class="card-subtitle text-muted">{{ config('wealthwish.'.app()->getLocale().'.category.'. $wealthWish->category) }}</h6>--}}
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @php $count = 1; @endphp
                            @foreach($wealthWish->wealthWishImages as $image)
                                @if($count == 1)
                                    <div class="carousel-item active">
                                        <img class="d-block img-fluid mx-auto rounded"
                                             src="{{ URL::asset('/images/wealth-wish/'.$image->path) }}"
                                             alt="{{ $count }} slide">
                                    </div>
                                @else
                                    <div class="carousel-item">
                                        <img class="d-block img-fluid mx-auto rounded"
                                             src="{{ URL::asset('/images/wealth-wish/'.$image->path) }}"
                                             alt="{{ $count }} slide">
                                    </div>
                                @endif
                                @php $count++; @endphp
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                           data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                           data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!-- end carousel -->
                </div><!-- end card-body -->
            </div>

        </div>
        <div class="col-md-6 col-xl-6">
            <!-- FV = PV x ( 1 + i ) ^ n -->
            <div class="card h-100">
                <div class="card-body"><h4>@lang('translation.Action_Plan')</h4>
                    <blockquote class="blockquote">
                        {{-- <h4 class="card-title">@lang('translation.Formula') : PMT = PV - (FV
                             / (1 + i)^n) / ((1 - (1 / (1 + i)^n)) / i)</h4>--}}
                        <h4 class="card-title">* @lang('translation.Normal_Savings')
                            : {{ number_format(divnum($wealthWish->expected_amount, $durationInMonths),2,'.',',') }}
                            / @lang('translation._Monthly')</h4>
                    </blockquote>
                    <form class="form" method="post"
                          action="{{ route('updateActionPlan',['wealthwishid'=> $wealthWish->id ]) }}"
                          id="action-plan">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token ?? '' }}">
                        <div class="form-group row">
                            <label for="staticfv" class="col-md-5 col-form-label">@lang('translation.Wish_Value')
                                :</label>
                            <div class="col-md-7">
                                <input type="text" readonly class="form-control-plaintext" id="staticfv"
                                       value="{{ number_format($wealthWish->expected_amount,2,'.',',') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticperiod" class="col-md-5 col-form-label">@lang('translation.Period') :</label>
                            <div class="col-md-7">
                                <input type="text" readonly class="form-control-plaintext" id="staticperiod"
                                       value="{{ $durationInMonths }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tool" class="col-md-5 col-form-label">@lang('translation.Tool') :</label>
                            <div class="col-md-7">
                            <select name="tool"  class="form-select">
                                @php $liquidAssets = \App\Models\Asset::whereIn('type',['liquid_asset'])->get(); @endphp
                                <option class="dropdown-header text-secondary" disabled>---------- @lang('translation.Liquid_Asset') ----------</option>
                                @foreach($liquidAssets as $value)
                                    <option value="{{ $value->id }}" {{ $value->id == $wealthWish->asset_id ? 'selected' : ''  }}>
                                        {{ App::getLocale() == 'en' ? $value->description : $value->description_th }}
                                    </option>
                                @endforeach
                                @php $investmentAssets = \App\Models\Asset::whereIn('type',['investment_asset'])->get(); @endphp
                                <option class="dropdown-header text-secondary" disabled>---------- @lang('translation.Investment_Asset') ----------</option>
                                @foreach($investmentAssets as $value)
                                    <option value="{{ $value->id }}" {{ $value->id == $wealthWish->asset_id ? 'selected' : ''  }}>
                                        {{ App::getLocale() == 'en' ? $value->description : $value->description_th }}
                                    </option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="annualrate" class="col-md-5 col-form-label">@lang('translation.Annual_Rate')
                                :</label>
                            <div class="col-md-7">
                                <input type="number" class="form-control" name="annualrate" id="annualrate"
                                       placeholder="%" value="{{ $wealthWish->expected_annual_rate }}" min="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="presentvalue"
                                   class="col-md-5 col-form-label">@lang('translation.Current_Savings') :</label>
                            <div class="col-md-7">
                                <input type="number" class="form-control" name="presentvalue" id="presentvalue"
                                       placeholder="THB" value="{{ $wealthWish->present_value }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payment" class="col-md-5 col-form-label">@lang('translation.DCA') :</label>
                            <div class="col-md-7">
                                <input type="number" readonly class="form-control text-primary" name="payment"
                                       id="payment" placeholder="THB" value="{{ $wealthWish->calculated_payment }}">
                            </div>
                        </div>
                    </form>
                    <div class="form-group row">
                        <p></p>
                        <div class="col-md-2"></div>
                        <div class="col-md-8" style="text-align: center;">
                            <button type="button" class="btn btn-md btn-outline-primary" onclick="calculatePMT()">
                                @lang('translation.Calculate')
                            </button>
                            <button type="button" class="btn btn-md btn-primary" onclick="formSubmit()"><i
                                    class="bx bxs-heart"></i> @lang('translation.Save_Plan')
                            </button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->

@endsection

@section('script')
    <script>
        function calculatePMT() {
            var payment = 0;
            var annualRate = $('#annualrate').val();
            if(annualRate == 0){
                var period = $('#staticperiod').val();
                var futureValue = $('#staticfv').val();
                var presentValue = $('#presentvalue').val();
                payment = (parseFloat(futureValue.replace(/,/g, '')) - presentValue) / period;
                $('#payment').val(payment.toFixed(2));
            } else {
                var period = $('#staticperiod').val();
                var futureValue = $('#staticfv').val();
                var presentValue = $('#presentvalue').val();
                payment = pmtWithAnnualRate(annualRate, period, presentValue, parseFloat(futureValue.replace(/,/g, '')), 0)
                $('#payment').val(-(payment.toFixed(2)));
            }
        }

        function pmtWithAnnualRate(annualRate, period, presentValue, futureValue, advancedPayments) {
            monthlyRate = annualRate / 1200
            t1 = 1 + monthlyRate
            t2 = Math.pow(t1, period)
            t3 = Math.pow(t1, (period - advancedPayments))
            return (presentValue - (futureValue / t2)) / (((1 - (1 / (t3))) / monthlyRate) + advancedPayments);
        }

        function formSubmit() {
            /** Re-calculate Payment */
            calculatePMT();
            $("#action-plan").submit();
        }
    </script>

@endsection
