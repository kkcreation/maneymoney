@extends('layouts.master')

@section('title') @lang('translation.Wealth_Wish') @endsection
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<meta name="lang" content="{{ App::getLocale() }}"/>
@section('css')

    <!-- dropzone css -->
    <link href="{{ URL::asset('/assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
    {{--   <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <!-- datepicker css -->
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.css') }}">
    {{-- <link href="{{ URL::asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    {{-- <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
           type="text/css"/>--}}
    <style>
        .dropzoneDragArea {
            background-color: #fbfdff;
            border: 1px dashed #c0ccda;
            border-radius: 6px;
            padding: 60px;
            text-align: center;
            margin-bottom: 15px;
            cursor: pointer;
        }

        .dropzone {
            box-shadow: 0px 2px 20px 0px #f2f2f2;
            border-radius: 10px;
        }

        .loading {
            z-index: 20;
            position: absolute;
            top: 0;
            left: -5px;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.2);
        }

        .loading-content {
            position: absolute;
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #5156be; /* Blue */
            border-radius: 50%;
            width: 50px;
            height: 50px;
            top: 45%;
            left: 50%;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.Wealth_Wish') @endslot
        @slot('title') @lang('translation.Wish_Edit') @endslot
    @endcomponent
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="bx bx-edit me-1"></i> @lang('translation.Wish_Edit')</h4>
                    {{-- <p class="card-title-desc">DropzoneJS is an open source library
                         that provides drag’n’drop file uploads with image previews.
                     </p>--}}
                </div>
                <div class="card-body">
                    <section id="loading">
                        <div id="loading-content"></div>
                    </section>
                    <div class="modal fade" id="alert-modal" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header py-3 px-4 border-bottom-0">
                                    <h5 class="modal-title" id="modal-title"><i
                                            class="bx bx-message-alt-error"></i> @lang('translation.Error')</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-hidden="true"></button>
                                </div>
                                <div class="modal-body p-4" id="modal-body">
                                </div>
                            </div>
                        </div>
                    </div>
                {{-- <div class="form-wrapper py-5">--}}
                <!-- form starts -->
                    <form action="{{ route('updateform.data') }}" name="wishupdateform" id="wishupdateform"
                          method="POST"
                          class="dropzone" enctype="multipart/form-data">

                        @csrf
                        <div class="form-group">

                            <input type="hidden" class="wealthwishid" name="wealthwishid" id="wealthwishid"
                                   value="{{ $wealthWish->id }}">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('wealthwish.attributes.category.'.app()->getLocale()) }}</label>
                                        <select name="category" class="form-select">
                                            @foreach(config('wealthwish.en.category') as $key => $value)
                                                <option
                                                    value="{{ $key }}" {{ $key == $wealthWish->category ? 'selected' : '' }}>{{ config('wealthwish.'.app()->getLocale().'.category.'.$key) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label
                                            for="wealthwish-title">{{ config('wealthwish.attributes.title.'.app()->getLocale()) }}</label>
                                        <input name="title" type="text" class="form-control"
                                               id="title"
                                               value="{{ $wealthWish->title }}"
                                               placeholder="eg. Buy a beautiful dream house, Happy Retirement, etc."
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('wealthwish.attributes.priority.'.app()->getLocale()) }}</label>
                                        <select name="priority" class="form-select">
                                            @foreach(config('wealthwish.en.priority') as $key => $value)
                                                <option
                                                    value="{{ $key }}" {{ $key == $wealthWish->priority ? 'selected' : '' }}> {{ config('wealthwish.'.app()->getLocale().'.priority.'.$key)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('wealthwish.attributes.effective_date.'.app()->getLocale()) }}</label>
                                        <input type="text" class="form-control" name="effective_date"
                                               id="effective_date"
                                               placeholder="eg. {{ \Carbon\Carbon::now()->format('Y-m-d') }}"
                                               value="{{ $wealthWish->effective_date }}" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('wealthwish.attributes.expected_achievement_date.'.app()->getLocale()) }}</label>
                                        <input type="text" class="form-control" name="expected_achievement_date"
                                               id="expected_achievement_date"
                                               placeholder="eg. {{ \Carbon\Carbon::now()->addYear(1)->format('Y-m-d') }}"
                                               value="{{ $wealthWish->expected_achievement_date }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('wealthwish.attributes.expected_amount.'.app()->getLocale()) }}</label>
                                        <input type="number" name="expected_amount" id="expected_amount"
                                               class="form-control" placeholder="eg. 100,000"
                                               value="{{ $wealthWish->expected_amount }}" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label
                                            class="form-label">{{ config('wealthwish.attributes.amount_term.'.app()->getLocale()) }}</label>
                                        <select name="amount_term" class="form-select">
                                            @foreach(config('wealthwish.en.amount_term') as $key => $value)
                                                <option
                                                    value="{{ $key }}" {{ $key == $wealthWish->amount_term ? 'selected' : '' }}>{{ config('wealthwish.'.app()->getLocale().'.amount_term.'.$key) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label
                                            for="wealthwish-description">{{ config('wealthwish.attributes.description.'.app()->getLocale()) }}</label>
                                        <textarea name="description" class="form-control"
                                                  id="description" required>{{ $wealthWish->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="dropzoneDragArea" class="dropzone">
                                <div class="dz-message needsclick">
                                    <div class="mb-3">
                                        <i class="display-4 text-muted bx bx-cloud-upload"></i>
                                    </div>
                                    <h5>@lang('translation.Drop_File_Here')</h5>
                                </div>
                            </div>
                            {{-- <div class="dropzone-previews"></div>--}}
                        </div>
                        <div class="form-group">
                            <br>
                            <button type="submit"
                                    class="btn btn-md btn-primary">@lang('translation.Save_Changes')</button>
                        </div>
                    </form>
                    <!-- form end -->
                </div>
                {{-- <div>
                     <form action="{{ route('dropzone.store') }}" method="post" enctype="multipart/form-data" id="image-upload" class="dropzone">
                         @csrf
                         <div class="fallback">
                             <input name="file" type="file" multiple="multiple">
                         </div>
                         <div class="dz-message needsclick">
                             <div class="mb-3">
                                 <i class="display-4 text-muted bx bx-cloud-upload"></i>
                             </div>

                             <h5>Drop files here or click to upload.</h5>
                         </div>
                     </form>
                 </div>--}}

                {{-- <div class="text-center mt-4">
                     <button type="submit" class="btn btn-primary waves-effect waves-light">Send
                         Files</button>
                 </div>--}}
                {{--   </div>--}}
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')

    <!-- dropzone js -->
    <script src="{{ URL::asset('/assets/libs/dropzone/min/dropzone.min.js') }}"></script>
    <!-- datepicker js -->
    <script src="{{ URL::asset('/assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/wealth-wish-create.init.js') }}"></script>

    {{--   <script src="{{ URL::asset('/assets/libs/bootstrap/bootstrap.min.js') }}"></script>--}}
    {{--    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>--}}
    {{--<script>
        Dropzone.options.imageUpload = {
            maxFilesize         :       3,
            acceptedFiles: ".jpeg,.jpg,.png,.gif"
        };
    </script>--}}
    <script>
        Dropzone.autoDiscover = false;
        // Dropzone.options.wishcreateform = false;
        let token = $('meta[name="csrf-token"]').attr('content');
        let lang = $('meta[name="lang"]').attr('content');
        $(function () {
            var myDropzone = new Dropzone("div#dropzoneDragArea", {
                paramName: "file",
                url: "{{ url('/storeimage') }}",
                //  previewsContainer: 'div.dropzone-previews',
                addRemoveLinks: true,
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 5,
                maxFiles: 5,
                maxFilesize: 3,
                params: {
                    _token: token
                },
                // The setting up of the dropzone
                init: function () {
                    var myDropzone = this;
                    var doSome;
                    $.ajax({
                        url: '{{ url('/wishimages/'.$wealthWish->id) }}',
                        type: 'get',
                        dataType: 'json',
                        success: function (response) {
                            $.each(response, function (key, value) {
                                var mockFile = {name: value.name, size: value.size};
                                myDropzone.emit("addedfile", mockFile);
                                myDropzone.emit("thumbnail", mockFile, value.path)
                                {
                                    $('[data-dz-thumbnail]').css('height', '120');
                                    $('[data-dz-thumbnail]').css('width', '120');
                                    $('[data-dz-thumbnail]').css('object-fit', 'cover');
                                }
                                ;
                                myDropzone.emit("complete", mockFile);
                            });
                        }
                    });

                    this.on('removedfile', function (file) {
                        $.ajax({
                            type: 'post',
                            url: '{{ url('/deleteimage') }}',
                            data: {name: file.name, _token: token},
                            dataType: 'html',
                            success: function (data) {
                                //var rep = JSON.parse(data)
                                $('.dz-message').css('display', 'none');
                            }
                        })
                    })

                    //form submission code goes here
                    $("form[name='wishupdateform']").submit(function (event) {
                        //Make sure that the form isn't actually being sent.
                        event.preventDefault();
                        URL = $("#wishupdateform").attr('action');
                        formData = $('#wishupdateform').serialize();
                        $.ajax({
                            type: 'POST',
                            url: URL,
                            data: formData,
                            success: function (result) {
                                if (result.status == "success") {
                                    // fetch the useid
                                    var wealthwishid = result.wealth_wish_id;
                                    $("#wealthwishid").val(wealthwishid); // inserting wealthwishid into hidden input field
                                    //process the queue
                                    //myDropzone.processQueue();
                                    if (myDropzone.files != "") {
                                        myDropzone.processQueue();
                                    } else {
                                        let url = "/wealth-wish-list";
                                        window.location = url;
                                    }
                                } else {
                                    console.log("error");
                                }
                            }
                        });
                    });
                    //Gets triggered when we submit the image.
                    this.on('sending', function (file, xhr, formData) {
                    });

                    this.on("success", function (file, response) {

                    });

                    this.on("queuecomplete", function () {

                    });

                    // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
                    // of the sending event because uploadMultiple is set to true.
                    this.on("sendingmultiple", function (file, xhr, formData) {
                        //fetch the wealth wish id from hidden input field and send that wealthwishid with our image
                        let wealthwishid = document.getElementById('wealthwishid').value;
                        formData.append('wealthwishid', wealthwishid);
                        // Gets triggered when the form is actually being sent.
                        // Hide the success button or the complete form.
                    });
                    this.on("successmultiple", function (files, response) {
                        // Gets triggered when the files have successfully been sent.
                        // Redirect user or notify of success.
                        /**
                         * redirect to wealth wish list page
                         * */
                        let url = "/wealth-wish-list";
                        window.location = url;
                        // doSome = new Promise(function (resolve, reject) {
                        //      if (lang == 'en') {
                        //          resolve('Successfully Uploaded All Images!');
                        //          reject('Failed Uploaded Images!');
                        //      } else {
                        //          resolve('อัพโหลดรูปภาพเสร็จสมบูรณ์!');
                        //          reject('อัพโหลดรูปภาพล้มเหลว!');
                        //      }
                        //  });

                    });
                    this.on("error", function (file, response) {
                        if (!file.accepted) {
                            $('#modal-body').text(response);
                            $('#alert-modal').modal('show');
                            this.removeFile(file);
                        }
                    });
                    this.on("errormultiple", function (files, response) {
                        // Gets triggered when there was an error sending the files.
                        // Maybe show form again, and notify user of error
                    });
                }
            });
        });

        function showLoading() {
            document.querySelector('#loading').classList.add('loading');
            document.querySelector('#loading-content').classList.add('loading-content');
        }

        function hideLoading() {
            document.querySelector('#loading').classList.remove('loading');
            document.querySelector('#loading-content').classList.remove('loading-content');
        }
    </script>

    {{--   <script type="text/javascript">
           $(function () {
               $('#datepicker').datepicker({
                   format: 'dd-mm-yyyy'
               });
           });
       </script>--}}
@endsection
