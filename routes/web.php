<?php

use App\Http\Controllers\DropzoneController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'root'])->name('root')->middleware('verified');
Route::get('wealth-health-check/{date}', [App\Http\Controllers\HomeController::class, 'wealthHealthCheck'])->name('wealthHealthCheck')->middleware('can:wealth-health-check');
Route::get('wealth-profile/{date}', [App\Http\Controllers\HomeController::class, 'wealthProfile'])->name('wealthProfile');

/** Financial Event */
Route::get('investment-memo', [App\Http\Controllers\FinancialEventController::class, 'index'])->name('financialCalendar')->middleware('can:wealth-accumulation');
Route::get('events', [App\Http\Controllers\FinancialEventController::class, 'events'])->middleware('can:wealth-accumulation');
Route::post('/manage-events', [App\Http\Controllers\FinancialEventController::class, 'manageEvents'])->middleware('can:wealth-accumulation');
Route::post('/fund-info', [App\Http\Controllers\FinancialEventController::class, 'getFundInfo'])->middleware('can:wealth-accumulation');
Route::get('autocomplete', [App\Http\Controllers\FinancialEventController::class, 'getAllFunds'])->name('autocomplete')->middleware('can:wealth-accumulation');
Route::get('portfolio', [App\Http\Controllers\FinancialEventController::class, 'getGoalBasedInvestmentList'])->name('getGoalBasedInvestmentList')->middleware('can:wealth-accumulation');
Route::get('portfolio-info', [App\Http\Controllers\FinancialEventController::class, 'getPortfolioInfo'])->name('getPortfolioInfo')->middleware('can:wealth-accumulation');

/** Admin Management */
Route::get('auth-member-list', [App\Http\Controllers\AdminController::class, 'authMemberList'])->name('authMemberList')->middleware('can:manage-member');
Route::get('member-info/{id}', [App\Http\Controllers\AdminController::class, 'memberUpdate'])->name('memberUpdate')->middleware('can:manage-member');
Route::post('/update-member-info', [App\Http\Controllers\AdminController::class, 'updateMemberInfo'])->name('updateMemberInfo')->middleware('can:manage-member');

/** Wealth Wish Feature */
Route::get('wealth-wish-list', [App\Http\Controllers\WealthWishController::class, 'wealthWishList'])->name('wealthWishList');
Route::get('dropzone', [DropzoneController::class, 'dropzone']);
Route::post('dropzone/store', [DropzoneController::class, 'dropzoneStore'])->name('dropzone.store');
Route::get('wealth-wish-create', [App\Http\Controllers\WealthWishController::class, 'wealthWishCreate'])->name('wealthWishCreate');
Route::get('wealth-wish-update/{id}', [App\Http\Controllers\WealthWishController::class, 'wealthWishUpdate'])->name('wealthWishUpdate');
Route::get('wealth-wish-detail/{id}', [App\Http\Controllers\WealthWishController::class, 'wealthWishDetail'])->name('wealthWishDetail');
Route::post('/updatedata', [App\Http\Controllers\WealthWishController::class, 'updateData'])->name('updateform.data');
Route::post('/storedata', [App\Http\Controllers\WealthWishController::class, 'storeData'])->name('form.data');
Route::post('/storeimage', [App\Http\Controllers\WealthWishController::class, 'storeImage']);
Route::post('/updateactionplan', [App\Http\Controllers\WealthWishController::class, 'updateActionPlan'])->name('updateActionPlan');
Route::get('wishimages/{id}', [App\Http\Controllers\WealthWishController::class, 'wealthWishImages'])->name('wealthWishImages');
Route::post('/deleteimage', [App\Http\Controllers\WealthWishController::class, 'deleteImage']);
Route::post('/deletedata', [App\Http\Controllers\WealthWishController::class, 'deleteData']);

/** Debt Killer Feature */
Route::get('debt-killer-list', [App\Http\Controllers\UserDebtController::class, 'debtKillerList'])->name('debtKillerList')->middleware('can:debt-killer');
Route::get('debt-killer-create', [App\Http\Controllers\UserDebtController::class, 'debtKillerCreate'])->name('debtKillerCreate')->middleware('can:debt-killer');
Route::get('debt-killer-update/{id}', [App\Http\Controllers\UserDebtController::class, 'debtKillerUpdate'])->name('debtKillerUpdate')->middleware('can:debt-killer');
Route::get('debt-killer-detail/{id}', [App\Http\Controllers\UserDebtController::class, 'debtKillerDetail'])->name('debtKillerDetail')->middleware('can:debt-killer');
Route::post('/updatedebtdata', [App\Http\Controllers\UserDebtController::class, 'updateData'])->name('updateform.debtdata')->middleware('can:debt-killer');
Route::post('/storedebtdata', [App\Http\Controllers\UserDebtController::class, 'storeData'])->name('form.debtdata')->middleware('can:debt-killer');
Route::post('/updatedebtactionplan', [App\Http\Controllers\UserDebtController::class, 'updateDebtActionPlan'])->name('updateDebtActionPlan')->middleware('can:debt-killer');
Route::post('/deletedebtdata', [App\Http\Controllers\UserDebtController::class, 'deleteDebtData'])->middleware('can:debt-killer');

Route::get('{any}', [App\Http\Controllers\HomeController::class, 'index'])->name('index')->middleware('verified');

//Update User Details
Route::post('/update-profile/{id}', [App\Http\Controllers\HomeController::class, 'updateProfile'])->name('updateProfile');
Route::post('/update-password/{id}', [App\Http\Controllers\HomeController::class, 'updatePassword'])->name('updatePassword');
//Update Wealth On-boarding Info
Route::post('/update-financial-statement/{id}', [App\Http\Controllers\WealthController::class, 'updateFinancialStatement'])->name('updateFinancialStatement');
/*Route::post('/update-balance-sheet/{id}', [App\Http\Controllers\WealthController::class, 'updateBalanceSheet'])->name('updateBalanceSheet');*/

/** Language Translation */
Route::get('index/{locale}', [App\Http\Controllers\HomeController::class, 'lang'])->middleware('verified');

/** MustVerifiedEmail Routes */
Route::get('/email/verify', function () {
    return view('auth-email-verification');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('index');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');
